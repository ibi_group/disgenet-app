package es.imim.DisGeNET.exceptions;

public class DisGeNetException extends Exception {

	public DisGeNetException() {
		// TODO Auto-generated constructor stub
	}

	public DisGeNetException(String message) {
		super(message);

	}

	public DisGeNetException(Throwable cause) {
		super(cause);
 
	}

	public DisGeNetException(String message, Throwable cause) {
		super(message, cause);
	}

	public DisGeNetException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
