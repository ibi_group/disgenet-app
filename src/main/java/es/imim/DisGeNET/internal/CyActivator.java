/*package es.imim.DisGeNET.internal;

import java.util.Properties;

import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.service.util.AbstractCyActivator;
import org.osgi.framework.BundleContext;

public class CyActivator extends AbstractCyActivator {

	@Override
	public void start(BundleContext context) throws Exception {

		CyApplicationManager cyApplicationManager = getService(context, CyApplicationManager.class);

		MenuAction action = new MenuAction(cyApplicationManager, "Hello World App");

		Properties properties = new Properties();

		registerAllServices(context, action, properties);
	}

}*/

package es.imim.DisGeNET.internal;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import es.imim.DisGeNET.internal.enrichment.EnrichmentService;
import es.imim.DisGeNET.internal.enrichment.ShowEnrichmentPanelTaskFactory;
import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.application.swing.CyAction;
import org.cytoscape.application.swing.CySwingApplication;
import org.cytoscape.application.swing.CytoPanelComponent;
import org.cytoscape.model.*;
import org.cytoscape.service.util.AbstractCyActivator;
import org.cytoscape.service.util.CyServiceRegistrar;
import org.cytoscape.session.events.SessionLoadedListener;
import org.cytoscape.task.NodeViewTaskFactory;
import org.cytoscape.task.read.LoadVizmapFileTaskFactory;
import org.cytoscape.util.swing.OpenBrowser;
import org.cytoscape.view.layout.CyLayoutAlgorithmManager;
import org.cytoscape.view.model.CyNetworkViewFactory;
import org.cytoscape.view.model.CyNetworkViewManager; // JP
import org.cytoscape.view.presentation.customgraphics.CyCustomGraphics2Factory;
import org.cytoscape.view.presentation.property.values.CyColumnIdentifierFactory;
import org.cytoscape.view.vizmap.VisualMappingManager;
import org.cytoscape.view.vizmap.VisualStyleFactory;
//JP
//import org.cytoscape.work.TaskFactory;
import org.cytoscape.work.SynchronousTaskManager;
import org.cytoscape.work.TaskIterator;
import org.cytoscape.work.TaskManager;
import org.cytoscape.work.swing.DialogTaskManager;
import org.osgi.framework.BundleContext;

import es.imim.DisGeNET.database.Database;
import es.imim.DisGeNET.internal.annotation.AnnotationService;
import es.imim.DisGeNET.internal.linkout.LinkoutService;
import es.imim.DisGeNET.internal.automation.NetworkAutomationImpl;
import es.imim.DisGeNET.styles.DisGenetStyleManager;

import static org.cytoscape.work.ServiceProperties.*;

//import org.cytoscape.application.swing.CySwingApplication;

public class CyActivator extends AbstractCyActivator {

	private Database disGeNETdb;
	private DisGeNETCytoPanel disgenetCytoPanel;
	private CyApplicationManager cyApplicationManager;
	private CySwingApplication cytoscapeDesktopService;
	private BundleContext context;
	private CyNetworkFactory networkFactory;
	private CyTableFactory cyTableFactory;
	private CyTableManager cyTableManager;
	private CyNetworkManager netMgr;
	private CyNetworkViewManager netViewMgr; // JP
	private CyNetworkTableManager netTableMgr;
	private VisualMappingManager visualMappingMgr ; // JP
	private CyNetworkViewFactory networkViewFactory;
	private CyTableFactory tableFactory;
	private TaskIterator globalTaskIterator;
	private DialogTaskManager dialogTaskManager;
	private TaskManager taskManager;
	private SynchronousTaskManager syncTaskManager;
	private CyTable disgenetNodeAttributesTable;
	private CyColumnIdentifierFactory columnIdentifierFactory;
	private CyServiceRegistrar serviceRegistrar;
	private LoadVizmapFileTaskFactory loadVizmapFileTaskFactor;
	private VisualStyleFactory visualStyleFactor;
	private CustomChartListener customGraphics;
	private Map<Long, CyTable> enrichmentTables;
	public static CyLayoutAlgorithmManager layoutAlgoManager;// JP
    private ShowEnrichmentPanelTaskFactory showEnrichmentFactory;

	private static CyActivator instance = null;
	@Override
	public void start(BundleContext context) throws Exception {

        instance = this;
		this.context = context;

		this.cyApplicationManager = getService(context, CyApplicationManager.class);
		this.cytoscapeDesktopService = getService(context, CySwingApplication.class);
		this.taskManager = getService(context, TaskManager.class);
		this.dialogTaskManager = getService(context, DialogTaskManager.class);
		this.syncTaskManager = getService(context, SynchronousTaskManager.class);
		this.serviceRegistrar = getService(context, CyServiceRegistrar.class);
		this.loadVizmapFileTaskFactor = getService(context, LoadVizmapFileTaskFactory.class);
		this.visualMappingMgr = getService(context, VisualMappingManager.class);
        this.visualStyleFactor = getService(context, VisualStyleFactory.class);
        this.cyTableManager = getService(context, CyTableManager.class);
        this.cyTableFactory = getService(context, CyTableFactory.class);
        this.columnIdentifierFactory = getService(context, CyColumnIdentifierFactory.class);
        this.globalTaskIterator = new TaskIterator();

      //To create networks, nodes, etc
        networkFactory = getService(context, CyNetworkFactory.class);
        tableFactory = getService(context, CyTableFactory.class);
        //To destroy networks
        netMgr = getService(context, CyNetworkManager.class);
        //To create network views
        networkViewFactory = getService(context, CyNetworkViewFactory.class);
        netViewMgr = getService(context, CyNetworkViewManager.class); // JP
        netTableMgr = getService(context, CyNetworkTableManager.class);
        layoutAlgoManager = getService(context, CyLayoutAlgorithmManager.class); // JP
        
		WebMenuAction webMenuaction = new WebMenuAction(cyApplicationManager, "Go to DisGeNET homepage", getService(context, OpenBrowser.class));
		AboutMenuAction aboutMenuaction = new AboutMenuAction(cyApplicationManager, "About", cytoscapeDesktopService);
		ChangeDBMenuAction dbMenuAction = new ChangeDBMenuAction(cyApplicationManager, "Change DB Folder");
		StartDisGeNETMenuAction startDisGeNETMenuaction = new StartDisGeNETMenuAction(cyApplicationManager, serviceRegistrar, "Start DisGeNET");
		//ExternalNetworksMenu externalNetworkMenuAction = new ExternalNetworksMenu(cyApplicationManager, "External Networks");
		//AnnotateGenesMenuAction annotGenesMenuAction = new AnnotateGenesMenuAction(cyApplicationManager, "Annotate genes with diseases from DisGeNET");
		
		
		registerService(context, startDisGeNETMenuaction, CyAction.class, new Properties());
		registerService(context, webMenuaction, CyAction.class, new Properties());
		registerService(context, dbMenuAction, CyAction.class, new Properties());
		registerService(context, aboutMenuaction, CyAction.class, new Properties());
//		registerService(context, externalNetworkMenuAction, CyAction.class, new Properties());z
//		registerService(context, annotGenesMenuAction, CyAction.class, new Properties());
		
		startDisGeNETMenuaction.set_activator(this);
		
		final NodeViewTaskFactory expandNodeContextMenuFactory = new ExpandCurrentNetActionTaskFactory();
		final Properties nodeProp = new Properties();
		nodeProp.setProperty(PREFERRED_MENU, NODE_APPS_MENU+".DisGeNET.Expand");
		nodeProp.setProperty(MENU_GRAVITY, "1.0");
		nodeProp.setProperty(TITLE, "Expand current net");
		registerService(context, expandNodeContextMenuFactory, NodeViewTaskFactory.class, nodeProp);
		
		final NodeViewTaskFactory expandIntoCurrentNet = new ExpandNewNetActionTaskFactory();
		final Properties nodeCurrentProp = new Properties();
		nodeCurrentProp.setProperty(PREFERRED_MENU, NODE_APPS_MENU+".DisGeNET.Expand");
		nodeCurrentProp.setProperty(MENU_GRAVITY, "2.0");
		nodeCurrentProp.setProperty(TITLE, "Create new net");
		registerService(context, expandIntoCurrentNet, NodeViewTaskFactory.class, nodeCurrentProp);

		this.showEnrichmentFactory = new ShowEnrichmentPanelTaskFactory();
        showEnrichmentFactory.reregister();

		
		new AnnotationService(serviceRegistrar);
		
		new LinkoutService(serviceRegistrar);

		new EnrichmentService(serviceRegistrar);

		enrichmentTables = new HashMap<>();
		
		LoadVizmapFileTaskFactory loadVizmapFileTaskFactory =  getService(this.context, LoadVizmapFileTaskFactory.class);
        DisGenetStyleManager disGeNetStyleManager = DisGenetStyleManager.getInstance(loadVizmapFileTaskFactory, visualMappingMgr,visualStyleFactor);
        disGeNetStyleManager.loadStyles();
        registerService(context, disGeNetStyleManager, SessionLoadedListener.class, new Properties());
        registerService(context, new NetworkAutomationImpl(), NetworkAutomationImpl.class, new Properties());
      
        this.customGraphics = new CustomChartListener();
        registerServiceListener(context, customGraphics, "addCustomGraphicsFactory", "removeCustomGraphicsFactory", CyCustomGraphics2Factory.class);

	}

	public CyNetworkTableManager getNetTableMgr() {
		return netTableMgr;
	}

	public void setNetTableMgr(CyNetworkTableManager netTableMgr) {
		this.netTableMgr = netTableMgr;
	}

	public CyColumnIdentifierFactory getColumnIdentifierFactory() {
		return columnIdentifierFactory;
	}

	public void setColumnIdentifierFactory(CyColumnIdentifierFactory columnIdentifierFactory) {
		this.columnIdentifierFactory = columnIdentifierFactory;
	}

	public CyTableManager getCyTableManager() {
		return cyTableManager;
	}

	public void setCyTableManager(CyTableManager cyTableManager) {
		this.cyTableManager = cyTableManager;
	}

	public CyTableFactory getCyTableFactory() {
		return cyTableFactory;
	}

	public void setCyTableFactory(CyTableFactory cyTableFactory) {
		this.cyTableFactory = cyTableFactory;
	}

	public CyApplicationManager getCyApplicationManager() {
		return cyApplicationManager;
	}

	public void setCyApplicationManager(CyApplicationManager cyApplicationManager) {
		this.cyApplicationManager = cyApplicationManager;
	}

	public CustomChartListener getCustomGraphics() {
		return customGraphics;
	}

	public void setCustomGraphics(CustomChartListener customGraphics) {
		this.customGraphics = customGraphics;
	}

	public CyServiceRegistrar getServiceRegistrar() {
		return serviceRegistrar;
	}

	public BundleContext getContext() {
		return context;
	}

	public void setContext(BundleContext context) {
		this.context = context;
	}

	public void setServiceRegistrar(CyServiceRegistrar serviceRegistrar) {
		this.serviceRegistrar = serviceRegistrar;
	}

	public static CyActivator getInstance(){
		return instance;
	}

	public CySwingApplication getDesktopService(){
		return this.cytoscapeDesktopService;
	}

	public CyTableFactory getTableFactoryService(){
		return this.tableFactory;
	}

	public CyNetworkFactory getNetworkFactoryService(){
		return this.networkFactory;
	}

	public CyNetworkManager getNetworkManagerService(){
		return this.netMgr;
	}

	public CyNetworkViewFactory getNetworkViewFactoryService(){
		return this.networkViewFactory;
	}

	public CyApplicationManager getApplicationManagerService(){
		return this.cyApplicationManager;
	}

	public CyNetworkViewManager getNetworkViewManagerService(){ //JP
		return this.netViewMgr;									//JP
	}														    //JP

	public TaskIterator getGlobalTaskIterator() {
		return this.globalTaskIterator;
	}
	
	public DialogTaskManager getDialogTaskManagerService(){
		return this.dialogTaskManager;
	}
	
	public TaskManager getTaskManagerService() {
		return this.taskManager;
	}
	
	public SynchronousTaskManager getSynchTaskManagerService() {
		return this.syncTaskManager;
	}
	
	public void unregister_disgenet_panel(DisGeNETCytoPanel disgenetCytoPanel){
		//Register CytoPanel
		registerService(this.context, disgenetCytoPanel, CytoPanelComponent.class, new Properties());
	}

	public void setDisgenetNodeAttributesTable(CyTable table){
		this.disgenetNodeAttributesTable=table;
	}

	public CyTable getDisgenetNodeAttributesTable(){
		return this.disgenetNodeAttributesTable;
	}

	public VisualMappingManager getVisualMappingMgr() {
		return visualMappingMgr;
	}

	public VisualStyleFactory getVisualStyleFactor() {
		return visualStyleFactor;
	}
	
	public void setDisGeNETdb(Database db) {
		this.disGeNETdb = db;
	}
	
	public Database  getDisGeNETdb() {
		return this.disGeNETdb;
	}

    public Map<Long, CyTable> getEnrichmentTables() {
        return enrichmentTables;
    }

    public ShowEnrichmentPanelTaskFactory getShowEnrichmentFactory() {
        return showEnrichmentFactory;
    }
}
