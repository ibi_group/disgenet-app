package es.imim.DisGeNET.internal.linkout;

import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyTable;
import org.cytoscape.task.AbstractEdgeViewTaskFactory;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.work.TaskIterator;

public class LinkoutEdgeTaskFactory extends AbstractEdgeViewTaskFactory {

	@Override
	public TaskIterator createTaskIterator(View<CyEdge> cyEdge, CyNetworkView cyNetworkView) {
		return new TaskIterator(new  LinkoutEdgeTask(cyNetworkView.getModel(),cyEdge.getModel()));
	}
	
	@Override
	public boolean isReady(View<CyEdge> cyEdge, CyNetworkView cyNetworkView) {
		boolean flag = false;
		CyTable edgeTable = cyNetworkView.getModel().getDefaultEdgeTable();
		String pmid = edgeTable.getRow(cyEdge.getModel().getSUID()).get("pmid", String.class);
		if(!pmid.equals("")) {
			flag=true;
		}
		return flag;
	}

}
