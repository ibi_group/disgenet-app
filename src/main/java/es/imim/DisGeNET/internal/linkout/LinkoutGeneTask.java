package es.imim.DisGeNET.internal.linkout;

import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyTable;
import org.cytoscape.util.swing.OpenBrowser;
import org.cytoscape.work.Task;
import org.cytoscape.work.TaskMonitor;

public class LinkoutGeneTask extends LinkoutNodeTask implements Task {
	
	private CyNetwork currentNet;
	private CyNode selectedNode;
	
	public LinkoutGeneTask(CyNetwork cyNetwork, CyNode cyNode) {
		super();
		this.currentNet = cyNetwork;
		this.selectedNode = cyNode;
	}

	@Override
	public void run(TaskMonitor taskMonitor) throws Exception {
		OpenBrowser openBrowser = super.getOpenBrowser();
		CyTable nodeTable = currentNet.getDefaultNodeTable();
		String entrezId = nodeTable.getRow(selectedNode.getSUID()).get("geneId", String.class);
		String url = LinkoutProperties.GENE_LINKOUT_URL+entrezId;
		openBrowser.openURL(url); 
	}

}
