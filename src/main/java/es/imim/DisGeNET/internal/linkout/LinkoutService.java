package es.imim.DisGeNET.internal.linkout;

import static org.cytoscape.work.ServiceProperties.*;

import java.util.Properties;

import org.cytoscape.service.util.CyServiceRegistrar;
import org.cytoscape.task.EdgeViewTaskFactory;
import org.cytoscape.task.NodeViewTaskFactory;

public class LinkoutService {
	
	public LinkoutService (CyServiceRegistrar serviceRegistrar) {
		final NodeViewTaskFactory linkoutGene = new LinkoutGeneTaskFactory ();
		final Properties linkoutGeneProp = new Properties();
		linkoutGeneProp.setProperty(PREFERRED_MENU, NODE_APPS_MENU+".DisGeNET.LinkOut");
		linkoutGeneProp.setProperty(MENU_GRAVITY, "1.0");
		linkoutGeneProp.setProperty(TITLE, "gene");
		serviceRegistrar.registerService(linkoutGene, NodeViewTaskFactory.class, linkoutGeneProp);
		
		final NodeViewTaskFactory linkoutDisease = new LinkoutDiseaseTaskFactory ();
		final Properties linkoutDiseaseProp = new Properties();
		linkoutDiseaseProp.setProperty(PREFERRED_MENU, NODE_APPS_MENU+".DisGeNET.LinkOut");
		linkoutDiseaseProp.setProperty(MENU_GRAVITY, "2.0");
		linkoutDiseaseProp.setProperty(TITLE, "disease");
		serviceRegistrar.registerService(linkoutDisease, NodeViewTaskFactory.class, linkoutDiseaseProp);
		
		final NodeViewTaskFactory linkoutVariant = new LinkoutVariantTaskFactory ();
		final Properties linkoutVariantProp = new Properties();
		linkoutVariantProp.setProperty(PREFERRED_MENU, NODE_APPS_MENU+".DisGeNET.LinkOut");
		linkoutVariantProp.setProperty(MENU_GRAVITY, "3.0");
		linkoutVariantProp.setProperty(TITLE, "variant");
		serviceRegistrar.registerService(linkoutVariant, NodeViewTaskFactory.class, linkoutVariantProp);
		
		final EdgeViewTaskFactory linkoutPubmed = new LinkoutEdgeTaskFactory ();
		final Properties linkoutPubmedProp =  new Properties();
		linkoutPubmedProp.setProperty(PREFERRED_MENU, EDGE_APPS_MENU+".DisGeNET.LinkOut");
		linkoutPubmedProp.setProperty(MENU_GRAVITY, "2.0");
		linkoutPubmedProp.setProperty(TITLE, "paper");
		serviceRegistrar.registerService(linkoutPubmed, EdgeViewTaskFactory.class, linkoutPubmedProp);
	}
	
	
}
