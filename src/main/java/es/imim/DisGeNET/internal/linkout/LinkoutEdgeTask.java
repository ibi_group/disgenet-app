package es.imim.DisGeNET.internal.linkout;

import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyTable;
import org.cytoscape.util.swing.OpenBrowser;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import es.imim.DisGeNET.internal.CyActivator;

public class LinkoutEdgeTask extends AbstractTask {

	private OpenBrowser openBrowser;
	private CyNetwork currentNet;
	private CyEdge selectedEdge;
	
	public LinkoutEdgeTask (CyNetwork currentNet, CyEdge selectedEdge) {
		this.openBrowser = CyActivator.getInstance().getServiceRegistrar().getService(OpenBrowser.class);
		this.currentNet = currentNet;
		this.selectedEdge = selectedEdge;
	}
	
	@Override
	public void run(TaskMonitor taskMonitor) throws Exception {
		CyTable edgeTable = currentNet.getDefaultEdgeTable();
		String pmid = edgeTable.getRow(selectedEdge.getSUID()).get("pmid",String.class);
		String url = LinkoutProperties.PUBMED_LINKOUT_URL+pmid;
		openBrowser.openURL(url);
	}

}
