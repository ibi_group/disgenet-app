package es.imim.DisGeNET.internal.linkout;

import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyTable;
import org.cytoscape.util.swing.OpenBrowser;
import org.cytoscape.work.Task;
import org.cytoscape.work.TaskMonitor;

public class LinkoutDiseaseTask extends LinkoutNodeTask implements Task {

	private CyNetwork currentNet;
	private CyNode selectedNode;
	
	public LinkoutDiseaseTask(CyNetwork cyNetwork, CyNode cyNode) {
		super();
		this.currentNet = cyNetwork;
		this.selectedNode = cyNode;
	}

	@Override
	public void run(TaskMonitor taskMonitor) throws Exception {
		OpenBrowser openBrowser = super.getOpenBrowser();
		CyTable nodeTable = currentNet.getDefaultNodeTable();
		String diseaseUmls = nodeTable.getRow(selectedNode.getSUID()).get("diseaseId", String.class);
		String url = LinkoutProperties.DISEASE_LINKOUT_URL+diseaseUmls;
		openBrowser.openURL(url); 
	}

}
