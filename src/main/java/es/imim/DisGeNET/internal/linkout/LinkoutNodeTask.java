package es.imim.DisGeNET.internal.linkout;

import org.cytoscape.util.swing.OpenBrowser;
import org.cytoscape.work.AbstractTask;

import es.imim.DisGeNET.internal.CyActivator;

public abstract class LinkoutNodeTask extends AbstractTask {
	
	private OpenBrowser openBrowser;

	public LinkoutNodeTask () {
		this.openBrowser = CyActivator.getInstance().getServiceRegistrar().getService(OpenBrowser.class);
	}

	public OpenBrowser getOpenBrowser() {
		return openBrowser;
	}

	public void setOpenBrowser(OpenBrowser openBrowser) {
		this.openBrowser = openBrowser;
	}

}
