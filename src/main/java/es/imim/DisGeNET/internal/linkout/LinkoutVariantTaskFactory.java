package es.imim.DisGeNET.internal.linkout;

import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyTable;
import org.cytoscape.task.AbstractNodeViewTaskFactory;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.work.TaskIterator;

public class LinkoutVariantTaskFactory extends AbstractNodeViewTaskFactory {

	@Override
	public TaskIterator createTaskIterator(View<CyNode> cyNodeView, CyNetworkView cyNetworkView) {
		return new TaskIterator(new LinkoutVariantTask(cyNetworkView.getModel(), cyNodeView.getModel()));
	}
	
	@Override
	public boolean isReady(View<CyNode> cyNodeView, CyNetworkView cyNetworkView) {
		boolean flag;
		CyTable nodeTable = cyNetworkView.getModel().getDefaultNodeTable();
		String nodeType = nodeTable.getRow(cyNodeView.getModel().getSUID()).get("nodeType", String.class,"");
		if(nodeType.equals("variant")) {
			flag =  true;
		}else{
			flag = false;
		}
		return flag;
	}


}
