package es.imim.DisGeNET.internal.linkout;

public class LinkoutProperties {

	public static final String GENE_LINKOUT_URL = "https://www.ncbi.nlm.nih.gov/gene/";
	public static final String VARIANT_LINKOUT_URL = "https://www.ncbi.nlm.nih.gov/SNP/snp_ref.cgi?rs=";
	public static final String DISEASE_LINKOUT_URL = "http://linkedlifedata.com/resource/umls/id/";
	public static final String PUBMED_LINKOUT_URL = "https://www.ncbi.nlm.nih.gov/pubmed/?term="; 
}
