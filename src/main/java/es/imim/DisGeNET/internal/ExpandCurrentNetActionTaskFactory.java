package es.imim.DisGeNET.internal;


import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyTable;
import org.cytoscape.task.AbstractNodeViewTaskFactory;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.work.TaskIterator;

import es.imim.DisGeNET.network.ExpandCurrentNetworkTask;
import es.imim.DisGeNET.validation.NetworkValidation;

public class ExpandCurrentNetActionTaskFactory extends AbstractNodeViewTaskFactory{

	@Override
	public TaskIterator createTaskIterator(View<CyNode> cyNodeView, CyNetworkView cyNetworkView) {
		return new TaskIterator(new ExpandCurrentNetworkTask(cyNetworkView, cyNodeView));
	}

	@Override
	public boolean isReady(View<CyNode> cyNodeView, CyNetworkView cyNetworkView) {
		boolean flag = true;
		CyTable nodeTable = cyNetworkView.getModel().getDefaultNodeTable();
		String nodeType = nodeTable.getRow(cyNodeView.getModel().getSUID()).get("nodeType", String.class,"");
		if(!NetworkValidation.isForeignNet(cyNetworkView.getModel())) {
			if(NetworkValidation.isVariantNet(cyNetworkView.getModel())) {
				if(nodeType.equals("gene")) {
					flag = false;
				}
				
			}
		}else {
			flag = false;
		}
		return flag;
	}
	
}
