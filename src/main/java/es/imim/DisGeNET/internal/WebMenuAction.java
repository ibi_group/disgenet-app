package es.imim.DisGeNET.internal;

import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;

import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.application.swing.AbstractCyAction;

import org.cytoscape.util.swing.OpenBrowser;

/**
 * Creates a new menu item under Apps menu section.
 *
 */
public class WebMenuAction extends AbstractCyAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private OpenBrowser browser;

	public WebMenuAction(CyApplicationManager cyApplicationManager, final String menuTitle, OpenBrowser openBrowser) {
		
		super(menuTitle, cyApplicationManager, null, null);

		setPreferredMenu("Apps.DisGeNET");
		setMenuGravity(3.0f);
		//setToolTipText("Go to DisGeNET homepage");

		browser = openBrowser;
		
	}

	public void actionPerformed(ActionEvent e) {
		this.browser.openURL("https://www.disgenet.org");
	}
}
