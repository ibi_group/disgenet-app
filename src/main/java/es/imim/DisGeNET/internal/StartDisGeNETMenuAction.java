package es.imim.DisGeNET.internal;

import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;

import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.application.swing.AbstractCyAction;
import org.cytoscape.service.util.CyServiceRegistrar;
import org.cytoscape.view.vizmap.VisualMappingManager;

import es.imim.DisGeNET.database.Database;
import es.imim.DisGeNET.database.DatabaseImpl;
import es.imim.DisGeNET.gui.GuiParameters;
import es.imim.DisGeNET.gui.MainPanel;

import java.sql.SQLException;
import java.util.Properties;

import org.cytoscape.application.swing.CySwingApplication;
import org.cytoscape.application.swing.CytoPanel;
import org.cytoscape.application.swing.CytoPanelComponent;
import org.cytoscape.application.swing.CytoPanelName;
import org.cytoscape.application.swing.CytoPanelState;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskIterator; 



/**
 * Creates a new menu item under Apps menu section.
 *
 */
public class StartDisGeNETMenuAction extends AbstractCyAction {
	
	private boolean running = false;
	private MainPanel mainPanel;
	private VisualMappingManager vmm;

	private CySwingApplication desktopApp;
	private CyServiceRegistrar serviceRegistrar;
	private CytoPanel cytoPanelWest;
	private DisGeNETCytoPanel disgenetPanel;

	private CyActivator activator;

	public StartDisGeNETMenuAction(CyApplicationManager cyApplicationManager, CyServiceRegistrar serviceRegistrar, final String menuTitle) {
		super(menuTitle, cyApplicationManager, null, null);
		
		setPreferredMenu("Apps.DisGeNET");
		setMenuGravity(1.0f);
		this.serviceRegistrar = serviceRegistrar;
		//this.disgenetPanel = controlPanel;
	}

	public void set_activator(CyActivator activator){
		this.activator = activator;
	}
	
	public void unregister() {		
		this.serviceRegistrar.unregisterService(this.disgenetPanel, CytoPanelComponent.class);
		this.disgenetPanel = null;		
	}

	public void actionPerformed(ActionEvent e) {
				
		if( this.disgenetPanel == null ){

			this.desktopApp = activator.getDesktopService();
			this.cytoPanelWest = this.desktopApp.getCytoPanel(CytoPanelName.WEST);
			this.disgenetPanel = new DisGeNETCytoPanel();
			try {
				DatabaseImpl.getInstance();
			}catch (Exception ex) {
				ex.printStackTrace();
				JOptionPane.showMessageDialog(null, "Error starting DisGeNET.");
			}
			try {
				this.mainPanel = new MainPanel(this);
				this.disgenetPanel.add(mainPanel);
				this.serviceRegistrar.registerService(this.disgenetPanel, CytoPanelComponent.class, new Properties());
			} catch (ClassNotFoundException exc) {
				exc.printStackTrace();
				JOptionPane.showMessageDialog(null, "Error starting DisGeNET.");
				this.disgenetPanel = null;
			} catch (SQLException exc) {
				exc.printStackTrace();
				JOptionPane.showMessageDialog(null, "Error starting DisGeNET.");
				this.disgenetPanel = null;
			} catch (Exception ex) {
				ex.printStackTrace();
				JOptionPane.showMessageDialog(null, "Error starting DisGeNET.");
				this.disgenetPanel = null;
			}
			
		}
		
		// JP
		else {
			JOptionPane.showMessageDialog(null, "The plugin is running already.");
		}
		// JP
		if (cytoPanelWest.getState() == CytoPanelState.HIDE) {
			cytoPanelWest.setState(CytoPanelState.DOCK);
		}

		int index = cytoPanelWest.indexOfComponent(this.disgenetPanel);
		if( index == -1 ){
			return;
		}
		cytoPanelWest.setSelectedIndex(index);


		//JAVI. Test to create default tables
		

		
		/*
		// display MainPanel in left cytoscape panel
		CytoscapeDesktop desktop = Cytoscape.getDesktop();
		// get the west cytopanel
		CytoPanel cytoPanel = desktop.getCytoPanel(SwingConstants.WEST);

		// check if the plugin is already running
		if (!running) {
			// load visual style if not already loaded
			if (!vmm.getCalculatorCatalog().getVisualStyleNames().contains(	"DisGeNETstyle")) {
				logger.info("adding visual style");
				vmm.getCalculatorCatalog().addVisualStyle(vistyle);
			}
				
			try {
				mainPanel = new MainPanel(this, vistyle);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			URL iconURL = DisGeNET.class.getResource("resources/DisGeNET_small.gif");
			if (iconURL != null) {
				ImageIcon icon = new ImageIcon(iconURL);
				String tip = "tooltip";
				// add the main panel together with a icon with text
				cytoPanel.add("", icon, mainPanel, tip); 
			} else {
				cytoPanel.add("DisGeNET", mainPanel);
			}
		} else {
			JOptionPane.showMessageDialog(null, "The plugin is running already.");
		}
		int index = cytoPanel.indexOfComponent(mainPanel);
		cytoPanel.setSelectedIndex(index);
		cytoPanel.setState(CytoPanelState.DOCK);
		setRunning(true);
		*/
	}

	//JAVI: To check if these methods are necessary
	public void setRunning(boolean opened) {
			this.running = opened;

			//JAVI
			//if (!isRunning() && vmm.getVisualStyle() == vistyle) {
			//	vmm.setVisualStyle("default");

				//JAVI
				//vmm.applyAppearances();
			//}
		}

	public boolean isRunning() {
		return running;
	}
}