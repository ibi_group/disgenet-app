package es.imim.DisGeNET.internal;

import java.awt.event.ActionEvent;

import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.application.swing.AbstractCyAction;

import es.imim.DisGeNET.database.DatabaseManager;
import es.imim.DisGeNET.database.DatabaseManagerImpl;




/**
 * Creates a new menu item under Apps menu section.
 *
 */
public class ChangeDBMenuAction extends AbstractCyAction {

	public ChangeDBMenuAction(CyApplicationManager cyApplicationManager, final String menuTitle) {
		super(menuTitle, cyApplicationManager, null, null);
		/*setToolTipText("Allows you to choose another folder for the DisGeNET database");*/
		setPreferredMenu("Apps.DisGeNET");
		setMenuGravity(2.0f);
	}

	public void actionPerformed(ActionEvent e) {
		DatabaseManager databaseManager = DatabaseManagerImpl.getInstance();
		databaseManager.setPreferences();
	}
}
