package es.imim.DisGeNET.internal;

import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;
import javax.swing.JDialog;

import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.application.swing.AbstractCyAction;
import org.cytoscape.application.swing.CySwingApplication;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import javax.swing.text.html.HTMLEditorKit;
import javax.swing.JEditorPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import java.awt.Desktop;
import java.io.IOException;

/**
 * Creates a new menu item under Apps menu section.
 *
 */
public class AboutMenuAction extends AbstractCyAction {

	private CySwingApplication cytoscapeDesktopService;
	private JDialog dialogPane;

	public AboutMenuAction(CyApplicationManager cyApplicationManager, final String menuTitle, CySwingApplication desktopService) {
		
		super(menuTitle, cyApplicationManager, null, null);
		setPreferredMenu("Apps.DisGeNET");
		setMenuGravity(5.0f);
		cytoscapeDesktopService = desktopService;
		dialogPane = null;
		
	}

	private class HyperlinkAction implements HyperlinkListener {
		public HyperlinkAction(JEditorPane pane) {
		}

		public void hyperlinkUpdate(HyperlinkEvent event) {
			if (event.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
				Desktop dt = Desktop.getDesktop();
				try {
					dt.browse(new URI(event.getURL().toString()));
				} catch (IOException e) {
					e.printStackTrace();
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}
			}
		}
	}


	public void actionPerformed(ActionEvent e) {

		if( dialogPane == null ){

			dialogPane = new JDialog(cytoscapeDesktopService.getJFrame(),"About DisGeNET", false);
			dialogPane.setResizable(false);

			// main panel for dialog box
			JEditorPane editorPane = new JEditorPane();
			// editorPane.setMargin(new Insets(10,10,10,10));
			editorPane.setEditable(false);
			editorPane.setEditorKit(new HTMLEditorKit());
			editorPane.addHyperlinkListener(new HyperlinkAction(editorPane));
			
			URL logoURL = getClass().getClassLoader().getResource("images/DisGeNET_banner_logo_small.png");
			//String iconPath = getClass().getClassLoader().getResource("/logo.png").toString();		
			URL gribLogoURL = getClass().getClassLoader().getResource("images/grib.gif");
			URL prbbLogoURL = getClass().getClassLoader().getResource("images/prbb.gif");
			URL upfLogoURL = getClass().getClassLoader().getResource("images/upf.gif");
			URL bgURL = getClass().getClassLoader().getResource("images/DisGeNET_banner_bg_small.jpg");

			//	System.err.println(gribLogoURL.toString());

			String logoCode = "";
			if (logoURL != null) {
				logoCode = "<center><img src='" + logoURL + "'></center>";
			}

			editorPane
					.setText("<html><body style=\"padding:10px\" background=\""
							+ bgURL
							+ "\">"
							+ logoCode
							+ "<table width = \"460px\"><tr>" +
                                    "<td valign=\"top\"><br/>" +
                                    "<center><img src='" + gribLogoURL + "'></center><br/>" +
                                    "<center><img src='" + upfLogoURL + "'></center><br/>" +
                                    "</td>" +
                                    "<td valign=\"top\"><br/><p><b>DisGeNET Cytoscape App v7.3.0 (April 2021)</b></p><br/>" +
									"<p><b><u>Authors:</p></u></b>Integrative Biomedical Informatics Group,<br/> Research Programme on Biomedical Informatics (GRIB) IMIM-UPF<br/>C/ Dr.Aiguader 88<br/>08003 Barcelona, Spain<br/>" +
                                    "<a href='https://www.disgenet.org'>https://www.disgenet.org</a></p></td>"+
									"</tr>" +
                                    "<tr>"+
									"<td colspan = \"2\"><p><b><u>Description</u></b><br/>" +
									"<p align = \"justify\">" +
								    "The DisGeNET Cytoscape App is designed to visualize, query and analyse a network representation of DisGeNET data (version 7.0). " +
                                    "It assists the user in the interpretation and exploration of the genetic basis of human complex diseases by a variety of built-in functions. " +
                                    "Using the DisGeNET Cytoscape App, gene-disease and variant-disease networks are easily generated. These networks can be restricted to:</br>" +
                                    "<ul>" +
                                        "<li>the original data source</li>" +
                                        "<li> the association type </li>" +
                                        "<li> a disease class of interest</li>" +
                                        "<li> specific diseases, genes, or variants, and lists and combinations of them</li>" +
                                        "<li> a range of score an Evidence Index range an Evidence Level category</li>"+
                                    "</ul>" +
                                    "<p>These networks can be filtered by source, disease class, or built around specific genes, or diseases.</br>" +
                                    "The DisGeNET app includes an automation module with a set of REST endpoints which allows users to create workflows executed entirely within Cytoscape" +
                                    " or by external tools (such as RStudio or Jupyter)</p></tr>" +
									"<tr><td>The DisGeNET app uses the following software libraries in whole or in part:" +
									"<ul>" +
									"<li>Java Tar Package, available at <a href='http://www.trustice.com/java/tar/index.shtml'>http://www.trustice.com/java/tar/index.shtml</a>" +
									"<li>SQLite JDBC Driver, available at <a href='https://github.com/xerial/sqlite-jdbc'>https://github.com/xerial/sqlite-jdbc</a>" +
									"</p></td>"+
									"</tr><tr>" +
									"<td colspan=\"2\"><br/><p><b><u>Reference:</u></b><br/>" +
									"<ul>" +
                                    "<li>Piñero J, Ramírez-Anguita JM, Saüch-Pitarch J, Ronzano F, Centeno E, Sanz F, Furlong LI: The DisGeNET knowledge platform for disease genomics: 2019 update doi: 10.1093/nar/gkz1021</li>" +
									"<li>Piñero J, Bravo A,  Queralt-Rosinach N,  Gutiérrez-Sacristán A,  Deu-Pons J, Centeno  E, García-García  J, Sanz F, Furlong LI: <b>DisGeNET: a comprehensive platform integrating information on human disease-associated genes and variants</b> <i>doi: 10.1093/nar/gkw943</i>" +
									"<li>Piñero J, Queralt-Rosinach N, Bravo À, Deu-Pons J, Bauer-Mehren A, Baron M, Sanz F, Furlong LI: <b>DisGeNET: a discovery platform for the dynamical exploration of human diseases and their genes</b> <i>doi: 10.1093/database/bav028 </i>" +
									"<li>Bauer-Mehren A, Rautschka M, Sanz F, Furlong LI: <b>DisGeNET - a Cytoscape plugin to visualize, integrate, search and analyze gene-disease networks.</b> <i> doi: 10.1093/bioinformatics/btq538</i>" +
									"<li>Bauer-Mehren A, Bundschus M, Rautschka M, Mayer MA, Sanz F, Furlong LI: <b>Network analysis of an integrated gene-disease database reveals functional modules in human disease.</b><i> doi: 10.1371/journal.pone.0020284.</i>" +
									"</p></td>"+
									"<tr><td align = \"justify\" colspan = \"2\"><p><b><u>License</u></b><br/>" +
									" The DisGeNET database and Knowledge discovery platform are provided to advance " +
									"the knowledge about human diseases and their associated genes, and are intended " +
									"to be used only for research and education. Any reproduction or use for commercial " +
									"purpose is prohibited without the prior express written permission " +
									"of the Integrative Biomedical Informatics group. "+
									"For more information, please visit our <a href='https://www.disgenet.org/legal'>legal notices page</a> " +
									"</p></td>" +
							  "</tr></table>"+
							"</P></body></html>");
			
			dialogPane.setContentPane(editorPane);

			dialogPane.pack();
		}

		dialogPane.setVisible(true);
	}
}