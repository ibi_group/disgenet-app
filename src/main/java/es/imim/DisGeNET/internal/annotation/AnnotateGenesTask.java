package es.imim.DisGeNET.internal.annotation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyTable;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;
import org.cytoscape.work.Tunable;
import org.cytoscape.work.util.ListSingleSelection;

import es.imim.DisGeNET.database.DatabaseImpl;
import es.imim.DisGeNET.exceptions.DisGeNetException;
import es.imim.DisGeNET.gui.GuiParameters;
import es.imim.DisGeNET.gui.GuiProps;
import es.imim.DisGeNET.internal.CyActivator;
import es.imim.DisGeNET.network.NetworkBuilder;
import es.imim.DisGeNET.tool.HelperFunctions;

public class AnnotateGenesTask extends AbstractTask {
    
	@Tunable(description="Select the column you want to search by: ", gravity=1,groups={"Columns"}, params="displayState=uncollapsed")
	public ListSingleSelection<Callable<CyColumn>> columnNames;
	
	@Tunable (description="Select source:", gravity=2,groups={"Sources"},params="displayState=uncollapsed")
	public ListSingleSelection<String> sources;
	
	@Tunable (description="Select Disease class:", gravity=3,params="displayState=collapsed",groups={"Filters"},context=Tunable.GUI_CONTEXT)
	public ListSingleSelection<String> diseaseClasses;
	
	@Tunable (description="Select Association type:", gravity=4,params="displayState=collapsed",groups={"Filters"},context=Tunable.GUI_CONTEXT)
	public ListSingleSelection<String> assocTypes;
	
	@Tunable (description="Select Evidence Level:", gravity=5, params="displayState=collapsed",groups={"Filters"},context=Tunable.GUI_CONTEXT)
	public ListSingleSelection<String> evidenceLevels;
	
	@Tunable (description="Select score range min value", gravity=6,params="displayState=collaspsed",groups={"Filters"},context=Tunable.GUI_CONTEXT,
			format="0.00")
	public Double minScoreRange;
	
	@Tunable (description="Select score range max value", gravity=7,params="displayState=collaspsed",groups={"Filters"},context=Tunable.GUI_CONTEXT,
			format="0.00")
	public Double maxScoreRange;
	
	private CyColumn selectedColumn;
	
    private CyNetwork currNetwork;
    private GuiParameters params;
    private Map <String,Long> nodeMap;

    public AnnotateGenesTask(CyNetworkView cyNetworkView) {
		this.currNetwork = cyNetworkView.getModel();
		this.params = new GuiParameters<>();
		this.sources = new ListSingleSelection<String>(Arrays.asList(GuiProps.SOURCE_OPTIONS));
		this.diseaseClasses = new ListSingleSelection<String>(Arrays.asList(GuiProps.DISEASE_CLASS_OPTIONS));
		this.assocTypes = new ListSingleSelection<String>(Arrays.asList(GuiProps.ASSOCIATION_TYPE_OPTIONS));
		this.evidenceLevels = new ListSingleSelection<String>(Arrays.asList(GuiProps.EL_OPTIONS));
		this.minScoreRange = 0.;
		this.maxScoreRange = 1.;
		this.columnNames = listNodeColumnsNames();
		this.nodeMap = new HashMap <String,Long>();
	}
	
    private ListSingleSelection<Callable<CyColumn>> listNodeColumnsNames() {
		CyTable nodeTable = currNetwork.getDefaultNodeTable();
		Collection<CyColumn> nodeColumns = nodeTable.getColumns();
		
		List<Callable<CyColumn>> columnNames = new ArrayList<Callable<CyColumn>>();
		
		for(final CyColumn column : nodeColumns) {
			if(!column.isPrimaryKey()) {
				columnNames.add(new Callable<CyColumn>() {
					public CyColumn call() { 
						return column; 
					}
					public String toString() { 
						return column.getName(); 
					}
				});
			}
		}
		return new ListSingleSelection<Callable<CyColumn>>(columnNames);
    }

	public String buildSearchField() throws DisGeNetException {
		String searchCondition = "";
		List<String> geneIds = new ArrayList<>();
		if (selectedColumn.getType().equals(String.class)) {
			List <String> geneSymbols = selectedColumn.getValues(String.class);
			DatabaseImpl db = DatabaseImpl.getInstance();
			geneIds = db.genNamesToId(geneSymbols);
			if(geneIds.isEmpty()) {
				geneIds = geneSymbols;
			}
		}else if (selectedColumn.getType().equals(Integer.class)) {
			List <Integer> geneIdsRaw = selectedColumn.getValues(Integer.class);
			for (Integer geneId : geneIdsRaw) {
				if(geneId!=null) {
					geneIds.add(geneId.toString());
				}
			}
		}else {
			throw new DisGeNetException("Invalid column data type... Must be a Integer or String");
		}

		searchCondition = HelperFunctions.implodeString(",", geneIds);
		
		return searchCondition;
			
	}
	
	@Override
	public void run(TaskMonitor taskMonitor) throws Exception {
		taskMonitor.setTitle("Annotate diseases to genes...");
		selectedColumn = columnNames.getSelectedValue().call();
		String source = sources.getSelectedValue();
		String diseaseClass = diseaseClasses.getSelectedValue();
//		String netName = currNetwork.getDefaultNetworkTable().getRow(currNetwork.getSUID()).get(CyNetwork.NAME,String.class);
		String searchCondition = buildSearchField();
		params.setActiveTab("GeneDisTabPane");
		params.setGenSearchText(searchCondition);
		params.setSource(source);
		params.setDiseaseClass(diseaseClass);
		params.setDiseaseClass(diseaseClasses.getSelectedValue());
		params.setAssociationType(assocTypes.getSelectedValue());
		params.setLowScore(minScoreRange.toString());
		params.setHighScore(maxScoreRange.toString());
		params.setEl(evidenceLevels.getSelectedValue());
		params.setNetworkName(buildNetworkName());
		NetworkBuilder networkBuilder = new NetworkBuilder(params);
//		CyNetwork cyNetwork = CyActivator.getInstance().getNetworkFactoryService().createNetwork();
//		CyNetworkView cyNetworkView = CyActivator.getInstance().getNetworkViewFactoryService().createNetworkView(cyNetwork);
//		cyNetwork.getRow(cyNetwork).set(CyNetwork.NAME, netName+"_gda"+"_"+source);
//		CyActivator.getInstance().getNetworkManagerService().addNetwork(cyNetwork);
//		CyActivator.getInstance().getNetworkViewManagerService().addNetworkView(cyNetworkView);
//		networkBuilder.createNodeAttributeColumns(cyNetwork);
//		networkBuilder.createCyEdgeAttributeColumns(cyNetwork);
		try {
			networkBuilder.buildGeneDiseaseNet(params);
		} catch (DisGeNetException dsgnEx) {
			dsgnEx.getStackTrace();
			throw new DisGeNetException("Error: The query didn't return any results, check that the colum contains gene identifiers for DisGeNET (entrezId or NCBI gene symbol)"); 
		}
	}
	
	private String buildNetworkName() {
		 String  network_name = "DisGeNET-GDA";
		 network_name += " - " + currNetwork.getDefaultNetworkTable().getColumn("name").getValues(String.class).get(0);
		 network_name += " - " + params.getSource();
		 network_name += " - " + params.getDiseaseClass();
		 return network_name;
	 }

}
