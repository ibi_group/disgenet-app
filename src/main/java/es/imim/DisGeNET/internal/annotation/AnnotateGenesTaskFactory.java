package es.imim.DisGeNET.internal.annotation;

import org.cytoscape.task.AbstractNetworkViewTaskFactory;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.work.TaskIterator;

import es.imim.DisGeNET.validation.NetworkValidation;


public class AnnotateGenesTaskFactory extends AbstractNetworkViewTaskFactory {

	@Override
	public TaskIterator createTaskIterator(CyNetworkView cyNetworkView) {
		return new TaskIterator(new AnnotateGenesTask(cyNetworkView));
	}
	
	@Override
	public boolean isReady (CyNetworkView cyNetworkView) {
		boolean flag = false;
		if(cyNetworkView!=null) {
			flag = NetworkValidation.isForeignNet(cyNetworkView.getModel());
			if(!flag) {
				flag = NetworkValidation.isProjectionNet(cyNetworkView.getModel(),"gg");
			}
		}
		return flag;
	}
	
	

}
