package es.imim.DisGeNET.internal.annotation;

import static org.cytoscape.work.ServiceProperties.*;

import java.util.Properties;

import org.cytoscape.service.util.CyServiceRegistrar;
import org.cytoscape.task.NetworkViewTaskFactory;


public class AnnotationService {
	
	public AnnotationService (CyServiceRegistrar serviceRegistrar) {
		
		final NetworkViewTaskFactory annotateGenes = new AnnotateGenesTaskFactory();
		final Properties annotGenesProp = new Properties();
		annotGenesProp.setProperty(PREFERRED_MENU, NETWORK_APPS_MENU+".DisGeNET.Genes");
		annotGenesProp.setProperty(IN_NETWORK_PANEL_CONTEXT_MENU, "true");
		annotGenesProp.setProperty(IN_MENU_BAR, "false");
		annotGenesProp.setProperty(MENU_GRAVITY, "1");
		annotGenesProp.setProperty(TITLE,"Annotate genes with diseases from the selected source");
		serviceRegistrar.registerService(annotateGenes, NetworkViewTaskFactory.class, annotGenesProp);
		
		
		final NetworkViewTaskFactory annotateVariants = new AnnotateVariantsTaskFactory();
		final Properties annotVariantsProp = new Properties();
		annotVariantsProp.setProperty(PREFERRED_MENU, NETWORK_APPS_MENU+".DisGeNET.Variants");
		annotVariantsProp.setProperty(IN_NETWORK_PANEL_CONTEXT_MENU, "true");
		annotVariantsProp.setProperty(IN_MENU_BAR, "false");
		annotVariantsProp.setProperty(MENU_GRAVITY, "1");
		annotVariantsProp.setProperty(TITLE,"Annotate variants with diseases from the selected source");
		serviceRegistrar.registerService(annotateVariants, NetworkViewTaskFactory.class, annotVariantsProp);
		
		final NetworkViewTaskFactory annotateDiseasesGenes = new AnnotateDiseasesGenesTaskFactory();
		final Properties annotDiseasesGenesProp = new Properties();
		annotDiseasesGenesProp.setProperty(PREFERRED_MENU, NETWORK_APPS_MENU+".DisGeNET.Diseases");
		annotDiseasesGenesProp.setProperty(IN_NETWORK_PANEL_CONTEXT_MENU, "true");
		annotDiseasesGenesProp.setProperty(IN_MENU_BAR, "false");
		annotDiseasesGenesProp.setProperty(MENU_GRAVITY, "1");
		annotDiseasesGenesProp.setProperty(TITLE,"Annotate diseases with genes from the selected source");
		serviceRegistrar.registerService(annotateDiseasesGenes, NetworkViewTaskFactory.class, annotDiseasesGenesProp);
		
		final NetworkViewTaskFactory annotateDiseasesVariants= new AnnotateDiseasesVariantsTaskFactory();
		final Properties annotDiseasesVariantsProp = new Properties();
		annotDiseasesVariantsProp.setProperty(PREFERRED_MENU, NETWORK_APPS_MENU+".DisGeNET.Diseases");
		annotDiseasesVariantsProp.setProperty(IN_NETWORK_PANEL_CONTEXT_MENU, "true");
		annotDiseasesVariantsProp.setProperty(IN_MENU_BAR, "false");
		annotDiseasesVariantsProp.setProperty(MENU_GRAVITY, "1");
		annotDiseasesVariantsProp.setProperty(TITLE,"Annotate diseases with variants from the selected source");
		serviceRegistrar.registerService(annotateDiseasesVariants, NetworkViewTaskFactory.class, annotDiseasesVariantsProp);
	}
}
