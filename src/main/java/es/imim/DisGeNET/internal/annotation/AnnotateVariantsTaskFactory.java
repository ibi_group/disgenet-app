package es.imim.DisGeNET.internal.annotation;

import org.cytoscape.task.NetworkViewTaskFactory;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.work.TaskIterator;

import es.imim.DisGeNET.validation.NetworkValidation;

public class AnnotateVariantsTaskFactory implements NetworkViewTaskFactory {

	@Override
	public TaskIterator createTaskIterator(CyNetworkView cyNetworkView) {
		return new TaskIterator(new AnnotateVariantsTask(cyNetworkView,"ALL"));
	}

	@Override
	public boolean isReady(CyNetworkView cyNetworkView) {
		boolean flag = false;
		if(cyNetworkView!=null) {
			flag = NetworkValidation.isForeignNet(cyNetworkView.getModel());
		}
		return flag;
	}

}
