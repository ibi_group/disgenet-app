package es.imim.DisGeNET.internal.annotation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyTable;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;
import org.cytoscape.work.Tunable;
import org.cytoscape.work.util.ListSingleSelection;

import es.imim.DisGeNET.exceptions.DisGeNetException;
import es.imim.DisGeNET.gui.GuiParameters;
import es.imim.DisGeNET.gui.GuiProps;
import es.imim.DisGeNET.network.NetworkBuilder;
import es.imim.DisGeNET.tool.HelperFunctions;

public class AnnotateVariantsTask extends AbstractTask {
	
	@Tunable(description="Select the column you want to search by: ", gravity=1,groups={"Columns"}, params="displayState=uncollapsed")
	public ListSingleSelection<Callable<CyColumn>> columnNames;
	
	@Tunable (description="Select source:", gravity=2,groups={"Sources"},params="displayState=uncollapsed")
	public ListSingleSelection<String> sources;
	
	@Tunable (description="Select Disease class:", gravity=3,params="displayState=collapsed",groups={"Filters"},context=Tunable.GUI_CONTEXT)
	public ListSingleSelection<String> diseaseClasses;
	
	@Tunable (description="Select Association type:", gravity=4,params="displayState=collapsed",groups={"Filters"},context=Tunable.GUI_CONTEXT)
	public ListSingleSelection<String> assocTypes;
		
	@Tunable (description="Select score range min value", gravity=6,params="displayState=collaspsed",groups={"Filters"},context=Tunable.GUI_CONTEXT,
			format="0.00")
	public Double minScoreRange;
	
	@Tunable (description="Select score range max value", gravity=7,params="displayState=collaspsed",groups={"Filters"},context=Tunable.GUI_CONTEXT,
			format="0.00")
	public Double maxScoreRange;
	
	private String selectedColumnName;
	private CyColumn selectedColumn;
	private CyNetwork currNetwork;
	private GuiParameters params;
	private Map <String,Long> nodeMap;

	public AnnotateVariantsTask(CyNetworkView cyNetworkView,String source) {
		 this.currNetwork = cyNetworkView.getModel();
		 this.params = new GuiParameters<>();
		 this.sources = new ListSingleSelection<>(Arrays.asList(GuiProps.VARIANT_SOURCE_OPTIONS));
		 this.diseaseClasses = new ListSingleSelection<String>(Arrays.asList(GuiProps.DISEASE_CLASS_OPTIONS));
		 this.assocTypes = new ListSingleSelection<String>(Arrays.asList(GuiProps.ASSOCIATION_TYPE_OPTIONS));
		 this.minScoreRange = 0.;
		 this.maxScoreRange = 1.;
		 this.columnNames = listNodeColumnsNames();
		 this.nodeMap = new HashMap <String,Long>();
	 }
	 
	 
	private ListSingleSelection<Callable<CyColumn>> listNodeColumnsNames() {
		CyTable nodeTable = currNetwork.getDefaultNodeTable();
		Collection<CyColumn> nodeColumns = nodeTable.getColumns();
		
		List<Callable<CyColumn>> columnNames = new ArrayList<Callable<CyColumn>>();
		
		for(final CyColumn column : nodeColumns) {
			if(!column.isPrimaryKey()) {
				columnNames.add(new Callable<CyColumn>() {
					public CyColumn call() { 
						return column; 
					}
					public String toString() { 
						return column.getName(); 
					}
				});
			}
		}
		return new ListSingleSelection<Callable<CyColumn>>(columnNames);
    }

	 public String buildSearchField() throws DisGeNetException {
		 String searchCondition;
		 List<String> variantIdsRaw = new ArrayList<>();
		 List<String> variantIds = new ArrayList<>();
		 if (selectedColumn.getType().equals(String.class)) {
			 variantIdsRaw = selectedColumn.getValues(String.class);
			 Iterator <String> iterator=variantIdsRaw.iterator();
			 while(iterator.hasNext()){
				 String snpId = iterator.next();
				 if(snpId.matches("(rs)\\d+")) {
					 variantIds.add("'"+snpId+"'");
				 }
			 }
		 }else {
			 throw new DisGeNetException("Invalid column data type... Must be a String");
		 }
		 if(variantIds.isEmpty()) {
			 throw new DisGeNetException("Error: The selected column is not a variant identifier for DisGeNET (dbSNP identifier)");
		 }
		 searchCondition = HelperFunctions.implodeString(",", variantIds);
		 return searchCondition;

	 }

	 @Override
	 public void run(TaskMonitor taskMonitor) throws Exception {
		 taskMonitor.setTitle("Annotate diseases to variants...");
		 selectedColumn = columnNames.getSelectedValue().call();
		 String source = sources.getSelectedValue();
		 String diseaseClass = diseaseClasses.getSelectedValue();
//		 String netName = currNetwork.getDefaultNetworkTable().getRow(currNetwork.getSUID()).get(CyNetwork.NAME,String.class);
		 String searchCondition = buildSearchField();
		 params.setActiveTab("VariantDisTabPane");
		 params.setVarSearchText(searchCondition);
		 params.setSource(source);
		 params.setDiseaseClass(diseaseClass);
		 params.setDiseaseClass(diseaseClasses.getSelectedValue());
		 params.setAssociationType(assocTypes.getSelectedValue());
		 params.setLowScore(minScoreRange.toString());
		 params.setHighScore(maxScoreRange.toString());
		 params.setNetworkName(buildNetworkName());
		 NetworkBuilder networkBuilder = new NetworkBuilder(params);
//		 CyNetwork cyNetwork = CyActivator.getInstance().getNetworkFactoryService().createNetwork();
//		 CyNetworkView cyNetworkView = CyActivator.getInstance().getNetworkViewFactoryService().createNetworkView(cyNetwork);
//		 cyNetwork.getRow(cyNetwork).set(CyNetwork.NAME, netName+"_vda"+"_"+source);
//		 CyActivator.getInstance().getNetworkManagerService().addNetwork(cyNetwork);
//		 CyActivator.getInstance().getNetworkViewManagerService().addNetworkView(cyNetworkView);
//		 networkBuilder.createNodeAttributeColumns(cyNetwork);
//		 networkBuilder.createCyEdgeAttributeColumns(cyNetwork);
		 networkBuilder.buildVariantDiseaseNet(params);
//		 CyNetwork cyNetwork = CyActivator.getInstance().getApplicationManagerService().getCurrentNetwork();
//		 if (cyNetwork.getNodeList().isEmpty()) {
//			 throw new DisGeNetException("No matches for the selected source. \n The new network will be shown empty.");
//		 }
	 }
	 
	 private String buildNetworkName() {
		 String  network_name = "DisGeNET-VDA";
		 network_name += " - " + currNetwork.getDefaultNetworkTable().getColumn("name").getValues(String.class).get(0);
		 network_name += " - " + params.getSource();
		 network_name += " - " + params.getDiseaseClass();
		 return network_name;
	 }
}
