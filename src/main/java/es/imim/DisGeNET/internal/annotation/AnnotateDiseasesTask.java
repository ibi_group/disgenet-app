package es.imim.DisGeNET.internal.annotation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyTable;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;
import org.cytoscape.work.Tunable;
import org.cytoscape.work.util.ListSingleSelection;

import es.imim.DisGeNET.database.DatabaseProps;
import es.imim.DisGeNET.exceptions.DisGeNetException;
import es.imim.DisGeNET.gui.GuiParameters;
import es.imim.DisGeNET.gui.GuiProps;
import es.imim.DisGeNET.network.NetworkBuilder;
import es.imim.DisGeNET.tool.HelperFunctions;

public class AnnotateDiseasesTask extends AbstractTask{
	
	@Tunable(description="Select the column you want to search by: ", gravity=1,groups={"Columns"}, params="displayState=uncollapsed")
	public ListSingleSelection<Callable<CyColumn>> columnNames;
	
	@Tunable (description="Select source:", gravity=2,groups={"Sources"},params="displayState=uncollapsed")
	public ListSingleSelection<String> sources;
	
	@Tunable (description="Select Disease class:", gravity=3,params="displayState=collapsed",groups={"Filters"},context=Tunable.GUI_CONTEXT)
	public ListSingleSelection<String> diseaseClasses;
	
	@Tunable (description="Select Association type:", gravity=4,params="displayState=collapsed",groups={"Filters"},context=Tunable.GUI_CONTEXT)
	public ListSingleSelection<String> assocTypes;
	
	@Tunable (description="Select Evidence Level:", gravity=5, params="displayState=collapsed",groups={"Filters"},context=Tunable.GUI_CONTEXT)
	public ListSingleSelection<String> evidenceLevels;
	
	@Tunable (description="Select score range min value", gravity=6,params="displayState=collaspsed",groups={"Filters"},context=Tunable.GUI_CONTEXT,
			format="0.00")
	public Double minScoreRange;
	
	@Tunable (description="Select score range max value", gravity=7,params="displayState=collaspsed",groups={"Filters"},context=Tunable.GUI_CONTEXT,
			format="0.00")
	public Double maxScoreRange;
	
	private CyColumn selectedColumn;
	
    private CyNetwork currNetwork;
    private GuiParameters params;
    private int type;

    public AnnotateDiseasesTask(CyNetworkView cyNetworkView, int type) {
    	this.type = type;
		this.currNetwork = cyNetworkView.getModel();
		this.params = new GuiParameters<>();
		this.sources = getSourcesList();
		this.diseaseClasses = new ListSingleSelection<String>(Arrays.asList(GuiProps.DISEASE_CLASS_OPTIONS));
		this.assocTypes = new ListSingleSelection<String>(Arrays.asList(GuiProps.ASSOCIATION_TYPE_OPTIONS));
		if (type==0) {
			this.evidenceLevels = new ListSingleSelection<String>(Arrays.asList(GuiProps.EL_OPTIONS));
		}
		this.minScoreRange = 0.;
		this.maxScoreRange = 1.;
		this.columnNames = listNodeColumnsNames();
	}
	
    private ListSingleSelection<String> getSourcesList() {
    	switch(this.type) {
    		case 0:
				return new ListSingleSelection<String>(Arrays.asList(GuiProps.SOURCE_OPTIONS));
    		case 1:
    			return new ListSingleSelection<String>(Arrays.asList(GuiProps.VARIANT_SOURCE_OPTIONS));
    	}
    	return null;
	}

	private ListSingleSelection<Callable<CyColumn>> listNodeColumnsNames() {
		CyTable nodeTable = currNetwork.getDefaultNodeTable();
		Collection<CyColumn> nodeColumns = nodeTable.getColumns();
		
		List<Callable<CyColumn>> columnNames = new ArrayList<>();
		
		for(final CyColumn column : nodeColumns) {
			if(!column.isPrimaryKey()) {
				columnNames.add(new Callable<CyColumn>() {
					public CyColumn call() { 
						return column; 
					}
					public String toString() { 
						return column.getName(); 
					}
				});
			}
		}
		return new ListSingleSelection<Callable<CyColumn>>(columnNames);
    }

	@Override
	public void run(TaskMonitor taskMonitor) throws Exception {
		
		selectedColumn = columnNames.getSelectedValue().call();
		String source = sources.getSelectedValue();
		String searchCondition = buildSearchField();
		params.setDisSearchText(searchCondition);
		params.setSource(source);
		params.setDiseaseClass(diseaseClasses.getSelectedValue());
		params.setAssociationType(assocTypes.getSelectedValue());
		params.setLowScore(minScoreRange.toString());
		params.setHighScore(maxScoreRange.toString());
		params.setNetworkName(buildNetworkName());
		NetworkBuilder networkBuilder = new NetworkBuilder(params);
		if(this.type==1) {
			taskMonitor.setTitle("Annotate variants to diseases...");
			params.setActiveTab("VariantDisTabPane");
			networkBuilder.buildVariantDiseaseNet(params);
		}else {
			taskMonitor.setTitle("Annotate genes to diseases...");
			params.setEl(evidenceLevels.getSelectedValue());
			params.setActiveTab("GeneDisTabPane");
			networkBuilder.buildGeneDiseaseNet(params);
		}
	}
	
	public String buildSearchField() throws DisGeNetException {
		String searchCondition = "";
		List<String> diseasesList = new ArrayList<>();
		if (selectedColumn.getType().equals(String.class)) {
			List<String> cuisList = selectedColumn.getValues(String.class);
			for (String cui : cuisList) {
				if(cui.matches(DatabaseProps.DISEASE_CODE_REGEX)) {
					diseasesList.add("'"+cui+"'");
				}
			}
			//cuisList.removeIf(s->!s.matches(DatabaseProps.DISEASE_CODE_REGEX));
		}else {
			throw new DisGeNetException("Invalid column data type... Must be a String");
		}
		if(diseasesList.isEmpty()) {
			throw new DisGeNetException("Error: The selected column is not a disease identifier for DisGeNET (CUI)");
		}
		searchCondition = HelperFunctions.implodeString(",", diseasesList);
		return searchCondition;
			
	}
	
	private String buildNetworkName() {
		 String  network_name = "DisGeNET-";
		 if (this.type==1) {
			 network_name = network_name.concat("VDA");
		 }else {
			 network_name = network_name.concat("GDA");
		 }
		 network_name += " - " + currNetwork.getDefaultNetworkTable().getColumn("name").getValues(String.class).get(0);
		 network_name += " - " + params.getSource();
		 network_name += " - " + params.getDiseaseClass();
		 return network_name;
	 }

}
