package es.imim.DisGeNET.internal.automation.parameters;

import io.swagger.annotations.ApiModelProperty;

public class VariantEnrichmentParams {

    @ApiModelProperty(
            value = "The SUID of the network",
            required = true
    )
    private Long networkId;

    @ApiModelProperty(
            value = "The column with the list of identifiers for the enrichment",
            required = true
    )
    private String columnName;

    @ApiModelProperty(
            value = "The name of the source to filter the annotations",
            allowableValues="CURATED, ALL, BEFREE, CLINVAR, GWASCAT, GWASDB, UNIPROT",
            required = true,
            example = "CURATED"
    )
    private String source;

    @ApiModelProperty(
            value = "The type of the column identifer",
            allowableValues="DBSNP",
            hidden = true,
            required = false,
            example = "DBSNP"
    )
    private String typeId;

//    @ApiModelProperty(
//            value = "The universe of the enrichment.",
//            allowableValues = "HUMAN, DISGENET",
//            required = true,
//            example = "DISGENET"
//    )
//    private String universe;

    @ApiModelProperty(
            value = "Create a new Variant-Disease DisGeNET network with the results of the enrichment",
            required = false,
            example = "true"
    )
    private boolean newNetwork;

//    @ApiModelProperty(
//            value = "Filter the results by a p-value threshold",
//            required = false,
//            example = "0.05"
//    )
    private double pvalueThreshold = 1;

//    @ApiModelProperty(
//            value = "Filter the results by a minimum number of variants of the list annotated to for the disease",
//            required = false,
//            example = "0"
//    )
    private Integer minNumberOfVariants = 0;

    public Long getNetworkId() {
        return networkId;
    }

    public void setNetworkId(Long networkId) {
        this.networkId = networkId;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

//    public String getUniverse() {
//        return universe;
//    }
//
//    public void setUniverse(String universe) {
//        this.universe = universe;
//    }

    public boolean isNewNetwork() {
        return newNetwork;
    }

    public void setNewNetwork(boolean newNetwork) {
        this.newNetwork = newNetwork;
    }

    public double getPvalueThreshold() {
        return pvalueThreshold;
    }

    public void setPvalueThreshold(double pvalueThreshold) {
        this.pvalueThreshold = pvalueThreshold;
    }

    public Integer getMinNumberOfVariants() {
        return minNumberOfVariants;
    }

    public void setMinNumberOfVariants(Integer minNumberOfVariants) {
        this.minNumberOfVariants = minNumberOfVariants;
    }
}
