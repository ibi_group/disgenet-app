package es.imim.DisGeNET.internal.automation.parameters;

import io.swagger.annotations.ApiModelProperty;

public class GeneEnrichmentParams {

    @ApiModelProperty(
            value = "The SUID of the network",
            required = true
    )
    private Long networkId;

    @ApiModelProperty(
            value = "The column with the list of identifiers for the enrichment",
            required = true
    )
    private String columnName;

    @ApiModelProperty(
            value = "The name of the source to filter the annotations",
            allowableValues= "CURATED, INFERRED, ANIMAL_MODELS, ALL, BEFREE, CGI, CLINGEN, CLINVAR, CTD_human, CTD_mouse, CTD_rat, GENOMICS_ENGLAND, GWASCAT, GWASDB, HPO, LHGDN, MGD, ORPHANET, PSYGENET, RGD, UNIPROT",
            required = true,
            example = "CURATED"
    )
    private String source;

    @ApiModelProperty(
            value = "The type of the column identifer",
            allowableValues = "ENTREZID, SYMBOL",
            required = true,
            example = "ENTREZID"
    )
    private String typeId;

    @ApiModelProperty(
            value = "Create a new Gene-Disease DisGeNET network with the results of the enrichment",
            required = false,
            example = "true"
    )
    private boolean newNetwork;

//    @ApiModelProperty(
//            value = "Filter the results by a p-value threshold",
//            required = false,
//            example = "0.05"
//    )
    private double pvalueThreshold = 1;

//    @ApiModelProperty(
//            value = "Filter the results by a minimum number of genes of the list annotated to for the disease",
//            required = false,
//            example = "0"
//    )
    private Integer minNumberOfGenes = 0;

//    @ApiModelProperty(
//            value = "The universe of the enrichment.",
//            allowableValues = "HUMAN, HUMAN_CODING, DISGENET",
//            required = true,
//            example = "DISGENET"
//    )
//    private String universe;

    public Long getNetworkId() {
        return networkId;
    }

    public void setNetworkId(Long networkId) {
        this.networkId = networkId;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    //    public String getUniverse() {
//        return universe;
//    }
//
//    public void setUniverse(String universe) {
//        this.universe = universe;
//    }

    public boolean isNewNetwork() {
        return newNetwork;
    }

    public void setNewNetwork(boolean newNetwork) {
        this.newNetwork = newNetwork;
    }

    public double getPValueThreshold() {
        return pvalueThreshold;
    }

    public void setPValueThreshold(double maxpValue) {
        this.pvalueThreshold = maxpValue;
    }

    public Integer getMinNumberOfGenes() {
        return minNumberOfGenes;
    }

    public void setMinNumberOfGenes(Integer minNumberOfGenes) {
        this.minNumberOfGenes = minNumberOfGenes;
    }
}
