package es.imim.DisGeNET.internal.automation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import es.imim.DisGeNET.internal.automation.parameters.*;
import es.imim.DisGeNET.internal.enrichment.AutomationGeneEnrichmentTask;
import es.imim.DisGeNET.internal.enrichment.AutomationVariantEnrichmentTask;
import es.imim.DisGeNET.internal.enrichment.EnrichmentTaskObserver;
import es.imim.DisGeNET.internal.enrichment.ShowEnrichmentPanelTask;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.work.Task;
import org.cytoscape.work.TaskIterator;

import es.imim.DisGeNET.gui.GuiParameters;
import es.imim.DisGeNET.gui.GuiProps;
import es.imim.DisGeNET.internal.CyActivator;
import es.imim.DisGeNET.network.NetTaskObserver;
import es.imim.DisGeNET.tool.OperationResult;
import es.imim.DisGeNET.validation.ParametersValidation;


public class NetworkAutomationImpl implements NetworkAutomation {
	
	private GuiParameters guiParams;
	
	public OperationResult createNetwork() {
		String message;
		Map netResult = null;
		NetTaskAutomation netTask = new NetTaskAutomation(guiParams);
		NetTaskObserver observer = new NetTaskObserver();
		message = "Operation completed succesfully";
		CyActivator.getInstance().getSynchTaskManagerService().execute(new TaskIterator(netTask),observer);
		netResult = observer.getResult();
		return new OperationResult (message, netResult, null);
	}

	public OperationResult createGeneEnrichment(GeneEnrichmentParams params){
	    String message;
	    Map enrichResult = null;
        CyNetwork cyNetwork = CyActivator.getInstance().getNetworkManagerService().getNetwork(params.getNetworkId());
        CyNetworkView cyNetworkView = CyActivator.getInstance().getNetworkViewManagerService().getNetworkViews(cyNetwork).iterator().next();
        AutomationGeneEnrichmentTask geneEnrichmentTask = new AutomationGeneEnrichmentTask(
                cyNetworkView, params.getNetworkId(), params.getColumnName(), params.getSource(), params.getTypeId(), "DISGENET", params.isNewNetwork(), params.getPValueThreshold(), params.getMinNumberOfGenes());
        EnrichmentTaskObserver observer = new EnrichmentTaskObserver();
        message = "Operation completed succesfully";
        Task showTask = new ShowEnrichmentPanelTask(CyActivator.getInstance().getShowEnrichmentFactory(), true, false);
        CyActivator.getInstance().getSynchTaskManagerService().execute(new TaskIterator(geneEnrichmentTask, showTask), observer);
        enrichResult = observer.getResult();
        return new OperationResult(message, enrichResult, null);
    }

    public OperationResult createVariantEnrichment(VariantEnrichmentParams params){
        String message;
        Map enrichResult = null;
        CyNetwork cyNetwork = CyActivator.getInstance().getNetworkManagerService().getNetwork(params.getNetworkId());
        CyNetworkView cyNetworkView = CyActivator.getInstance().getNetworkViewManagerService().getNetworkViews(cyNetwork).iterator().next();
        AutomationVariantEnrichmentTask variantEnrichmentTask = new AutomationVariantEnrichmentTask(
                cyNetworkView, params.getNetworkId(), params.getColumnName(), params.getSource(), "DBSNP", "DISGENET", params.isNewNetwork(), params.getPvalueThreshold(), params.getMinNumberOfVariants());
        EnrichmentTaskObserver observer = new EnrichmentTaskObserver();
        message = "Operation completed succesfully";
        Task showTask = new ShowEnrichmentPanelTask(CyActivator.getInstance().getShowEnrichmentFactory(), true, false);
        CyActivator.getInstance().getSynchTaskManagerService().execute(new TaskIterator(variantEnrichmentTask, showTask), observer);
        enrichResult = observer.getResult();
        return new OperationResult(message, enrichResult, null);
    }


	@Override
	public OperationResult getAppInfo() {
		Map <String,String> info = new HashMap();
		String message = "DisGeNET App";
		info.put("dataVersion", "7");
		info.put("appVersion","7.3.0");
		info.put("apiVersion","1.1");
		info.put("appName", "DisGeNET");
		info.put("description","DisGeNET-Automation service for Cytoscape.");
		return new OperationResult(message,info,null);
	}
	
//	@Override
//	public OperationResult createGeneProjectionNetwork(GeneProjParams params) {
//		Map errors = ParametersValidation.geneProjParameters(params);
//		OperationResult result;
//		if(errors.isEmpty()) {
//			guiParams = new GuiParameters<>();
//			guiParams.setActiveTab("geneProj_TabPane");
//			guiParams.setSource(params.getSource().trim());
//			guiParams.setDiseaseClass(assingDisClass(params.getDiseaseClass()));
//			guiParams.setGenSearchText(assignSearchText(params.getGeneSearch()));
//			result = createNetwork();
//		}else {
//			String message = "Operation failed, check the parameter errors.";
//			result = new OperationResult(message,null,errors);
//		}
//		return result;
//	}
//
//	@Override
//	public OperationResult createDiseaseProjectionNetwork(DiseaseProjParams params) {
//		Map errors = ParametersValidation.diseaseProjParameters(params);
//		OperationResult result;
//		if(errors.isEmpty()) {
//			guiParams = new GuiParameters<>();
//			guiParams.setActiveTab("disProj_TabPane");
//			guiParams.setSource(params.getSource().trim());
//			guiParams.setDiseaseClass(assingDisClass(params.getDiseaseClass()));
//			guiParams.setDisSearchText(assignSearchText(params.getDiseaseSearch()));
//			result = createNetwork();
//		}else {
//			String message = "Operation failed, check the parameter errors.";
//			result = new OperationResult(message,null,errors);
//		}
//		return result;
//	}
	
	@Override
	public OperationResult createVariantDiseaseNetwork(VariantDiseaseParams params) {
		Map errors = ParametersValidation.variantDiseaseParameters(params);
		OperationResult result;
		if(errors.isEmpty()) {
			guiParams = new GuiParameters<>();
			guiParams.setActiveTab("VariantDisTabPane");
			guiParams.setSource(params.getSource().trim());
			guiParams.setAssociationType(assignAssocTypeVariants(params.getAssocType()));
			guiParams.setDiseaseClass(assingDisClass(params.getDiseaseClass()));
			guiParams.setVarSearchText(assignSearchText(params.getVariantSearch()));
			guiParams.setDisSearchText(assignSearchText(params.getDiseaseSearch()));
			guiParams.setGenSearchText(assignSearchText(params.getGeneSearch()));
			guiParams.setLowScore(assignMinScore(params.getInitialScoreValue()));
			guiParams.setHighScore(assignMaxScore(params.getFinalScoreValue()));
			guiParams.setVarGeneDisNetwork(params.isShowGenes());
			result = createNetwork();
		}else {
			String message = "Operation failed, check the parameter errors.";
			result = new OperationResult(message,null,errors);
		}
		return result;
	}
	
	@Override
	public OperationResult createGeneDiseaseNetwork(GeneDiseaseParams params) {
		Map errors = ParametersValidation.geneDiseaseParameters(params);
		OperationResult result;
		if(errors.isEmpty()) {
			guiParams = new GuiParameters<>();
			guiParams.setActiveTab("GeneDisTabPane");
			guiParams.setSource(params.getSource().trim());
			guiParams.setAssociationType(assignAssocType(params.getAssocType()));
			guiParams.setDiseaseClass(assingDisClass(params.getDiseaseClass()));
			guiParams.setGenSearchText(assignSearchText(params.getGeneSearch()));
			guiParams.setDisSearchText(assignSearchText(params.getDiseaseSearch()));
			guiParams.setLowScore(assignMinScore(params.getInitialScoreValue()));
			guiParams.setHighScore(assignMaxScore(params.getFinalScoreValue()));
			result = createNetwork();
		}else {
			String message = "Operation failed, check the parameter errors.";
			result = new OperationResult(message,null,errors);
		}
		return result;
	}

    @Override
    public OperationResult performGeneDiseaseEnrichment(GeneEnrichmentParams params) {
        Map errors = ParametersValidation.geneEnrichmentParameters(params);
        OperationResult result;
        if(errors.isEmpty()){
            result = createGeneEnrichment(params);
        }else{
            String message = "Operation failed, check the parameter errors.";
            result = new OperationResult(message,null,errors);
        }
        return result;
    }

    @Override
    public OperationResult performVariantDiseaseEnrichment(VariantEnrichmentParams params) {
        Map errors = ParametersValidation.variantEnrichmentParameters(params);
        OperationResult result;
        if(errors.isEmpty()){
            result = createVariantEnrichment(params);
        }else{
            String message = "Operation failed, check the parameter errors.";
            result = new OperationResult(message,null,errors);
        }
        return result;
    }

    private String assignAssocType (String assocType) {
		ArrayList<String> assocTypes = new ArrayList<>(Arrays.asList(GuiProps.ASSOCIATION_TYPE_OPTIONS));
		if(!assocTypes.contains(assocType)) {
			assocType = "Any";
		}
		return assocType.trim();
	}
	
	private String assignAssocTypeVariants (String assocType) {
		ArrayList<String> assocTypes = new ArrayList<>(Arrays.asList(GuiProps.VARIANT_ASSOCIATION_TYPE_OPTIONS));
		if(!assocTypes.contains(assocType)) {
			assocType = "Any";
		}
		return assocType.trim();
	}
	
	private String assingDisClass (String disClass) {
		ArrayList<String> diseaseClasses = new ArrayList<>(Arrays.asList(GuiProps.DISEASE_CLASS_OPTIONS));
		if(!diseaseClasses.contains(disClass)) {
			disClass = "Any";
		}
		return disClass.trim();
	}
	
	private String assignSearchText (String textField) {
		if(textField==null) {
			textField="";
		}
		return textField.trim();
	}
	
	private String assignMinScore (String minScore) {
		if(minScore==null) {
			minScore="0.0";
		}
		return minScore.trim();
	}
	
	private String assignMaxScore (String maxScore) {
		if(maxScore==null) {
			maxScore="1.0";
		}
		return maxScore.trim();
	}
	
}
