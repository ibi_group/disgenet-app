package es.imim.DisGeNET.internal.automation;

import java.util.HashMap;
import java.util.Map;

import org.cytoscape.model.CyNetwork;
import org.cytoscape.work.ObservableTask;
import org.cytoscape.work.TaskMonitor;

import es.imim.DisGeNET.gui.GuiParameters;
import es.imim.DisGeNET.internal.CyActivator;
import es.imim.DisGeNET.network.NetTask;

public class NetTaskAutomation extends NetTask implements ObservableTask {

	private Map <String,String> result;
	
	public NetTaskAutomation(GuiParameters curParams) {
		super(curParams);
	}
	
	@Override
	public void run(TaskMonitor taskMonitor) throws Exception {
		super.run(taskMonitor);
		CyNetwork network = CyActivator.getInstance().getApplicationManagerService().getCurrentNetwork();
		String netSUID = Long.toString(network.getSUID());
		String netName = network.getRow(network).get("name", String.class);
		String nodeCount = Integer.toString(network.getNodeCount());
		String edgeCount = Integer.toString(network.getEdgeCount());
		result = new HashMap<>();
		result.put("networkSUID", netSUID);
		result.put("networkName",netName);
		result.put("numberOfNodes", nodeCount);
		result.put("numberOfEdges", edgeCount);
	}
	
	@Override
	public <R> R getResults(Class<? extends R> type) {
		if(Map.class.equals(type)) {
			return (R) result;
		}
		return null;
	}

}
