package es.imim.DisGeNET.internal.automation.parameters;

import io.swagger.annotations.ApiModelProperty;

public class DiseaseProjParams implements AutomationParams {

	@ApiModelProperty (
			value="The source of the data", 
			example="UNIPROT", 
			allowableValues="CURATED, INFERRED, ANIMAL_MODELS, ALL, BEFREE, CGI, CLINGEN, CLINVAR, CTD_human, CTD_mouse, CTD_rat, GENOMICS_ENGLAND, GWASCAT, GWASDB, HPO, LHGDN, MGD, ORPHANET, PSYGENET, RGD, UNIPROT",
			required=true
			)
	private String source;
	
	@ApiModelProperty (value="The disease class", example="Neoplasms")
	private String diseaseClass;
	
	@ApiModelProperty (value="The disease search text, see DisGeNET tutorial for the possible values", example="*", required=true)
	private String diseaseSearch;

	public DiseaseProjParams() {
		
	}
	
	public DiseaseProjParams(String source, String diseaseClass, String diseaseSearch) {
		this.source = source;
		this.diseaseClass = diseaseClass;
		this.diseaseSearch = diseaseSearch;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDiseaseClass() {
		return diseaseClass;
	}

	public void setDiseaseClass(String diseaseClass) {
		this.diseaseClass = diseaseClass;
	}

	public String getDiseaseSearch() {
		return diseaseSearch;
	}

	public void setDiseaseSearch(String diseaseSearch) {
		this.diseaseSearch = diseaseSearch;
	}
	
}
