package es.imim.DisGeNET.internal.automation.parameters;

import io.swagger.annotations.ApiModelProperty;

public class VariantDiseaseParams implements AutomationParams {
	
	@ApiModelProperty (value="The source of the data", 
			example="UNIPROT",
            allowableValues="CURATED, ALL, BEFREE, CLINVAR, GWASCAT, GWASDB, UNIPROT",
			required=true
			)
	private String source;
	
	@ApiModelProperty (value="The association type", example="Genetic Variation")
	private String assocType;
	
	@ApiModelProperty (value="The disease class", example="Neoplasms")
	private String diseaseClass;
	
	@ApiModelProperty (value="The disease search text, see DisGeNET tutorial for the possible values", example=" ")
	private String diseaseSearch;
	
	@ApiModelProperty (value="The gene search text, see DisGeNET tutorial for the possible values", example=" ")
	private String geneSearch;
	
	@ApiModelProperty (value="The variant search text, see DisGeNET tutorial for the possible values", example=" ")
	private String variantSearch;
	
	@ApiModelProperty (value="The minimum value of the score range.", example="0.0")	
	private String initialScoreValue;
	
	@ApiModelProperty (value="The max value of the score range..", example="1.0")
	private String finalScoreValue;
	
	@ApiModelProperty (value="Show the genes associated to the variants.", example="false", allowableValues="true,false")
	private boolean showGenes;
	
	public VariantDiseaseParams() {
		
	}
	
	public VariantDiseaseParams(String source, String assocType, String diseaseClass, String diseaseSearch,
			String geneSearch, String variantSearch, String initialScoreValue, String finalScoreValue, boolean showGenes) {
		this.source = source;
		this.assocType = assocType;
		this.diseaseClass = diseaseClass;
		this.diseaseSearch = diseaseSearch;
		this.geneSearch = geneSearch;
		this.variantSearch = variantSearch;
		this.initialScoreValue = initialScoreValue;
		this.finalScoreValue = finalScoreValue;
		this.showGenes = showGenes;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getAssocType() {
		return assocType;
	}

	public void setAssocType(String assocType) {
		this.assocType = assocType;
	}

	public String getDiseaseClass() {
		return diseaseClass;
	}

	public void setDiseaseClass(String diseaseClass) {
		this.diseaseClass = diseaseClass;
	}

	public String getDiseaseSearch() {
		return diseaseSearch;
	}

	public void setDiseaseSearch(String diseaseSearch) {
		this.diseaseSearch = diseaseSearch;
	}

	public String getGeneSearch() {
		return geneSearch;
	}

	public void setGeneSearch(String geneSearch) {
		this.geneSearch = geneSearch;
	}

	public String getVariantSearch() {
		return variantSearch;
	}

	public void setVariantSearch(String variantSearch) {
		this.variantSearch = variantSearch;
	}

	public String getInitialScoreValue() {
		return initialScoreValue;
	}

	public void setInitialScoreValue(String lowScore) {
		this.initialScoreValue = lowScore;
	}

	public String getFinalScoreValue() {
		return finalScoreValue;
	}

	public void setFinalScoreValue(String highScore) {
		this.finalScoreValue = highScore;
	}

	public boolean isShowGenes() {
		return showGenes;
	}

	public void setShowGenes(boolean showGenes) {
		this.showGenes = showGenes;
	}
	
}
