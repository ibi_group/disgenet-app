package es.imim.DisGeNET.internal.automation.parameters;

import io.swagger.annotations.ApiModelProperty;

public class GeneDiseaseParams implements AutomationParams {
	
	@ApiModelProperty (
			value="The source of the data", 
			example="UNIPROT", 
			allowableValues="CURATED, INFERRED, ANIMAL_MODELS, ALL, BEFREE, CGI, CLINGEN, CLINVAR, CTD_human, CTD_mouse, CTD_rat, GENOMICS_ENGLAND, GWASCAT, GWASDB, HPO, LHGDN, MGD, ORPHANET, PSYGENET, RGD, UNIPROT",
			required=true
			)
	private String source;
	
	@ApiModelProperty (value="The association type", example="Biomarker")
	private String assocType;
	
	@ApiModelProperty (value="The disease class", example="Neoplasms")
	private String diseaseClass;
	
	@ApiModelProperty (value="The disease search text, see DisGeNET tutorial for the possible values", example=" ")
	private String diseaseSearch;
	
	@ApiModelProperty (value="The gene search text, see DisGeNET tutorial for the possible values", example=" ")
	private String geneSearch;
	
	@ApiModelProperty (value="The minimum value of the score range.", example="0.0")	
	private String initialScoreValue;
	
	@ApiModelProperty (value="The max value of the score range.", example="1.0")
	private String finalScoreValue;

	public GeneDiseaseParams() {

	}

	public GeneDiseaseParams(String source, String assocType, String diseaseClass, String diseaseSearch, String geneSearch, String initialScoreValue, String finalScoreValue) {
		this.source = source;
		this.assocType = assocType;
		this.diseaseClass = diseaseClass;
		this.diseaseSearch = diseaseSearch;
		this.geneSearch = geneSearch;
		this.initialScoreValue = initialScoreValue;
		this.finalScoreValue = finalScoreValue;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDiseaseClass() {
		return diseaseClass;
	}

	public void setDiseaseClass(String diseaseClass) {
		this.diseaseClass = diseaseClass;
	}

	public String getAssocType() {
		return assocType;
	}

	public void setAssocType(String assocType) {
		this.assocType = assocType;
	}

	public String getDiseaseSearch() {
		return diseaseSearch;
	}

	public void setDiseaseSearch(String diseaseSearch) {
		this.diseaseSearch = diseaseSearch;
	}

	public String getGeneSearch() {
		return geneSearch;
	}

	public void setGeneSearch(String geneSearch) {
		this.geneSearch = geneSearch;
	}

	public String getInitialScoreValue() {
		return initialScoreValue;
	}

	public void setLowScore(String initialScoreValue) {
		this.initialScoreValue = initialScoreValue;
	}

	public String getFinalScoreValue() {
		return finalScoreValue;
	}

	public void setFinalScoreValue(String finalScoreValue) {
		this.finalScoreValue = finalScoreValue;
	}
	
}
