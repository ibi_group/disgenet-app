package es.imim.DisGeNET.internal.automation;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import es.imim.DisGeNET.internal.automation.parameters.*;
import es.imim.DisGeNET.tool.OperationResult;
import io.swagger.annotations.*;

@Api (tags="Apps: DisGeNET")
@Path("/disgenet/v7")
public interface NetworkAutomation {
	
	@ApiOperation(
			value = "Implementation notes.",
			notes = "Basic information about the api.",
			response = OperationResult.class)
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("version")
	public OperationResult getAppInfo ();
	
	@ApiOperation(
		    value = "Generates a new gene-disease network.",
		    notes = "Creates a new gene-disease network with the given parameters.", 
		    response= OperationResult.class)
	@POST
    @Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("gene-disease-net")
	public OperationResult createGeneDiseaseNetwork(
			@ApiParam(value = "The network parameters", required = true)
			GeneDiseaseParams params
	);
	
	@ApiOperation(
		    value = "Generates a new variant-disease network.",
		    notes = "Creates a new variant-disease network with the given parameters.", 
		    response= OperationResult.class)
	@POST
    @Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("variant-disease-net")
	public OperationResult createVariantDiseaseNetwork(
			@ApiParam(value = "The network parameters", required = true)
			VariantDiseaseParams params
	);
	
//	@ApiOperation(
//		    value = "Generates a new gene-gene network.",
//		    notes = "Creates a new gene-gene network with the given parameters.",
//		    response= OperationResult.class)
//	@POST
//	@Produces(MediaType.APPLICATION_JSON)
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Path("gene-projection-net")
//	public OperationResult createGeneProjectionNetwork(
//			@ApiParam(value = "The network parameters", required = true)
//			GeneProjParams params
//	);
//
//	@ApiOperation(
//		    value = "Generates a new disease-disease network.",
//		    notes = "Creates a new disease-disease network with the given parameters.",
//		    response= OperationResult.class)
//	@POST
//	@Produces(MediaType.APPLICATION_JSON)
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Path("disease-projection-net")
//	public OperationResult createDiseaseProjectionNetwork(
//			@ApiParam(value = "The network parameters", required = true)
//			DiseaseProjParams params
//	);

    @ApiOperation(
            value = "Performs a disease enrinchment on an existing network.",
            notes = "Performs a disease enrinchment on an existing network, using data from DisGeNET.",
            response= OperationResult.class)
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("gene-enrichment")
	public OperationResult performGeneDiseaseEnrichment(
	        @ApiParam(value = "The enrichment parameters", required = true)
            GeneEnrichmentParams params
    );

    @ApiOperation(
            value = "Performs a disease enrinchment on an existing network.",
            notes = "Performs a disease enrinchment on an existing network, using data from DisGeNET.",
            response= OperationResult.class)
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("variant-enrichment")
    public OperationResult performVariantDiseaseEnrichment(
            @ApiParam(value = "The enrichment parameters", required = true)
            VariantEnrichmentParams params
    );
}
