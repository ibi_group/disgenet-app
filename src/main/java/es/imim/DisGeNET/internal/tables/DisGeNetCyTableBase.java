package es.imim.DisGeNET.internal.tables;

import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyTable;


import es.imim.DisGeNET.exceptions.DisGeNetException;
import es.imim.DisGeNET.gui.GuiParameters;

import java.util.Iterator;
import java.util.Map;


/**
 * Class that loads all the data for the table networks.
 * @author jsauch
 */

public class DisGeNetCyTableBase implements DisGeNETCyTableNodeInterface{
   
    private GuiParameters params;
    
    public DisGeNetCyTableBase() {
    	
    }
    
    public DisGeNetCyTableBase(GuiParameters params) {
    	this.params = params;
    }
    
    @Override
    public void addNodeColumns(CyTable cyTable) {
    	DisGeNetCyTableDisease disColumns;
    	DisGeNetCyTableGene genColumns;
    	DisGeNetCyTableVariant varColumns;
    	String option = params.getActiveTab();
    	if(cyTable.getColumn("nodeType")==null) {
    		cyTable.createColumn("nodeType", String.class, false);
    	}
    	switch(option) {
    	case "GeneDisTabPane":
    		disColumns = new DisGeNetCyTableDisease(params);
    		genColumns = new DisGeNetCyTableGene(params);
    		disColumns.addNodeColumns(cyTable);
    		genColumns.addNodeColumns(cyTable);
    		if(cyTable.getColumn("nrAssociatedDiseases")==null) {
    			cyTable.createColumn("nrAssociatedDiseases", Integer.class, false);
    		}
    		if(cyTable.getColumn("nrAssociatedGenes")==null) {
    			cyTable.createColumn("nrAssociatedGenes", Integer.class, false);
    		}
    		break;
    	case "VariantDisTabPane":
    		disColumns = new DisGeNetCyTableDisease(params);
    		varColumns = new DisGeNetCyTableVariant(params);
    		disColumns.addNodeColumns(cyTable);
    		varColumns.addNodeColumns(cyTable);
    		if(cyTable.getColumn("nrAssociatedDiseases")==null) {
    			cyTable.createColumn("nrAssociatedDiseases", Integer.class, false);
    		}
    		if(cyTable.getColumn("nrAssociatedVariants")==null) {
    			cyTable.createColumn("nrAssociatedVariants", Integer.class, false);
    		}
    		break;
    	case "disProj_TabPane":
    		disColumns = new DisGeNetCyTableDisease(params);
    		disColumns.addNodeColumns(cyTable);
    		if(cyTable.getColumn("nrAssociatedDiseases")==null) {
    			cyTable.createColumn("nrAssociatedDiseases", Integer.class, false);
    		}
    		break;
    	case "geneProj_TabPane":
    		genColumns = new DisGeNetCyTableGene(params);
    		genColumns.addNodeColumns(cyTable);
    		if(cyTable.getColumn("nrAssociatedGenes")==null) {
    			cyTable.createColumn("nrAssociatedGenes", Integer.class, false);
    		}
    		break;
    	default:
    		System.out.println("default case");
    		break;
    	}
    	if(cyTable.getColumn("styleName")==null) {
    		cyTable.createColumn("styleName", String.class, false);
    	}
    	if(cyTable.getColumn("styleSize")==null) {
    		cyTable.createColumn("styleSize", Integer.class, false);
    	}
    }
    
    @Override
    public void addEdgeColumns(CyTable cyTable) {
		String option = params.getActiveTab();
		if(cyTable.getColumn("source")==null) {
			cyTable.createColumn("source", String.class, false);
		}
		switch(option) {
		case "GeneDisTabPane":
			createGdaEdgeColumns(cyTable);
			break;
		case "VariantDisTabPane":
			createVdaEdgeColumns(cyTable);
			break;
		case "disProj_TabPane":
			DisGeNetCyTableDisease disColumns = new DisGeNetCyTableDisease(params);
			disColumns.addEdgeColumns(cyTable);
			break;
		case "geneProj_TabPane":
			DisGeNetCyTableGene genColumns = new DisGeNetCyTableGene(params);
			genColumns.addEdgeColumns(cyTable);
			break;
		default:
			System.out.println("default case");
			break;
		}
		
	}
    
    @Override
    public void fillNodeColumns(CyTable cyTable, Map <String,Long> attributes) throws DisGeNetException {
    	DisGeNetCyTableDisease disColumns;
    	DisGeNetCyTableGene genColumns;
    	DisGeNetCyTableVariant varColumns;
    	String option = params.getActiveTab();
    	switch(option) {
    	case "GeneDisTabPane":
    		disColumns = new DisGeNetCyTableDisease(params);
    		genColumns = new DisGeNetCyTableGene(params);
    		disColumns.fillNodeColumns(cyTable,attributes);
    		genColumns.fillNodeColumns(cyTable,attributes);
    		break;
    	case "VariantDisTabPane":
    		disColumns = new DisGeNetCyTableDisease(params);
    		varColumns = new DisGeNetCyTableVariant(params);
    		disColumns.fillNodeColumns(cyTable,attributes);
    		varColumns.fillNodeColumns(cyTable,attributes);
    		break;
    	case "disProj_TabPane":
    		disColumns = new DisGeNetCyTableDisease(params);
    		disColumns.fillNodeColumns(cyTable,attributes);
    		break;
    	case "geneProj_TabPane":
    		genColumns = new DisGeNetCyTableGene(params);
    		genColumns.fillNodeColumns(cyTable,attributes);
    		break;
    	default:
    		System.out.println("default case");
    		break;
    	}
    }


	@Override
	public void fillEdgeColumns(CyNetwork cyNetwork, Map<CyEdge, Map<String, Object>> edgeAttributes) {
		DisGeNetCyTableDisease disColumns;
    	DisGeNetCyTableGene genColumns;
    	DisGeNetCyTableVariant varColumns;
    	String option = params.getActiveTab();
    	switch (option) {
    	case "VariantDisTabPane":
    		fillVdaEdgeColumns(cyNetwork, edgeAttributes);
    		break;
		case "GeneDisTabPane":
			fillGdaEdgeColumns(cyNetwork, edgeAttributes);
			break;
		case "disProj_TabPane":
			disColumns = new DisGeNetCyTableDisease();
			disColumns.fillEdgeColumns(cyNetwork, edgeAttributes);
			break;
		case "geneProj_TabPane":
			genColumns = new DisGeNetCyTableGene();
			genColumns.fillEdgeColumns(cyNetwork, edgeAttributes);
			break;
		default:
			break;
		}
	}

    public void createGdaEdgeColumns(CyTable cyTable) {
    	if(cyTable.getColumn("association")==null) {
			cyTable.createColumn("association", String.class, false);
		}
    	if(cyTable.getColumn("associationType")==null) {
			cyTable.createColumn("associationType", String.class, false);
		}
		if(cyTable.getColumn("sentence")==null) {
			cyTable.createColumn("sentence", String.class, false);
		}
		if(cyTable.getColumn("pmid")==null) {
			cyTable.createColumn("pmid", String.class, false);
		}
		if(cyTable.getColumn("score")==null) {
			cyTable.createColumn("score", Double.class, false);
		}
		if(cyTable.getColumn("evidence index")==null) {
			cyTable.createColumn("evidence index", Double.class, false);				
		}
		if(cyTable.getColumn("evidence level")==null) {
			cyTable.createColumn("evidence level", String.class, false);				
		}
        if(cyTable.getColumn("year")==null) {
            cyTable.createColumn("year", Integer.class, false);
        }
    }
    
    private void fillGdaEdgeColumns(CyNetwork cyNetwork, Map<CyEdge, Map<String, Object>> edgeAttributes) {
    	Iterator <CyEdge> edgesIt = edgeAttributes.keySet().iterator();
		CyEdge edge;
		Map <String,Object> attributes;
		Map <String,Object> cleanNulls; 
		while(edgesIt.hasNext()) {
			edge = edgesIt.next();
			attributes = edgeAttributes.get(edge);
			cyNetwork.getRow(edge).set("interaction", attributes.get("interaction"));
			cyNetwork.getRow(edge).set("association", attributes.get("assoc"));
			cyNetwork.getRow(edge).set("associationType", attributes.get("assocType"));
			cyNetwork.getRow(edge).set("source", attributes.get("org_src"));
			cyNetwork.getRow(edge).set("sentence", attributes.get("sentence"));
			cyNetwork.getRow(edge).set("pmid", attributes.get("pmid"));
			cyNetwork.getRow(edge).set("evidence level", attributes.get("el"));
			try {
				cyNetwork.getRow(edge).set("score",Double.parseDouble(attributes.get("score").toString()));
			}catch (NumberFormatException e) {
				cyNetwork.getRow(edge).set("score", null);
			}
			try {
			    Double ei = Double.parseDouble(attributes.get("ei").toString());
                ei = Math.round(ei * 100.0) / 100.0;
				cyNetwork.getRow(edge).set("evidence index",ei);
			}catch (NumberFormatException e) {
				cyNetwork.getRow(edge).set("evidence index", null);
			}catch (NullPointerException e) {
				cyNetwork.getRow(edge).set("evidence index", null);
			}
            try {
                cyNetwork.getRow(edge).set("year",Integer.parseInt(attributes.get("year").toString()));
            }catch (NumberFormatException e) {
                cyNetwork.getRow(edge).set("year", null);
            }catch (NullPointerException e) {
                cyNetwork.getRow(edge).set("year", null);
            }
		}
    }
    
    public void createVdaEdgeColumns(CyTable cyTable) {
    	if(cyTable.getColumn("association")==null) {
			cyTable.createColumn("association", String.class, false);
		}
    	if(cyTable.getColumn("associationType")==null) {
			cyTable.createColumn("associationType", String.class, false);
		}
		if(cyTable.getColumn("sentence")==null) {
			cyTable.createColumn("sentence", String.class, false);
		}
		if(cyTable.getColumn("pmid")==null) {
			cyTable.createColumn("pmid", String.class, false);
		}
		if(cyTable.getColumn("score")==null) {
			cyTable.createColumn("score", Double.class, false);
		}
		if(cyTable.getColumn("evidence index")==null) {
			cyTable.createColumn("evidence index", Double.class, false);				
		}
        if(cyTable.getColumn("year")==null) {
            cyTable.createColumn("year", Integer.class, false);
        }
    }
    
    private void fillVdaEdgeColumns(CyNetwork cyNetwork, Map<CyEdge, Map<String, Object>> edgeAttributes) {
    	Iterator <CyEdge> edgesIt = edgeAttributes.keySet().iterator();
		CyEdge edge;
		Map <String,Object> attributes;
		while(edgesIt.hasNext()) {
			edge = edgesIt.next();
			attributes = edgeAttributes.get(edge);
			cyNetwork.getRow(edge).set("interaction", attributes.get("interaction"));
			cyNetwork.getRow(edge).set("association", attributes.get("assoc"));
			cyNetwork.getRow(edge).set("associationType", attributes.get("assocType"));
			cyNetwork.getRow(edge).set("source", attributes.get("org_src"));
			cyNetwork.getRow(edge).set("sentence", attributes.get("sentence"));
			cyNetwork.getRow(edge).set("pmid", attributes.get("pmid"));
			try {
				cyNetwork.getRow(edge).set("score",Double.parseDouble(attributes.get("score").toString()));
			}catch (NumberFormatException e) {
				cyNetwork.getRow(edge).set("score", null);
			}
			try {
                Double ei = Double.parseDouble(attributes.get("ei").toString());
                ei = Math.round(ei * 100.0) / 100.0;
                cyNetwork.getRow(edge).set("evidence index",ei);
			}catch (NumberFormatException e) {
				cyNetwork.getRow(edge).set("evidence index", null);
			}catch (NullPointerException e) {
				cyNetwork.getRow(edge).set("evidence index", null);
			}
			try {
			    cyNetwork.getRow(edge).set("year", Integer.parseInt(attributes.get("year").toString()));
            }catch (NumberFormatException e) {
                cyNetwork.getRow(edge).set("year", null);
            }catch (NullPointerException e) {
                cyNetwork.getRow(edge).set("year", null);
            }
		}
    }
}
  