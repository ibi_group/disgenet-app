package es.imim.DisGeNET.internal.tables;

import java.util.Map;

import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyTable;

import es.imim.DisGeNET.exceptions.DisGeNetException;

public interface DisGeNETCyTableNodeInterface {
	
	public void addNodeColumns(CyTable cyTable);
	public void addEdgeColumns(CyTable cyTable);
	public void fillNodeColumns(CyTable cyTable, Map <String,Long> suids) throws DisGeNetException;
	public void fillEdgeColumns(CyNetwork cyNetwork, Map<CyEdge, Map<String, Object>> edgeAttributes);
	
	
	
}
