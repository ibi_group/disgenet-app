package es.imim.DisGeNET.internal.tables;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyRow;
import org.cytoscape.model.CyTable;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import es.imim.DisGeNET.database.DatabaseImpl;
import es.imim.DisGeNET.exceptions.DisGeNetException;
import es.imim.DisGeNET.gui.GuiParameters;
/**
 * Class to manage all the parameters relative to the gene attributes of the cytoscape table.
 * @author jsauch
 *
 */
public class DisGeNetCyTableGene implements DisGeNETCyTableNodeInterface {
   
	private GuiParameters params;
	private DatabaseImpl db;
	
	public DisGeNetCyTableGene(GuiParameters params) {
		this.params = params;
		this.db = DatabaseImpl.getInstance();
	}

	public DisGeNetCyTableGene() {
	}

	public GuiParameters getParams() {
		return params;
	}

	public void setParams(GuiParameters params) {
		this.params = params;
	}
	
	/**
	 * Adds all the colums related to the diseases to the cytoscape table.
	 * @param geneTable the table to add the columns.
	 */
	@Override
	public void addNodeColumns(CyTable geneTable) {
		if(geneTable.getColumn("geneId")==null) {
			geneTable.createColumn("geneId", String.class, false);
    	}
		if(geneTable.getColumn("geneName")==null) {
			geneTable.createColumn("geneName", String.class, false);
    	}
		if(geneTable.getColumn("DSI")==null) {
			geneTable.createColumn("DSI", Double.class, false);
		}
		if(geneTable.getColumn("DPI")==null) {
			geneTable.createColumn("DPI", Double.class, false);
		}
		if(geneTable.getColumn("pLI")==null) {
			geneTable.createColumn("pLI", Double.class, false);
		}
		
	}
	
	/**
	 * Fills the gene related columns with the data retrived from the current search parameters.
	 * @param geneTable the table to fill the columns.
	 * @param suids the list of all the nodes with their identifiers.
	 * @throws DisGeNetException low level exception with the error message.
	 */
	@Override
	public void fillNodeColumns(CyTable geneTable, Map <String,Long> suids) throws DisGeNetException {
		
		HashMap<String, HashMap<String, String>> attributes = db.getGeneAttributes(params);
		Iterator<String> attrIt = attributes.keySet().iterator();
		while( attrIt.hasNext() ){
			String geneId = attrIt.next();
			HashMap<String, String> gene_attributes = attributes.get(geneId);
			if(suids.containsKey(geneId)) {
				Long nodeSuid =  suids.get(geneId);
				CyRow row = geneTable.getRow(nodeSuid);
				row.set("name", geneId.trim());
				row.set("nodeType", "gene");
				row.set("geneId", geneId.trim());
				row.set("geneName", gene_attributes.get("geneName").trim());
				try {
					row.set("DSI", Double.parseDouble(gene_attributes.get("dsi")));
				} catch (Exception e) {
					row.set("DSI", null);
				} 
		        try {
					row.set("DPI", Double.parseDouble(gene_attributes.get("dpi")));
				} catch (Exception e) {
					row.set("DPI", null);
				}
		        try {
					row.set("pLI", Double.parseDouble(gene_attributes.get("pli")));
				} catch (Exception e) {
					row.set("pLI", null);
				}
			}
		}
	}

	@Override
	public void addEdgeColumns(CyTable cyTable) {
		if(cyTable.getColumn("nrAssociatedDiseases")==null) {
			cyTable.createColumn("nrAssociatedDiseases", Integer.class, false);
		}
		if(cyTable.getColumn("styleSize")==null) {
			cyTable.createColumn("styleSize", Integer.class, false);
		}
	}

	@Override
	public void fillEdgeColumns(CyNetwork cyNetwork, Map<CyEdge, Map<String, Object>> edgeAttributes) {
		Iterator <CyEdge> edgesIt = edgeAttributes.keySet().iterator();
		CyEdge edge;
		Map <String,Object> attributes;
		while(edgesIt.hasNext()) {
			edge = edgesIt.next();
			attributes = edgeAttributes.get(edge);
			cyNetwork.getRow(edge).set("interaction", attributes.get("interaction"));
			try {
				cyNetwork.getRow(edge).set("nrAssociatedDiseases", Integer.parseInt(attributes.get("nrAssociatedDiseases").toString())/2);
			}catch (NumberFormatException e) {
				cyNetwork.getRow(edge).set("nrAssociatedDiseases", 0);
			}
			try {
				cyNetwork.getRow(edge).set("styleSize", Integer.parseInt(attributes.get("nrAssociatedDiseases").toString())/2);
			} catch (NumberFormatException e) {
				cyNetwork.getRow(edge).set("styleSize", 0);
			}
		}
	}

  
}