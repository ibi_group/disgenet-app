package es.imim.DisGeNET.internal.tables;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyRow;
import org.cytoscape.model.CyTable;

import es.imim.DisGeNET.database.DatabaseImpl;
import es.imim.DisGeNET.gui.GuiParameters;

/**
 * Class to manage all the parameters relative to the gene attributes of the cytoscape table.
 * @author jsauch
 */

public class DisGeNetCyTableVariant implements DisGeNETCyTableNodeInterface {

	private GuiParameters params;
	private DatabaseImpl db;
	
	public DisGeNetCyTableVariant(GuiParameters params) {
		this.params = params;
		this.db = DatabaseImpl.getInstance();
	}

	public GuiParameters getParams() {
		return params;
	}

	public void setParams(GuiParameters params) {
		this.params = params;
	}
	
	/**
	 * Adds all the colums related to the diseases to the cytoscape table.
	 * @param variantTable the table to add the columns.
	 */
	@Override
	public void addNodeColumns(CyTable variantTable) {
		if(variantTable.getColumn("variantId")==null) {
			variantTable.createColumn("variantId",String.class,false);	
		}
		if(variantTable.getColumn("variantClass")==null) {
			variantTable.createColumn("variantClass",String.class,false);
		}
        if(variantTable.getColumn("chromosome")==null) {
        	variantTable.createColumn("chromosome",String.class,false);
        }
        if(variantTable.getColumn("coordinates")==null) {
        	variantTable.createColumn("coordinates",String.class,false);
        }
//        if(variantTable.getColumn("origen")==null) {
//        	variantTable.createColumn("origen",String.class,false);
//        }
        if(variantTable.getColumn("most_severe_consequence")==null) {
        	variantTable.createColumn("most_severe_consequence",String.class,false);
        }
//        if(variantTable.getColumn("clinical_significance")==null) {
//        	variantTable.createColumn("clinical_significance",String.class,false);
//        }
//        if(variantTable.getColumn("reference")==null) {
//        	variantTable.createColumn("reference",String.class,false);
//        }
//        if(variantTable.getColumn("reference")==null) {
//        	variantTable.createColumn("reference",String.class,false);
//        }
//        if(variantTable.getColumn("alteration")==null) {
//        	variantTable.createColumn("alteration",String.class,false);
//        }
//        if(variantTable.getColumn("AF_GENOME")==null) {
//        	variantTable.createColumn("AF_GENOME",String.class,false);
//        }
//        if(variantTable.getColumn("AF_EXOME")==null) {
//        	variantTable.createColumn("AF_EXOME",String.class,false);
//        }
        if(variantTable.getColumn("DSI")==null) {
        	variantTable.createColumn("DSI",Double.class,false);
        }
        if(variantTable.getColumn("DPI")==null) {
        	variantTable.createColumn("DPI",Double.class,false);
        }
        if(variantTable.getColumn("associatedGenes")==null) {
        	variantTable.createListColumn("associatedGenes",String.class,false);
        }
        
	}
	
	/**
	 * Fills the variant related columns with the data retrived from the current search parameters.
	 * @param variantTable the table to fill the columns.
	 * @param suids the list of all the nodes with their identifiers.
	 */
	@Override
	public void fillNodeColumns(CyTable variantTable, Map <String,Long> suids) {
		
		HashMap<String, HashMap<String, String>> attributes = db.getVariantAttributes(params);
		Iterator<String> attrIt = attributes.keySet().iterator();
		while( attrIt.hasNext() ){
			String variantId = attrIt.next();
			HashMap<String, String> variant_attributes = attributes.get(variantId);
			if(suids.containsKey(variantId)) {
				Long nodeSuid =  suids.get(variantId);
				CyRow row = variantTable.getRow(nodeSuid);
				row.set("shared name", variantId);
				row.set("name", variantId);
				row.set("nodeType", "variant");
		        row.set("variantId", variantId.trim());
		        row.set("variantClass", variant_attributes.get("varClass"));
		        row.set("chromosome", variant_attributes.get("varChrom"));
		        row.set("coordinates", variant_attributes.get("varCoord"));
//		        row.set("origen", variant_attributes.get("origen"));
		        row.set("most_severe_consequence", variant_attributes.get("mostSevCons"));
//		        row.set("clinical_significance", variant_attributes.get("clinSignifc"));
//		        row.set("reference", variant_attributes.get("ref"));
//		        row.set("alteration", variant_attributes.get("alt"));
//		        row.set("AF_GENOME", variant_attributes.get("af1kG"));
//		        row.set("AF_EXOME", variant_attributes.get("afExac"));
		        try {
					row.set("DSI", Double.parseDouble(variant_attributes.get("dsi")));
				} catch (Exception e) {
					row.set("DSI", null);
				}
		        try {
					row.set("DPI", Double.parseDouble(variant_attributes.get("dpi")));
				} catch (Exception e) {
					row.set("DPI", null);
				}
		        if(variant_attributes.get("assocGenes")!=null) {
		        	row.set("associatedGenes", Arrays.asList(variant_attributes.get("assocGenes").trim().split(",")));
		        }
		        
			}
		}
	}

	@Override
	public void addEdgeColumns(CyTable cyTable) {
		// NOT USED IN THE CURRENT STATE
	}

	@Override
	public void fillEdgeColumns(CyNetwork cyNetwork, Map<CyEdge, Map<String, Object>> edgeAttributes) {
		// NOT USED IN THE CURRENT STATE
		
	}

}
