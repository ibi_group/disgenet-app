package es.imim.DisGeNET.internal.tables;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyRow;
import org.cytoscape.model.CyTable;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import es.imim.DisGeNET.database.DatabaseImpl;
import es.imim.DisGeNET.exceptions.DisGeNetException;
import es.imim.DisGeNET.gui.GuiParameters;
/**
 * Class to manage all the parameters relative to the diseases attributes of the cytoscape table.
 * @author jsauch
 *
 */
public class DisGeNetCyTableDisease implements DisGeNETCyTableNodeInterface {
   
	private GuiParameters params;
	private DatabaseImpl db;
	
	public DisGeNetCyTableDisease(GuiParameters params) {
		this.params = params;
		this.db = DatabaseImpl.getInstance();
	}

	public DisGeNetCyTableDisease() {
	}

	public GuiParameters getParams() {
		return params;
	}

	public void setParams(GuiParameters params) {
		this.params = params;
	}
	
	/**
	 * Adds all the colums related to the diseases to the cytoscape table.
	 * @param disTable the table to add the columns.
	 */
	@Override
	public void addNodeColumns(CyTable disTable) {
		if(disTable.getColumn("diseaseId")==null) {
			disTable.createColumn("diseaseId", String.class, false);
		}
		if(disTable.getColumn("diseaseName")==null) {
			disTable.createColumn("diseaseName", String.class, false);
		}
		if(disTable.getColumn("diseaseClass")==null) {
			disTable.createListColumn("diseaseClass", String.class, false);
		}
		if(disTable.getColumn("diseaseClassName")==null) {
			disTable.createListColumn("diseaseClassName", String.class, false);
		}
	}
	
	/**
	 * Fills the disease related columns with the data retrived from the current search parameters.
	 * @param disTable the table to fill the columns.
	 * @param suids the list of all the nodes with their identifiers.
	 * @throws DisGeNetException low level exception with the error message.
	 */
	@Override
	public void fillNodeColumns(CyTable disTable, Map <String,Long> suids) throws DisGeNetException {
		HashMap<String, HashMap<String, String>> attributes = db.getDiseaseAttributes(params);
		Iterator<String> attrIt = attributes.keySet().iterator();
	      while( attrIt.hasNext() ){
	        String diseaseId = attrIt.next();
	        HashMap<String, String> dis_attributes = attributes.get(diseaseId);
	        if(suids.containsKey(diseaseId)) {
	        	Long nodeSuid = suids.get(diseaseId);
	        	CyRow row = disTable.getRow(nodeSuid);
				row.set("name", diseaseId);
	        	row.set("nodeType", "disease");
	        	row.set("diseaseId", diseaseId);
	        	row.set("diseaseName", dis_attributes.get("diseaseName"));
	        	if(dis_attributes.get("diseaseClass")!=null) {
	        		row.set("diseaseClass", Arrays.asList(dis_attributes.get("diseaseClass").split(",")));
	        	}else {
	        		row.set("diseaseClass", null);
	        	}
	        	if(dis_attributes.get("diseaseClassName")!=null){
	        		row.set("diseaseClassName", Arrays.asList(dis_attributes.get("diseaseClassName").split(",")));
	        	}else {
	        		row.set("diseaseClassName", null);
	        	}
	        }
	      }
	}

	@Override
	public void addEdgeColumns(CyTable cyTable) {
		if(cyTable.getColumn("nrCommonGenes")==null) {
			cyTable.createColumn("nrCommonGenes", Integer.class, false,0);
		}
		if(cyTable.getColumn("styleSize")==null) {
			cyTable.createColumn("styleSize", Integer.class, false,0);
		}
	}

	@Override
	public void fillEdgeColumns(CyNetwork cyNetwork, Map<CyEdge, Map<String, Object>> edgeAttributes) {
		Iterator <CyEdge> edgesIt = edgeAttributes.keySet().iterator();
		CyEdge edge;
		Map <String,Object> attributes;
		while(edgesIt.hasNext()) {
			edge = edgesIt.next();
			attributes = edgeAttributes.get(edge);
			cyNetwork.getRow(edge).set("interaction", attributes.get("interaction"));
			cyNetwork.getRow(edge).set("source", attributes.get("source"));
			cyNetwork.getRow(edge).set("nrCommonGenes", attributes.get("nrCommonGenes"));
			cyNetwork.getRow(edge).set("styleSize", attributes.get("nrCommonGenes"));
		}
	}
}
