package es.imim.DisGeNET.internal;


import org.cytoscape.application.swing.CytoPanelName;
import org.cytoscape.application.swing.CytoPanelComponent;

import java.awt.Component;

import javax.swing.JPanel;

import javax.swing.Icon;

public class DisGeNETCytoPanel extends JPanel implements CytoPanelComponent {


	public DisGeNETCytoPanel(){
	}

	public String getTitle(){
		return "DisGeNET";
	}

	public Icon getIcon(){
		return null;
	}

	public Component getComponent(){
		return this;
	}

	@Override
	public CytoPanelName getCytoPanelName(){
		return CytoPanelName.WEST;
	}


}