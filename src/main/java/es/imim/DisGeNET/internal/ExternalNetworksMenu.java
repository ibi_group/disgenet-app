package es.imim.DisGeNET.internal;

import java.awt.event.ActionEvent;

import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.application.swing.AbstractCyAction;

public class ExternalNetworksMenu extends AbstractCyAction {
	
	private static final long serialVersionUID = 1L;

	
	public ExternalNetworksMenu(CyApplicationManager cyApplicationManager, final String menuTitle) {
		super(menuTitle, cyApplicationManager, null, null);
		setPreferredMenu("Apps.DisGeNET");
		setMenuGravity(4.0f);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
	}   
	

}
