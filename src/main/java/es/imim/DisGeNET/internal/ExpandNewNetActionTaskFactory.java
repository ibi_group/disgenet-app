package es.imim.DisGeNET.internal;

import org.cytoscape.model.CyNode;
import org.cytoscape.task.AbstractNodeViewTaskFactory;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.work.TaskIterator;

import es.imim.DisGeNET.network.ExpandNewNetworkTask;
import es.imim.DisGeNET.validation.NetworkValidation;

public class ExpandNewNetActionTaskFactory extends AbstractNodeViewTaskFactory {

	@Override
	public TaskIterator createTaskIterator(View<CyNode> cyNodeView, CyNetworkView cyNetworkView) {
		return new TaskIterator(new ExpandNewNetworkTask(cyNetworkView, cyNodeView));
	}

	@Override
	public boolean isReady(View<CyNode> cyNodeView, CyNetworkView cyNetworkView) {
		boolean flag = true;
		if(NetworkValidation.isForeignNet(cyNetworkView.getModel())) {
			flag = false;
		}
		return flag;
	}

}
