package es.imim.DisGeNET.internal.enrichment;

import org.cytoscape.service.util.CyServiceRegistrar;
import org.cytoscape.task.NetworkViewTaskFactory;

import java.util.Properties;

import static org.cytoscape.work.ServiceProperties.*;
import static org.cytoscape.work.ServiceProperties.TITLE;

public class EnrichmentService {

    public EnrichmentService (CyServiceRegistrar serviceRegistrar) {

        final NetworkViewTaskFactory enrichGenes = new GeneEnrichmentTaskFactory();
        final Properties enrichGeneProps = new Properties();
        enrichGeneProps.setProperty(PREFERRED_MENU, NETWORK_APPS_MENU+".DisGeNET.Genes");
        enrichGeneProps.setProperty(IN_NETWORK_PANEL_CONTEXT_MENU, "true");
        enrichGeneProps.setProperty(IN_MENU_BAR, "false");
        enrichGeneProps.setProperty(MENU_GRAVITY, "2");
        enrichGeneProps.setProperty(TITLE,"Perform disgenet enrichment");
        serviceRegistrar.registerService(enrichGenes, NetworkViewTaskFactory.class, enrichGeneProps);

        final NetworkViewTaskFactory enrichVariants = new VariantEnrichmentTaskFactory();
        final Properties enrichVariantsProps = new Properties();
        enrichVariantsProps.setProperty(PREFERRED_MENU, NETWORK_APPS_MENU+".DisGeNET.Variants");
        enrichVariantsProps.setProperty(IN_NETWORK_PANEL_CONTEXT_MENU, "true");
        enrichVariantsProps.setProperty(IN_MENU_BAR, "false");
        enrichVariantsProps.setProperty(MENU_GRAVITY, "2");
        enrichVariantsProps.setProperty(TITLE,"Perform disgenet enrichment");
        serviceRegistrar.registerService(enrichVariants, NetworkViewTaskFactory.class, enrichVariantsProps);

    }
}
