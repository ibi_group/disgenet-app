package es.imim.DisGeNET.internal.enrichment;


import es.imim.DisGeNET.internal.CyActivator;
import org.cytoscape.task.AbstractNetworkViewTaskFactory;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.work.TaskIterator;

public class VariantEnrichmentTaskFactory extends AbstractNetworkViewTaskFactory {

    @Override
    public TaskIterator createTaskIterator(CyNetworkView cyNetworkView) {
        TaskIterator tasks = new TaskIterator(new VariantEnrichmentTask(cyNetworkView));
        tasks.append(new ShowEnrichmentPanelTask(CyActivator.getInstance().getShowEnrichmentFactory(), true, false));
        return tasks;
    }

    @Override
    public boolean isReady (CyNetworkView cyNetworkView) {
        return true;
    }

}