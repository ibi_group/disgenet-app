package es.imim.DisGeNET.internal.enrichment;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.table.AbstractTableModel;

import org.cytoscape.model.CyRow;
import org.cytoscape.model.CyTable;

public class EnrichmentTableModel extends AbstractTableModel {
    private String[] columnNames;
    private CyTable cyTable;
    private String[] rowNames;

    public EnrichmentTableModel(CyTable cyTable, String[] columnNames) {
        this.columnNames = columnNames;
        this.cyTable = cyTable;
        initData();
    }

    public int getColumnCount() {
        return columnNames.length;
    }

    public int getRowCount() {
        return rowNames.length;
    }

    public int getAllRowCount() {
        return cyTable.getRowCount();
    }

    public String getColumnName(int col) {
        return columnNames[col];
    }

    public String[] getRowNames() {
        return rowNames;
    }

    public Object getValueAt(int row, int col) {
        final String colName = columnNames[col];
        final String rowName = rowNames[row];
        // swingColumns = new String[] { colShowChart, colName, colDescription, colFDR,
        // colGenesCount, colGenes, colGenesSUID };
        //if (colName.equals(EnrichmentTerm.colShowChart)) {
        //	return cyTable.getRow(rowName).get(colName, Boolean.class);
        //} else
        return cyTable.getRow(rowName).get(colName, cyTable.getColumn(colName).getType());
    }

    public Object getValueAt(int row, String colName) {
        // final String colName = columnNames[col];
        final String rowName = rowNames[row];
        // swingColumns = new String[] { colShownChart, colName, colDescription, colFDR,
        // colGenesCount, colGenes, colGenesSUID };
        //if (colName.equals(EnrichmentTerm.colShowChart)) {
        //	return cyTable.getRow(rowName).get(colName, Boolean.class);
        //} else
        return cyTable.getRow(rowName).get(colName, cyTable.getColumn(colName).getType());
    }

//    public void filter(List<TermCategory> categories, boolean removeRedundancy, double cutoff) {
//        List<CyRow> rows = cyTable.getAllRows();
//        Long[] rowArray = new Long[rows.size()];
//        int i = 0;
//        for (CyRow row : rows) {
//            String termCategory = row.get(EnrichmentTerm.colCategory, String.class);
//            if (categories.size() == 0 || inCategory(categories, termCategory)) {
//                rowArray[i] = row.get(EnrichmentTerm.colID, Long.class);
//                i++;
//            }
//        }
//
//        // Now we have the categories of interest.  Remove redundancy if desired
//        if (removeRedundancy)
//            rowNames = removeRedundancy(rowArray, i, cutoff);
//        else
//            rowNames = Arrays.copyOf(rowArray, i);
//
//        fireTableDataChanged();
//    }

    public Class<?> getColumnClass(int c) {
        final String colName = columnNames[c];
        // return cyTable.getColumn(colName).getClass();
        // if (colName.equals(EnrichmentTerm.colShowChart)) {
        //	return Boolean.class;
        //} else
        return cyTable.getColumn(colName).getType();
    }

    public boolean isCellEditable(int row, int col) {
        // columnNames[col].equals(EnrichmentTerm.colShowChart) ||
        return false;
    }

    public void setValueAt(Object value, int row, int col) {
        final String colName = columnNames[col];
        final String rowName = rowNames[row];
        // if (colName.equals(EnrichmentTerm.colShowChart)) {
        // 	if (cyTable.getColumn(EnrichmentTerm.colShowChart) == null) {
        //		cyTable.createColumn(EnrichmentTerm.colShowChart, Boolean.class, false);
        //	}
        //	cyTable.getRow(rowName).set(colName, value);
        // }
        fireTableCellUpdated(row, col);
    }

    private void initData() {
        List<CyRow> rows = cyTable.getAllRows();
        // Object[][] data = new Object[rows.size()][EnrichmentTerm.swingColumns.length];
        rowNames = new String[rows.size()];
        int i = 0;
        for (CyRow row : rows) {
            rowNames[i] = row.get("diseaseid", String.class);
            i++;
        }
    }
}
