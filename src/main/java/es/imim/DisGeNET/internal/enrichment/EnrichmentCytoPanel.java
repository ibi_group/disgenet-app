package es.imim.DisGeNET.internal.enrichment;

import es.imim.DisGeNET.exceptions.DisGeNetException;
import es.imim.DisGeNET.internal.CyActivator;
import es.imim.DisGeNET.internal.utils.TextIcon;
import org.cytoscape.application.events.SetCurrentNetworkEvent;
import org.cytoscape.application.events.SetCurrentNetworkListener;
import org.cytoscape.application.swing.CytoPanelComponent2;
import org.cytoscape.application.swing.CytoPanelName;
import org.cytoscape.model.*;
import org.cytoscape.model.events.RowSetRecord;
import org.cytoscape.model.events.RowsSetEvent;
import org.cytoscape.model.events.RowsSetListener;
import org.cytoscape.util.swing.IconManager;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.work.Task;
import org.cytoscape.work.TaskIterator;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.util.*;
import java.util.List;

public class EnrichmentCytoPanel extends JPanel implements CytoPanelComponent2, ListSelectionListener, ActionListener, RowsSetListener, TableModelListener, SetCurrentNetworkListener {

    EnrichmentTableModel tableModel;
    CyActivator cyAct;
    Map<Long, JTable> enrichmentTables;
    List<String> availableTables;
    final Font iconFont;


    JLabel labelRows;
    JPanel topPanel;
    JPanel mainPanel;
    JScrollPane scrollPane;
    boolean clearSelection = false;

    JButton butFilter;
    JButton butExportTable;
    JMenuItem menuItemReset;
    JPopupMenu popupMenu;

    final String butSettingsName = "Network-specific enrichment panel settings";
    final String butFilterName = "Filter enrichment table";
    final String butDrawChartsName = "Draw charts using default color palette";
    final String butResetChartsName = "Reset charts";
    final String butEnrichmentMapName = "Create EnrichmentMap";
    final String butAnalyzedNodesName = "Select all analyzed nodes";
    final String butExportTableDescr = "Export enrichment table";

    public EnrichmentCytoPanel() {
        this.setLayout(new BorderLayout());
        this.enrichmentTables = new HashMap<Long, JTable>();
        this.cyAct = CyActivator.getInstance();
        cyAct.getServiceRegistrar().registerService(this, SetCurrentNetworkListener.class, new Properties());
        IconManager iconManager = cyAct.getServiceRegistrar().getService(IconManager.class);
        iconFont = iconManager.getIconFont(22.0f);
        initPanel();
    }

    public void initPanel() {
        CyNetwork network = cyAct.getCyApplicationManager().getCurrentNetwork();
        if (network == null)
            return;
        initPanel(network);
    }

    public void initPanel(CyNetwork network) {
        this.removeAll();
        Map <Long, CyTable> netEnrichTables = cyAct.getEnrichmentTables();
        CyTable currCyTable = null;
        if(network !=null){
            currCyTable = netEnrichTables.get(network.getSUID());
        }
        if(currCyTable == null) {
            mainPanel = new JPanel(new BorderLayout());
            JLabel label = new JLabel("No enrichment has been retrieved for this network.",
                    SwingConstants.CENTER);
            mainPanel.add(label, BorderLayout.CENTER);
        }else{
//            JPanel buttonsPanelLeft = new JPanel();
//            BoxLayout layoutLeft = new BoxLayout(buttonsPanelLeft, BoxLayout.LINE_AXIS);
//            buttonsPanelLeft.setLayout(layoutLeft);
//            butFilter = new JButton(IconManager.ICON_FILTER);
//            butFilter.setFont(iconFont);
//            butFilter.setName(butFilterName);
//            butFilter.addActionListener(this);
//            butFilter.setToolTipText(butFilterName);
//            butFilter.setBorderPainted(false);
//            butFilter.setContentAreaFilled(false);
//            butFilter.setFocusPainted(false);
//            butFilter.setBorder(BorderFactory.createEmptyBorder(2, 10, 2, 10));
//
//            buttonsPanelLeft.add(butFilter);


            JPanel panelMiddle = new JPanel(new BorderLayout());
            JTable currentTable = createJTable(currCyTable);
            enrichmentTables.put(network.getSUID(),currentTable);
            // System.out.println("show table: " + showTable);
            labelRows = new JLabel("");
            labelRows.setHorizontalAlignment(JLabel.RIGHT);
            Font labelFont = labelRows.getFont();
            labelRows.setFont(labelFont.deriveFont((float)(labelFont.getSize() * 0.8)));
            panelMiddle.add(labelRows, BorderLayout.EAST);

            topPanel = new JPanel(new BorderLayout());
//            topPanel.add(buttonsPanelLeft, BorderLayout.WEST);
//            topPanel.add(panelMiddle, BorderLayout.CENTER);
//            this.add(topPanel, BorderLayout.NORTH);
            mainPanel = new JPanel(new BorderLayout());
            scrollPane = new JScrollPane(currentTable);
            mainPanel.setLayout(new GridLayout(1, 1));
            mainPanel.add(scrollPane, BorderLayout.CENTER);
        }
        this.add(mainPanel, BorderLayout.CENTER);
        this.revalidate();
        this.repaint();

    }

    private JTable createJTable(CyTable cyTable) {
        Collection<CyColumn> cyColumns = cyTable.getColumns();
        String[] columnNames = new String[cyColumns.size()];
        int i = 0;
        for(CyColumn cyColumn : cyColumns){
            columnNames[i] = cyColumn.getName();
            i++;
        }
        tableModel = new EnrichmentTableModel(cyTable, columnNames);
        JTable jTable = new JTable(tableModel);
        TableColumnModel tcm = jTable.getColumnModel();
        if(cyTable.getColumns().size()>12) {
            tcm.removeColumn(tcm.getColumn(12));
            tcm.getColumn(10).setCellRenderer(new DecimalFormatRenderer());
            tcm.getColumn(11).setCellRenderer(new DecimalFormatRenderer());
        }else{
            tcm.removeColumn(tcm.getColumn(11));
            tcm.getColumn(9).setCellRenderer(new DecimalFormatRenderer());
            tcm.getColumn(10).setCellRenderer(new DecimalFormatRenderer());
        }
         jTable.setDefaultEditor(Object.class, null);
        // jTable.setPreferredScrollableViewportSize(jTable.getPreferredSize());
        jTable.setFillsViewportHeight(true);
        jTable.setAutoCreateRowSorter(true);
        jTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        jTable.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        jTable.getSelectionModel().addListSelectionListener(this);
        jTable.getModel().addTableModelListener(this);
        jTable.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                if (e.isPopupTrigger() || SwingUtilities.isRightMouseButton(e)) {
                    JTable source = (JTable) e.getSource();
                    int row = source.rowAtPoint(e.getPoint());
                    int column = source.columnAtPoint(e.getPoint());
                    if (!source.isRowSelected(row)) {
                        source.changeSelection(row, column, false, false);
                    }
                }
            }
            public void mouseReleased(MouseEvent e) {
                if (e.isPopupTrigger() || SwingUtilities.isRightMouseButton(e)) {
                    JTable source = (JTable) e.getSource();
                    int row = source.rowAtPoint(e.getPoint());
                    int column = source.columnAtPoint(e.getPoint());
                    if (!source.isRowSelected(row)) {
                        source.changeSelection(row, column, false, false);
                    }
                }
            }
        });
        return jTable;
    }

    @Override
    public String getIdentifier() {
        return "es.imim.DisGeNET.Enrichment";
    }

    @Override
    public Component getComponent() {
        return this;
    }

    @Override
    public CytoPanelName getCytoPanelName() {
        return CytoPanelName.SOUTH;
    }

    @Override
    public String getTitle() {
        return "DisGeNET Enrichment";
    }

    @Override
    public Icon getIcon() {
        return null;
    }

    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting())
            return;
        CyNetwork network = cyAct.getCyApplicationManager().getCurrentNetwork();
        if (network == null)
            return;
        JTable table = enrichmentTables.get(network.getSUID());
        if (table.getSelectedColumnCount() == 1 && table.getSelectedRow() > -1) {
            if (table.getSelectedRowCount() == 1)
                clearNetworkSelection(network);
            for (int row: table.getSelectedRows()) {
                Object cellContent = table.getModel().getValueAt(table.convertRowIndexToModel(row), table.getColumnCount());
                if (cellContent instanceof List) {
                    List<Long> nodeIDs = (List<Long>) cellContent;
                    for (Long nodeID : nodeIDs) {
                        network.getDefaultNodeTable().getRow(nodeID).set(CyNetwork.SELECTED, true);
                    }
                }
            }
        }else if(table.getSelectedRowCount() == 0){
            clearNetworkSelection(network);
        }

    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(butFilter)) {
            CyNetwork net = cyAct.getApplicationManagerService().getCurrentNetwork();
            Map<Long, CyTable> enrichCyTables = cyAct.getEnrichmentTables();
            CyTable cyTable = enrichCyTables.get(net.getSUID());
            Integer maxGene = null;
            if(cyTable.getColumn("# genes")!=null){
                List<Integer> nGenes = cyTable.getColumn("# genes").getValues(Integer.class);
                maxGene = Collections.max(nGenes);
                Task task = null;
                try {
                    int[] indexCols = {6,10,11};
                    task = new FilterEnrichmentTask(this, net, 1, maxGene, indexCols);
                    CyActivator.getInstance().getTaskManagerService().execute(new TaskIterator(task));
                } catch (DisGeNetException disGeNetException) {
                    disGeNetException.printStackTrace();
                }
            }else{
                List<Integer> nGenes = cyTable.getColumn("# variants").getValues(Integer.class);
                maxGene = Collections.max(nGenes);
                Task task = null;
                try {
                    int[] indexCols = {5,9,10};
                    task = new FilterEnrichmentTask(this, net, 1, maxGene, indexCols);
                    CyActivator.getInstance().getTaskManagerService().execute(new TaskIterator(task));
                } catch (DisGeNetException disGeNetException) {
                    disGeNetException.printStackTrace();
                }
            }
        }
    }

    @Override
    public void tableChanged(TableModelEvent tableModelEvent) {
    }

    public void handleEvent(RowsSetEvent rse) {
        CyNetworkManager networkManager = cyAct.getServiceRegistrar().getService(CyNetworkManager.class);
        CyNetwork selectedNetwork = null;
        if (rse.containsColumn(CyNetwork.SELECTED)) {
            Collection<RowSetRecord> columnRecords = rse.getColumnRecords(CyNetwork.SELECTED);
            for (RowSetRecord rec : columnRecords) {
                CyRow row = rec.getRow();
                if (row.toString().indexOf("FACADE") >= 0)
                    continue;
                Long networkID = row.get(CyNetwork.SUID, Long.class);
                Boolean selectedValue = (Boolean) rec.getValue();
                if (selectedValue && networkManager.networkExists(networkID)) {
                    selectedNetwork = networkManager.getNetwork(networkID);
                }
            }
        }

        if (selectedNetwork != null) {
            initPanel(selectedNetwork);
            return;
        }
        // experimental: clear term selection when all network nodes are unselected
        CyNetwork network = cyAct.getCyApplicationManager().getCurrentNetwork();
        JTable currentTable = enrichmentTables.get(selectedNetwork.getSUID());
        if (!clearSelection && network != null && currentTable != null) {
            List<CyNode> nodes = network.getNodeList();
            for (CyNode node : nodes) {
                if (network.getRow(node).get(CyNetwork.SELECTED, Boolean.class)) {
                    return;
                }
            }
            currentTable.clearSelection();
        }
    }

    private void clearNetworkSelection(CyNetwork network) {
        List<CyNode> nodes = network.getNodeList();
        clearSelection = true;
        for (CyNode node : nodes) {
            if (network.getRow(node).get(CyNetwork.SELECTED, Boolean.class)) {
                network.getRow(node).set(CyNetwork.SELECTED, false);
            }
        }
        clearSelection = false;
    }

    @Override
    public void handleEvent(SetCurrentNetworkEvent event) {
        CyNetwork network = event.getNetwork();
        initPanel(network);
    }

    static class DecimalFormatRenderer extends DefaultTableCellRenderer {
        private static final DecimalFormat formatter = new DecimalFormat("0.#####E0");
        private static final DecimalFormat formatter2 = new DecimalFormat("0.#####");

        public Component getTableCellRendererComponent(JTable table, Object value,
                                                       boolean isSelected, boolean hasFocus, int row, int column) {
            try {
                if (value != null && (double) value < 0.001) {
                    value = formatter.format((Number) value);
                }else{
                    value = formatter2.format((Number) value);
                }
            } catch (Exception ex) {
                // ignore and return original value
            }
            return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row,
                    column);
        }
    }
}