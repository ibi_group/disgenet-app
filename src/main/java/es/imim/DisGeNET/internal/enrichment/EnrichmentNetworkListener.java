package es.imim.DisGeNET.internal.enrichment;

import es.imim.DisGeNET.internal.CyActivator;
import org.cytoscape.application.events.SetCurrentNetworkEvent;
import org.cytoscape.application.events.SetCurrentNetworkListener;
import org.cytoscape.work.TaskIterator;

public class EnrichmentNetworkListener implements SetCurrentNetworkListener {

    public EnrichmentNetworkListener() {

    }

    @Override
    public void handleEvent(SetCurrentNetworkEvent e) {
    }

}
