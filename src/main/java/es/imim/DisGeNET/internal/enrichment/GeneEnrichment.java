package es.imim.DisGeNET.internal.enrichment;

import java.util.List;

public class GeneEnrichment {
    public static final String enrichmentURL = "https://www.disgenet.org/api/enrichment/genes";
    //public static final String enrichmentURL = "http://localhost:5050/api/enrichment/genes";
    String cui;
    String name;
    String source;
    String universe;
    String geneRatio;
    String bgRatio;
    Double pValue;
    Double adjPValue;
    List<Integer> geneIds;
    List<String> geneSymbols;
    List<Long> geneNodes;

    public static final String colID = "disease id";
    public static final String colName = "disease name";
    public static final String colSource = "disease name";
    public static final String colGenesRatio = "gene ratio";
    public static final String colGenesBG = "bg ratio";
    public static final String colListGenes = "intersection EntrezIds";
    public static final String colListSymbols = "intersection Symbols";
    public static final String colPvalue = "p-value";
    public static final String colPvalueAdj = "adjusted p-value";
    public static final String colGenesSUID = "nodes.SUID";

    public static final String[] swingColumnsEnrichment = new String[] {colID, colName, colSource,colGenesRatio, colGenesBG, colListGenes, colListSymbols, colPvalue, colPvalueAdj, colGenesSUID};

    public GeneEnrichment() {

    }

    public GeneEnrichment(String cui, String name, String source, String universe, String geneRatio, String bgRatio, Double pValue, Double adjPValue, List<Integer> geneIds, List<String> geneSymbols, List<Long> geneNodes) {
        this.cui = cui;
        this.name = name;
        this.source = source;
        this.universe = universe;
        this.geneRatio = geneRatio;
        this.bgRatio = bgRatio;
        this.pValue = pValue;
        this.adjPValue = adjPValue;
        this.geneIds = geneIds;
        this.geneSymbols = geneSymbols;
        this.geneNodes = geneNodes;
    }

    public int getNumberGenes(){return this.geneIds.size();}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getUniverse() {
        return universe;
    }

    public void setUniverse(String universe) {
        this.universe = universe;
    }

    public String getGeneRatio() {
        return geneRatio;
    }

    public void setGeneRatio(String geneRatio) {
        this.geneRatio = geneRatio;
    }

    public String getBgRatio() {
        return bgRatio;
    }

    public void setBgRatio(String bgRatio) {
        this.bgRatio = bgRatio;
    }

    public Double getpValue() {
        return pValue;
    }

    public void setpValue(Double pValue) {
        this.pValue = pValue;
    }

    public Double getAdjPValue() {
        return adjPValue;
    }

    public void setAdjPValue(Double adjPValue) {
        this.adjPValue = adjPValue;
    }

    public List<Integer> getGeneIds() {
        return geneIds;
    }

    public void setGeneIds(List<Integer> geneIds) {
        this.geneIds = geneIds;
    }

    public List<String> getGeneSymbols() {
        return geneSymbols;
    }

    public void setGeneSymbols(List<String> geneSymbols) {
        this.geneSymbols = geneSymbols;
    }

    public List<Long> getGeneNodes() {
        return geneNodes;
    }

    public void setGeneNodes(List<Long> geneNodes) {
        this.geneNodes = geneNodes;
    }

}
