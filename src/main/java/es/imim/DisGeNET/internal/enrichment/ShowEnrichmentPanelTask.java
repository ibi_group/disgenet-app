package es.imim.DisGeNET.internal.enrichment;

import es.imim.DisGeNET.internal.CyActivator;
import org.cytoscape.application.swing.*;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import java.awt.*;
import java.util.Properties;

public class ShowEnrichmentPanelTask  extends AbstractTask {

    final boolean show;
    final boolean noSignificant;
    final ShowEnrichmentPanelTaskFactory factory;

    public ShowEnrichmentPanelTask(ShowEnrichmentPanelTaskFactory factory, boolean show, boolean noSignificant) {
        this.factory = factory;
        this.show = show;
        this.noSignificant = noSignificant;
    }

    public void run(TaskMonitor monitor) {
        if (show)
            monitor.setTitle("Show enrichment panel");
        else
            monitor.setTitle("Hide enrichment panel");

        CySwingApplication swingApplication = CyActivator.getInstance().getServiceRegistrar().getService(CySwingApplication.class);
        CytoPanel cytoPanel = swingApplication.getCytoPanel(CytoPanelName.SOUTH);

        // If the panel is not already registered, create it
        if (show && cytoPanel.indexOfComponent("es.imim.DisGeNET.Enrichment") < 0) {
            CytoPanelComponent2 panel = new EnrichmentCytoPanel();

            // Register it
            CyActivator.getInstance().getServiceRegistrar().registerService(panel, CytoPanelComponent.class, new Properties());

            if (cytoPanel.getState() == CytoPanelState.HIDE)
                cytoPanel.setState(CytoPanelState.DOCK);

            cytoPanel.setSelectedIndex(
                    cytoPanel.indexOfComponent("es.imim.DisGeNET.Enrichment"));

        } else if (show && cytoPanel.indexOfComponent("es.imim.DisGeNET.Enrichment") >= 0) {
            EnrichmentCytoPanel panel = (EnrichmentCytoPanel) cytoPanel.getComponentAt(
                    cytoPanel.indexOfComponent("es.imim.DisGeNET.Enrichment"));
            panel.initPanel();
        } else if (!show && cytoPanel.indexOfComponent("es.imim.DisGeNET.Enrichment") >= 0) {
            int compIndex = cytoPanel.indexOfComponent("es.imim.DisGeNET.Enrichment");
            Component panel = cytoPanel.getComponentAt(compIndex);
            if (panel instanceof CytoPanelComponent2) {
                // Unregister it
                CyActivator.getInstance().getServiceRegistrar().unregisterService(panel, CytoPanelComponent.class);
            }
        }

        factory.reregister();
    }

    public static boolean isPanelRegistered() {
        CySwingApplication swingApplication = CyActivator.getInstance().getServiceRegistrar().getService(CySwingApplication.class);
        CytoPanel cytoPanel = swingApplication.getCytoPanel(CytoPanelName.SOUTH);
        return cytoPanel.indexOfComponent("es.imim.DisGeNET.Enrichment") >= 0;
    }

}
