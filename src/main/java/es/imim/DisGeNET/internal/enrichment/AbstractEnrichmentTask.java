package es.imim.DisGeNET.internal.enrichment;

import java.io.*;
import java.net.URL;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Callable;
import javax.net.ssl.HttpsURLConnection;

import es.imim.DisGeNET.exceptions.DisGeNetException;
import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyTable;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import org.cytoscape.work.util.ListSingleSelection;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


public abstract class AbstractEnrichmentTask extends AbstractTask {


    private List<JSONObject> enrichList;
    private JSONObject enrichObject;
    private String diseaseParameters;
    private CyNetwork currNetwork;
    private HttpURLConnection conn;
    private String urlParams;

    public AbstractEnrichmentTask(CyNetworkView cyNetworkView) {
        this.currNetwork =  cyNetworkView.getModel();
    }


    @Override
    public void run(TaskMonitor taskMonitor) throws Exception {
        StringBuilder inline = new StringBuilder();
        this.enrichList = new ArrayList<JSONObject>();
        try {
            byte[] postData = urlParams.getBytes(StandardCharsets.UTF_8);
            int postDataLength = postData.length;
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
            conn.setRequestProperty("charset", "utf-8");
            conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(true);
            conn.setUseCaches(false);
            try(DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
                wr.write( postData );
            }
            //conn.connect();
            int responseCode = conn.getResponseCode();
            if(responseCode!=200){
                InputStream stream = conn.getErrorStream();
                String errorMessage;
                try (Scanner scanner = new Scanner(stream)) {
                    scanner.useDelimiter("\\Z");
                    errorMessage = scanner.next();
                }
                throw new DisGeNetException("HttpResponseCode "+responseCode+"\n"+errorMessage);
            }else{
                Scanner sc = new Scanner(conn.getInputStream());
                while (sc.hasNext()){
                    inline.append(sc.nextLine());
                }
                sc.close();
                JSONParser parser = new JSONParser();
                JSONObject responseObject = (JSONObject) parser.parse(inline.toString());
                JSONArray resultArray = (JSONArray) responseObject.get("results");
                for (Object o : resultArray) {
                    JSONObject jsonObject = (JSONObject) o;
                    this.enrichList.add(jsonObject);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new Exception(e);
        }
    }

    public List<JSONObject> getEnrichList() {
        return enrichList;
    }

    public CyNetwork getCurrNetwork() {
        return currNetwork;
    }

    public void setUrlParams(String urlParams){ this.urlParams = urlParams;}

    public ListSingleSelection<Callable<CyColumn>> listNodeColumnsNames() {
        CyTable nodeTable = currNetwork.getDefaultNodeTable();
        Collection<CyColumn> nodeColumns = nodeTable.getColumns();

        List<Callable<CyColumn>> columnNames = new ArrayList<Callable<CyColumn>>();

        for(final CyColumn column : nodeColumns) {
            if(!column.isPrimaryKey()) {
                columnNames.add(new Callable<CyColumn>() {
                    public CyColumn call() {
                        return column;
                    }
                    public String toString() {
                        return column.getName();
                    }
                });
            }
        }
        return new ListSingleSelection<Callable<CyColumn>>(columnNames);
    }

    public HttpURLConnection getConn() {
        return conn;
    }

    public void openConnection(URL apiUrl) throws IOException {
        if(apiUrl.toString().startsWith("https://")){
            this.conn = (HttpsURLConnection) apiUrl.openConnection();
        }else{
            this.conn = (HttpURLConnection) apiUrl.openConnection();
        }
    }
}
