package es.imim.DisGeNET.internal.enrichment;

import es.imim.DisGeNET.internal.CyActivator;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskFactory;
import org.cytoscape.work.TaskIterator;

import java.util.Properties;

import static org.cytoscape.work.ServiceProperties.*;
import static org.cytoscape.work.ServiceProperties.IN_MENU_BAR;

public class ShowEnrichmentPanelTaskFactory extends AbstractTaskFactory {
    boolean show = false;
    boolean noSignificant = false;

    public ShowEnrichmentPanelTaskFactory() {
    }

    public TaskIterator createTaskIterator() {
        return new TaskIterator(new ShowEnrichmentPanelTask(this, show, noSignificant));
    }

    public TaskIterator createTaskIterator(boolean show, boolean noSignificant) {
        return new TaskIterator(new ShowEnrichmentPanelTask(this, show, noSignificant));
    }

    public void reregister() {
        CyActivator.getInstance().getServiceRegistrar().unregisterService(this, TaskFactory.class);
        Properties props = new Properties();
        props.setProperty(PREFERRED_MENU, "Apps.DisGeNET");
        props.setProperty(COMMAND_NAMESPACE, "disgenet");
        if (ShowEnrichmentPanelTask.isPanelRegistered()) {
            props.setProperty(TITLE, "Hide enrichment panel");
//            props.setProperty(COMMAND, "hide enrichment");
//            props.setProperty(COMMAND_DESCRIPTION,
//                    "Hide the enrichment panel");
            show = false;
        } else {
            props.setProperty(TITLE, "Show enrichment panel");
//            props.setProperty(COMMAND, "show enrichment");
//            props.setProperty(COMMAND_DESCRIPTION,
//                    "Show the enrichment panel");
            show = true;
        }
//        props.setProperty(COMMAND_SUPPORTS_JSON, "true");
//        props.setProperty(COMMAND_EXAMPLE_JSON, "{}");
        props.setProperty(MENU_GRAVITY, "2.0");
        props.setProperty(IN_MENU_BAR, "true");
        CyActivator.getInstance().getServiceRegistrar().registerService(this, TaskFactory.class, props);
    }

    public boolean isReady() {
        // We always want to be able to shut it off
        if (!show)
            return true;

        CyNetwork net = CyActivator.getInstance().getCyApplicationManager().getCurrentNetwork();
        if (net == null)
            return false;
        else {
            return true;
        }
    }
}