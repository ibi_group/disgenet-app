package es.imim.DisGeNET.internal.enrichment;


import es.imim.DisGeNET.exceptions.DisGeNetException;
import es.imim.DisGeNET.gui.GuiParameters;
import es.imim.DisGeNET.gui.GuiProps;
import es.imim.DisGeNET.internal.CyActivator;
import es.imim.DisGeNET.network.NetworkBuilder;
import es.imim.DisGeNET.tool.HelperFunctions;
import org.cytoscape.model.*;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.work.ObservableTask;
import org.cytoscape.work.TaskMonitor;
import org.cytoscape.work.Tunable;
import org.cytoscape.work.util.ListSingleSelection;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.net.URL;
import java.util.*;
import java.util.concurrent.Callable;

public class AutomationVariantEnrichmentTask extends AbstractEnrichmentTask implements ObservableTask {

    private Long selectedNetwork;
    private CyTableFactory tableFactory;
    private String apiEndpoint;
    private CyColumn selectedColumn;
    private Long tableSUID;
    private Map<Object, Long> variantNodeMap;
    private CyTable currNetNodeTable;
    private Map<String, Object> result;

    public String columnName;

    public String source;

    public String typeId;

    public String universe;

    public boolean newNetwork;

    public double pvalueThreshold;

    public Integer minNumVariants;

    public AutomationVariantEnrichmentTask(CyNetworkView cyNetworkView, Long selectedNetwork, String columnName, String source, String typeId, String universe, boolean newNetwork, double pvalueThreshold, Integer minNumVariants) {
        super(cyNetworkView);
        this.selectedNetwork = selectedNetwork;
        this.apiEndpoint = "https://www.disgenet.org/api/enrichment/variants";
        this.variantNodeMap = new HashMap<>();
        this.currNetNodeTable = super.getCurrNetwork().getDefaultNodeTable();
        this.tableFactory = CyActivator.getInstance().getTableFactoryService();
        this.columnName = columnName;
        this.source = source;
        this.typeId = typeId;
        this.universe = universe;
        this.newNetwork = newNetwork;
        this.pvalueThreshold = pvalueThreshold;
        this.minNumVariants = minNumVariants;
    }

    @Override
    public void run(TaskMonitor taskMonitor) throws Exception {
        taskMonitor.setTitle("Generating enrichment with diseases for the variants...");
        CyNetwork network = CyActivator.getInstance().getNetworkManagerService().getNetwork(selectedNetwork);
        this.selectedColumn = network.getDefaultNodeTable().getColumn(columnName);
        String stringVariantList;
        if(selectedColumn.getType().equals(String.class)){
            List<String> geneList = selectedColumn.getValues(String.class);
            stringVariantList = HelperFunctions.implodeString(",", geneList);

        }
        else{
            throw new DisGeNetException("The type of the column must be String and contain variant DBSNP SNPIDs ");
        }
        String urlParams = "".concat("source="+source+"&variants="+ stringVariantList +"&universe="+universe);
        URL apiUrl = new URL(apiEndpoint);
        super.setUrlParams(urlParams);
        super.openConnection(apiUrl);
        super.run(taskMonitor);
        List<JSONObject> enrichList = super.getEnrichList();
        this.result = new HashMap<>();
        result.put("enrichmentResult", enrichList);
        CyTable enrichTable = populateEnrichmentTable(enrichList, selectedColumn);
        Map<Long, CyTable> enrichmentTables = CyActivator.getInstance().getEnrichmentTables();
        enrichmentTables.put(super.getCurrNetwork().getSUID(), enrichTable);
        if(newNetwork){
            Long originalNetSUID = super.getCurrNetwork().getSUID();
            List <String> diseaseList = enrichTable.getColumn("diseaseid").getValues(String.class);
            List <List> variantsList = enrichTable.getColumn("intersection Variants").getValues(List.class);
            Set <String> variantSet = new HashSet<>();
            for (List<String> variantList : variantsList){
                variantSet.addAll(variantList);
            }
            String diseaseStringList = HelperFunctions.implodeString(";", diseaseList);
            String variantStringList = HelperFunctions.implodeString(";", variantSet);
            GuiParameters params = new GuiParameters();
            params.setDisSearchText(diseaseStringList);
            params.setVarSearchText(variantStringList);
            params.setActiveTab("VariantDisTabPane");
            params.setNetworkName(super.getCurrNetwork().getDefaultNetworkTable().getColumn("name").getValues(String.class).get(0)+" DisGeNET variant enrichment network");
            NetworkBuilder networkBuilder = new NetworkBuilder(params);
            try {
                networkBuilder.buildVariantDiseaseNet(params);
            } catch (DisGeNetException dsgnEx) {
                dsgnEx.getStackTrace();
                throw new DisGeNetException("Error: The query didn't return any results, check that the colum contains gene identifiers for DisGeNET (entrezId or NCBI gene symbol)");
            }
            variantNodeMap = new HashMap<>();
            CyNetwork newNet = CyActivator.getInstance().getCyApplicationManager().getCurrentNetwork();
            Map <String, Object> newNetProps = new HashMap<>();
            newNetProps.put("SUID", newNet.getSUID());
            newNetProps.put("Number of genes", variantSet.size());
            newNetProps.put("Number of diseases", diseaseList.size());
            result.put("newNetworkProperties", newNetProps);
            currNetNodeTable = newNet.getDefaultNodeTable();
            CyTable copyEnrichTable = populateEnrichmentTable(enrichList, currNetNodeTable.getColumn("variantId"));
            enrichmentTables.put(newNet.getSUID(), copyEnrichTable);
            CyNetwork net = CyActivator.getInstance().getNetworkManagerService().getNetwork(originalNetSUID);
            Collection<CyNetworkView> networkViews = CyActivator.getInstance().getNetworkViewManagerService().getNetworkViews(net);
            CyActivator.getInstance().getCyApplicationManager().setCurrentNetwork(net);
            CyActivator.getInstance().getCyApplicationManager().setCurrentNetworkView(networkViews.iterator().next());


        }
    }

    private CyTable populateEnrichmentTable(List<JSONObject> enrichList, CyColumn targetColumn) {
        CyTable enrichTable;
        enrichTable = tableFactory.createTable("Disgenet Disease Enrichment", "diseaseid", String.class, false, false);
        enrichTable.createColumn("disease name", String.class,false);
        enrichTable.createListColumn("diseaseClass", String.class,false);
        enrichTable.createListColumn("diseaseClassName", String.class,false);
//        enrichTable.createColumn("semantic type", String.class,false);
        enrichTable.createColumn("source", String.class,false);
        enrichTable.createColumn("variant_ratio", String.class, false);
        enrichTable.createColumn("# variants", Integer.class, false);
        enrichTable.createColumn("bg_ratio", String.class, false);
        enrichTable.createListColumn("intersection Variants", String.class, false);
        enrichTable.createColumn("pvalue", Double.class, false);
        enrichTable.createColumn("adjusted_pvalue", Double.class, false);
        enrichTable.createListColumn("nodes.SUID", Long.class, false);
        CyRow enrichRow;
        for (JSONObject item : enrichList) {
            JSONArray variantList = (JSONArray) item.get("intersection");
            if(variantList.size()>=minNumVariants && Double.parseDouble(item.get("adjusted_pvalue").toString())<= pvalueThreshold) {
                enrichRow = enrichTable.getRow(item.get("diseaseid"));
                enrichRow.set("source", item.get("source"));
                enrichRow.set("disease name", item.get("disease_name"));
                List<String> diseaseClasses = new ArrayList<>();
                List<String> diseaseClassesNames = new ArrayList<>();
                JSONObject mapDiseaseClass = (JSONObject) item.get("disease_class");
                for (Object key : mapDiseaseClass.keySet()) {
                    diseaseClasses.add(key.toString());
                    diseaseClassesNames.add(mapDiseaseClass.get(key).toString());
                }
                enrichRow.set("diseaseClass", diseaseClasses);
                enrichRow.set("diseaseClassName", diseaseClassesNames);
                List<String> variantIntersection = new ArrayList<>();
                List<Long> variantNodes = new ArrayList<>();
                for (Object vId : variantList) {
                    variantIntersection.add(vId.toString());
                    if (targetColumn.getType().equals(String.class)) {
                        Long variantNode = getNodeForGene(vId.toString(), targetColumn.getName());
                        if (variantNode != null) {
                            variantNodes.add(variantNode);
                        }
                    }
                }
                enrichRow.set("# variants", variantIntersection.size());
                enrichRow.set("variant_ratio", item.get("variant_ratio"));
                enrichRow.set("bg_ratio", item.get("bg_ratio"));
                enrichRow.set("intersection Variants", variantIntersection);
                enrichRow.set("pvalue", new Double(item.get("pvalue").toString()));
                enrichRow.set("adjusted_pvalue", new Double(item.get("adjusted_pvalue").toString()));
                enrichRow.set("nodes.SUID", variantNodes);
            }
        }
        return enrichTable;
    }

    private Long getNodeForGene(String variantId, String targetColumn){
        if(variantNodeMap.containsKey(variantId)) {
            return variantNodeMap.get(variantId);
        }else{
            CyColumn primaryKey = currNetNodeTable.getPrimaryKey();
            Collection <CyRow> rows = currNetNodeTable.getMatchingRows(targetColumn, variantId);
            Long nodeID = null;
            for(CyRow row : rows){
                nodeID = (Long) row.get(primaryKey.getName(),primaryKey.getType());
                variantNodeMap.put(variantId,nodeID);
            }
            return nodeID;
        }
    }

    @Override
    public <R> R getResults(Class<? extends R> type) {
        if(Map.class.equals(type)){
            return (R) result;
        }
        return null;
    }

    @Override
    public List<Class<?>> getResultClasses() {
        return null;
    }
}
