package es.imim.DisGeNET.internal.enrichment;

import java.net.URL;
import java.util.*;
import java.util.concurrent.Callable;

import es.imim.DisGeNET.exceptions.DisGeNetException;
import es.imim.DisGeNET.gui.GuiParameters;
import es.imim.DisGeNET.gui.GuiProps;
import es.imim.DisGeNET.internal.CyActivator;
import es.imim.DisGeNET.network.NetworkBuilder;
import es.imim.DisGeNET.tool.HelperFunctions;
import org.cytoscape.model.*;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.work.ObservableTask;
import org.cytoscape.work.TaskMonitor;
import org.json.simple.JSONObject;


public class AutomationGeneEnrichmentTask extends AbstractEnrichmentTask implements ObservableTask {

    private Long selectedNetwork;
    private String apiEndpoint;
    private CyColumn selectedColumn;
    private Long tableSUID;
    private Map<Object, Long> geneNodeMap;
    private CyTable currNetNodeTable;
    private CyTableFactory tableFactory;
    private Map<String, Object> result;

    public String columnName;

    public String source;

    public String typeId;

    public String universe;

    public boolean newNetwork;

    public double pvalueThreshold = 1;

    public Integer minNumGenes = 0;

    public AutomationGeneEnrichmentTask(CyNetworkView cyNetworkView, Long selectedNetwork, String columnName, String source, String typeId, String universe, boolean newNetwork, double pvalueThreshold, Integer minNumGenes) {
        super(cyNetworkView);
        this.selectedNetwork = selectedNetwork;
        this.apiEndpoint = GeneEnrichment.enrichmentURL;
        this.geneNodeMap = new HashMap<>();
        this.currNetNodeTable = super.getCurrNetwork().getDefaultNodeTable();
        this.tableFactory = CyActivator.getInstance().getTableFactoryService();
        this.columnName = columnName;
        this.source = source;
        this.typeId = typeId;
        this.universe = universe;
        this.newNetwork = newNetwork;
        this.pvalueThreshold = pvalueThreshold;
        this.minNumGenes = minNumGenes;
    }

    @Override
    public void run(TaskMonitor taskMonitor) throws Exception {
        taskMonitor.setTitle("Generating enrichment with diseases for the genes...");
        CyNetwork network = CyActivator.getInstance().getNetworkManagerService().getNetwork(selectedNetwork);
        this.selectedColumn = network.getDefaultNodeTable().getColumn(columnName);
        String stringGeneList;
        if(selectedColumn.getType().equals(String.class)){
            List <String> geneList = selectedColumn.getValues(String.class);
            stringGeneList = HelperFunctions.implodeString(",", geneList);

        }
        else if(selectedColumn.getType().equals(Integer.class)){
            List <Integer> geneList = selectedColumn.getValues(Integer.class);
            geneList = selectedColumn.getValues(Integer.class);
            stringGeneList = HelperFunctions.implodeInteger(",", geneList);
        }
        else{
            throw new DisGeNetException("The type of the column must be Integer or String and contain Gene EntrezIds sor NCBI Gene Symbols ");
        }
        String urlParams = "".concat("source="+source+"&genes="+stringGeneList+"&typeid="+typeId+"&universe="+universe);
        URL apiUrl = new URL(apiEndpoint);
        super.setUrlParams(urlParams);
        super.openConnection(apiUrl);
        super.run(taskMonitor);
        List<JSONObject> enrichList = super.getEnrichList();
        this.result = new HashMap<>();
        result.put("enrichmentResult", enrichList);
        CyTable enrichTable = populateEnrichmentTable(enrichList, selectedColumn);
        Map<Long, CyTable> enrichmentTables = CyActivator.getInstance().getEnrichmentTables();
        enrichmentTables.put(super.getCurrNetwork().getSUID(), enrichTable);
        if(newNetwork){
            Long originalNetSUID = super.getCurrNetwork().getSUID();
            List <String> diseaseList = enrichTable.getColumn("diseaseId").getValues(String.class);
            List <List> geneLists = enrichTable.getColumn("intersection EntrezID").getValues(List.class);
            Set <Integer> geneSet = new HashSet<>();
            for (List<Integer> geneList : geneLists){
                geneSet.addAll(geneList);
            }
            String diseaseStringList = HelperFunctions.implodeString(";", diseaseList);
            String geneStringList = HelperFunctions.implodeInteger(";", geneSet);
            GuiParameters params = new GuiParameters();
            params.setDisSearchText(diseaseStringList);
            params.setGenSearchText(geneStringList);
            params.setSource(source);
            params.setActiveTab("GeneDisTabPane");
            params.setNetworkName(super.getCurrNetwork().getDefaultNetworkTable().getColumn("name").getValues(String.class).get(0)+" DisGeNET gene enrichment network");
            NetworkBuilder networkBuilder = new NetworkBuilder(params);
            try {
                networkBuilder.buildGeneDiseaseNet(params);
            } catch (DisGeNetException dsgnEx) {
                dsgnEx.getStackTrace();
                throw new DisGeNetException("Error: The query didn't return any results, check that the colum contains gene identifiers for DisGeNET (entrezId or NCBI gene symbol)");
            }
            geneNodeMap = new HashMap<>();
            CyNetwork newNet = CyActivator.getInstance().getCyApplicationManager().getCurrentNetwork();
            Map <String, Object> newNetProps = new HashMap<>();
            newNetProps.put("SUID", newNet.getSUID());
            newNetProps.put("Number of genes", geneSet.size());
            newNetProps.put("Number of diseases", diseaseList.size());
            result.put("newNetworkProperties", newNetProps);
            currNetNodeTable = newNet.getDefaultNodeTable();
            CyTable copyEnrichTable = populateEnrichmentTable(enrichList, newNet.getDefaultNodeTable().getColumn("geneName"));
            enrichmentTables.put(newNet.getSUID(), copyEnrichTable);
            CyNetwork net = CyActivator.getInstance().getNetworkManagerService().getNetwork(originalNetSUID);
            Collection<CyNetworkView> networkViews = CyActivator.getInstance().getNetworkViewManagerService().getNetworkViews(net);
            CyActivator.getInstance().getCyApplicationManager().setCurrentNetwork(net);
            CyActivator.getInstance().getCyApplicationManager().setCurrentNetworkView(networkViews.iterator().next());
        }
    }

    private CyTable populateEnrichmentTable(List<JSONObject> enrichList, CyColumn targetColumn) {
        CyTable enrichTable;
        enrichTable = tableFactory.createTable("Disgenet Disease Enrichment", "diseaseId", String.class, false, false);
        enrichTable.createColumn("disease name", String.class,false);
        enrichTable.createListColumn("diseaseClass", String.class,false);
        enrichTable.createListColumn("diseaseClassName", String.class,false);
//        enrichTable.createColumn("semantic type", String.class,false);
        enrichTable.createColumn("source", String.class,false);
        enrichTable.createColumn("gene_ratio", String.class, false);
        enrichTable.createColumn("# genes", Integer.class, false);
        enrichTable.createColumn("bg_ratio", String.class, false);
        enrichTable.createListColumn("intersection EntrezID", Integer.class, false);
        enrichTable.createListColumn("intersection Symbols", String.class, false);
        enrichTable.createColumn("pvalue", Double.class, false);
        enrichTable.createColumn("adjusted_pvalue", Double.class, false);
        enrichTable.createListColumn("nodes.SUID", Long.class, false);
        CyRow enrichRow;
        for (JSONObject item : enrichList) {
            JSONObject mapIntersection = (JSONObject) item.get("intersection");
            if(mapIntersection.size()>= minNumGenes && Double.parseDouble(item.get("adjusted_pvalue").toString())<=pvalueThreshold) {
                enrichRow = enrichTable.getRow(item.get("diseaseid"));
                enrichRow.set("source", item.get("source"));
                enrichRow.set("disease name", item.get("disease_name"));
                List<String> diseaseClasses = new ArrayList<>();
                List<String> diseaseClassesNames = new ArrayList<>();
                JSONObject mapDiseaseClass = (JSONObject) item.get("disease_class");
                for (Object key : mapDiseaseClass.keySet()) {
                    diseaseClasses.add(key.toString());
                    diseaseClassesNames.add(mapDiseaseClass.get(key).toString());
                }
                enrichRow.set("diseaseClass", diseaseClasses);
                enrichRow.set("diseaseClassName", diseaseClassesNames);
                List<Integer> geneIntersection = new ArrayList<>();
                List<String> symbolIntersection = new ArrayList<>();
                List<Long> geneNodes = new ArrayList<>();
                for (Object key : mapIntersection.keySet()) {
                    geneIntersection.add(Integer.parseInt(key.toString()));
                    symbolIntersection.add(mapIntersection.get(key).toString());
                    if (targetColumn.getType().equals(String.class)) {
                        Long geneNode = getNodeForGene(mapIntersection.get(key).toString(), targetColumn.getName());
                        if (geneNode != null) {
                            geneNodes.add(geneNode);
                        }
                    } else {
                        Long geneNode = getNodeForGene(Integer.parseInt(key.toString()), targetColumn.getName());
                        if (geneNode != null) {
                            geneNodes.add(geneNode);
                        }
                    }
                }
                enrichRow.set("# genes", geneIntersection.size());
                enrichRow.set("gene_ratio", item.get("gene_ratio"));
                enrichRow.set("bg_ratio", item.get("bg_ratio"));
                enrichRow.set("intersection EntrezID", geneIntersection);
                enrichRow.set("intersection Symbols", symbolIntersection);
                enrichRow.set("pvalue", new Double(item.get("pvalue").toString()));
                enrichRow.set("adjusted_pvalue", new Double(item.get("adjusted_pvalue").toString()));
                enrichRow.set("nodes.SUID", geneNodes);
            }
        }
        return enrichTable;
    }

    private Long getNodeForGene(String geneId, String targetColumn){
        if(geneNodeMap.containsKey(geneId)) {
            return geneNodeMap.get(geneId);
        }else{
            CyColumn primaryKey = currNetNodeTable.getPrimaryKey();
            Collection <CyRow> rows = currNetNodeTable.getMatchingRows(targetColumn, geneId);
            Long nodeID = null;
            for(CyRow row : rows){
                nodeID = (Long) row.get(primaryKey.getName(),primaryKey.getType());
                geneNodeMap.put(geneId,nodeID);
            }
            return nodeID;
        }
    }

    private Long getNodeForGene(Integer geneId, String targetColumn){
        if(geneNodeMap.containsKey(geneId)) {
            return geneNodeMap.get(geneId);
        }else{
            CyColumn primaryKey = currNetNodeTable.getPrimaryKey();
            Collection <CyRow> rows = currNetNodeTable.getMatchingRows(targetColumn, geneId);
            Long nodeID = null;
            for(CyRow row : rows){
                nodeID = (Long) row.get(primaryKey.getName(),primaryKey.getType());
                geneNodeMap.put(geneId,nodeID);
            }
            return nodeID;
        }
    }


    @Override
    public <R> R getResults(Class<? extends R> type) {
        if(Map.class.equals(type)){
            return (R) result;
        }
        return null;
    }

    @Override
    public List<Class<?>> getResultClasses() {
        return null;
    }
}
