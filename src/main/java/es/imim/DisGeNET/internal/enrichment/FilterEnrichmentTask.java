package es.imim.DisGeNET.internal.enrichment;

import static org.cytoscape.view.presentation.property.BasicVisualLexicon.NODE_VISIBLE;

import es.imim.DisGeNET.exceptions.DisGeNetException;
import es.imim.DisGeNET.internal.CyActivator;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;
import org.cytoscape.work.Tunable;
import org.cytoscape.work.util.BoundedInteger;

import javax.swing.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FilterEnrichmentTask extends AbstractTask {

    private EnrichmentCytoPanel enrichmentPanel;
    private CyNetwork network;
    private CyNetworkView networkView;
    private Integer minGeneNumber;
    private Integer maxGeneNumber;
    private Double ogPValueCutoff;
    private Double adjPValueCutoff;
    private Set<CyNode> visibleNodes;
    private Set<CyNode> notVisibleNodes;
    private int[] indexCols;

    @Tunable(description="Min number of genes", groups={"Number of genes"}, params="slider=true", listenForChange="Max number of genes")
    public BoundedInteger numGeneMinimum;

    @Tunable(description="Max number of genes", groups={"Number of genes"}, params="slider=true", listenForChange="Min number of genes")
    public BoundedInteger numGeneMaximum;

    @Tunable(description="pvalue cutoff", groups={"pValue"})
    public String ogPvalue;

    @Tunable(description="Adjusted pvalue cutoff", groups={"pValue"})
    public String adjPValue;

    public FilterEnrichmentTask(EnrichmentCytoPanel enrichmentPanel, CyNetwork network, Integer lNitems, Integer hNitems, int[] indexCols) throws DisGeNetException {
        this.enrichmentPanel = enrichmentPanel;
        this.network = network;
        this.numGeneMinimum = new BoundedInteger(1, 1, hNitems, false,false);
        this.numGeneMaximum = new BoundedInteger(1,1, hNitems, false,false);
        this.networkView = CyActivator.getInstance().getNetworkViewManagerService().getNetworkViews(network).iterator().next();
        visibleNodes = new HashSet();
        notVisibleNodes = new HashSet();
        this.indexCols = indexCols;
   }


    @Override
    public void run(TaskMonitor taskMonitor) throws Exception {
        Long tableSuid = network.getSUID();
        JTable enrichTable = enrichmentPanel.enrichmentTables.get(tableSuid);
        this.ogPValueCutoff = stringToDouble(ogPvalue);
        this.adjPValueCutoff = stringToDouble(adjPValue);
        for (int i=0; i < enrichTable.getRowCount(); i++){
            int rowNGenes = (int) enrichTable.getValueAt(i,6);
            Double rowOgPValue = (Double) enrichTable.getValueAt(i, 10);
            Double rowAdjPValue = (Double) enrichTable.getValueAt(i, 11);
            Object cellContent = enrichTable.getModel().getValueAt(enrichTable.convertRowIndexToModel(i), enrichTable.getColumnCount());
            List<Long> nodesIDs;
            if (cellContent instanceof List) {
                nodesIDs = (List<Long>) cellContent;
            }else{
                nodesIDs = new ArrayList<>();
            }
            for(Long nodeSuid: nodesIDs){
                CyNode node = network.getNode(nodeSuid);
                if(rowNGenes>=numGeneMinimum.getValue() && rowNGenes<=numGeneMaximum.getValue() && rowOgPValue<ogPValueCutoff && rowAdjPValue < adjPValueCutoff){
                    visibleNodes.add(node);
                }else {
                    if(!visibleNodes.contains(node)){
                        notVisibleNodes.add(node);
                    }
                }
            }

        }
        notVisibleNodes.removeAll(visibleNodes);
        System.out.println(visibleNodes);
        System.out.println(notVisibleNodes);
        this.showHideNodes();
    }

    private void showHideNodes(){
        for (CyNode node : visibleNodes){
            networkView.getNodeView(node).clearValueLock(NODE_VISIBLE);
        }
        for (CyNode node : notVisibleNodes){
            networkView.getModel().getRow(node).set(CyNetwork.SELECTED, false);
            networkView.getNodeView(node).clearValueLock(NODE_VISIBLE);
            networkView.getNodeView(node).setLockedValue(NODE_VISIBLE, false);
            networkView.getNodeView(node).setVisualProperty(NODE_VISIBLE, false);
        }
    }

    private Double stringToDouble(String stringNumber) throws DisGeNetException {
        Double value;
        try{
            value = Double.valueOf(stringNumber).doubleValue();
        }catch (NumberFormatException nEx){
            value = 1.00;
        }catch (Exception ex){
            throw new DisGeNetException("The introduced pvalue cannot be converted to a decimal value");
        }
        return value;
    }
}
