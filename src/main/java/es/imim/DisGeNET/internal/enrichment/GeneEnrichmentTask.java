package es.imim.DisGeNET.internal.enrichment;

import java.net.URL;
import java.util.*;
import java.util.concurrent.Callable;

import es.imim.DisGeNET.exceptions.DisGeNetException;
import es.imim.DisGeNET.gui.GuiParameters;
import es.imim.DisGeNET.gui.GuiProps;
import es.imim.DisGeNET.internal.CyActivator;
import es.imim.DisGeNET.network.NetworkBuilder;
import es.imim.DisGeNET.tool.HelperFunctions;
import org.cytoscape.model.*;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.work.TaskMonitor;
import org.cytoscape.work.Tunable;
import org.cytoscape.work.util.ListSingleSelection;
import org.json.simple.JSONObject;


public class GeneEnrichmentTask extends AbstractEnrichmentTask{

    private String apiEndpoint;
    private CyColumn selectedColumn;
    private Long tableSUID;
    private Map<Object, Long> geneNodeMap;
    private CyTable currNetNodeTable;
    private CyTableFactory tableFactory;

    @Tunable(description="Select the column you want to search by: ", gravity=1,groups={"Columns"}, params="displayState=uncollapsed")
    public ListSingleSelection<Callable<CyColumn>> columnNames;

    @Tunable (description="Select the annotation source, the selected source will be used as the background of genes:", gravity=2,groups={"Sources"},params="displayState=uncollapsed")
    public ListSingleSelection<String> sources;

    @Tunable (description="Select gene identifier type:", gravity=2,groups={"Genes"},params="displayState=uncollapsed")
    public ListSingleSelection<String> typeId;

    //@Tunable (description="Select the gene universe:", gravity=2,groups={"Genes"},params="displayState=uncollapsed")
    //public ListSingleSelection<String> universe;

    @Tunable (description="Create a gene-disease network  with the enrichment results:", gravity=3, groups={"Network"}, params="displayState=uncollapsed")
    public boolean newNetwork;

    @Tunable (description="Filter the results by a p-value threshold:", gravity=4, groups={"Results filters"}, params="displayState=uncollapsed", format="0.00")
    public double pvalueThreshold = 0.05;

    @Tunable (description = "Filter the results by a minimum number of genes of the list annotated to for the disease:", gravity=5, groups={"Results filters"}, params="displayState=uncollapsed", format="0")
    public Integer minNumGenes = 0;

    public GeneEnrichmentTask(CyNetworkView cyNetworkView) {
        super(cyNetworkView);
        this.sources = new ListSingleSelection<String>(Arrays.asList(GuiProps.SOURCE_OPTIONS));
        this.typeId = new ListSingleSelection<String>(Arrays.asList("ENTREZID", "SYMBOL"));
        //this.universe = new ListSingleSelection<String>(Arrays.asList("DISGENET"));//, "HUMAN", "HUMAN_CODING"));
        this.columnNames = super.listNodeColumnsNames();
        this.apiEndpoint = GeneEnrichment.enrichmentURL;
        this.geneNodeMap = new HashMap<>();
        this.currNetNodeTable = super.getCurrNetwork().getDefaultNodeTable();
        this.tableFactory = CyActivator.getInstance().getTableFactoryService();
    }

    @Override
    public void run(TaskMonitor taskMonitor) throws Exception {
        taskMonitor.setTitle("Generating enrichment with diseases for the genes...");
        this.selectedColumn = columnNames.getSelectedValue().call();
        String stringGeneList;
        if(selectedColumn.getType().equals(String.class)){
            List <String> geneList = selectedColumn.getValues(String.class);
            stringGeneList = HelperFunctions.implodeString(",", geneList);

        }
        else if(selectedColumn.getType().equals(Integer.class)){
            List <Integer> geneList = selectedColumn.getValues(Integer.class);
            geneList = selectedColumn.getValues(Integer.class);
            stringGeneList = HelperFunctions.implodeInteger(",", geneList);
        }
        else{
            throw new DisGeNetException("The type of the column must be Integer or String and contain Gene EntrezIds sor NCBI Gene Symbols ");
        }
        String urlParams = "".concat("source="+sources.getSelectedValue()+"&genes="+stringGeneList+"&typeid="+typeId.getSelectedValue());
        URL apiUrl = new URL(apiEndpoint);
        super.setUrlParams(urlParams);
        super.openConnection(apiUrl);
        super.run(taskMonitor);
        List<JSONObject> enrichList = super.getEnrichList();
        CyTable enrichTable = populateEnrichmentTable(enrichList, selectedColumn);
        Map<Long, CyTable> enrichmentTables = CyActivator.getInstance().getEnrichmentTables();
        enrichmentTables.put(super.getCurrNetwork().getSUID(), enrichTable);
        if(newNetwork){
            Long originalNetSUID = super.getCurrNetwork().getSUID();
            List <String> diseaseList = enrichTable.getColumn("diseaseId").getValues(String.class);
            List <List> geneLists = enrichTable.getColumn("intersection EntrezID").getValues(List.class);
            Set <Integer> geneSet = new HashSet<>();
            for (List<Integer> geneList : geneLists){
                geneSet.addAll(geneList);
            }
            String diseaseStringList = HelperFunctions.implodeString(";", diseaseList);
            String geneStringList = HelperFunctions.implodeInteger(";", geneSet);
            GuiParameters params = new GuiParameters();
            params.setDisSearchText(diseaseStringList);
            params.setGenSearchText(geneStringList);
            params.setSource(sources.getSelectedValue());
            params.setActiveTab("GeneDisTabPane");
            params.setNetworkName(super.getCurrNetwork().getDefaultNetworkTable().getColumn("name").getValues(String.class).get(0)+" DisGeNET gene enrichment network");
            NetworkBuilder networkBuilder = new NetworkBuilder(params);
            try {
                networkBuilder.buildGeneDiseaseNet(params);
            } catch (DisGeNetException dsgnEx) {
                dsgnEx.getStackTrace();
                throw new DisGeNetException("Error: The query didn't return any results, check that the colum contains gene identifiers for DisGeNET (entrezId or NCBI gene symbol)");
            }
            geneNodeMap = new HashMap<>();
            CyNetwork newNet = CyActivator.getInstance().getCyApplicationManager().getCurrentNetwork();
            currNetNodeTable = newNet.getDefaultNodeTable();
            CyTable copyEnrichTable = populateEnrichmentTable(enrichList, newNet.getDefaultNodeTable().getColumn("geneName"));
            enrichmentTables.put(newNet.getSUID(), copyEnrichTable);
            CyNetwork net = CyActivator.getInstance().getNetworkManagerService().getNetwork(originalNetSUID);
            Collection<CyNetworkView> networkViews = CyActivator.getInstance().getNetworkViewManagerService().getNetworkViews(net);
            CyActivator.getInstance().getCyApplicationManager().setCurrentNetwork(net);
            CyActivator.getInstance().getCyApplicationManager().setCurrentNetworkView(networkViews.iterator().next());
        }
    }

    private CyTable populateEnrichmentTable(List<JSONObject> enrichList, CyColumn targetColumn) {
        CyTable enrichTable;
        enrichTable = tableFactory.createTable("Disgenet Disease Enrichment", "diseaseId", String.class, false, false);
        enrichTable.createColumn("disease name", String.class,false);
        enrichTable.createListColumn("diseaseClass", String.class,false);
        enrichTable.createListColumn("diseaseClassName", String.class,false);
//        enrichTable.createColumn("semantic type", String.class,false);
        enrichTable.createColumn("source", String.class,false);
        enrichTable.createColumn("gene_ratio", String.class, false);
        enrichTable.createColumn("# genes", Integer.class, false);
        enrichTable.createColumn("bg_ratio", String.class, false);
        enrichTable.createListColumn("intersection EntrezID", Integer.class, false);
        enrichTable.createListColumn("intersection Symbols", String.class, false);
        enrichTable.createColumn("pvalue", Double.class, false);
        enrichTable.createColumn("adjusted_pvalue", Double.class, false);
        enrichTable.createListColumn("nodes.SUID", Long.class, false);
        CyRow enrichRow;
        for (JSONObject item : enrichList) {
            JSONObject mapIntersection = (JSONObject) item.get("intersection");
            if(mapIntersection.size()>= minNumGenes && Double.parseDouble(item.get("adjusted_pvalue").toString())<= pvalueThreshold) {
                enrichRow = enrichTable.getRow(item.get("diseaseid"));
                enrichRow.set("source", item.get("source"));
                enrichRow.set("disease name", item.get("disease_name"));
                List<String> diseaseClasses = new ArrayList<>();
                List<String> diseaseClassesNames = new ArrayList<>();
                JSONObject mapDiseaseClass = (JSONObject) item.get("disease_class");
                for (Object key : mapDiseaseClass.keySet()) {
                    diseaseClasses.add(key.toString());
                    diseaseClassesNames.add(mapDiseaseClass.get(key).toString());
                }
                enrichRow.set("diseaseClass", diseaseClasses);
                enrichRow.set("diseaseClassName", diseaseClassesNames);
                List<Integer> geneIntersection = new ArrayList<>();
                List<String> symbolIntersection = new ArrayList<>();
                List<Long> geneNodes = new ArrayList<>();
                for (Object key : mapIntersection.keySet()) {
                    geneIntersection.add(Integer.parseInt(key.toString()));
                    symbolIntersection.add(mapIntersection.get(key).toString());
                    if (targetColumn.getType().equals(String.class)) {
                        Long geneNode = getNodeForGene(mapIntersection.get(key).toString(), targetColumn.getName());
                        if (geneNode != null) {
                            geneNodes.add(geneNode);
                        }
                    } else {
                        Long geneNode = getNodeForGene(Integer.parseInt(key.toString()), targetColumn.getName());
                        if (geneNode != null) {
                            geneNodes.add(geneNode);
                        }
                    }
                }
                enrichRow.set("# genes", geneIntersection.size());
                enrichRow.set("gene_ratio", item.get("gene_ratio"));
                enrichRow.set("bg_ratio", item.get("bg_ratio"));
                enrichRow.set("intersection EntrezID", geneIntersection);
                enrichRow.set("intersection Symbols", symbolIntersection);
                enrichRow.set("pvalue", new Double(item.get("pvalue").toString()));
                enrichRow.set("adjusted_pvalue", new Double(item.get("adjusted_pvalue").toString()));
                enrichRow.set("nodes.SUID", geneNodes);
            }
        }
        return enrichTable;
    }

    private Long getNodeForGene(String geneId, String targetColumn){
        if(geneNodeMap.containsKey(geneId)) {
            return geneNodeMap.get(geneId);
        }else{
            CyColumn primaryKey = currNetNodeTable.getPrimaryKey();
            Collection <CyRow> rows = currNetNodeTable.getMatchingRows(targetColumn, geneId);
            Long nodeID = null;
            for(CyRow row : rows){
                nodeID = (Long) row.get(primaryKey.getName(),primaryKey.getType());
                geneNodeMap.put(geneId,nodeID);
            }
            return nodeID;
        }
    }

    private Long getNodeForGene(Integer geneId, String targetColumn){
        if(geneNodeMap.containsKey(geneId)) {
            return geneNodeMap.get(geneId);
        }else{
            CyColumn primaryKey = currNetNodeTable.getPrimaryKey();
            Collection <CyRow> rows = currNetNodeTable.getMatchingRows(targetColumn, geneId);
            Long nodeID = null;
            for(CyRow row : rows){
                nodeID = (Long) row.get(primaryKey.getName(),primaryKey.getType());
                geneNodeMap.put(geneId,nodeID);
            }
            return nodeID;
        }
    }


}
