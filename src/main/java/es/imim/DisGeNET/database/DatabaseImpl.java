package es.imim.DisGeNET.database;


/**
	DisGeNET: A Cytoscape plugin to visualize, integrate, search and analyze gene-disease networks
	Copyright (C) 2010  Michael Rautschka, Anna Bauer-Mehren

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import es.imim.DisGeNET.Status;
import es.imim.DisGeNET.exceptions.DisGeNetException;
import es.imim.DisGeNET.gui.GuiParameters;
import es.imim.DisGeNET.tool.HelperFunctions;

/**
 * Class implements all queries to create networks
 * @author anna
 *
 */
public class DatabaseImpl implements Database {

	private static final long serialVersionUID = 3335292288961808603L;

	/* Instance variable from database */
	private static DatabaseImpl instance = null;

	/* DatabaseManager */
	private DatabaseManager manager = null;

	/* Constructor */
	public DatabaseImpl() {
		manager = DatabaseManagerImpl.getInstance();
		manager.init();
	}

	public DatabaseManager getManager() {
		return this.manager;
	}

	/* ****************************************************************** */

	public static DatabaseImpl getInstance() {
	
		if (instance == null) {
			instance = new DatabaseImpl();
		}
		return instance;
	}

	private String getAssociatedDiseasesBaseQuery(String geneId, String src) {
		
		String source = getGroupedSourcesQueryCondition(src);
		
		String query = "SELECT DISTINCT(diseaseId) as diseaseId FROM geneDiseaseNetwork, diseaseAttributes, geneAttributes"
				+ " WHERE geneId = "
				+ geneId
				+ " AND geneAttributes.geneNID=geneDiseaseNetwork.geneNID"
				+ " AND diseaseAttributes.diseaseNID=geneDiseaseNetwork.diseaseNID AND"
				+ source;

		return query;

	}
	
	private String getVariantAssociatedDiseasesBaseQuery(String variantId, String src) {
				
		src = getVariantGroupedSourcesQueryCondition(src);
		
		String query = "SELECT DISTINCT(diseaseId) as diseaseId FROM variantDiseaseNetwork, diseaseAttributes, variantAttributes"
				+ " WHERE variantId = "
				+ variantId
				+ " AND variantAttributes.variantNID=variantDiseaseNetwork.variantNID"
				+ " AND diseaseAttributes.diseaseNID=variantDiseaseNetwork.diseaseNID AND"
				+ src;

		return query;

	}


	@Override
	public ArrayList<String> getAssociatedDiseases(String geneId, String src,
			GuiParameters params) {
		
		ArrayList<String> associatedDiseases = new ArrayList<String>();

		String query = this.getAssociatedDiseasesBaseQuery(geneId, src);

		ResultSet rs = this.runQuery(query);

		try {
			
		    int count = rs.getRow();
			while (rs.next() && !params.isNetBuildInterrupted()) {
				associatedDiseases.add(rs.getString("diseaseId"));
			}
			rs.close();
			Statement tmpStmt = rs.getStatement();
			tmpStmt.close();

			
		} catch (SQLException e) {
			e.printStackTrace();
		} 

		return associatedDiseases;
	}


	@Override
	public ArrayList<String> getAssociatedDiseases(String geneId, String src,
			String assocType, GuiParameters params) {
		ArrayList<String> associatedDiseases = new ArrayList<String>();
		String query = this.getAssociatedDiseasesBaseQuery(geneId, src);
		//query = query + " AND " + assocType;
		query += " AND "+this.getAssocTypeQueryCondition(assocType);

		ResultSet subRs = this.runQuery(query);
		try {
			while (subRs.next() && !params.isNetBuildInterrupted()) {
				associatedDiseases.add(subRs.getString("diseaseId"));
			}
			//subRs.close();
			Statement tmpStmt =subRs.getStatement();//EC
			tmpStmt.close();//EC
			subRs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return associatedDiseases;
	}
	
	public List<String> getVariantAssociatedDiseases(String variantId, String src, String assocType, GuiParameters params){
		List<String> associatedDiseases = new ArrayList<String>();
		String query = this.getVariantAssociatedDiseasesBaseQuery(variantId, src);
		query += " AND "+this.getVariantAssocTypeQueryCondition(assocType);
		
		ResultSet subRs = this.runQuery(query);
		
		try {
			while (subRs.next() && !params.isNetBuildInterrupted()) {
				associatedDiseases.add(subRs.getString("diseaseId"));
			}
			Statement tmpStmt =subRs.getStatement();
			tmpStmt.close();//EC
			subRs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return associatedDiseases;
		
	}
	
	@Override
	public ArrayList<String> getAssociatedGeneNames(String diseaseId,
			String src, String assocType, GuiParameters params) {
		ArrayList<String> geneNames = new ArrayList<String>();

		Query query_obj = new Query();

		query_obj.addColumn("DISTINCT geneName");

		query_obj.addTable("geneAttributes");
		query_obj.addTable("geneDiseaseNetwork");
		query_obj.addTable("diseaseAttributes");

		query_obj.addCondition("geneAttributes.geneNID=geneDiseaseNetwork.geneNID");
		query_obj.addCondition("diseaseAttributes.diseaseId=\""+diseaseId+"\"");
		query_obj.addCondition("diseaseAttributes.diseaseNID=geneDiseaseNetwork.diseaseNID");
		query_obj.addCondition("source=\""+src+"\"");

		if( !assocType.equals("Any") ){
			query_obj.addCondition("associationType=\""+assocType+"\"");
		}

		String query = query_obj.build(); 

		ResultSet subRs = this.runQuery(query);
		try {
			while (subRs.next() && !params.isNetBuildInterrupted()) {
				String geneName = subRs.getString("geneName");
				geneNames.add(geneName);
			}
			//subRs.close();
			Statement tmpStmt = subRs.getStatement(); //EC
			tmpStmt.close();//EC
			subRs.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return geneNames;
	}

	@Override
	public HashMap<String, String> getDiseaseClassForAllDiseases(GuiParameters params) {

		HashMap<String, String> allDiseaseWithClass = new HashMap<String, String>();

		//String query = "select diseaseId, diseaseClassName from diseaseAttributes, diseaseClass WHERE diseaseAttributes.diseaseClassNID=diseaseClass.diseaseClassNID;";

		Query query_obj = new Query();

		query_obj.addColumn("DISTINCT diseaseId");
		query_obj.addColumn("diseaseClassName");

		query_obj.addTable("diseaseAttributes");
		query_obj.addTable("disease2class");
		query_obj.addTable("diseaseClass");


		query_obj.addCondition("diseaseAttributes.diseaseNID=disease2class.diseaseNID");
		query_obj.addCondition("disease2class.diseaseClassNID=diseaseClass.diseaseClassNID");

		ResultSet rs = null;
		rs = this.runQuery(query_obj.build());

		try {
			
		    int count = rs.getRow();
		    		    
			while (rs.next() && !params.isNetBuildInterrupted()) {
				// iterate over fields and store field id + value
				String diseaseId = rs.getString("diseaseId");
				String diseaseClass = rs.getString("diseaseClassName");
				allDiseaseWithClass.put(diseaseId, diseaseClass);

			}
			//rs.close();
			Statement tmpStmt = rs.getStatement();//EC
			tmpStmt.close();//EC
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return allDiseaseWithClass;
	}

	@Override
	public HashMap<String, String> getDiseaseClassForAllGenes(String source, GuiParameters params) {
		HashMap<String, String> allGenesWithClass = new HashMap<String, String>();
		

		// get disease class mapping
		HashMap<String, String> allDiseases = getDiseaseClassForAllDiseases(params);
		String query = "select geneId from geneAttributes;";

		ResultSet rs = null;
		rs = this.runQuery(query);

		try {
			
		    int count = rs.getRow();
		    		    
			while (rs.next() && !params.isNetBuildInterrupted()) {
				// iterate over fields and store field id + value
				String geneId = rs.getString("geneId");

				String diseaseClass = "null";
				ArrayList<String> diseaseClasses = new ArrayList<String>();
				ArrayList<String> diseases = getAssociatedDiseases(geneId,
						source, params);
				Iterator<String> it = diseases.iterator();
				while (it.hasNext()) {
					String[] disClasses = allDiseases.get(it.next())
							.split(", ");
					
					for (String disClass : disClasses) {
						if (!diseaseClasses.contains(disClass)) {
							diseaseClasses.add(disClass);
						}
					}
				}
				if (diseaseClasses.size() > 0) {
					Collections.sort(diseaseClasses);
					diseaseClass = diseaseClasses.toString();
					diseaseClass = diseaseClass.replace("]", "");
					diseaseClass = diseaseClass.replace("[", "");
				}
				//(geneId);
				//( diseaseClass);
				allGenesWithClass.put(geneId, diseaseClass);

			}
//			rs.close();
			Statement tmpStmt = rs.getStatement();
			tmpStmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return allGenesWithClass;
	}

	@Override
	public String getDiseaseClassId(String diseaseClassName,GuiParameters params) {

		String diseaseClass = "";

		String query = "select * from diseaseClass where diseaseClassName LIKE \"%"
				+ diseaseClassName + "%\";";
		//(query);
		ResultSet rs = null;

		rs = this.runQuery(query);

		try {
			while (rs.next() && !params.isNetBuildInterrupted()) {
				// iterate over fields and store field id + value
				diseaseClass = rs.getString("diseaseClass");
			}
//			rs.close();
			Statement tmpStmt = rs.getStatement();
			tmpStmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return diseaseClass;

	}

	//CREATED BY JAVI
	private String getGroupedSourcesQueryCondition(String src){
		String query = "";
		if(src!=null) {
			if( src.equals("CURATED") ){
				//query = "source IN (\"UNIPROT\",\"CTD_human\")";
				query = "source IN (\"UNIPROT\",\"CTD_human\",\"ORPHANET\",\"PSYGENET\",\"CGI\",\"CLINGEN\",\"GENOMICS_ENGLAND\")";
			}
			else if( src.equals("ANIMAL_MODELS")){
				query = "source IN (\"CTD_mouse\",\"CTD_rat\",\"RGD\",\"MGD\")";
			}
			else if( src.equals("INFERRED")) {
				query = "source IN (\"HPO\",\"GWASDB\",\"GWASCAT\",\"CLINVAR\")";
			}
			else if( src.equals("ALL")){
				query = "";
			}
			else{
				query = "source=\""+src+"\"";
			}
		}
		return query;
	}
	
	private String getVariantGroupedSourcesQueryCondition(String src) {
		String query = "";
		if (src!=null) {
			if (src.equals("CURATED")) {
				query = "source IN (\"UNIPROT\",\"CLINVAR\",\"GWASCAT\",\"GWASDB\")";
			} else if (src.equals("ALL")) {
				query = "";
			} else {
				query = "source=\"" + src + "\"";
			} 
		}
		return query;
	}

	///CREATED BY JAVI
	private String getAssocTypeQueryCondition(String assocType){
		String query_condition = "";
		if (assocType!=null) {
			if (assocType.matches("Therapeutic")) {
				query_condition = "associationType = \"Therapeutic\"";
			} else if (assocType.matches("Genetic Alteration")) {
				query_condition = "associationType IN (" + "\"GeneticVariation\", " + "\"CausalMutation\", "
						+ "\"GermlineCausalMutation\", " + "\"GermlineModifyingMutation\", " + "\"ModifyingMutation\", "
						+ "\"SomaticCausalMutation\", " + "\"SomaticModifyingMutation\", "
						+ "\"ChromosomalRearrangement\", " + "\"FusionGene\", " + "\"SusceptibilityMutation\")";
			} else if (assocType.matches("Genetic Variation")) {
				query_condition = "associationType IN (" + "\"GeneticVariation\", " + "\"CausalMutation\", "
						+ "\"GermlineCausalMutation\", " + "\"GermlineModifyingMutation\", " + "\"ModifyingMutation\", "
						+ "\"SomaticCausalMutation\", " + "\"SomaticModifyingMutation\", "
						+ "\"SusceptibilityMutation\")";
			} else if (assocType.matches("Causal Mutation")) {
				query_condition = "associationType IN ( \"GermlineCausalMutation\", "
						+ "\"SomaticCausalMutation\")";
			} else if (assocType.matches("Somatic Mutation")) {
				query_condition = "associationType IN (" + "\"GermlineModifyingMutation\", "
						+ "\"SomaticModifyingMutation\")";
			} else if (assocType.matches("Biomarker")) {
				query_condition = "associationType IN (" + "\"GeneticVariation\", " + "\"Biomarker\", "
						+ "\"CausalMutation\", " + "\"ChromosomalRearrangement\", " + "\"FusionGene\", "
						+ "\"GermlineCausalMutation\", " + "\"GermlineModifyingMutation\", " + "\"ModifyingMutation\", "
						+ "\"SomaticCausalMutation\", " + "\"SomaticModifyingMutation\", "
						+ "\"SusceptibilityMutation\", " + "\"AlteredExpression\", "
						+ "\"PostTranslationalModification\")";
			} else if (assocType.matches("Germline Causal Mutation")) {
				query_condition = "associationType = \"GermlineCausalMutation\"";
			} else if (assocType.matches("Somatic Causal Mutation")) {
				query_condition = "associationType = \"SomaticCausalMutation\"";
			} else if (assocType.matches("Germline Modifying Mutation")) {
				query_condition = "associationType = \"GermlineModifyingMutation\"";
			} else if (assocType.matches("Somatic Modifying Mutation")) {
				query_condition = "associationType = \"SomaticModifyingMutation\"";
			} else if (assocType.matches("Fusion Gene")) {
				query_condition = "associationType = \"FusionGene\"";
			} else if (assocType.matches("Chromosomal Rearrangement")) {
				query_condition = "associationType = \"ChromosomalRearrangement\"";
			} else if (assocType.matches("Susceptibility Mutation")) {
				query_condition = "associationType = \"SusceptibilityMutation\"";
			} else if (assocType.matches("Altered Expression")) {
				query_condition = "associationType = \"AlteredExpression\"";
			} else if (assocType.matches("PostTranslational Modification")) {
				query_condition = "associationType = \"PostTranslationalModification\"";
			} else if (assocType.equals("Any")) {
				query_condition = "";
			} 
		}
		return query_condition;
	}
	
	private String getVariantAssocTypeQueryCondition(String assocType) {
		String query_condition = "";
		if (assocType!=null) {
			if (assocType.matches("Causal Mutation")) {
				query_condition = "associationType = \"CausalMutation\"";
			} else if (assocType.matches("Susceptibility Mutation")) {
				query_condition = "associationType = 'SusceptibilityMutation'";
			} else if (assocType.matches("Genetic Variation")) {
				query_condition = "associationType IN (" + "\"GeneticVariation\", " + "\"CausalMutation\", "
						+ "\"SusceptibilityMutation\")";
			}
		}else {
			query_condition = "";
		}
		
		return query_condition;
	}
	
	//CREATED BY JAVI
	
	private String getDiseaseClassQueryCondition(String disClass, String diseaseAttributesTable_alias, String diseaseClassTable_alias, String disease2class_alias){
		String query_condition = "";
		if( !(disClass.equals("Any") || disClass.equals("")) ){

			query_condition = diseaseClassTable_alias+".diseaseClassNID="+disease2class_alias+".diseaseClassNID"
					+ " AND "+disease2class_alias+".diseaseNID="+diseaseAttributesTable_alias+".diseaseNID"
					+ " AND "+diseaseClassTable_alias+".diseaseClassName LIKE \"%"+disClass+"%\"";
		}
		return query_condition;
	}

	//CREATED BY JAVI
	private String getDiseaseClassQueryCondition(String disClass){
		if(disClass!=null) {
			disClass = getDiseaseClassQueryCondition(disClass, "diseaseAttributes", "diseaseClass", "disease2class");
		}else {
			disClass = "";
		}
		return disClass;
	}

	public ArrayList<String> getGeneList(GuiParameters curParams) throws SQLException {

		ArrayList<String> list = new ArrayList<String>();

		String src = curParams.getSource();

		String source_query = this.getGroupedSourcesQueryCondition(src);

		Query query_obj = new Query();

		query_obj.addColumn("DISTINCT geneName");

		query_obj.addTable("geneAttributes");

		if( !source_query.equals("") ){
			query_obj.addTable("geneDiseaseNetwork");
			query_obj.addCondition("geneDiseaseNetwork.geneNID=geneAttributes.geneNID");
			query_obj.addCondition(source_query);
		}
		
		String assocType = curParams.getAssociationType();
		String assoc_type_condition = this.getAssocTypeQueryCondition(assocType);


		if (!assocType.equals("Any")) {
			query_obj.addCondition(assoc_type_condition);
		}
		
		String disClass = curParams.getDiseaseClass();
		String disClass_query = this.getDiseaseClassQueryCondition(disClass);
		
		if( !disClass.equals("Any") ){
			query_obj.addTable("diseaseClass");
			query_obj.addTable("disease2Class");	
			query_obj.addTable("diseaseAttributes");
			query_obj.addCondition("diseaseAttributes.diseaseNID=geneDiseaseNetwork.diseaseNID"); //remove
			query_obj.addCondition(disClass_query);
		}

		ResultSet rs = null;
		rs = this.runQuery(query_obj.build());
					   		    		    
		while (rs.next() && !curParams.isNetBuildInterrupted()) {
			String geneName = rs.getString("geneName");
			list.add(geneName);
		}
		//			rs.close();
		Statement tmpStmt = rs.getStatement();
		tmpStmt.close();
		rs.close();
		

		// sort list in ascending order so the shortest entries will show up
		// (which are limited to 50 in SearchField at the moment)
		// without limiting the number desc order and addElementAt(...,0) in
		// SearchField might be better

		Collections.sort(list, new HelperFunctions().new byLineLengthAsc());
		return list;
	}

	public ArrayList<String> getDiseaseList(GuiParameters curParams) {

		ArrayList<String> list = new ArrayList<String>();

		// source
		String src = curParams.getSource();
		String src_conditions = this.getGroupedSourcesQueryCondition(src);

		// disease class
		String disClass = curParams.getDiseaseClass();

		Query query_obj = new Query();
		
		query_obj.addColumn("DISTINCT diseaseName");
		
		query_obj.addTable("diseaseAttributes");

		if( !src_conditions.equals("") ){
			query_obj.addTable("geneDiseaseNetwork");
			query_obj.addCondition(src_conditions);
			query_obj.addCondition("geneDiseaseNetwork.diseaseNID=diseaseAttributes.diseaseNID");
		}
		if( !disClass.equals("Any") ){
			query_obj.addTable("diseaseClass");
			query_obj.addTable("disease2class");
			query_obj.addCondition(this.getDiseaseClassQueryCondition(disClass));
	}
		String query = query_obj.build();

		ResultSet rs = null;	
		
		rs = this.runQuery(query);

		try {
					    
			while (rs.next() && !curParams.isNetBuildInterrupted()) {
				String disName = rs.getString("diseaseName");
				list.add(disName);
			}
			Statement tmpStmt = rs.getStatement();
			tmpStmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Collections.sort(list, new HelperFunctions().new byLineLengthAsc());

		return list;
	}
		
	public ArrayList<String> getVariantList (GuiParameters curParams){
		ArrayList<String> list = new ArrayList<String>();
		
		String src = curParams.getSource();

		String source_query = this.getVariantGroupedSourcesQueryCondition(src);
		
		String assocType = curParams.getAssociationType();
		String assoc_type_condition = this.getVariantAssocTypeQueryCondition(assocType);
		
		String disClass = curParams.getDiseaseClass();
		String disClass_query = this.getDiseaseClassQueryCondition(disClass);
		
		Query query_obj = new Query();

		query_obj.addColumn("DISTINCT variantId");

		query_obj.addTable("variantAttributes");
		
		if( !source_query.equals("") || !assocType.equals("Any") || !disClass.equals("Any") ){
			query_obj.addTable("variantDiseaseNetwork");
			query_obj.addCondition("variantDiseaseNetwork.variantNID = variantAttributes.variantNID");
			if( !source_query.equals("")){
				query_obj.addCondition(source_query);
			}
			if (!assocType.equals("Any")) {
				query_obj.addCondition(assoc_type_condition);
			}
			
			if( !disClass.equals("Any") ){
				query_obj.addTable("diseaseClass");
				query_obj.addTable("diseaseAttributes");
				query_obj.addTable("disease2Class");	
				query_obj.addCondition("diseaseAttributes.diseaseNID=variantDiseaseNetwork.diseaseNID"); //remove
				query_obj.addCondition(disClass_query);
			}
			
		}

		String query = query_obj.build();
		ResultSet rs = null;	
		rs = this.runQuery(query);

		try {
					    		    		    
			while (rs.next() && !curParams.isNetBuildInterrupted()) {
				String variantId = rs.getString("variantId");
				list.add(variantId);
			}
//			rs.close();
			Statement tmpStmt = rs.getStatement();
			tmpStmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Collections.sort(list, new HelperFunctions().new byLineLengthAsc());
		return list;
	}
	
	public ArrayList<String> getDiseaseListVariant(GuiParameters curParams){
		
		ArrayList<String> list = new ArrayList<String>();

		// source
		String src = curParams.getSource();
		String src_conditions = getVariantGroupedSourcesQueryCondition(src);

		String assocType = curParams.getAssociationType();
		String assoc_type_condition = getVariantAssocTypeQueryCondition(assocType);
				
		// disease class
		String disClass = curParams.getDiseaseClass();
		
		Query query_obj = new Query();
				
		query_obj.addColumn("DISTINCT diseaseName");
		
		query_obj.addTable("diseaseAttributes");
		
		
		if( !src_conditions.equals("") || !assocType.equals("Any") || !disClass.equals("Any") ){
			query_obj.addTable("variantDiseaseNetwork");
			query_obj.addCondition("variantDiseaseNetwork.diseaseNID=diseaseAttributes.diseaseNID");
			if( !src_conditions.equals("")){
				query_obj.addCondition(src_conditions);
			}
			if (!assocType.equals("Any")) {
				query_obj.addCondition(assoc_type_condition);
			}
			
			if( !disClass.equals("Any") ){
				query_obj.addTable("diseaseClass");
				query_obj.addTable("disease2class");
				query_obj.addCondition(this.getDiseaseClassQueryCondition(disClass));
			}
			
		}
				
		String query = query_obj.build();
		ResultSet rs = null;	
		
		//int rowCount = this.numRowsQuery(queryCount);
		rs = this.runQuery(query);

		try {
					    
			while (rs.next() && !curParams.isNetBuildInterrupted()) {
				String disName = rs.getString("diseaseName");
				list.add(disName);
			}
			//rs.close();
			Statement tmpStmt = rs.getStatement();
			tmpStmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		// sort list in ascending order so the shortest entries will show up
		// (which are limited to 50 in SearchField at the moment)
		// without limiting the number desc order and addElementAt(...,0) in
		// SearchField might be better
		// Collections.sort(list, new byLineLengthDesc());

		//JAVI
		//Uncommented
		Collections.sort(list, new HelperFunctions().new byLineLengthAsc());
		return list;
	}
	
	public ArrayList<String> getGeneListVariant(GuiParameters curParams){
		
		ArrayList<String> list = new ArrayList<String>();
		
		String src = curParams.getSource();
		String src_conditions = this.getVariantGroupedSourcesQueryCondition(src);

		String assocType = curParams.getAssociationType();
		String assoc_type_condition = this.getVariantAssocTypeQueryCondition(assocType);
		
		String disClass = curParams.getDiseaseClass();
		String disClassCondition = "";
		if(!disClass.equals("Any")) {
			disClassCondition = "AND diseaseClass.diseaseClass LIKE '%"+disClass+"%'";
		}
		
		Query query_obj = new Query();
		Query subQuery_obj = new Query();
		
		query_obj.addColumn("DISTINCT geneAttributes.geneName");
		
				
		if( !src_conditions.equals("") || !assocType.equals("Any") || !disClass.equals("Any")){
			
			subQuery_obj.addColumn("DISTINCT variantNID");
			subQuery_obj.addTable("variantDiseaseNetwork INNER JOIN (\n" + 
					"			 	diseaseAttributes disAtt LEFT JOIN (disease2class\n" + 
					"			 		INNER JOIN diseaseClass \n" + 
					"			 		ON disease2class.diseaseClassNID=diseaseClass.diseaseClassNID "+disClassCondition+" ) AS disClass\n" + 
					"			 		ON disClass.diseaseNID=disAtt.diseaseNID) AS disToVar \n" + 
					"			 		ON disToVar.diseaseNID=variantDiseaseNetwork.diseaseNID ");
			if(!src_conditions.equals("")) {
				subQuery_obj.addCondition(src_conditions);
			}
			if(!assoc_type_condition.equals("")) {
				subQuery_obj.addCondition(assoc_type_condition);
			}
			String subQuery = subQuery_obj.build();
			query_obj.addTable("geneAttributes INNER JOIN variantGene\n" + 
					"		ON geneAttributes.geneNID = variantGene.geneNID AND variantGene.variantNID IN\n" + 
					"			("+subQuery+");");
		}else{
			query_obj.addTable("geneAttributes");
		}
				
		String query = query_obj.build();
		ResultSet rs = null;	
		
		//int rowCount = this.numRowsQuery(queryCount);
		rs = this.runQuery(query);

		try {
					    
			while (rs.next() && !curParams.isNetBuildInterrupted()) {
				String geneName = rs.getString("geneName");
				list.add(geneName);
			}
			//rs.close();
			Statement tmpStmt = rs.getStatement();
			tmpStmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		//JAVI
		//Uncommented
		Collections.sort(list, new HelperFunctions().new byLineLengthAsc());
		return list;
	}
		
	/**
	 * FOR SQLITE ONLY, throws a previous select to count the number of rows the main query will return
	 * @param query the count rows query
	 * @return number of rows the query will return.
	 */
	private int numRowsQuery(String query) {
		ResultSet rsCount =  this.runQuery(query);
		int rowNum;
		try {
			rowNum = rsCount.getInt("RowCount");
			if(rsCount!=null) {
				Statement tmpStmt = rsCount.getStatement();
				tmpStmt.close();
				rsCount.close();
			}
		} catch (SQLException e) {
			rowNum = 0;
			e.printStackTrace();
		}
		
		return rowNum;
	}
	
	/**
	 * Adds the given tables and conditions to a the given queries.
	 * @param queriesArray an arrayList of query objects.
	 * @param tables string array containing the tables for the queries.
	 * @param conditions string array containig the conditions
	 * @return the queries with the new parameters, remember to build when retriving.
	 */
	private ArrayList<Query> multiQueryBuilder( ArrayList<Query> queriesArray, 
								 	ArrayList<String> tables, 
							 		ArrayList<String> conditions)
	{
		for (Query query : queriesArray) {
			for (String table : tables) {
				query.addTable(table);
			}
			for (String condition : conditions) {
				query.addCondition(condition);
			}
		}
		
		return queriesArray;
	}

	@Override
	public String getGeneIdWildCardGda(String geneName, GuiParameters params) {
		String geneId = "";
		String geneId2 = "";

		ArrayList<String> allGeneIds = new ArrayList<String>();

		//JAVI TOCHECK: IT IS THE SAME LIKE "" THAN = ""?
		String query = "select geneId "
				+ "from geneAttributes INNER JOIN geneDiseaseNetwork g2d ON geneAttributes.geneNID = g2d.geneNID "
				+ "where geneName like \"%"
				+ geneName + "%\" AND source = "+params.getSource()+";";

		ResultSet rs = null;
		rs = this.runQuery(query);
		//(query);

		try {
			while (rs.next() && !params.isNetBuildInterrupted()) {
				// iterate over fields and store field id + value
				geneId = rs.getString("geneId");
				allGeneIds.add(geneId);
			}

//			rs.close();
			Statement tmpStmt = rs.getStatement();
			tmpStmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (!allGeneIds.isEmpty()) {
			Iterator<String> geneIdsIt = allGeneIds.iterator();
			geneId = "";
			while (geneIdsIt.hasNext()) {
				String gene = geneIdsIt.next();
				geneId += "or geneId = '" + gene + "'";
				geneId2 += ", '" + gene + "'";
			}
			geneId = geneId.replaceFirst("or geneId = ", "");
			geneId2 = geneId2.replaceFirst(", ", "");
		}
		return geneId2;
	}
	
	public String getGeneIdWildCardVda(String geneName, GuiParameters params) {
		String geneId = "";
		String geneId2 = "";

		ArrayList<String> allGeneIds = new ArrayList<String>();

		//JAVI TOCHECK: IT IS THE SAME LIKE "" THAN = ""?
		String query = "select geneId "
				+ "from geneAttributes INNER JOIN variantGene g2v ON geneAttributes.geneNID = g2v.geneNID "
				+ "where geneName like \"%"
				+ geneName + "%\" AND source = "+params.getSource()+";";

		ResultSet rs = null;
		rs = this.runQuery(query);
		//(query);

		try {
			while (rs.next() && !params.isNetBuildInterrupted()) {
				// iterate over fields and store field id + value
				geneId = rs.getString("geneId");
				allGeneIds.add(geneId);
			}

//			rs.close();
			Statement tmpStmt = rs.getStatement();
			tmpStmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (!allGeneIds.isEmpty()) {
			Iterator<String> geneIdsIt = allGeneIds.iterator();
			geneId = "";
			while (geneIdsIt.hasNext()) {
				String gene = geneIdsIt.next();
				geneId += "or geneId = '" + gene + "'";
				geneId2 += ", '" + gene + "'";
			}
			geneId = geneId.replaceFirst("or geneId = ", "");
			geneId2 = geneId2.replaceFirst(", ", "");
		}
		return geneId2;
	}
	
	@Override
	public String getGeneId(String geneName) {
		String geneId = "";

		String query = "SELECT geneId FROM geneAttributes where geneName = \""
				+ geneName + "\";";

		ResultSet rs = null;
		rs = this.runQuery(query);
		//(query);

		try {
			while (rs.next()) {
				// iterate over fields and store field id + value
				geneId = rs.getString("geneId");
			}

//			rs.close();
			Statement tmpStmt = rs.getStatement();
			tmpStmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return geneId;
	}
			
	@Override
	public ArrayList<String> getGeneIdList(GuiParameters params) {

		ArrayList<String> genes = new ArrayList<String>();
		String geneId = "";
		String query = "SELECT DISTINCT(geneId) from geneAttributes;";

		ResultSet rs = null;
		rs = this.runQuery(query);

		try {
			while (rs.next() && !params.isNetBuildInterrupted()) {
				// iterate over fields and store field id + value
				geneId = rs.getString("geneId");
				genes.add(geneId);
			}
			
			Statement tmpStmt = rs.getStatement();
			tmpStmt.close();
			rs.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return genes;
	}

	@Override
	public String getDiseaseIdWildCardGDA(String diseaseName, GuiParameters params) {


		String diseaseId = "";
		String diseaseId2 = "";
		ArrayList<String> allDiseaseIds = new ArrayList<String>();
		String query = "SELECT diseaseId FROM diseaseAttributes "
				+ "INNER JOIN geneDiseaseNetwork g2d ON diseaseAttributes.diseaseNID=g2d.diseaseNID"
				+ " WHERE diseaseName like \"%"
				+ diseaseName.toLowerCase() + "%\" AND g2d.source="+params.getSource()+";";
		ResultSet rs = null;
		rs = this.runQuery(query);

		try {
			
			while (rs.next() && !params.isNetBuildInterrupted()) {
				// iterate over fields and store field id + value
				diseaseId = rs.getString("diseaseId");
				allDiseaseIds.add(diseaseId);
			}
//			rs.close();
			Statement tmpStmt = rs.getStatement();
			tmpStmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (!allDiseaseIds.isEmpty()) {
			diseaseId = "";
			diseaseId2 = "";
			Iterator<String> diseaseIdsIt = allDiseaseIds.iterator();
			while (diseaseIdsIt.hasNext()) {
				String disease = diseaseIdsIt.next();
				diseaseId += " or diseaseId = '" + disease + "'";
				diseaseId2 += ", '" + disease + "'";
			}
			diseaseId = diseaseId.replaceFirst(" or diseaseId = ", "");
			diseaseId2 = diseaseId2.replaceFirst(", ", "");
		}
		return diseaseId2;
	}
	
	public String getDiseaseIdWildCardVDA(String diseaseName, GuiParameters params) {


		String diseaseId = "";
		String diseaseId2 = "";
		ArrayList<String> allDiseaseIds = new ArrayList<String>();
		String query = "SELECT diseaseId FROM diseaseAttributes "
				+ "INNER JOIN variantDiseaseNetwork v2d ON diseaseAttributes.diseaseNID=g2d.diseaseNID"
				+ " WHERE LOWER (diseaseName) like \"%"
				+ diseaseName.toLowerCase() + "%\" AND v2d.source="+params.getSource()+";";
		ResultSet rs = null;
		rs = this.runQuery(query);

		try {
			
			while (rs.next() && !params.isNetBuildInterrupted()) {
				// iterate over fields and store field id + value
				diseaseId = rs.getString("diseaseId");
				allDiseaseIds.add(diseaseId);
			}
//			rs.close();
			Statement tmpStmt = rs.getStatement();
			tmpStmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (!allDiseaseIds.isEmpty()) {
			diseaseId = "";
			diseaseId2 = "";
			Iterator<String> diseaseIdsIt = allDiseaseIds.iterator();
			while (diseaseIdsIt.hasNext()) {
				String disease = diseaseIdsIt.next();
				diseaseId += " or diseaseId = '" + disease + "'";
				diseaseId2 += ", '" + disease + "'";
			}
			diseaseId = diseaseId.replaceFirst(" or diseaseId = ", "");
			diseaseId2 = diseaseId2.replaceFirst(", ", "");
		}
		return diseaseId2;
	}

	public String getDiseaseId(String diseaseName) {


		String diseaseId = "";

		Query query_obj = new Query();

		query_obj.addColumn("diseaseId");
		query_obj.addTable("diseaseAttributes");
		query_obj.addCondition("diseaseName=\""+diseaseName+"\"");

		ResultSet rs = this.runQuery(query_obj.build());
		
		try {
			while (rs.next()) {
				diseaseId = rs.getString("diseaseId");
			}
			Statement tmpStmt = rs.getStatement();
			tmpStmt.close();
			rs.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return diseaseId;
	}
	
	public String getVariantIdWildCard(String variantId, GuiParameters params) {
		
		String variantId2 = "";

		ArrayList<String> allVariantIds = new ArrayList<String>();

		String query = "select variantId from variantAttributes INNER JOIN  where variantId like \"%"
				+ variantId + "%\";";

		ResultSet rs = null;
		rs = this.runQuery(query);

		try {
			while (rs.next() && !params.isNetBuildInterrupted()) {
				// iterate over fields and store field id + value
				variantId = rs.getString("variantId");
				allVariantIds.add(variantId);
			}
			Statement tmpStmt = rs.getStatement();
			tmpStmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (!allVariantIds.isEmpty()) {
			Iterator<String> variantIdsIt = allVariantIds.iterator();
			variantId = "";
			while (variantIdsIt.hasNext()) {
				String variant = variantIdsIt.next();
				variantId += "or variantId = '" + variant + "'";
				variantId2 += ", '" + variant + "'";
			}
			variantId = variantId.replaceFirst("or variantId = ", "");
			variantId2 = variantId2.replaceFirst(", ", "");
		}
		return variantId2;
	}

	@Override
		public HashMap<String, HashMap<String, String>> getDiseaseAttributes(GuiParameters params) throws DisGeNetException {
			HashMap<String, HashMap<String, String>> diseaseAttributesALL = new HashMap<String, HashMap<String, String>>();
	
			String src = params.getSource();
			String source_condition = "";
			int type;
			if(params.getActiveTab().equals("VariantDisTabPane")) {
				source_condition = this.getVariantGroupedSourcesQueryCondition(src);
				type = 1;
			}else {
				source_condition = this.getGroupedSourcesQueryCondition(src);
				type = 0;
			}
					
			String disClass = params.getDiseaseClass();
			String disClass_condition = "";
			if(disClass!=null && !disClass.equals("Any")) {
				disClass_condition = " AND diseaseClass.diseaseClassName LIKE '%"+disClass+"%'";
			}
			String searchDisText = params.getDisSearchText();
						
			Query query_obj = new Query();
			//MANAGE WITH CAUTION
	//		//SELECT
			query_obj.addColumn("disAtt.diseaseId");
			query_obj.addColumn("disAtt.diseaseName");
			query_obj.addColumn("GROUP_CONCAT(disCls.diseaseClass) AS diseaseClass");
			query_obj.addColumn("GROUP_CONCAT(disCls.diseaseClassName) AS diseaseClassName");
	//		//FROM
			query_obj.addTable("diseaseAttributes disAtt LEFT JOIN (disease2class \n" + 
					"	INNER JOIN diseaseClass\n" + 
					"		ON disease2class.diseaseClassNID=diseaseClass.diseaseClassNID) AS disCls  \n" + 
					"ON disAtt.diseaseNID=disCls.diseaseNID");
			//GROUP BY
			if(!source_condition.equals("")) {
				String subQuery = "";
				if(params.getActiveTab().equals("VariantDisTabPane")) {
					subQuery =
						"disAtt.diseaseNID IN ("
						+ "SELECT diseaseAttributes.diseaseNID FROM variantDiseaseNetwork, diseaseAttributes "
						+ "WHERE variantDiseaseNetwork.diseaseNID=diseaseAttributes.diseaseNID AND variantDiseaseNetwork."+source_condition+")";
				}else {
					subQuery = 
						"disAtt.diseaseNID IN ("
						+ "SELECT diseaseAttributes.diseaseNID FROM geneDiseaseNetwork, diseaseAttributes "
						+ "WHERE geneDiseaseNetwork.diseaseNID=diseaseAttributes.diseaseNID AND geneDiseaseNetwork."+source_condition+")";	
				}
				query_obj.addCondition(subQuery);
			}
			if(searchDisText!=null && !searchDisText.equals("")&&!params.getActiveTab().equals("disProj_TabPane")) {
				searchDisText = buildDiseaseSearchCondition(searchDisText,params.getSource(), type);
				query_obj.addCondition(searchDisText);
			}
			query_obj.setGroupBy("GROUP BY disAtt.diseaseId;");
			
					
			String query = query_obj.build();
			ResultSet rs = null;
			rs = this.runQuery(query);
			
			try {
			    		    
				while (rs.next()) {
					// iterate over fields and store field id + value
	
					String diseaseId = rs.getString("diseaseId");
					HashMap<String, String> diseaseAttributes = new HashMap<String, String>();
					diseaseAttributes.put("diseaseId", diseaseId);
					diseaseAttributes.put("diseaseName", rs	.getString("diseaseName"));
					diseaseAttributes.put("diseaseClass", rs.getString("diseaseClass"));
					diseaseAttributes.put("diseaseClassName", rs.getString("diseaseClassName"));
					diseaseAttributesALL.put(diseaseId, diseaseAttributes);
				}
				Statement tmpStmt = rs.getStatement();//EC
				tmpStmt.close();//EC
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			return diseaseAttributesALL;
		}

	public HashMap<String, HashMap<String, String>> getGeneAttributes(
			GuiParameters curParams) throws DisGeNetException {
		
		HashMap<String, HashMap<String, String>> geneAttributes = new HashMap<String, HashMap<String, String>>();

		String query = "";

		String source = curParams.getSource();
		String source_condition_query = this.getGroupedSourcesQueryCondition(source);

		String assocType = curParams.getAssociationType();
		String assoc_type_condition="";
		if(assocType!=null) {
			assoc_type_condition = this.getAssocTypeQueryCondition(assocType);
		}
		String search_text = "";
		if(curParams.getGenSearchText()!=null && !curParams.getActiveTab().equals("geneProj_TabPane")) {
			search_text =  curParams.getGenSearchText();
		}
						
		Query query_obj = new Query();

		query_obj.addTable("geneAttributes");
		query_obj.addTable("geneDiseaseNetwork");
		query_obj.addCondition("geneDiseaseNetwork.geneNID=geneAttributes.geneNID");

		if( !source_condition_query.equals("") ){
			query_obj.addCondition(source_condition_query);
		}
		if(!assoc_type_condition.equals("")) {
			query_obj.addCondition(assoc_type_condition);
		}
		
		if (!search_text.equals("")) {
			search_text = buildGeneSearchCondition(search_text,curParams.getSource(),0);
			query_obj.addCondition(search_text);
		}

		query_obj.addColumn("geneId");
		query_obj.addColumn("geneName");
		query_obj.addColumn("DSI");
		query_obj.addColumn("DPI");
		query_obj.addColumn("pLI");

		query = query_obj.build();		
		ResultSet rs = null;
		if (!query.equals("")) {
			rs = this.runQuery(query);
			
			try {

				while (rs.next()) {
					// global attributes
					String geneId = rs.getString("geneId").trim();
					String geneName = rs.getString("geneName");
					String dsi = rs.getString("DSI");
					String dpi = rs.getString("DPI");
					String pli = rs.getString("pLI");
					
					HashMap<String, String> attributes = new HashMap<String, String>();
					attributes.put("geneId", geneId);
					attributes.put("geneName", geneName);
					attributes.put("dsi",dsi);
					attributes.put("dpi",dpi);
					attributes.put("pli",pli);
					geneAttributes.put(geneId, attributes);
				}
				Statement tmpStmt = rs.getStatement();
				tmpStmt.close();
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
				//("Could not retrieve gene name list from DB.");
			}
		} else {
			//("no query possible");
		}

		return geneAttributes;
	}

	public HashMap<String, HashMap<String, String>> getVariantAttributes(
			GuiParameters curParams) {
		
		HashMap<String, HashMap<String, String>> variantAttributes = new HashMap<String, HashMap<String, String>>();

		String subQueryCondition = "";
		
		String src = curParams.getSource();
		String src_condition = this.getVariantGroupedSourcesQueryCondition(src);
		if(!src_condition.equals("")) {
			subQueryCondition = "WHERE "+src_condition;
		}
		// genetic association
		String assocType = curParams.getAssociationType();
		String assoc_type_condition = this.getVariantAssocTypeQueryCondition(assocType);
		if(!assoc_type_condition.equals("")) {
			if(!src_condition.equals("")) {
				subQueryCondition += "AND "+assoc_type_condition;
			}else {
				subQueryCondition = "WHERE "+assoc_type_condition;
			}
			
		}
		
//		String search_text =  curParams.getVarSearchText();
				
		String query = "";

		Query query_obj = new Query();
		query_obj.addTable("variantAttributes LEFT JOIN (variantGene \n" + 
				"	INNER JOIN geneAttributes\n" + 
				"		ON variantGene.geneNID=geneAttributes.geneNID) AS genAtt \n" + 
				"	ON variantAttributes.variantNID=genAtt.variantNID AND variantAttributes.variantNID IN \n" + 
				"		(SELECT variantNID \n" + 
				"		 FROM variantDiseaseNetwork\n" + 
				"		 "+subQueryCondition+")\n");
		query_obj.addColumn("*");
		query_obj.addColumn("GROUP_CONCAT(DISTINCT geneName) AS associated_genes");
		query_obj.setGroupBy("GROUP BY variantId;");
		
		query = query_obj.build();
		ResultSet rs = null;
		if (!query.equals("")) {
			rs = this.runQuery(query);
			try {

				while (rs.next()) {
					// global attributes
					String variantId = rs.getString("variantId").trim();
					String varClass = rs.getString("s");
					String varChrom = rs.getString("chromosome");
					String varCoord = rs.getString("coord");
					String assocGenes = rs.getString("associated_genes");
//					String origen = rs.getString("origen");
					String mostSevCons = rs.getString("most_severe_consequence");
//					String clinSignifc = rs.getString("clinical_significance");
//					String ref = rs.getString("ref");
//					String alt = rs.getString("alt");
//					String af1kG = rs.getString("af_genome");
//					String afExac = rs.getString("af_exome");
					String dsi = Double.toString(rs.getDouble("DSI"));
					String dpi = Double.toString(rs.getDouble("DPI"));

					HashMap<String, String> attributes = new HashMap<String, String>();
					attributes.put("variantId", variantId);
					attributes.put("varClass", varClass);
					attributes.put("varChrom",varChrom);
					attributes.put("varCoord",varCoord);
					attributes.put("assocGenes",assocGenes);
//					attributes.put("origen",origen);
					attributes.put("mostSevCons",mostSevCons);
//					attributes.put("clinSignifc",clinSignifc);
//					attributes.put("ref", ref);
//					attributes.put("alt", alt);
//					attributes.put("af1kG", af1kG);
//					attributes.put("afExac", afExac);
					attributes.put("dsi", dsi);
					attributes.put("dpi",dpi);
					variantAttributes.put(variantId, attributes);
				}
				Statement tmpStmt = rs.getStatement();
				tmpStmt.close();
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
				//("Could not retrieve gene name list from DB.");
			}
		} else {
			//("no query possible");
		}

		return variantAttributes;
	}

	@Override
	public HashMap<String, ArrayList<String>> getGeneDiseaseNetworkBySrc(GuiParameters curParams) throws DisGeNetException {
		HashMap<String, ArrayList<String>> result = new HashMap<String, ArrayList<String>>();
		ArrayList<String> errors = new ArrayList<>();

		// source
		String src = curParams.getSource();
		String src_condition = this.getGroupedSourcesQueryCondition(src);

		// genetic association
		String assocType = curParams.getAssociationType();
		String assoc_type_condition = this.getAssocTypeQueryCondition(assocType);

		// disease class
		String disClass = curParams.getDiseaseClass();
		String disClass_condition = this.getDiseaseClassQueryCondition(disClass);
		
		String filterEl = curParams.getEl();
		
		// score
		Double scoreHighFilter = 1.0, eiHighFilter = 1.0;
		Double scoreLowFilter = 0.0, eiLowFilter = 0.0;
		if(curParams.getHighScore()!=null && !curParams.getHighScore().equals("") ){
			scoreHighFilter = Double.parseDouble(curParams.getHighScore());
		}
		if(curParams.getLowScore()!=null && !curParams.getLowScore().equals("") ){
			scoreLowFilter = Double.parseDouble(curParams.getLowScore());
		}
		
		if(curParams.getHighEi()!=null && !curParams.getHighEi().equals("") ){
			eiHighFilter = Double.parseDouble(curParams.getHighEi());
		}
		if(curParams.getLowEi()!=null && !curParams.getLowEi().equals("") ){
			eiLowFilter = Double.parseDouble(curParams.getLowEi());
		}

		// search field text
		String searchDisText = curParams.getDisSearchText();
		String searchGenText = curParams.getGenSearchText();
		String search_text_condition = "";
		String sc1 = "";
		String sc2 = "";	
		
		sc1 = buildDiseaseSearchCondition(searchDisText,curParams.getSource(), 0);
		
		sc2 = buildGeneSearchCondition(searchGenText,curParams.getSource(), 1);
				
		search_text_condition = sc1 ;
		
		if (!sc1.equals("") && !sc2.equals("")  ){
			
			search_text_condition += " AND " ;
			search_text_condition += sc2 ; 
		
		} 	
		else if (sc1.equals("") && !sc2.equals("")  ){
			
			search_text_condition = sc2 ;		
		} 
		else if(!sc1.equals("") && !sc2.equals("")  ){		
		}
	
		ResultSet rs;

		Query query_obj = new Query();
		Query query_rowCount = new Query();
		ArrayList<Query> queriesArray = new ArrayList<>();
		query_obj.addColumn("*");
		query_rowCount.addColumn("count(*) AS RowCount");
		//Loads the predefined queries into an array
		queriesArray.add(query_obj);
		queriesArray.add(query_rowCount);
		//Loads the needed tables into an array list. 
		ArrayList<String> tables = new ArrayList<>(Arrays.asList(
				"geneDiseaseNetwork",
				"diseaseAttributes",
				"geneAttributes"
		));
		//Loads the needed conditions into an array list.
		ArrayList<String> conditions = new ArrayList<>(Arrays.asList(
				"geneDiseaseNetwork.geneNID=geneAttributes.geneNID",
				"geneDiseaseNetwork.diseaseNID=diseaseAttributes.diseaseNID"
		));
		
		if( !assoc_type_condition.equals("") ){
			conditions.add(assoc_type_condition);
		}

		if( !src_condition.equals("") ){
			conditions.add(src_condition);

		}

		if( !disClass_condition.equals("") ){
			tables.add("disease2class");
			tables.add("diseaseClass");
			conditions.add(disClass_condition);
		}

		if( !search_text_condition.equals("") ){
			conditions.add(search_text_condition);

		}

		if( scoreLowFilter>0.0 ){
			conditions.add("score>="+scoreLowFilter);
		}

		if( scoreHighFilter<1.0 ){
			conditions.add("score<="+scoreHighFilter);
		}
		
		if( eiLowFilter>0.0 ){
			conditions.add("EI>="+eiLowFilter);
		}

		if( eiHighFilter<1.0 ){
			conditions.add("EI<="+eiHighFilter);
		}
		
		if(filterEl!=null && !filterEl.equals("Any")) {
			conditions.add(String.format("EL ='%s'", filterEl));
		}

		ArrayList<Query> queryBuilder = multiQueryBuilder(queriesArray,tables,conditions);
		
		String query = queryBuilder.get(0).build(); //retrieves the builded query.
		String queryCount = queryBuilder.get(1).build(); //retrieves the builded row count query
		String interaction,dis, gen, association, associationType, org_src, pmid, sentence,  score,ei, el,year = "";
		
		ArrayList<String> interactionList = new  ArrayList<String>();
		ArrayList<String> disList = new ArrayList<String>();
		ArrayList<String> genList = new ArrayList<String>();
		ArrayList<String> assocList = new ArrayList<String>();
		ArrayList<String> assocTypeList = new ArrayList<String>();
		ArrayList<String> srcList = new ArrayList<String>();
		ArrayList<String> pmidsList = new ArrayList<String>();
		ArrayList<String> sentenceList = new ArrayList<String>();
		ArrayList<String> scoreList = new ArrayList<String>();
		ArrayList<String> yearList = new ArrayList<String>();
		ArrayList<String> eiList = new ArrayList<String>();
		ArrayList<String> elList = new ArrayList<String>();
		//Query result var
		if (!query.equals("")) {
			int rowCount = this.numRowsQuery(queryCount);
			if (rowCount >0 && rowCount<200000) {
				try {
					rs = this.runQuery(query);
					while (rs.next() && !curParams.isNetBuildInterrupted()) {
						interaction = rs.getString("NID");
						dis = rs.getString("diseaseId");
						gen = rs.getString("geneId");
						association = rs.getString("association");
						associationType = rs.getString("associationType");
						org_src = rs.getString("source");
						//label = rs.getString("label");
						pmid = rs.getString("pmid");
						sentence = rs.getString("sentence");
						//JAVI
						//snpId = rs.getString("snpId");
						//snpId = "TO CHANGE JAVI";
						score = rs.getString("score");
						ei = rs.getString("EI");
						el = rs.getString("EL");
						year = rs.getString("year");
						
						interactionList.add(interaction);
						disList.add(dis);
						genList.add(gen);
						assocList.add(association);
						srcList.add(org_src);
						assocTypeList.add(associationType);
//						labelList.add(label);
						sentenceList.add(sentence);
						scoreList.add(score);
						eiList.add(ei);
						elList.add(el);
						pmidsList.add(pmid);
						//snpIdsList.add(snpId);
						yearList.add(year);
					}
					Statement tmpStmt = rs.getStatement();
					tmpStmt.close();
					rs.close();

				} catch (SQLException e) {
					errors.add("Something went wrong, try again later, check the log file for more info.");
					result.put("ERRORS",errors);
				}
			} else if (rowCount > 200000) {
				errors.add("Network too big to be created, add params to your search.");
				result.put("ERRORS",errors);
			} else {
				errors.add("No results for the given parameters.");
				result.put("ERRORS",errors);
			}
			
		}
		result.put("intList",interactionList);
		result.put("disList", disList);
		result.put("genList", genList);
		result.put("assocList", assocList);
		result.put("assocTypeList", assocTypeList);
		result.put("srcList", srcList);
		result.put("scoreList", scoreList);
		result.put("eiList",eiList);
		result.put("elList",elList);
		result.put("sentenceList", sentenceList);
		result.put("pmidsList", pmidsList);
		result.put("yearList", yearList);

		return result;
	}
	
	@Override
	public Map <String, String> getAssociatedGenesToVariants(List<String> genes) throws SQLException{
		HashMap<String, String> result = new HashMap<String, String>();
		ArrayList<String> geneIdsRaw = genNamesToId(genes);
		Set<String> geneIds = new LinkedHashSet<>();
		geneIds.addAll(geneIdsRaw);
		String geneSearch = "geneId IN "+multipleSearchParams(geneIds);
		Query queryStruct = new Query();
		queryStruct.addColumn("geneId, geneName");
		queryStruct.addTable("geneAttributes");
		queryStruct.addCondition(geneSearch);
		String query = queryStruct.build();
		ResultSet rs = runQuery(query);
		while(rs.next()) {
			String geneId = rs.getString(1);
			String geneName = rs.getString(2);
			
			result.put(geneName, geneId);
		}
		return result;
	}
	
	@Override
	public HashMap<String, ArrayList<String>> getVariantDiseaseNetworkBySrc(GuiParameters curParams) throws DisGeNetException {
		HashMap<String, ArrayList<String>> result = new HashMap<String, ArrayList<String>>();
		ArrayList<String> errors = new ArrayList<>();
		
		String src = curParams.getSource();
		String src_condition = this.getVariantGroupedSourcesQueryCondition(src);

		// genetic association
		String assocType = curParams.getAssociationType();
		String assoc_type_condition = this.getVariantAssocTypeQueryCondition(assocType);

		// disease class
		String disClass = curParams.getDiseaseClass();
		String disClass_condition = this.getDiseaseClassQueryCondition(disClass);
		
		// score ei
		Double scoreHighFilter = 1.0, eiHighFilter = 1.0;
		Double scoreLowFilter = 0.0, eiLowFilter = 0.0;
		if(curParams.getHighScore()!=null && !curParams.getHighScore().equals("") ){
			scoreHighFilter = Double.parseDouble(curParams.getHighScore());
		}
		if(curParams.getLowScore()!=null && !curParams.getLowScore().equals("") ){
			scoreLowFilter = Double.parseDouble(curParams.getLowScore());
		}
		
		if(curParams.getHighEi()!=null && !curParams.getHighEi().equals("") ){
			eiHighFilter = Double.parseDouble(curParams.getHighEi());
		}
		if(curParams.getLowEi()!=null && !curParams.getLowEi().equals("") ){
			eiLowFilter = Double.parseDouble(curParams.getLowEi());
		}
		
		// search field text
		String searchDisText = curParams.getDisSearchText();
		String searchVarText = curParams.getVarSearchText();
		String searchGenText = curParams.getGenSearchText();
		//JAVI ADDED
		String search_text_condition = "";
		String sc1 = "";
		String sc2 = "";
		String sc3 = "";
								
		sc1 = buildDiseaseSearchCondition(searchDisText,curParams.getSource(),1);
		
		sc2 = buildVariantSearchCondition(searchVarText,curParams.getSource());
		
		sc3 = buildGeneSearchCondition(searchGenText,curParams.getSource(),1);
		
		ArrayList<String> textBoxes = new ArrayList<>(Arrays.asList(sc1,sc2,sc3));
		
		search_text_condition = textConditionConcat(textBoxes);

		ResultSet rs;
		
		Query query_obj = new Query();
		Query query_rowCount = new Query();
		ArrayList<Query> queriesArray = new ArrayList<>();
		query_obj.addColumn("DISTINCT *");
		query_rowCount.addColumn("count(*) AS RowCount");
		//Loads the predefined queries into an array
		queriesArray.add(query_obj);
		queriesArray.add(query_rowCount);
		//Loads the needed tables into an array list. 
		ArrayList<String> tables = new ArrayList<>(Arrays.asList(
				"variantDiseaseNetwork",
				"diseaseAttributes",
				"variantAttributes"
		));
		//Loads the needed conditions into an array list.
		ArrayList<String> conditions = new ArrayList<>(Arrays.asList(
				"variantDiseaseNetwork.variantNID=variantAttributes.variantNID",
				"variantDiseaseNetwork.diseaseNID=diseaseAttributes.diseaseNID"
				
		));
		
		if(searchGenText!=null&&!searchGenText.equals("")) {
			tables.add("geneAttributes");
			tables.add("variantGene");
			conditions.add("variantAttributes.variantNID=variantGene.variantNID");
			conditions.add("variantGene.geneNID=geneAttributes.geneNID");
		}

		if(!src_condition.equals("") ){
			conditions.add(src_condition);
		}
		
		if( !assoc_type_condition.equals("") ){
			conditions.add(assoc_type_condition);
		}

		if( !disClass_condition.equals("") ){
			tables.add("disease2class");
			tables.add("diseaseClass");
			conditions.add(disClass_condition);
		}

		if( !search_text_condition.equals("") ){
			conditions.add(search_text_condition);

		}

		if( scoreLowFilter>0.0 ){
			conditions.add("score>="+scoreLowFilter);
		}

		if( scoreHighFilter<1.0 ){
			conditions.add("score<="+scoreHighFilter);
		}
		
		if( eiLowFilter>0.0 ){
			conditions.add("EI>="+eiLowFilter);
		}

		if( eiHighFilter<1.0 ){
			conditions.add("EI<="+eiHighFilter);
		}
		ArrayList<Query> queryBuilder = multiQueryBuilder(queriesArray,tables,conditions);
		
		String query = queryBuilder.get(0).build(); //retrieves the builded query.
		String queryCount = queryBuilder.get(1).build(); //retrieves the builded row count query
		String interaction, dis, variant, association, associationType, org_src, label, pmid, sentence,  snpId, score, ei,year = "";
		ArrayList<String> interactionList = new ArrayList<>();
		ArrayList<String> disList = new ArrayList<String>();
		ArrayList<String> varList = new ArrayList<String>();
		ArrayList<String> assocList = new ArrayList<String>();
		ArrayList<String> assocTypeList = new ArrayList<String>();
		ArrayList<String> srcList = new ArrayList<String>();
		ArrayList<String> pmidsList = new ArrayList<String>();
		ArrayList<String> sentenceList = new ArrayList<String>();
		ArrayList<String> scoreList = new ArrayList<String>();
		ArrayList<String> eiList = new ArrayList<String>();
		ArrayList<String> yearList = new ArrayList<String>();
		
		if (!query.equals("")) {
			int rowCount = this.numRowsQuery(queryCount);
			if (rowCount >0 && rowCount<200000) {
				try {
					rs = this.runQuery(query);
					while (rs.next() && !curParams.isNetBuildInterrupted()) {
						interaction = rs.getString("NID");
						dis = rs.getString("diseaseId");
						variant = rs.getString("variantId");
						association = rs.getString("association");
						associationType = rs.getString("associationType");
						org_src = rs.getString("source");
						pmid = rs.getString("pmid");
						sentence = rs.getString("sentence");
						score = rs.getString("score");
						ei = rs.getString("EI");
						year = rs.getString("year");
						
						interactionList.add(interaction);
						disList.add(dis);
						varList.add(variant);
						assocList.add(association);
						srcList.add(org_src);
						assocTypeList.add(associationType);
						sentenceList.add(sentence);
						scoreList.add(score);
						eiList.add(ei);
						pmidsList.add(pmid);
						//snpIdsList.add(snpId);
						yearList.add(year);
					}
					Statement tmpStmt = rs.getStatement();
					tmpStmt.close();
					rs.close();

				} catch (SQLException e) {
					errors.add("Something went wrong, try again later, check the log file for more info.");
					result.put("ERRORS",errors);
				}
			} else if (rowCount > 200000) {
				errors.add("Network too big to be created, add params to your search.");
				result.put("ERRORS",errors);
			} else {
				errors.add("No results for the given parameters.");
				result.put("ERRORS",errors);
			}
			
		}
		result.put("intList",interactionList);
		result.put("disList", disList);
		result.put("varList", varList);
		result.put("assocList", assocList);
		result.put("assocTypeList", assocTypeList);
		result.put("srcList", srcList);
		result.put("scoreList", scoreList);
		result.put("eiList", eiList);
		result.put("sentenceList", sentenceList);
		result.put("pmidsList", pmidsList);
		result.put("yearList", yearList);

		return result;
	
	}

	@Override
	public HashMap<String, ArrayList<String>> getGeneProjectionBySrc(GuiParameters curParams) {
		
		HashMap<String, ArrayList<String>> result = new HashMap<>();
		ArrayList<String> errors = new ArrayList<>();
		
		// source
		String src = curParams.getSource();

		String source_condition_query = this.getGroupedSourcesQueryCondition(src);

		// search text
		String searchText = curParams.getGenSearchText();
		
		// disease class
		String disClass = curParams.getDiseaseClass();
		
		String disClass_condition_query = this.getDiseaseClassQueryCondition(disClass);

		String search_text_condition = "";
	
		if (!searchText.equals("")) {

			ArrayList <String> pieces = new ArrayList<>(Arrays.asList(searchText.split(";")));
			ArrayList <String> geneNamesToId = new ArrayList<>();
			ArrayList <String> genesWildCard = new ArrayList<>();
			Set <String> genesIds = new LinkedHashSet<>();
			
			if(!pieces.isEmpty()) {
				Iterator <String> itPieces = pieces.iterator(); 
				do {
					String piece = itPieces.next();
					if(piece.contains("*")) {
						piece = piece.replace("*","%");
						genesWildCard = genWildCardGDA(piece,curParams.getSource());
						genesIds.addAll(genesWildCard);
					}
					else {
						geneNamesToId.add(piece);
					}
					
				}while(itPieces.hasNext());
				if(!geneNamesToId.isEmpty()) {
					genesIds.addAll(genNamesToId(geneNamesToId));
				}
				search_text_condition = "geneId IN "+multipleSearchParams(genesIds);
			}
		}
		
		if (!search_text_condition.equals("")) {
			Query query_obj = new Query();
			Query query_rowCount = new Query();
			ArrayList<Query> queriesArray = new ArrayList<>();
			query_obj.addColumn("DISTINCT g1.geneId AS geneId1");
			query_obj.addColumn("g2.geneId AS geneId2");
			query_obj.addColumn("count(distinct n1.diseaseNID) AS nrCommonDiseases");
			query_rowCount.addColumn("count(g2.geneId) AS RowCount");
			queriesArray.add(query_obj);
			queriesArray.add(query_rowCount);
			ArrayList<String> tables = new ArrayList<>(Arrays.asList("geneAttributes AS g1", "geneAttributes AS g2",
					"geneDiseaseNetwork n1", "geneDiseaseNetwork n2"));
			ArrayList<String> conditions = new ArrayList<>(
					Arrays.asList("n1.geneNID=g1.geneNID", "n2.geneNID=g2.geneNID", "n1.diseaseNID=n2.diseaseNID",
							"n1.source=n2.source", "n1.geneNID!=n2.geneNID"));
			if (!disClass_condition_query.equals("")) {
				query_obj.addTable("diseaseAttributes");
				query_obj.addTable("disease2class");
				query_obj.addTable("diseaseClass");
				query_obj.addCondition("diseaseAttributes.diseaseNID=n1.diseaseNID");
				query_obj.addCondition(disClass_condition_query);
			}
			if (!source_condition_query.equals("")) {
				conditions.add("n1." + source_condition_query);
			}
			if (!search_text_condition.equals("")) {
				conditions.add("g1." + search_text_condition);
			}
			query_obj.setGroupBy("GROUP BY n1.geneNID, n2.geneNID");
			
			ArrayList<Query> queryBuilder = multiQueryBuilder(queriesArray, tables, conditions);
			String query = queryBuilder.get(0).build();
			String queryCount = queryBuilder.get(1).build();
			ResultSet rs = null;
			String gen1, gen2 = "";
			ArrayList<String> gen1List = new ArrayList<String>();
			ArrayList<String> gen2List = new ArrayList<String>();
			ArrayList <String> nrCommonDiseases = new ArrayList<String>();
			if (!query.equals("")) {
				int rowCount = this.numRowsQuery(queryCount);
				if (rowCount > 0 && rowCount < 200000) {
					try {
						rs = this.runQuery(query);
						while (rs.next() && !curParams.isNetBuildInterrupted()) {
							gen1 = rs.getString("geneId1");
							gen2 = rs.getString("geneId2");
							//ADDED BY JAVI
							gen1List.add(gen1);
							gen2List.add(gen2);
							//PEP
							nrCommonDiseases.add(rs.getString("nrCommonDiseases"));
						}
						Statement tmpStmt = rs.getStatement();
						tmpStmt.close();
						rs.close();

					} catch (SQLException e) {
						//("JAVI2");
						errors.add("Something went wrong, try again later, check the log file for more info.");
						result.put("ERRORS",errors);
					}
				} else if (rowCount > 200000) {
					errors.add("Network too big to be created, add params to your search.");
					result.put("ERRORS",errors);
				} else {
					errors.add("No results for the current parameters.");
					result.put("ERRORS",errors);
				}
				result.put("gen1List", gen1List);
				result.put("gen2List", gen2List);
				result.put("nrCommonDiseases",nrCommonDiseases);
			} 
		}else {
			errors.add("Enter a gene to filter the projection. (Use '*' in the search field to see a list of genes)");
			result.put("ERRORS",errors);
		}
		//("gen1Length: \t" + gen1List.size());
		//("gen2Length: \t" + gen2List.size());

		return result;
	}

	@Override
	public HashMap<String, ArrayList<String>> getDiseaseProjectionBySrc(GuiParameters curParams) {
		HashMap<String, ArrayList<String>> result = new HashMap<>();
		ArrayList<String> errors = new ArrayList<>();
		// source
		String src = curParams.getSource();
		String source_condition = this.getGroupedSourcesQueryCondition(src);

		// disease class
		String disClass = curParams.getDiseaseClass();
		String dis_class_condition1 = this.getDiseaseClassQueryCondition(disClass, "d1", "dc1", "d2c1");
		String dis_class_condition2 = this.getDiseaseClassQueryCondition(disClass, "d2", "dc2", "d2c2");

		String searchText = curParams.getDisSearchText();
	
		String search_text_condition = "";
	
		if (!searchText.equals("")) {
			ArrayList <String> pieces = new ArrayList<>(Arrays.asList(searchText.split(";")));
			ArrayList <String> diseaseNamesToId = new ArrayList<>();
			ArrayList <String> diseasesWildCard = new ArrayList<>();
			Set <String> diseaseIds = new LinkedHashSet<>();
			
			if(!pieces.isEmpty()) {
				Iterator <String> itPieces = pieces.iterator(); 
				do {
					String piece = itPieces.next();
					if(piece.contains("*")) {
						piece = piece.replace("*","%");
						diseasesWildCard = disWildCardGDA(piece,curParams.getSource());
						diseaseIds.addAll(diseasesWildCard);
					}
					else if(!piece.matches(DatabaseProps.DISEASE_CODE_REGEX)) {
						diseaseNamesToId.add(piece);
					}else {
						diseaseIds.add(piece);
					}
					
				}while(itPieces.hasNext());
				if(!diseaseNamesToId.isEmpty()) {
					diseaseIds.addAll(disNamesToId(diseaseNamesToId));
				}
				search_text_condition = "diseaseId IN "+multipleSearchParams(diseaseIds);
			}
		}
		
		Query query_obj = new Query();
		Query query_rowCount = new Query();
		ArrayList<Query> queriesArray = new ArrayList<>();
			
		query_obj.addColumn("DISTINCT d1.diseaseId AS diseaseId1");
		query_obj.addColumn("d2.diseaseId AS diseaseId2");
		query_obj.addColumn("GROUP_CONCAT(distinct n1.source) AS sources");
		query_obj.addColumn("count(distinct n1.source) AS nrSources");
		query_obj.addColumn("count(distinct n1.geneNID) AS nrCommonGenes");
		
		query_rowCount.addColumn("COUNT(d2.diseaseId) AS RowCount"); 

		queriesArray.add(query_obj);
		queriesArray.add(query_rowCount);
		
		ArrayList<String> tables = new ArrayList<>(Arrays.asList(
				"geneDiseaseNetwork AS n1","geneDiseaseNetwork AS n2",
				"diseaseAttributes AS d1", "diseaseAttributes AS d2"
				));
		ArrayList<String> conditions = new ArrayList<>(Arrays.asList(
				"d1.diseaseNID=n1.diseaseNID","d2.diseaseNID=n2.diseaseNID",
				"n1.geneNID=n2.geneNID",
				"n1.source=n2.source",
				"n1.diseaseNID!=n2.diseaseNID"
				));
		//GROUP BY disease pairs
		query_obj.setGroupBy("GROUP BY n1.diseaseNID, n2.diseaseNID");

		if( !source_condition.equals("") ){
			conditions.add("n1."+source_condition);
		}

		if( !dis_class_condition1.equals("") ){
			tables.add("diseaseClass AS dc1");
			tables.add("disease2class AS d2c1");
			conditions.add(dis_class_condition1);
		}
		if( !dis_class_condition2.equals("") ){
			tables.add("diseaseClass AS dc2");
			tables.add("disease2class AS d2c2");
			conditions.add(dis_class_condition2);
		}

		if( !search_text_condition.equals("") ){
			conditions.add("d1."+search_text_condition);
		}
		
		if (!search_text_condition.equals("")) {
			ArrayList<Query> queryBuilder = multiQueryBuilder(queriesArray, tables, conditions);
			String query = queryBuilder.get(0).build();
			String queryCount = queryBuilder.get(1).build();
			ResultSet rs = null;
			String dis1, dis2 = "";
			result = new HashMap<String, ArrayList<String>>();
			ArrayList<String> dis1List = new ArrayList<String>();
			ArrayList<String> dis2List = new ArrayList<String>();
			ArrayList<String> sources = new ArrayList<String>();
			ArrayList<String> nrSources = new ArrayList<String>();
			ArrayList<String> nrCommonGenes = new ArrayList<String>();
			if (!query.equals("")) {
				int rowCount = this.numRowsQuery(queryCount);
				if (rowCount > 0 && rowCount < 200000) {
					try {
						rs = this.runQuery(query);
						while (rs.next() && !curParams.isNetBuildInterrupted()) {
							dis1 = rs.getString("diseaseId1");
							dis2 = rs.getString("diseaseId2");

							dis1List.add(dis1);
							dis2List.add(dis2);

							sources.add(rs.getString("sources"));
							nrSources.add(rs.getString("nrSources"));
							nrCommonGenes.add(rs.getString("nrCommonGenes"));
						}
						Statement tmpStmt = rs.getStatement();
						tmpStmt.close();
						rs.close();

					} catch (SQLException e) {
						//("JAVI3");
						errors.add("Something went wrong, try again later, check the log file for more info.");
						result.put("ERRORS",errors);
					}
				} else if (rowCount > 200000) {
					errors.add("Network too big to be created, add params to your search. ");
					result.put("ERRORS",errors);
				} else {
					errors.add("No results for the given parameters.");
					result.put("ERRORS",errors);
				}
			}
			result.put("dis1List", dis1List);
			result.put("dis2List", dis2List);
			result.put("nrSources", nrSources);
			result.put("nrCommonGenes", nrCommonGenes);
			result.put("sources", sources);
		}else {
			errors.add("Enter a disease to filter the projection. (Use '*' in the search field to see a list of diseases)");
			result.put("ERRORS",errors);
		}
		return result;
	}

	@Override
	public HashMap<String, String> getEdgeAttributes(String assocId,GuiParameters params) {

		HashMap<String, String> edgeAttributes = new HashMap<String, String>();
//		// String query =
//		// "select * from geneDiseaseNetwork where association = \"" + assocId
//		// +"\";";
//		//JAVI: Add snpId
//		String query = "select associationType, label, score, source, pmid, sentence, year, \"-\" AS snpId FROM geneDiseaseNetwork where association = \""
//				+ assocId + "\";";
//
//		ResultSet rs = null;
//		rs = this.runQuery(query);
//
//		try {
//			while (rs.next() && !params.isNetBuildInterrupted()) {
//				// iterate over fields and store field id + value
//				edgeAttributes.put("associationType", rs.getString("associationType")+ "");
//				//edgeAttributes.put("label", rs.getString("label"));
//				edgeAttributes.put("source", rs.getString("source"));
//				edgeAttributes.put("pmid", rs.getString("pmid"));
//				edgeAttributes.put("sentence", rs.getString("sentence"));
//				edgeAttributes.put("score", rs.getString("score"));
//				//edgeAttributes.put("snpId", rs.getString("snpId"));
//				edgeAttributes.put("year", rs.getString("year"));
//			}
//			
//			Statement tmpStmt = rs.getStatement();
//			tmpStmt.close();
//			rs.close();
//		} catch (SQLException e) {
//			e.printStackTrace();
//			logger.error("Could not retrieve gene name list from DB.");
//		}
		return edgeAttributes;
	}
	
	@Override
	public Map <String, List<String>> getDiseasesAssociatedToGenes (GuiParameters params) throws DisGeNetException{
		Map <String, List<String>> results = new HashMap<>();
		Query queryObj = new Query();
		queryObj.addColumn("diseaseId,geneId,association,associationType,source,pmid,sentence,score");
		queryObj.addTable("geneDiseaseNetwork");
		queryObj.addTable("geneAttributes");
		queryObj.addTable("diseaseAttributes");
		queryObj.addCondition("geneDiseaseNetwork.geneNID=geneAttributes.geneNID");
		queryObj.addCondition("geneDiseaseNetwork.diseaseNID=disAtt.diseaseNID");
		queryObj.addCondition("geneId IN ("+params.getGenSearchText()+")");
		String query = queryObj.build();
		ResultSet rs = null;
		rs = runQuery(query);
		try {
			while(rs.next()) {
				rs.getInt("geneId");
				
			}
		}catch (SQLException e) {
			e.printStackTrace();
			throw new DisGeNetException("An error occured during the query pls try again later.");
		}
		return null;
	}

	@Override
	public ResultSet runQuery(String query) {
		ResultSet rs = null;
		Connection con = manager.getCon();

		
		Statement stmt = null;
		try {
			//JAVI ADDED:
			/*
			JOptionPane.showMessageDialog(null, "Connection: "+con.toString());
			 */
			con.setAutoCommit(false);
			stmt = con.createStatement();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			QueryExecutor executor = new QueryExecutor();
			rs = executor.executeQuery(stmt, query);
			
			//("aaa ");			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 
		
		return rs;
		
	}
	
	private String buildDiseaseSearchCondition(String searchDisText,String source,int type) throws DisGeNetException {
		String condition = "";
		if (searchDisText!= null && !searchDisText.equals("")&&!searchDisText.matches(DatabaseProps.DISEASE_SEARCH_REGEX)) {

			ArrayList <String> pieces = new ArrayList<>(Arrays.asList(searchDisText.split(";")));
			ArrayList <String> diseaseNamesToId = new ArrayList<>();
			ArrayList <String> diseasesWildCard = new ArrayList<>();
			Set <String> diseaseIds = new LinkedHashSet<>();

			if(!pieces.isEmpty()) {
				Iterator <String> itPieces = pieces.iterator(); 
				do {
					String piece = itPieces.next().trim();
					if(piece.contains("*")) {
						piece = piece.replace("*","%");
						switch(type) {
							case 0:
								diseasesWildCard = disWildCardGDA(piece,source);
								break;
							case 1:
								diseasesWildCard = disWildCardVDA(piece, source);
								break;
							default:
								throw new DisGeNetException("Incorrect type of network");
						}
						
						diseaseIds.addAll(diseasesWildCard);
					}
					else if(!piece.matches(DatabaseProps.DISEASE_CODE_REGEX)) {
						diseaseNamesToId.add(piece);
					}else {
						diseaseIds.add(piece);
					}

				}while(itPieces.hasNext());
				if(!diseaseNamesToId.isEmpty()) {
					diseaseIds.addAll(disNamesToId(diseaseNamesToId));
				}
				condition = "diseaseId IN "+multipleSearchParams(diseaseIds);
			}
		}else if(searchDisText!= null && !searchDisText.equals("")&&searchDisText.matches(DatabaseProps.DISEASE_SEARCH_REGEX)) {
			condition = "diseaseId IN ("+searchDisText+")";
		}
		return condition;
	}
	
	/**
	 * Builds the string for the gene search condition.
	 * @param searchGenText the given parameters for the search.
	 * @return SQL condition format with the pattern IN (item1,...)
	 * @throws DisGeNetException low level exception 
	 */
	private String buildGeneSearchCondition(String searchGenText,String source,int type) throws DisGeNetException {
		String condition = "";
		if (searchGenText != null && !searchGenText.equals("") && !searchGenText.matches(DatabaseProps.GENE_SEARCH_REGEX)) {

			ArrayList <String> pieces = new ArrayList<>(Arrays.asList(searchGenText.split(";")));
			ArrayList <String> geneNamesToId = new ArrayList<>();
			ArrayList <String> genesWildCard = new ArrayList<>();
			Set <String> genesIds = new LinkedHashSet<>();
			
			if(!pieces.isEmpty()) {
				Iterator <String> itPieces = pieces.iterator(); 
				do {
					String piece = itPieces.next().trim();
					if(piece.contains("*")) {
						piece = piece.replace("*","%");
						switch (type) {
							case 0:
								genesWildCard = genWildCardGDA(piece,source);
								break;
							case 1:
								genesWildCard = genWildCardVDA(piece, source);
								break;
							default:
								throw new DisGeNetException("Incorrect type of network");
						}
						genesIds.addAll(genesWildCard);
					}else if(piece.matches("\\d+")) {
						genesIds.add(piece);
					}
					else {
						geneNamesToId.add(piece);
						
					}
					
				}while(itPieces.hasNext());
				if(!geneNamesToId.isEmpty()) {
					genesIds.addAll(genNamesToId(geneNamesToId));
				}
				condition = "geneId IN "+multipleSearchParams(genesIds);
			}
		}else if(searchGenText != null && !searchGenText.equals("") && searchGenText.matches(DatabaseProps.GENE_SEARCH_REGEX)){
			condition = "geneId IN ("+searchGenText+")";
		}
		return condition;
	}
	
	private String buildVariantSearchCondition(String searchVarText,String source) {
		String condition = "";
		if (searchVarText!=null && !searchVarText.equals("") && !searchVarText.matches(DatabaseProps.VARIANT_STRING_SEARCH)) {

			ArrayList <String> pieces = new ArrayList<>(Arrays.asList(searchVarText.split(";")));
			ArrayList <String> variantWildCard = new ArrayList<>();
			Set <String> variantIds = new LinkedHashSet<>();

			if(!pieces.isEmpty()) {
				Iterator <String> itPieces = pieces.iterator(); 
				do {
					String piece = itPieces.next().trim();
					if(piece.contains("*")) {
						piece = piece.replace("*","%");
						variantWildCard = varWildCard(piece,source);
						variantIds.addAll(variantWildCard);
					}else {
						variantIds.add(piece);
					}

				}while(itPieces.hasNext());

				condition = "variantId IN "+multipleSearchParams(variantIds);
			}
		}else if(searchVarText!=null&&!searchVarText.equals("")&& searchVarText.matches(DatabaseProps.VARIANT_STRING_SEARCH)) {
			condition = "variantId IN ("+searchVarText+")";
			
		}
		return condition;
	}

	/**
	 * Concatenates an array string into a multiple sql format string.
	 * @param pieces the search fields 
	 * @return String containing the next format '(item1,item2...)' or '()' in case the arrayList is empty
	 */
	public String multipleSearchParams(Set<String> pieces) {
		Iterator <String> itPieces = pieces.iterator();
		String query_condition = "(";
		
		while(itPieces.hasNext()) {
			query_condition = query_condition.concat("\""+itPieces.next()+"\"");
			if(!itPieces.hasNext()) {
				break;
			}
			query_condition += ",";
		}
		query_condition+=")";
		return query_condition;
	}
	/**
	 * Searches all the id's for the given disease names
	 * @param pieces ArrayList containing all the disease names as strings.
	 * @param curParams the current parameters of the search.
	 * @return all the found ids for the given names.
	 */
	private ArrayList<String> disNamesToId(ArrayList<String> pieces) {
		Iterator <String> itPieces = pieces.iterator();
		ArrayList <String> ids = new ArrayList<>();
		String disName = "";
		while(itPieces.hasNext()) {
			disName = (String) itPieces.next();
			String id = getDiseaseId(disName);
			if(!id.equals("")) {
				ids.add(id);
			}
		}
		return ids;
	}
	
	/**
	 * Searches all the id's for the given gene names
	 * @param pieces List containing all the gene names as strings.
	 * @return all the found ids for the given names.
	 */
	public ArrayList<String> genNamesToId(List<String> pieces) {
		Iterator <String> itPieces = pieces.iterator();
		ArrayList <String> ids = new ArrayList<>();
		String genName;
		while(itPieces.hasNext()) {
			genName = itPieces.next();
			if (genName!=null) {
				String id = getGeneId(genName);
				if(!id.trim().equals("") && id!=null) {
					ids.add(id);
				}
			}
		}
		return ids;
	}
	
	/**
	 * Returns all the ids associated for the matching name disease GDA.
	 * @param diseaseName string that contains the condition search for the disease name.
	 * @return list of all the disease id's found for the given disease name.
	 */
	private ArrayList<String> disWildCardGDA(String diseaseName,String source){
		
		String addSource = "";
		if(source!=null&&!source.equals("ALL")) {
			addSource = "AND "+getGroupedSourcesQueryCondition(source);
		}
		
		ArrayList<String> allDiseaseIds = new ArrayList<String>();
		String query = "SELECT DISTINCT diseaseId FROM diseaseAttributes INNER JOIN geneDiseaseNetwork g2d "
				+ "ON diseaseAttributes.diseaseNID=g2d.diseaseNID "+addSource
				+ " WHERE diseaseName like \""+ diseaseName.toLowerCase()+ "\" ;";
		
		String diseaseId = "";
		ResultSet rs = null;
		rs = this.runQuery(query);

		try {
			while (rs.next()) {
				// iterate over fields and store field id + value
				diseaseId = rs.getString("diseaseId");
				allDiseaseIds.add(diseaseId);
			}
//			rs.close();
			Statement tmpStmt = rs.getStatement();
			tmpStmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return allDiseaseIds;
	}
	
	/**
	 * Returns all the ids associated for the matching name disease for the VDA.
	 * @param diseaseName string that contains the condition search for the disease name.
	 * @return list of all the disease id's found for the given disease name.
	 */
	private ArrayList<String> disWildCardVDA(String diseaseName,String source){
		
		String addSource = "";
		if(source!=null&&!source.equals("ALL")) {
			addSource = "AND "+getVariantGroupedSourcesQueryCondition(source);
		}
		
		ArrayList<String> allDiseaseIds = new ArrayList<String>();
		String query = "SELECT DISTINCT diseaseId FROM diseaseAttributes INNER JOIN variantDiseaseNetwork v2d "
				+ "ON diseaseAttributes.diseaseNID=v2d.diseaseNID "	+addSource
				+ " WHERE diseaseName like \""+ diseaseName.toLowerCase()+ "\" ;";
		String diseaseId = "";
		ResultSet rs = null;
		rs = this.runQuery(query);

		try {
			while (rs.next()) {
				// iterate over fields and store field id + value
				diseaseId = rs.getString("diseaseId");
				allDiseaseIds.add(diseaseId);
			}
//			rs.close();
			Statement tmpStmt = rs.getStatement();
			tmpStmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return allDiseaseIds;
	}
	
	/**
	 * Returns all the ids associated for the matching name gene for the given GDA.
	 * @param geneName string that contains the condition search for the gene name.
	 * @return list of all the gene id's found for the given gene name.
	 */
	private ArrayList<String> genWildCardGDA (String geneName, String source){
		
		String addSource = "";
		if(source!=null&&!source.equals("ALL")) {
			addSource = " AND "+getGroupedSourcesQueryCondition(source);
		}
		
		ArrayList<String> allGenesIds = new ArrayList<String>();
		String query = "SELECT DISTINCT geneId from geneAttributes INNER JOIN geneDiseaseNetwork g2d"
				+ " ON geneAttributes.geneNID=g2d.geneNID"+addSource
				+ " WHERE geneName like \""+ geneName+ "\" ;";
		String geneId = "";
		ResultSet rs = null;
		rs = this.runQuery(query);
		try {
			
			while (rs.next()) {
				// iterate over fields and store field id + value
				geneId = rs.getString("geneId");
				allGenesIds.add(geneId);
			}
			Statement tmpStmt = rs.getStatement();
			tmpStmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return allGenesIds;
	}
	
	/**
	 * Returns all the ids associated for the matching name gene for the given GDA.
	 * @param geneName string that contains the condition search for the gene name.
	 * @return list of all the gene id's found for the given gene name.
	 */
	private ArrayList<String> genWildCardVDA (String geneName, String source){
		
		String addSource = "";
		if(source!=null&&!source.equals("ALL")) {
			addSource = "AND "+getVariantGroupedSourcesQueryCondition(source);
		}
		
		ArrayList<String> allGenesIds = new ArrayList<String>();
		String query = "SELECT DISTINCT geneId from geneAttributes INNER JOIN variantGene g2v ON geneAttributes.geneNID=g2v.geneNID "
				+ " INNER JOIN variantDiseaseNetwork v2d ON g2v.variantNID=v2d.variantNID "+addSource 
				+ " where geneName like '"
				+ geneName +"';";
		String geneId = "";
		ResultSet rs = null;
		rs = this.runQuery(query);

		try {
			
			while (rs.next()) {
				// iterate over fields and store field id + value
				geneId = rs.getString("geneId");
				allGenesIds.add(geneId);
			}
			Statement tmpStmt = rs.getStatement();
			tmpStmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return allGenesIds;
	}
	
	private ArrayList<String> varWildCard (String variantId,String source){
		ArrayList<String> allVariantIds = new ArrayList<String>();
		
		String addSource = "";
		if(source!=null&&!source.equals("ALL")) {
			addSource = "AND "+getVariantGroupedSourcesQueryCondition(source);
		}
		
		String query = "select DISTINCT variantId from variantAttributes INNER JOIN variantDiseaseNetwork v2d "
				+ " ON variantAttributes.variantNID=v2d.variantNID"+addSource
				+ " WHERE variantId LIKE " + variantId + "\";";

		ResultSet rs = null;
		rs = this.runQuery(query);

		try {
			while (rs.next()) {
				// iterate over fields and store field id + value
				variantId = rs.getString("variantId");
				allVariantIds.add(variantId);
			}
//			rs.close();
			Statement tmpStmt = rs.getStatement();
			tmpStmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return allVariantIds;
	}
	
	private String textConditionConcat (ArrayList <String> conditions) {
		Iterator <String> condIt =  conditions.iterator();
		String condition;
		String text_condition = "";
		boolean first = true;
				
		while(condIt.hasNext()) {
			condition = condIt.next();
			if(first && !condition.equals("")) {
				text_condition = condition;
				first = false;
			}else if(!text_condition.equals("")&& !condition.equals("")) {
				text_condition += " AND ";
				text_condition += condition;
			}

		}
		
		return text_condition;
	}

	@Override
	public ArrayList<String> getGeneDiseaseList(GuiParameters curParams) {
		// TODO Auto-generated method stub
		return null;
	}

}
