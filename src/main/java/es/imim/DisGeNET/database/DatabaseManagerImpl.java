package es.imim.DisGeNET.database;

/**
	DisGeNET: A Cytoscape plugin to visualize, integrate, search and analyze gene-disease networks
	Copyright (C) 2010  Michael Rautschka, Anna Bauer-Mehren
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.prefs.Preferences;
import javax.swing.Icon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

/*JAVI*/
//import com.l2fprod.common.swing.JDirectoryChooser;

//JAVI
//import org.cytoscape.task.ui.JTaskConfig;

import org.cytoscape.work.TaskManager;
import org.cytoscape.work.TaskObserver;
import org.cytoscape.application.CyUserLog;

//JAVI UNCOMMENTED
//import org.sqlite.SQLiteJDBCLoader;

import es.imim.DisGeNET.Status;
import es.imim.DisGeNET.tool.DownloadDBTask;
import es.imim.DisGeNET.tool.HelperFunctions;

import org.apache.tools.ant.types.resources.selectors.Type.FileDir;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.SynchronousTaskManager;
import org.cytoscape.work.TaskIterator;


//Added to access the services
import es.imim.DisGeNET.internal.CyActivator;

/**
 * Implementation of the DatabaseManager to handle db connection
 * If SQLite DB file cannot be found in chosen folder be it will automatically
 * downloaded from our server
 * @author michael
 *
 */
public class DatabaseManagerImpl implements DatabaseManager {

		private Preferences prefs;
	
		/* Instance variable of the DatabaseManager */
		private static DatabaseManagerImpl instance = null;
		
		/* connection variable */
		private Connection con = null;

		// logger = LoggerFactory.getLogger(getClass());

		/* MySQL connection parameters */
		private String mysql_dbhost = null;
		private String mysql_dbuser = null;
		private String mysql_dbpass = null;
		private String mysql_db = null;
		
		private final String sqlite_db_name = "disgenet_2020.db";
		private String connType;
		private String pathToDB;

		//static Logger logger = LoggerFactory.getLogger(ChangeDBMenuAction.class);
		/* ****************************************************************** */
						
		private DatabaseManagerImpl() {
			//prefs = Preferences.userRoot().node(this.getClass().getName());
			prefs = Preferences.userNodeForPackage(this.getClass());
			this.connType = prefs.get("DB_HOST_v7", null);
			this.pathToDB = prefs.get("pathToDB_v7", null);
		}
		
		/**
		 * Singleton implementation of the database class.
		 * @return an instance of the object.
		 */
		public static DatabaseManagerImpl getInstance() {			
			if (instance == null) {
				instance = new DatabaseManagerImpl();
			}
			return instance;
		}
		
		/**
		 * Initializes the database connection. 
		 * @return Status the status operation.
		 */
		public Status init() {
			boolean chkParam=false;
			do {
				chkParam = checkPreferences();
				
			}while(!chkParam);
			try {
				//CONECT USING SQLITE
				if( prefs.get("DB_HOST_v7","NOT_DEFINED_YET").equals("local")) {
						String path = pathToDB;
						String filename = path + System.getProperty("file.separator") + this.sqlite_db_name;
						System.out.println(filename + " database location"); 
										
						/* loading database specific driver */
						try {
							Class.forName("org.sqlite.JDBC");
							con = DriverManager.getConnection("jdbc:sqlite:" + filename);
						} catch (ClassNotFoundException e) {
							JOptionPane.showMessageDialog(null,
								e.toString());;
							return Status.DB_DRIVER_ERROR;
						}
						catch (SQLException e) {
							JOptionPane.showMessageDialog(null,
								e.toString());
							return Status.DB_CONNECTION_ERROR;
						}
					return Status.SUCCESS;
					}
				
				//CONNECT USING MYSQL
				else if( prefs.get("DB_HOST_v7","NOT_DEFINED_YET").equals("remote") ){
					try {
						Class.forName("com.mysql.jdbc.Driver");
						con = DriverManager.getConnection("jdbc:mysql://"+this.mysql_dbhost+"/"+this.mysql_db,this.mysql_dbuser,this.mysql_dbpass);
					}
					catch (ClassNotFoundException e) {
						JOptionPane.showMessageDialog(null,
								"Database connection is not possible. error with driver");
					}
					catch (SQLException e) {
							JOptionPane.showMessageDialog(null,
								e.toString());
					}
					return Status.SUCCESS;
				}
				else {

					try {
						checkDBFile();
					}catch (IOException e1) {
		                return Status.CONNECTION_ERROR;		
			        }
					
				}
			}
			catch (Exception e1) {
                return Status.CONNECTION_ERROR;		
            }
			return Status.CONNECTION_ERROR;
		}
		
		
		/**
		 * Checks if the configuration of the database is setted correctly.
		 * @return true in case the preferences file exists, or false in case it isn't or error.
		 * @author jsauch
		 */
		private boolean checkPreferences() {
			boolean flag = false;
			if(pathToDB==null||connType==null){
				if(setPreferences()==1) {
					flag = true;
				}
			}else {
				String path = pathToDB;
				Path fileDir = Paths.get(path);
				if(Files.notExists(fileDir)) {
					setPreferences();
				}
				String filename = path + System.getProperty("file.separator") + this.sqlite_db_name;
				File file = new File(filename);
    			if (!file.exists() || file.length()==0) {
    				downloadDB(filename);
    			}
    			flag = true;
    			
			}
			return flag;
		}
				
		/**
		 * Sets the parameters for the database connection.
		 * @return 1 in case of succes, or 0 in case of error, the return is forced to one.
		 * @author jsauch
		 */
		@Override
		public int setPreferences() { 
			int flag = 1;
			pathToDB = setPathToDB();
			connType = "local";
			prefs.put("DB_HOST_v7",connType);
            prefs.put("pathToDB_v7", pathToDB);
			return flag; 
		}
				
		/**
		 * Sets the pathToDB, given by the user.
		 * @return the path to the database.
		 */
		private String setPathToDB() {
			String path = "";
			 
			JFileChooser choose = new JFileChooser();
			JOptionPane.showMessageDialog(null,
					"Choose a directory where the DisGeNET database will be automatically downloaded and unpacked");
						
			choose.setDialogTitle("Please select the  directory for the Database");
			choose.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			int retval = choose.showOpenDialog(null);
			
	        if (retval == JFileChooser.APPROVE_OPTION) {
        	
	            File file = choose.getSelectedFile();
	            
	            path = file.getPath();
	            if(file.isDirectory()) {
	            	String filename = "";
	    			
	    			filename = path + System.getProperty("file.separator") + sqlite_db_name;
	    			file = new File(filename);
	    			if (!file.exists() || file.length()==0) {
	    				downloadDB(filename);
	    			}else {
	    				JOptionPane.showMessageDialog(null,
	    						"Database file found!");
	    			}
	            }
	        }else {
	        	choose.setCurrentDirectory(null);
	        	File file = choose.getCurrentDirectory();
	            
	            path = file.getPath();
	            if(file.isDirectory()) {
	            	String filename = "";
	    			
	    			filename = path + System.getProperty("file.separator") + sqlite_db_name;
	    			file = new File(filename);
	    			if (!file.exists() || file.length()==0) {
	    				downloadDB(filename);
	    			}else {
	    				JOptionPane.showMessageDialog(null,
	    						"Database file found!");
	    			}
	            }
	        }
	        return path;
		}
				
		
		private void downloadDB(String filename) {
			DownloadDBTask downloadDBTask  = new DownloadDBTask("https://www.disgenet.org/download/sqlite/current/2020", filename, pathToDB);
			TaskIterator taskIterator = CyActivator.getInstance().getGlobalTaskIterator();
			taskIterator.append(downloadDBTask);
			//CyActivator.getInstance().getDialogTaskManagerService().execute(taskIterator);
            System.out.println("GOING TO DOWNLOAD");
		}
				
		/* ****************************************************************** */
	
		/**
		 * Closes the database connection.
		 * @return Status the status of the operation.
		 */
		public Status close() {
			try {
				con.close();
			} catch (SQLException e) {
				return Status.DB_CLOSE_CON_ERROR;
			}
			return Status.SUCCESS;
		}
		
		/* ****************************************************************** */

		/**
		 * Gets the connection to the database.
		 * @return Connection the connection to the database.
		 */
		public Connection getCon() {			
			return con;
		}
	
		/* ****************************************************************** */
	
		
		
		public boolean checkDBFile() throws IOException {
			
			
			if (pathToDB==null) {
				setDBFolder();
				
			} else {
			}
			
			File fpath = new File(pathToDB);
			if (!fpath.exists()) {
				boolean success = fpath.mkdir();
				if (!success) {
					System.out
					.println("Could not create directory for database file.");
				}
			}

			String filename = "";
			
			filename = fpath + System.getProperty("file.separator") + this.sqlite_db_name;
			//filename = fpath + System.getProperty("file.separator") + "DisGeNET_2014.db";
			//(filename);
			File file = new File(filename);
			
			if (!file.exists() || file.length()==0) {

				//JAVI: Database file does not exist
				JOptionPane.showMessageDialog(null,
					"Database file does not exist. Going to dowload it.");
				
				DownloadDBTask downloadDBTask  = new DownloadDBTask("https://www.disgenet.org/download/sqlite/current/2020", filename, pathToDB);
				TaskIterator taskIterator = CyActivator.getInstance().getGlobalTaskIterator();
				taskIterator.append(downloadDBTask);
//				CyActivator.getInstance().getDialogTaskManagerService().execute(taskIterator);
				//("task " +task);

				/* JAVI
				JTaskConfig config = new JTaskConfig();
		        config.displayCancelButton(true);
		        config.displayStatus(true); */
		        
		        //JAVI
				//TaskManager.executeTask(task, config);
			}

			//JAVI COMMENTED
			/*
			if (!file.exists()) {
				// show popup that something went wrong
				
				JOptionPane.showMessageDialog(null,
					"Something went wrong while downloading the database file. Please try again later.");
				
				return false;
			}*/

			return true;
		}

		public void setDatabaseLocal(){
			prefs.put("DB_HOST_v7","local");
			prefs.put("pathToDB_v7", "NOT_DEFINED_YET");
		}

		public void setDatabaseRemote(){
			prefs.put("DB_HOST_v7","remote");
		}

		public String getDatabaseHost(){
			return prefs.get("DB_HOST_v7","NOT_DEFINED_YET");
		}

		public boolean setMySQLparameters(String host, String user, String pass, String db){
			prefs.put("MYSQL_USER", user);
			prefs.put("MYSQL_PASS", pass);
			prefs.put("MYSQL_HOST", host);
			prefs.put("MYSQL_DB", db);
			this.load_mysql_parameters();
			return true;
		}

		private void load_mysql_parameters(){
			this.mysql_dbhost = prefs.get("MYSQL_HOST","NOT_DEFINED_YET");
			this.mysql_dbuser = prefs.get("MYSQL_USER","NOT_DEFINED_YET");
			this.mysql_dbpass = prefs.get("MYSQL_PASS","NOT_DEFINED_YET");
			this.mysql_db = prefs.get("MYSQL_DB","NOT_DEFINED_YET");			
		}

		@Override
		public String setDBFolder() {
			// TODO Auto-generated method stub
			return null;
		}
}
