package es.imim.DisGeNET.database;

/**
	DisGeNET: A Cytoscape plugin to visualize, integrate, search and analyze gene-disease networks
    Copyright (C) 2010  Michael Rautschka, Anna Bauer-Mehren

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.Serializable;
import java.rmi.Remote;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import es.imim.DisGeNET.exceptions.DisGeNetException;
import es.imim.DisGeNET.gui.GuiParameters;

	/**
	 * 
	 * @author mrautschka
	 *
	 */

	public interface Database extends Remote, Serializable {

		/* ****************************************************************** */
		/*  SQL methods for gene network									  */
		/* ****************************************************************** */

		public ArrayList<String> getAssociatedDiseases(String geneId, String src, GuiParameters curParams);
		
		/* ****************************************************************** */
		
		public ArrayList<String> getAssociatedDiseases(String geneId, String src,
				String assocType, GuiParameters curParams);

		/* ****************************************************************** */

		public String getGeneId(String geneName);
		
		/* ****************************************************************** */
		
		public String getGeneIdWildCardGda(String geneName, GuiParameters params);
		
		/* ****************************************************************** */
		
		public HashMap<String, HashMap<String, String>> getGeneAttributes(GuiParameters curParams) throws DisGeNetException;
		
		/* ****************************************************************** */
		
		public HashMap<String, String> getEdgeAttributes(String assocId, GuiParameters curParams);
		
		
		/* ****************************************************************** */
		
		public HashMap<String, ArrayList<String>> getGeneProjectionBySrc(GuiParameters curParams);
		
		/* ****************************************************************** */
		
	
		public ArrayList<String> getGeneList(GuiParameters curParams) throws SQLException;
		
		public ArrayList<String> getVariantList (GuiParameters curParams);
		
		/* ****************************************************************** */
	

	
		/* ****************************************************************** */
		/*  SQL methods for disease network									  */
		/* ****************************************************************** */
		
		//JAVI
		public HashMap<String, HashMap<String, String>> getDiseaseAttributes(GuiParameters curParams) throws DisGeNetException;
		
		/* ****************************************************************** */

		public HashMap<String, String> getDiseaseClassForAllDiseases(GuiParameters curParams);
		
		/* ****************************************************************** */
		
		public HashMap<String, String> getDiseaseClassForAllGenes(String source, GuiParameters curParams);
		
		/* ****************************************************************** */
				
		public String getDiseaseClassId(String diseaseClassName, GuiParameters curParams);
		
		/* ****************************************************************** */
		
		public String getDiseaseIdWildCardGDA(String diseaseName, GuiParameters curParams);
		
		/* ****************************************************************** */
		
		public HashMap<String, ArrayList<String>> getDiseaseProjectionBySrc(GuiParameters curParams);
		
		/* ****************************************************************** */
		
		public ArrayList<String> getAssociatedGeneNames(String diseaseId, String source, String associationType, GuiParameters curParams);		
	
		/* ****************************************************************** */
		
		public ArrayList<String> getDiseaseList(GuiParameters curParams);
		
		/* ****************************************************************** */
		
		
		/* ****************************************************************** */
		/*  SQL methods for gene disease network							  */
		/* ****************************************************************** */

		public ArrayList<String> getGeneDiseaseList(GuiParameters curParams);
		
		/* ****************************************************************** */
		
		public HashMap<String, ArrayList<String>> getGeneDiseaseNetworkBySrc(GuiParameters curParams) throws DisGeNetException;
		
		/* ****************************************************************** */
		
		/* ****************************************************************** */
		/* general methods													  */
		/* ****************************************************************** */
		
		public ResultSet runQuery(String query);

		ArrayList<String> getGeneIdList(GuiParameters params);

		public ArrayList<String> getDiseaseListVariant(GuiParameters params);
		
		public ArrayList<String> getGeneListVariant(GuiParameters params);

		public HashMap<String, ArrayList<String>> getVariantDiseaseNetworkBySrc(GuiParameters curParams) throws DisGeNetException;
		
		public Map <String, String> getAssociatedGenesToVariants(List<String> genes) throws SQLException;

		public Map<String, List<String>> getDiseasesAssociatedToGenes(GuiParameters params) throws DisGeNetException;



}