package es.imim.DisGeNET.database;
/**
 * Class that contains all the properties for constant values. 
 * @author jsauch
 */
public class DatabaseProps {

	public static final String DISEASE_CODE_REGEX = "^C[0-9]{7}$";
	public static final String DISEASE_SEARCH_REGEX = "('C[0-9]{7}',)*'C[0-9]{7}'";
	
	public static final String GENE_SEARCH_REGEX = "(\\d+,)*\\d+";
	
	public static final String VARIANT_STRING_SEARCH = "('(rs)\\d+',)*'(rs)\\d+'";

}
