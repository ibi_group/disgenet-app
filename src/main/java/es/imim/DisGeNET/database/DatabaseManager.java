package es.imim.DisGeNET.database;

/**
	DisGeNET: A Cytoscape plugin to visualize, integrate, search and analyze gene-disease networks
	Copyright (C) 2010  Michael Rautschka, Anna Bauer-Mehren
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.io.IOException;
import java.sql.Connection;

import es.imim.DisGeNET.Status;

/**
 * This interface handles the database connection for all database functions
 * @author mrautschka
 *
 */

public interface DatabaseManager {
	
		/**
		 * Gets the current connection to the database.
		 * @return Connection
		 */
		public Connection getCon();
		
		/**
		 * Initializes the connection to the database.
		 * @return Status
		 */
		public Status init();
		
		/**
		 * Closes the connection to the database.
		 * @return Status
		 */
		public Status close();

		/**
		 * Allows to change the preference settings for the
		 * database folder
		 * @return path to folder
		 */
		public String setDBFolder();
		
		/**
		 * Checks for presence of database file in defined 
		 * folder and downloads it if not present
		 * @return true in case the database file exists or false in case not.
		 * @throws IOException in case an exception occurs handling the file.  
		 */
		public boolean checkDBFile() throws IOException;

		public void setDatabaseLocal();
		public void setDatabaseRemote();
		public String getDatabaseHost();
		public boolean setMySQLparameters(String host, String user, String pass, String db);

		public int setPreferences();
}
