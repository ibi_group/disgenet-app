package es.imim.DisGeNET.database;

/**
	DisGeNET: A Cytoscape plugin to visualize, integrate, search and analyze gene-disease networks
    Copyright (C) 2015  Michael Rautschka, Anna Bauer-Mehren, Javier García-García

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.ArrayList;
import java.util.List;
import java.lang.String;

public class Query{

    private List<String> tables;
    private List<String> columns;
    private List<String> conditions;
    private String group_by;
	
    /* Constructor */
    public Query() {
        this.tables = new ArrayList<String>();
        this.columns = new ArrayList<String>();
        this.conditions  = new ArrayList<String>();
        this.group_by = null;
    }

    public void addTable(String table){
        this.tables.add(table);
    }

    public void addColumn(String column){
        this.columns.add(column);
    }

    public void addCondition(String condition){
        this.conditions.add(condition);
    }

    public void setGroupBy(String group_by){
        this.group_by = group_by;
    }

    private String concat(List<String> list, String delimiter){
        String joined = "";
        boolean first = true;
        for( String item:list){
            if (first){
                first=false;
            }
            else{
                joined += delimiter;
            }
            joined += item;
        }
        return joined;
    }

    public String build(){
        
        String query = "SELECT  ";
        query += this.concat(this.columns, ",");
        query += " FROM ";
        query += this.concat(this.tables, ",");
        
        if( this.conditions.size()>0 ){
            query += " WHERE ";
            query += this.concat(this.conditions, " AND ");
        }

        if( this.group_by!=null ){
            query += " "+this.group_by;
        }

        return query;
    }
	
}