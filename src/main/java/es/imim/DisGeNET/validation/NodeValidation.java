package es.imim.DisGeNET.validation;

public class NodeValidation {
	
	public final static String DISEASE_CUI_REGEX = "^C[0-9]{7}$";
	public final static String VARIANT_SNP_REGEX = "rs\\d+";
	
	/**
	 * Filters the given string and checks for one of the following matches, gene symbol, disease cui or variant snp.
	 * @param nodeEntry the string to check.
     * @return the type of the node as a string
	 */
	public static String filterNodeType(String nodeEntry) {
		String flag = "";
		if(nodeEntry.matches(DISEASE_CUI_REGEX)){
			flag = "disease";
		}else if(nodeEntry.matches(VARIANT_SNP_REGEX)) {
			flag = "variant";
		}else {
			flag = "gene";
		}
		
		return flag;
	}
	
}
