package es.imim.DisGeNET.validation;

import java.util.*;

import es.imim.DisGeNET.gui.GuiProps;
import es.imim.DisGeNET.internal.CyActivator;
import es.imim.DisGeNET.internal.automation.parameters.*;
import org.cytoscape.model.CyColumn;

public class ParametersValidation {
	
	public static ArrayList<String> sourceOptions;
	public static ArrayList<String> assocTypeOptions;
	public static ArrayList<String> disClassOptions = new ArrayList<>(Arrays.asList(GuiProps.DISEASE_CLASS_OPTIONS));
	public static List<String> enrichmentTypeIds;
	public static List<String> enrichmentUniverse;
	
	public static Map geneDiseaseParameters(GeneDiseaseParams params) {
		HashMap<String, String> errors = new HashMap<>();
		Double minScore, maxScore;
		if(params.getSource()!=null) {
			sourceOptions = new ArrayList<>(Arrays.asList(GuiProps.SOURCE_OPTIONS));
			if(!checkSource(params.getSource().trim())) {
				errors.put("source", "Invalid source, check the possible source options.");
			}
		}else {
			errors.put("source", "Missing source, the source field is required.");
		}
		if(params.getAssocType()!=null) {
			assocTypeOptions = new ArrayList<>(Arrays.asList(GuiProps.ASSOCIATION_TYPE_OPTIONS));
			if(!checkAssocType(params.getAssocType().trim())) {
				errors.put("assocType", "Invalid association type, check the possible association types.");
			}
		}
		if(params.getDiseaseClass()!=null) {
			if(!checkDiseaseClass(params.getDiseaseClass().trim())) {
				errors.put("diseaseClass", "Invalid disease class, check the possible disease classes");
			}
		}
		if(params.getInitialScoreValue()!=null || params.getFinalScoreValue()!=null) {
			String validScore = checkScores(params.getInitialScoreValue(), params.getFinalScoreValue());
			if(!validScore.equals("")) {
				errors.put("scoreRange", validScore);
			}
		}
		return errors;
	}

	public static Map variantDiseaseParameters(VariantDiseaseParams params) {
		HashMap<String, String> errors = new HashMap<>();
		if (params.getSource()!=null) {
			sourceOptions = new ArrayList<>(Arrays.asList(GuiProps.VARIANT_SOURCE_OPTIONS));
			if (!checkSource(params.getSource().trim())) {
				errors.put("source", "Invalid datasource, check the possible source options.");
			} 
		}else {
			errors.put("source", "Missing source, the source field is required.");
		}
		if (params.getAssocType()!=null) {
			assocTypeOptions = new ArrayList<>(Arrays.asList(GuiProps.VARIANT_ASSOCIATION_TYPE_OPTIONS));
			if (!checkAssocType(params.getAssocType().trim())) {
				errors.put("assocType", "Invalid association type, check the possible association types.");
			} 
		}
		if(params.getDiseaseClass()!=null) {
			if(!checkDiseaseClass(params.getDiseaseClass().trim())) {
				errors.put("diseaseClass", "Invalid disease class, check the possible disease classes");
			}
		}
		if(params.getInitialScoreValue()!=null || params.getFinalScoreValue()!=null) {
			String validScore = checkScores(params.getInitialScoreValue(), params.getFinalScoreValue());
			if(!validScore.equals("")) {
				errors.put("scoreRange", validScore);
			}
		}
		return errors;
	}
	
	public static Map geneProjParameters(GeneProjParams params) {
		HashMap<String, String> errors = new HashMap<>();
		if(params.getSource()!=null) {
			sourceOptions = new ArrayList<>(Arrays.asList(GuiProps.SOURCE_OPTIONS));
			if(!checkSource(params.getSource().trim())) {
				errors.put("source", "Invalid source, check the possible source options.");
			}
		}else {
			errors.put("source", "Missing source, the source field is required.");
		}
		if(params.getDiseaseClass()!=null) {
			if(!checkDiseaseClass(params.getDiseaseClass().trim())) {
				errors.put("disClass", "Invalid disease class, check the possible disease classes");
			}
		}
		return errors;
	}
	
	public static Map diseaseProjParameters(DiseaseProjParams params) {
		HashMap<String, String> errors = new HashMap<>();
		if(params.getSource()!=null) {
			sourceOptions = new ArrayList<>(Arrays.asList(GuiProps.SOURCE_OPTIONS));
			if(!checkSource(params.getSource().trim())) {
				errors.put("source", "Invalid source, check the possible source options.");
			}
		}else {
			errors.put("source", "Missing source, the source field is required.");
		}
		if(params.getDiseaseClass()!=null) {
			if(!checkDiseaseClass(params.getDiseaseClass().trim())) {
				errors.put("diseaseClass", "Invalid disease class, check the possible disease classes");
			}
		}
		return errors;
	}

	public static Map geneEnrichmentParameters(GeneEnrichmentParams params){
	    Map<String, String> errors = new HashMap<>();
	    if(params.getNetworkId()==null){
	        errors.put("networkId", "Required field. Please add a valid value for the networkId field.");

        }else{
            if(params.getColumnName()!=null){
                CyColumn selectedCol = CyActivator.getInstance().getNetworkManagerService().getNetwork(params.getNetworkId()).getDefaultNodeTable().getColumn(params.getColumnName());
                if(selectedCol==null){
                    errors.put("columnName", "The introduced column does not exist in the node table for the selected network.");
                }
            }else{
                errors.put("columnName", "Required field. Please add a valid value for the columnName field.");
            }
        }
        if(params.getSource()!=null) {
            sourceOptions = new ArrayList<>(Arrays.asList(GuiProps.SOURCE_OPTIONS));
            if(!checkSource(params.getSource().trim())) {
                errors.put("source", "Invalid source, check the possible source options.");
            }
        }else {
            errors.put("source", "Required field. Please add a valid value for the source field.");
        }
        if(params.getTypeId()!=null){
            enrichmentTypeIds = new ArrayList<>(Arrays.asList("ENTREZID", "SYMBOL"));
            if(!checkTypeId(params.getTypeId())){
                errors.put("typeId", "Invalid typeId, check the possible typeId options.");
            }
        }else{
            errors.put("typeId", "Required field. Please add a valid value for the typeId field.");
        }
//        if(params.getUniverse()!=null){
//            enrichmentUniverse = new ArrayList<>(Arrays.asList("DISGENET","HUMAN","HUMAN_CODING"));
//            if(!checkUniverse(params.getUniverse())) {
//                errors.put("universe", "Invalid universe, check the possible universe options.");
//            }
//        }else{
//            errors.put("universe", "Required field. Please add a valid value for the universe field.");
//        }
        return errors;
    }

    public static Map variantEnrichmentParameters(VariantEnrichmentParams params){
        Map<String, String> errors = new HashMap<>();
        if(params.getNetworkId()==null){
            errors.put("networkId", "Required field. Please add a valid value for the networkId field.");

        }else{
            if(params.getColumnName()!=null){
                CyColumn selectedCol = CyActivator.getInstance().getNetworkManagerService().getNetwork(params.getNetworkId()).getDefaultNodeTable().getColumn(params.getColumnName());
                if(selectedCol==null){
                    errors.put("columnName", "The introduced column does not exist in the node table for the selected network.");
                }
            }else{
                errors.put("columnName", "Required field. Please add a valid value for the columnName field.");
            }
        }
        if(params.getSource()!=null) {
            sourceOptions = new ArrayList<>(Arrays.asList(GuiProps.VARIANT_SOURCE_OPTIONS));
            if(!checkSource(params.getSource().trim())) {
                errors.put("source", "Invalid source, check the possible source options.");
            }
        }else {
            errors.put("source", "Required field. Please add a valid value for the source field.");
        }
        if(params.getTypeId()!=null){
            enrichmentTypeIds = new ArrayList<>(Arrays.asList("DBSNP"));
            if(!checkTypeId(params.getTypeId())){
                errors.put("typeId", "Invalid typeId, check the possible typeId options.");
            }
        }
//        if(params.getUniverse()!=null){
//            enrichmentUniverse = new ArrayList<>(Arrays.asList("DISGENET","HUMAN","HUMAN_CODING"));
//            if(!checkUniverse(params.getUniverse())) {
//                errors.put("universe", "Invalid universe, check the possible universe options.");
//            }
//        }else{
//            errors.put("universe", "Required field. Please add a valid value for the universe field.");
//        }
        return errors;
    }

    private static boolean checkUniverse(String universe) {
	    boolean flag=false;
	    if(enrichmentUniverse.contains(universe.toUpperCase())){
	        flag=true;
        }
	    return flag;
    }

    private static boolean checkSource(String source) {
		boolean flag=false;
		if(sourceOptions.contains(source)) {
			flag=true;
		}
		return flag;
	}
	
	private static boolean checkAssocType(String assocType) {
		boolean flag = false;
		if(assocTypeOptions.contains(assocType)) {
			flag=true;
		}
		return flag;
	}
	
	private static boolean checkDiseaseClass(String diseaseClass) {
		boolean flag = false;
		if(disClassOptions.contains(diseaseClass)) {
			flag = true;
		}
		return flag;
	}

	private static boolean checkTypeId(String typeId){
	    boolean flag = false;
	    if(enrichmentTypeIds.contains(typeId.toUpperCase())){
	        flag = true;
        }
	    return flag;
    }
	
	private static String checkScores (String lowScore, String highScore) {
		String message = "";
		Double minScore, maxScore;
		if(lowScore!=null) {
			try {
				minScore = Double.parseDouble(lowScore);
				if(!checkScoreRange(minScore)) {
					message+="The score range minimun value is out of bounds. [0.0-1.0]";
				}
			}catch (NumberFormatException e) {
				minScore = 0.0;
				message += "The score range minimum value is not a double value or is malformed.\n";
			}
		}else {
			minScore = 0.0;
		}
		if(highScore!=null) {
			try {
				maxScore = Double.parseDouble(highScore);
				if(!checkScoreRange(maxScore)) {
					message+="The score range max value is out of bounds. [0.0-1.0]";
				}
			}catch (NumberFormatException e) {
				maxScore = 1.0;
				message += "The score range max value is not a double value or is malformed.\n";
			}
		}else {
			maxScore = 1.0;
		}
		if(minScore>maxScore) {
			message += "Invalid score range, the minimum value is greater than the max value.";
		}
		return message;
	}

	private static boolean checkScoreRange(Double score) {
		boolean flag = false;
		if(score>=0&&score<=1) {
			flag=true;
		}
		return flag;
	}
	
}
