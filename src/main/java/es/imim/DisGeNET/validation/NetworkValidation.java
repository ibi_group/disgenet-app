 package es.imim.DisGeNET.validation;

import java.util.List;

import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyTable;

import es.imim.DisGeNET.internal.CyActivator;

public class NetworkValidation {
	
	/**
	 * Checks if the current network is a variant-disease network.
	 * @param cyNetwork the network to check. 
	 * @return true in case the network is a variant network or false in case there's no network or isn't a variant.
	 */
	public static boolean isVariantNet(CyNetwork cyNetwork) {
		boolean flag = false;
		if(cyNetwork!=null) {
			CyTable nodeTable = cyNetwork.getDefaultNodeTable();
			CyColumn nodeType = nodeTable.getColumn("nodeType");
			if(nodeType.getValues(String.class).contains("variant")) {
				flag=true;
			}
		}
		return flag;
	}
	
	public static boolean isProjectionNet(CyNetwork cyNetwork, String interaction) {
		boolean flag = false;
		if(cyNetwork!=null) {
			CyTable edgeTable = cyNetwork.getDefaultEdgeTable();
			CyColumn interactionColumn = edgeTable.getColumn("interaction");
			if(interactionColumn.getValues(String.class).contains(interaction)) {
				flag = true;
			}
		}
		return flag;
	}
	
	/**
	 * Checks if the the variant network is already expanded.
	 * @param cyNetwork the network to check.
	 * @return true in case the variant network us already expanded false in case not.
	 */
	public static boolean isExpandedWithGenes(CyNetwork cyNetwork) {
		boolean flag = false;
		if(isVariantNet(cyNetwork)) {
			if(cyNetwork!=null) {
				CyTable nodeTable = cyNetwork.getDefaultNodeTable();
				CyColumn nodeType = nodeTable.getColumn("nodeType");
				if(nodeType.getValues(String.class).contains("gene")) {
					flag=true;
				}
			}
		}
		return flag;
	}
	
	/**
	 * Checks if the current network is original DisGeNET network or not.
	 * @param cyNetwork the network to check the origin.
	 * @return true in case the network was formed in disgenet or false in case not.
	 */
	public static boolean isForeignNet(CyNetwork cyNetwork) {
		boolean flag = true;
		if(cyNetwork!=null) {
			CyTable nodeTable = cyNetwork.getDefaultNodeTable();
			CyColumn nodeTypeColumn = nodeTable.getColumn("nodeType");
			if(nodeTypeColumn!=null) {
				List<String> nodeTypes = nodeTypeColumn.getValues(String.class);
				if(nodeTypes.contains("disease")||nodeTypes.contains("gene")||nodeTypes.contains("variant")) {
					flag = false;
				}
			}
		}
		return flag;
	}
}
