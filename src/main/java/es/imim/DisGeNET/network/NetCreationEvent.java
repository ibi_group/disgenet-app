package es.imim.DisGeNET.network;

/**
	DisGeNET: A Cytoscape plugin to visualize, integrate, search and analyze gene-disease networks
	Copyright (C) 2010  Michael Rautschka, Anna Bauer-Mehren
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.EventObject;

/** part of event handler class to allow updating the progress bar from different task */
public class NetCreationEvent extends EventObject {
	
	private static final long serialVersionUID = 5173255186316764515L;
		private Double _pct;
	    
	    public NetCreationEvent( Object source, Double pct ) {
	        super( source );
	        _pct = pct;
	    }
	    
	    public Double pct() {
	        return _pct;
	    }
	
}
