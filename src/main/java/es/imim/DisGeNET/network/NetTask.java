package es.imim.DisGeNET.network;


import org.cytoscape.model.CyNetwork;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import es.imim.DisGeNET.exceptions.DisGeNetException;
import es.imim.DisGeNET.gui.GuiParameters;
import es.imim.DisGeNET.internal.CyActivator;

public class NetTask extends AbstractTask {

		private TaskMonitor taskMonitor = null;
		private boolean interrupted = false;

		public boolean isInterrupted() {
			return interrupted;
		}

		private boolean completedSuccessfully = false;
		private GuiParameters curParams;

		public NetTask(GuiParameters curParams) {
			this.curParams = curParams;
		}

		//JAVI
		//public void run() {
		@Override
		public void run(TaskMonitor taskMonitor) throws Exception {
			NetworkBuilder nb = new NetworkBuilder();
			//JAVI
			this.setTaskMonitor(taskMonitor);

			try {
				//JAVI
				//taskMonitor.setPercentCompleted(0);
				//taskMonitor.setStatus("Building network...");
				taskMonitor.setProgress(0);
				taskMonitor.setStatusMessage("Building network...");

				nb.addNetCreationListener(new NetCompletionChangeEvent(	taskMonitor));

				curParams.setEmptyNet(false);
			
				if (curParams.getActiveTab().equals("GeneDisTabPane")) {
					curParams.setActiveTab("GeneDisTabPane");
					nb.buildGeneDiseaseNet(curParams);
										
				}
				// variant diseaseNetwork
				else if (curParams.getActiveTab().equals("VariantDisTabPane")) {
					curParams.setActiveTab("VariantDisTabPane");
					nb.buildVariantDiseaseNet(curParams);
				}
				else if (curParams.getActiveTab().equals("disProj_TabPane")) {
					curParams.setActiveTab("disProj_TabPane");
					nb.buildDiseaseNet(curParams);
				} 
				// gene projections
				else if (curParams.getActiveTab().equals("geneProj_TabPane")) {
					curParams.setActiveTab("geneProj_TabPane");
					nb.buildGeneNet(curParams);
				}
				
				if(CyActivator.getInstance().getApplicationManagerService().getCurrentNetwork()!=null){
				    completedSuccessfully = true;
				}
			}catch (DisGeNetException e) {
				throw new DisGeNetException (e.getMessage());
			}
		}
		
		public boolean isCompletedSuccessfully() {
			return completedSuccessfully;
		}

		//JAVI
		//public void halt() {
		public void cancel() {
			this.interrupted = true;
			curParams.setNetBuildInterrupted(true);
		}

		public void setTaskMonitor(TaskMonitor taskMonitor)
				throws IllegalThreadStateException {
			if (this.taskMonitor != null) {
				throw new IllegalStateException(
						"Task Monitor has already been set.");
			}
			this.taskMonitor = taskMonitor;
		}

		public GuiParameters getCurParams() {
			return curParams;
		}

		public void setCurParams(GuiParameters curParams) {
			this.curParams = curParams;
		}
		
		
		
	}