package es.imim.DisGeNET.network;

/**
	DisGeNET: A Cytoscape plugin to visualize, integrate, search and analyze gene-disease networks
	Copyright (C) 2010  Michael Rautschka, Anna Bauer-Mehren
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import org.cytoscape.work.TaskMonitor;


/** part of event handler class to allow updating the progress bar from different task */
public class NetCompletionChangeEvent implements NetCreationListener {
	
	private TaskMonitor taskMonitor = null;
	
	public NetCompletionChangeEvent(TaskMonitor taskMonitor) {
		this.taskMonitor = taskMonitor;
	}
	
	@Override
	public void NetCreationEventReceived(NetCreationEvent event) {
		Double i = event.pct();
		//JAVI. To check. need to divide by 100?
		//this.taskMonitor.setPercentCompleted(i);
		this.taskMonitor.setProgress(i);
	}
}
