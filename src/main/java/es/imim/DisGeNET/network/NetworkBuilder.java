package es.imim.DisGeNET.network;

/**
	DisGeNET: A Cytoscape plugin to visualize, integrate, search and analyze gene-disease networks
	Copyright (C) 2010  Michael Rautschka, Anna Bauer-Mehren

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.lang.Integer;
import java.lang.String;

import java.lang.Long;

import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyTable;
import org.cytoscape.model.CyRow;

import org.cytoscape.view.layout.CyLayoutAlgorithm;
import org.cytoscape.view.model.*;
import org.cytoscape.view.vizmap.*;
import org.cytoscape.work.TaskIterator;
import org.cytoscape.work.swing.DialogTaskManager;
//import org.cytoscape.data.Semantics;
//import org.cytoscape.model.attribute.Semantics;



import es.imim.DisGeNET.database.Database;
import es.imim.DisGeNET.database.DatabaseImpl;
import es.imim.DisGeNET.database.DatabaseProps;
import es.imim.DisGeNET.exceptions.DisGeNetException;
import es.imim.DisGeNET.gui.GuiParameters;
//Added to access the services
import es.imim.DisGeNET.internal.CyActivator;
import es.imim.DisGeNET.internal.tables.DisGeNetCyTableBase;
import es.imim.DisGeNET.internal.tables.DisGeNetCyTableDisease;
import es.imim.DisGeNET.internal.tables.DisGeNetCyTableGene;
import es.imim.DisGeNET.styles.DisGenetStyleManager;
import es.imim.DisGeNET.tool.HelperFunctions;



/**
 * Main class for - creating the networks 
 * - creating networks with edge attributes 
 * - creating additional attributes 
 * - handle the progress listeners 
 * - retrieve all node names from current net for search 
 * - marking nodes from search
 * 
 * @author michael
 * 
 */
public class NetworkBuilder {

	private ArrayList<NetCreationListener> _listeners = new ArrayList<NetCreationListener>();
	private Double _pct = 0.0;
	private Database db = null;
	private GuiParameters params;
	private Map <String,Long> nodeAttributes =  new HashMap<>();

	//ADDED BY JAVI
	//TO HAVE A CONTROL OF WHICH NODES HAVE BEEN ADDED
	private HashMap<Long, HashMap<String, Long>> network2nodeSUIDs = null;

	public NetworkBuilder() {
		this.init();
	}

	public NetworkBuilder(GuiParameters params) {
		this.params = params;
		this.init();
	}

	private void init(){		
		db = DatabaseImpl.getInstance();	
		network2nodeSUIDs = new HashMap<Long, HashMap<String, Long>>();
	}

	public Database getDB() {
		return db;
	}

	//ADDED BY JAVI
	private String createNetworkName(GuiParameters params){

		String network_name = "DefaultNet";
		String netName = params.getNetworkName();
		if(netName==null) {
			if (params.getActiveTab().equals("GeneDisTabPane")) {
				network_name = "DisGeNET-GDA";
				network_name += " - " + params.getSource();
				
				if (HelperFunctions.checkStringIsNotEmpty(params.getAssociationType())) {
					network_name += " - " + params.getAssociationType();
				}
				if (HelperFunctions.checkStringIsNotEmpty(params.getDiseaseClass())) {
					network_name += " - " + params.getDiseaseClass();
				}

				if (HelperFunctions.checkStringIsNotEmpty(params.getDisSearchText())) {
					network_name += " - " + params.getDisSearchText();
				}

				if (HelperFunctions.checkStringIsNotEmpty(params.getGenSearchText())) {
					network_name += " - " + params.getGenSearchText();
				}
				try {
					if(params.getHighScore()!=null && params.getLowScore()!=null) {
						Double score_up_filter = Double.parseDouble(params.getHighScore());
						Double score_low_filter = Double.parseDouble(params.getLowScore());

						if( score_up_filter<1.0 || score_low_filter>0.0 ){
							network_name += " - " + score_low_filter + ":" + score_up_filter;
						}
					}
				}catch (NumberFormatException e) {
					e.printStackTrace();;
				}

			}
			else if( params.getActiveTab().equals("disProj_TabPane")) {
				network_name = "DisGeNET-disProj";
				network_name += " - " + params.getSource();
				network_name += " - " + params.getDiseaseClass();

				if (HelperFunctions.checkStringIsNotEmpty(params.getDisSearchText())) {
					network_name += " - " + params.getDisSearchText();
				}			
			}
			else if( params.getActiveTab().equals("geneProj_TabPane")){
				network_name = "DisGeNET-genProj";
				network_name += " - " + params.getSource();
				network_name += " - " + params.getDiseaseClass();			

				if (HelperFunctions.checkStringIsNotEmpty(params.getGenSearchText())) {
					network_name += " - " + params.getGenSearchText();
				}
			}
			else if (params.getActiveTab().equals("VariantDisTabPane")) {
				network_name = "DisGeNET-VDA";
				network_name += " - " + params.getSource();
				
				if (HelperFunctions.checkStringIsNotEmpty(params.getAssociationType())) {
					network_name += " - " + params.getAssociationType();
				}
				if (HelperFunctions.checkStringIsNotEmpty(params.getDiseaseClass())) {
					network_name += " - " + params.getDiseaseClass();
				}
				if (HelperFunctions.checkStringIsNotEmpty(params.getVarSearchText())) {
					network_name += " - " + params.getVarSearchText();
				}

				if (HelperFunctions.checkStringIsNotEmpty(params.getDisSearchText())) {
					network_name += " - " + params.getDisSearchText();
				}
				
				if (HelperFunctions.checkStringIsNotEmpty(params.getGenSearchText())) {
					network_name += " - " + params.getGenSearchText();
				}
				
				try {
					if(params.getHighScore()!=null && params.getLowScore()!=null) {
						Double score_up_filter = Double.parseDouble(params.getHighScore());
						Double score_low_filter = Double.parseDouble(params.getLowScore());

						if( score_up_filter<1.0 || score_low_filter>0.0 ){
							network_name += " - " + score_low_filter + ":" + score_up_filter;
						}
					}
				}catch (NumberFormatException e) {
					e.printStackTrace();;
				}
			}

		}else {
			network_name = params.getNetworkName();
		}
		
		return network_name;
	}

	//ADDED BY JAVI
	public CyNetwork createCyNetwork(String name){
		CyNetwork cyNetwork = CyActivator.getInstance().getNetworkFactoryService().createNetwork();
		cyNetwork.getRow(cyNetwork).set(CyNetwork.NAME, name);
		CyActivator.getInstance().getNetworkManagerService().addNetwork(cyNetwork);
		HashMap<String, Long> nodesHash = new HashMap<String, Long>();
		//(nodesHash.isEmpty()+this.getClass().getName()+"line 170");
		this.network2nodeSUIDs.put(cyNetwork.getSUID(), nodesHash);

		this.createNodeAttributeColumns(cyNetwork);

		return cyNetwork;
	}
	
	//ADDED BY JAVI
	private CyNode getCyNode(CyNetwork cyNetwork, String node_name){
		return cyNetwork.getNode(nodeAttributes.get(node_name));
	}

	//ADDED BY JAVI
	//INSERT NODE AVOIDING DUPLICATED NAMES
	private CyNode addCyNode(CyNetwork cyNetwork, String node_name){
		CyNode node;
		if( this.network2nodeSUIDs.get(cyNetwork.getSUID()).containsKey(node_name) ){
			node = this.getCyNode(cyNetwork, node_name);
			if(!this.nodeAttributes.containsKey(node_name)){
				this.nodeAttributes.put(node_name,node.getSUID());
			}
		}
		else{
			node = cyNetwork.addNode();
			cyNetwork.getRow(node).set(CyNetwork.NAME, node_name);
			this.network2nodeSUIDs.get(cyNetwork.getSUID()).put(node_name, node.getSUID());
			this.nodeAttributes.put(node_name,node.getSUID());
		}
		return node;
	}

	//ADDED BY JAVI
	private boolean hasCyNode(CyNetwork cyNetwork, String node_name){
		return this.network2nodeSUIDs.get(cyNetwork.getSUID()).containsKey(node_name);
	}

	//ADDED BY JAVI
	private void addCyNodeAttribute(CyNetwork cyNetwork, CyNode cyNode, String attributeField, Object attributeValue ){
		cyNetwork.getRow(cyNode).set(attributeField, attributeValue);
	}


	public void createNodeAttributeColumns(CyNetwork cyNetwork){
		CyTable cyTable = cyNetwork.getDefaultNodeTable();
		DisGeNetCyTableBase nodeColumns =  new DisGeNetCyTableBase(params);
		nodeColumns.addNodeColumns(cyTable);
	}

	public void createCyEdgeAttributeColumns(CyNetwork cyNetwork){
		CyTable cyTable = cyNetwork.getDefaultEdgeTable();
		cyTable.createColumn("source", String.class, false);
		cyTable.createColumn("associationType", String.class, false);
		cyTable.createColumn("sentence", String.class, false);
		cyTable.createColumn("pmid", String.class, false);
		cyTable.createColumn("score", Double.class, false);
		cyTable.createColumn("evidence index", Double.class, false);
	}

	private void expandNetWithEdgeAttributes(ArrayList<String> intList,
			ArrayList<String> disList, ArrayList<String> genVarList,
			ArrayList<String> assocList, ArrayList<String> assocTypeList,
			ArrayList<String> srcList,
			ArrayList<String> sentenceList,
			ArrayList<String> pmidsList,
			ArrayList<String> scoreList,
			ArrayList<String> eiList,
			ArrayList<String> elList, GuiParameters params) throws DisGeNetException
	{
		this.params = params;
		CyNetwork cyNetwork = CyActivator.getInstance().getApplicationManagerService().getCurrentNetwork();
		addColumnsToProjection(cyNetwork,params);
		CyTable edgeTable = cyNetwork.getDefaultEdgeTable();
		Iterator<String> intIt = intList.iterator();
		Iterator<String> disIt = disList.iterator();
		Iterator<String> genIt = genVarList.iterator();
		Iterator<String> assocIt = assocList.iterator();
		Iterator<String> assocTypeListIt = assocTypeList.iterator();
		Iterator<String> srcListIt = srcList.iterator();
		Iterator<String> sentenceListIt = sentenceList.iterator();
		Iterator<String> pmidsListIt = pmidsList.iterator();
		Iterator<String> scoreListIt = scoreList.iterator();
		Iterator<String> eiListIt = eiList.iterator();
		Iterator<String> elListIt = elList.iterator();
		
		while(disIt.hasNext()&&genIt.hasNext()) {
			String interaction = intIt.next();
			String dis = disIt.next();
			String gen = genIt.next();
			String association = assocIt.next();
			String assocType = assocTypeListIt.next();
			String org_src = srcListIt.next();
			String sentence = sentenceListIt.next();
			String pmid = pmidsListIt.next();
			String score = scoreListIt.next();
			String ei = eiListIt.next();
			String el = null;
			if(elListIt.hasNext()) {
				el = elListIt.next();
			}
			
			CyNode disNode;
			if(nodeAttributes.get(dis)==null) {
				disNode = cyNetwork.addNode();
				cyNetwork.getRow(disNode).set(CyNetwork.NAME, dis);
				nodeAttributes.put(dis, disNode.getSUID());
			}else {
				disNode = cyNetwork.getNode(nodeAttributes.get(dis));
			}

			CyNode geneNode; 
			if(nodeAttributes.get(gen)==null) {
				geneNode = cyNetwork.addNode();
				cyNetwork.getRow(geneNode).set(CyNetwork.NAME, gen);
				nodeAttributes.put(gen, geneNode.getSUID());
			}else {
				geneNode = cyNetwork.getNode(nodeAttributes.get(gen));
			}
			
			if(!edgeTable.getColumn("interaction").getValues(String.class).contains(interaction)) {
				CyEdge edge = cyNetwork.addEdge(disNode, geneNode, false);
				edgeTable.getRow(edge.getSUID()).set("interaction", interaction);
				edgeTable.getRow(edge.getSUID()).set("association",association);
				edgeTable.getRow(edge.getSUID()).set("associationType", assocType);
				edgeTable.getRow(edge.getSUID()).set("source", org_src);
				edgeTable.getRow(edge.getSUID()).set("sentence", sentence);
				edgeTable.getRow(edge.getSUID()).set("pmid", pmid);
				try {
					edgeTable.getRow(edge.getSUID()).set("score", Double.parseDouble(score));
				}
				catch (Exception e) {
					edgeTable.getRow(edge.getSUID()).set("score", null);
				}
				try {
					cyNetwork.getRow(edge).set("evidence index",Double.parseDouble(ei));
				}
				catch (Exception e) {
					cyNetwork.getRow(edge).set("evidence index",null);
				}
				if(edgeTable.getColumn("evidence level")!=null) {
					cyNetwork.getRow(edge).set("evidence level",el);
				}
				
			}
			
			
		}
		DisGeNetCyTableBase table = new DisGeNetCyTableBase(params);
		table.addNodeColumns(cyNetwork.getDefaultNodeTable());
		table.fillNodeColumns(cyNetwork.getDefaultNodeTable(), nodeAttributes);
		initAdditionalStyleAttributes(params);
	}

	/**
	 * Expands the selected gene disease network using the selected nodes as reference.
	 * @param curParams the parameters of the current network.
	 * @param nodeMap the nodes that exist in the current network.
	 * @return map containing the existing nodes plus the added ones, with the name as key and the suid as value.
	 * @throws DisGeNetException low level exception with the message for the user. 
	 * @throws SQLException in case of database access error.
	 */
	public Map <String,Long> expandGeneDiseaseNet(GuiParameters curParams,Map<String,Long> nodeMap) throws SQLException, DisGeNetException {
		//		GuiParameters curParams = new GuiParameters();
		this.nodeAttributes = nodeMap;
		ArrayList <String> errors = new ArrayList<>();
		HashMap<String, ArrayList<String>> geneDisNet = db.getGeneDiseaseNetworkBySrc(curParams);
		ArrayList<String> intList = geneDisNet.get("intList");
		ArrayList<String> disList = geneDisNet.get("disList");
		ArrayList<String> genList = geneDisNet.get("genList");
		ArrayList<String> assoList = geneDisNet.get("assocList");
		ArrayList<String> srcList = geneDisNet.get("srcList");
		ArrayList<String> assocTypeList = geneDisNet.get("assocTypeList");
		ArrayList<String> pmidsList = geneDisNet.get("pmidsList");
		ArrayList<String> sentenceList = geneDisNet.get("sentenceList");
		ArrayList<String> scoreList = geneDisNet.get("scoreList");
		ArrayList<String> eiList = geneDisNet.get("eiList");
		ArrayList<String> elList = geneDisNet.get("elList");
		CyNetwork mynet = CyActivator.getInstance().getCyApplicationManager().getCurrentNetwork();
		expandNetWithEdgeAttributes(intList, disList, genList, assoList, assocTypeList, srcList, sentenceList, pmidsList, scoreList,eiList, elList,curParams);
		
		if(mynet==null) {
			errors.add("Network creation cancelled by the user.");
			throw new DisGeNetException(errors.get(0));
		}
		else if (mynet.getNodeCount() > 0){
			CyNetworkView netView = CyActivator.getInstance().getApplicationManagerService().getCurrentNetworkView();
			applyNetworkStyle(netView);
			applyGridLayout(netView);
		}
		else {
			throw new DisGeNetException("No results for the given parameters.");
		}
		
		return nodeAttributes;
	}

	/**
	 * Expands the selected variant disease network using the selected nodes as reference.
	 * @param curParams the parameters of the current network.
	 * @param nodeMap the nodes that exist in the current network.
	 * @return map containing the existing nodes plus the added ones, with the name as key and the suid as value.
	 * @throws DisGeNetException low level exception with the error message for the user.
	 * @throws SQLException in case of database access error.
	 */
	public Map <String,Long> expandVariantDiseaseNet(GuiParameters curParams, Map<String,Long> nodeMap) throws SQLException, DisGeNetException  {
		this.nodeAttributes = nodeMap;
		ArrayList<String> errors = new ArrayList<>();
		HashMap<String, ArrayList<String>> varDisNet = db.getVariantDiseaseNetworkBySrc(curParams);
		ArrayList<String> intList = varDisNet.get("intList");
		ArrayList<String> disList = varDisNet.get("disList");
		ArrayList<String> varList = varDisNet.get("varList");
		ArrayList<String> assoList = varDisNet.get("assocList");
		ArrayList<String> srcList = varDisNet.get("srcList");
		ArrayList<String> assocTypeList = varDisNet.get("assocTypeList");
		ArrayList<String> pmidsList = varDisNet.get("pmidsList");
		ArrayList<String> sentenceList = varDisNet.get("sentenceList");
		ArrayList<String> scoreList = varDisNet.get("scoreList");		
		ArrayList<String> eiList = varDisNet.get("eiList");
		
		expandNetWithEdgeAttributes(intList, disList, varList, assoList, assocTypeList, srcList, sentenceList, pmidsList, scoreList,eiList, new ArrayList<String>(),curParams);
		
		CyNetwork mynet = CyActivator.getInstance().getCyApplicationManager().getCurrentNetwork();
		
		if(mynet==null) {
			errors.add("Network creation cancelled by the user.");
			throw new DisGeNetException(errors.get(0));
		}
		else if (mynet.getNodeCount() > 0){
			System.out.println(mynet.getNodeCount());
			CyNetworkView netView = CyActivator.getInstance().getApplicationManagerService().getCurrentNetworkView();
			applyNetworkStyle(netView);
			applyGridLayout(netView);
		}else {
			throw new DisGeNetException("No results for the given parameters.");
		}
		return nodeAttributes;
	}
	
	/**
	 * Checks if the current network is a projections and adds the needed columns to it.
	 * @param mynet the current network.
	 * @param curParams the paramters that indicates if its  projection or not.
	 */
	private void addColumnsToProjection(CyNetwork mynet, GuiParameters curParams) {
		DisGeNetCyTableBase edgeColumns = new DisGeNetCyTableBase();
		DisGeNetCyTableGene extraGenColumns = new DisGeNetCyTableGene();
		DisGeNetCyTableDisease extraDisColumns = new DisGeNetCyTableDisease();
		edgeColumns.createGdaEdgeColumns(mynet.getDefaultEdgeTable());
		extraGenColumns.addNodeColumns(mynet.getDefaultNodeTable());
		extraDisColumns.addNodeColumns(mynet.getDefaultNodeTable());
	}

	/**
	 * Get the gene/disease net information from the database and call generic
	 * function to build net
	 * @param curParams the current parameters of the network.
	 * @throws DisGeNetException low level exception with the message error for the user. 
	 */
	public void buildGeneDiseaseNet(GuiParameters curParams) throws DisGeNetException {
		HashMap<String, ArrayList<String>> geneDisNet = db.getGeneDiseaseNetworkBySrc(curParams);
		ArrayList<String> errors = new ArrayList<>();
		if(geneDisNet!=null&&!geneDisNet.containsKey("ERRORS")) {
				
			ArrayList<String> intList = geneDisNet.get("intList");
			ArrayList<String> disList = geneDisNet.get("disList");
			ArrayList<String> genList = geneDisNet.get("genList");
			ArrayList<String> assoList = geneDisNet.get("assocList");
			ArrayList<String> srcList = geneDisNet.get("srcList");
			ArrayList<String> assocTypeList = geneDisNet.get("assocTypeList");
			ArrayList<String> pmidsList = geneDisNet.get("pmidsList");
			ArrayList<String> sentenceList = geneDisNet.get("sentenceList");
			ArrayList<String> scoreList = geneDisNet.get("scoreList");
			ArrayList<String> eiList = geneDisNet.get("eiList");
			ArrayList<String> elList = geneDisNet.get("elList");
            ArrayList<String> yearList = geneDisNet.get("yearList");

			buildNetWithEdgeAttributes(intList,disList, genList, assoList, assocTypeList,
					srcList, sentenceList, pmidsList, scoreList,eiList, elList, yearList, curParams);
			CyNetwork mynet	= CyActivator.getInstance().getApplicationManagerService().getCurrentNetwork();
			
			if(mynet==null) {
				errors.add("Network creation cancelled by the user.");
				throw new DisGeNetException(errors.get(0));
			}
			else if (mynet.getNodeCount() > 0){
				CyNetworkView myView = CyActivator.getInstance().getNetworkViewFactoryService().createNetworkView(mynet);
				CyNetworkViewManager networkViewManager =  CyActivator.getInstance().getNetworkViewManagerService();
				applyNetworkStyle(myView);
				applyGridLayout(myView);
 				networkViewManager.addNetworkView(myView);
					
			}else {
				errors.add("The network is empty.");
				throw new DisGeNetException(errors.get(0));
			}
		}else {
			errors = geneDisNet.get("ERRORS");
			throw new DisGeNetException(errors.get(0));
		}
	}

	/**
	 * Get the variant/disease net information from the database and call generic
	 * function to build net
	 * @param curParams the current parameters of the network.
	 * @throws DisGeNetException low level exception with the message error for the user.
	 * @throws SQLException in case of database access error.
	 */
	public void buildVariantDiseaseNet (GuiParameters curParams) throws DisGeNetException, SQLException {
		HashMap<String, ArrayList<String>> variantDisNet = db.getVariantDiseaseNetworkBySrc(curParams);
		ArrayList<String> errors = new ArrayList<>();
		if(variantDisNet!=null&&!variantDisNet.containsKey("ERRORS")) {
				
			ArrayList<String> intList = variantDisNet.get("intList");
			ArrayList<String> disList = variantDisNet.get("disList");
			ArrayList<String> varList = variantDisNet.get("varList");
			ArrayList<String> assoList = variantDisNet.get("assocList");
			ArrayList<String> srcList = variantDisNet.get("srcList");
			ArrayList<String> assocTypeList = variantDisNet.get("assocTypeList");
			//ArrayList<String> labelList = geneDisNet.get("labelList");
			ArrayList<String> pmidsList = variantDisNet.get("pmidsList");
			ArrayList<String> sentenceList = variantDisNet.get("sentenceList");
			//ArrayList<String> snpIdList = geneDisNet.get("snpIdsList");
			ArrayList<String> scoreList = variantDisNet.get("scoreList");
			ArrayList<String> eiList = variantDisNet.get("eiList");
            ArrayList<String> yearList = variantDisNet.get("yearList");
	
			buildNetWithEdgeAttributes(intList,disList, varList, assoList, assocTypeList,srcList, sentenceList, pmidsList, scoreList, eiList, new ArrayList<String>(), yearList,curParams);
			CyNetwork mynet	= CyActivator.getInstance().getApplicationManagerService().getCurrentNetwork();
			
			if(curParams.isVarGeneDisNetwork()) {
				expandVariantNetWithGenes(mynet);
			}
			
			if(mynet==null) {
				errors.add("Network creation cancelled by the user.");
				throw new DisGeNetException(errors.get(0));
			}
			else if (mynet.getNodeCount() > 0){
								
				CyNetworkView myView = CyActivator.getInstance().getNetworkViewFactoryService().createNetworkView(mynet);
				CyNetworkViewManager networkViewManager =  CyActivator.getInstance().getNetworkViewManagerService();
				applyNetworkStyle(myView);
				applyGridLayout(myView);
 				networkViewManager.addNetworkView(myView);
					
			}else {
				errors.add("The network is empty.");
				throw new DisGeNetException(errors.get(0));
			}
		}else {
			errors = variantDisNet.get("ERRORS");
			throw new DisGeNetException(errors.get(0));
		}
	
	}
	
	/**
	 * Calls the method to expand a variant disease network and updates the view.
	 * @throws DisGeNetException low level exception with the message error for the user. 
	 * @throws SQLException low level exception with the message error for the user.
	 */
	public void showVariantNetWithGenes() throws DisGeNetException, SQLException {
		CyNetwork currNet = CyActivator.getInstance().getApplicationManagerService().getCurrentNetwork();
		List <CyNode> gnodes = expandVariantNetWithGenes(currNet);
		CyNetworkView netView = CyActivator.getInstance().getApplicationManagerService().getCurrentNetworkView();
		Set<View<CyNode>> gnodesView = new LinkedHashSet<>();
		applyNetworkStyle(netView);
		for (CyNode node : gnodes) {
			gnodesView.add(netView.getNodeView(node));
		}
		CyLayoutAlgorithm layoutAlgorithm = CyActivator.layoutAlgoManager.getLayout("force-directed");
		TaskIterator tasks = layoutAlgorithm.createTaskIterator(netView, layoutAlgorithm.getDefaultLayoutContext(), gnodesView, null);
		DialogTaskManager dialogTaskManager =  CyActivator.getInstance().getDialogTaskManagerService();
		dialogTaskManager.execute(tasks);
		//applyGridLayout(netView);
	}

	/**
	 * Expands the current variant disease network with the genes retrived from the associatedGenes column.
	 * @param currNet the selected network.
	 * @throws DisGeNetException low level exception with the message error for the user.
	 * @throws SQLException low level exception with the message error for the user.
	 */
	private List<CyNode> expandVariantNetWithGenes(CyNetwork currNet) throws DisGeNetException, SQLException {
		CyTable nodeTable = currNet.getDefaultNodeTable();
		Collection <CyRow> variantRows = nodeTable.getMatchingRows("nodeType", "variant");
		List <String> genes = new ArrayList<>();
		List <CyNode> geneNodes = new ArrayList<>();
		for (CyRow variantRow : variantRows) {
			List <String> rowGenes = variantRow.getList("associatedGenes", String.class);
			if (rowGenes!=null) {
				genes.addAll(rowGenes);
			}
		}
		Map <String,String> geneMap = db.getAssociatedGenesToVariants(genes);
		if(!geneMap.isEmpty()) {
			 nodeTable.createColumn("geneId", String.class, false);
			 nodeTable.createColumn("geneName", String.class, false);
			 for (CyRow variantRow : variantRows) {
				CyNode variantNode = currNet.getNode(variantRow.get("suid", Long.class));
				List <String> assocGenes = variantRow.getList("associatedGenes", String.class);
				if(assocGenes!=null) {
					for (String gene : assocGenes) {
						if(geneMap.containsKey(gene)) {
							Long geneSuid = nodeAttributes.get(gene);
							CyNode geneNode;
							if(geneSuid==null) {
								geneNode = currNet.addNode();
								geneNodes.add(geneNode);
								nodeAttributes.put(gene,geneNode.getSUID());
								nodeTable.getRow(geneNode.getSUID()).set("name",geneMap.get(gene));
								nodeTable.getRow(geneNode.getSUID()).set("shared name",geneMap.get(gene));
								nodeTable.getRow(geneNode.getSUID()).set("geneId", geneMap.get(gene));
								nodeTable.getRow(geneNode.getSUID()).set("geneName", gene);
								nodeTable.getRow(geneNode.getSUID()).set("nodeType", "gene");
								nodeTable.getRow(geneNode.getSUID()).set("styleName", gene);
							}else {
								geneNode = currNet.getNode(geneSuid);
							}
							CyEdge interaction = currNet.addEdge(variantNode, geneNode, false);
							currNet.getDefaultEdgeTable().getRow(interaction.getSUID()).set("interaction", "variant-gene");
							nodeTable.getRow(geneNode.getSUID()).set("nrAssociatedVariants", currNet.getNeighborList(geneNode, CyEdge.Type.ANY).size());
							nodeTable.getRow(geneNode.getSUID()).set("styleSize", currNet.getNeighborList(geneNode, CyEdge.Type.ANY).size());
						}
					}
				}
				
			}
		}
		return geneNodes;
	}
	
	/**
	 * Removes the gene nodes from the variant disease network and updates the view.
	 */
	public void removeGenesFromVariantNet() {
		CyNetwork currNet = CyActivator.getInstance().getApplicationManagerService().getCurrentNetwork();
		CyTable nodeTable = currNet.getDefaultNodeTable();
		if(nodeTable.getColumn("nodeType").getValues(String.class).contains("gene")) {
			Collection<CyRow> geneRows = nodeTable.getMatchingRows("nodeType", "gene");
			Collection<CyNode> geneNodes = new ArrayList<>();
			for (CyRow cyRow : geneRows) {
				geneNodes.add(currNet.getNode(cyRow.get("suid",Long.class)));
			}
			currNet.removeNodes(geneNodes);
		}
		if(nodeTable.getColumn("geneId")!=null) {
			nodeTable.deleteColumn("geneId");
		}
		if(nodeTable.getColumn("geneName")!=null) {
			nodeTable.deleteColumn("geneName");
		}
		CyNetworkView netView = CyActivator.getInstance().getApplicationManagerService().getCurrentNetworkView();
		netView.updateView();
	}

	/**
	 * Generic function to create disease network
	 * @param curParams the current parameters of the network.
	 * @throws DisGeNetException low level exception with the message error for the user.
	 */
	public void buildDiseaseNet(GuiParameters curParams) throws DisGeNetException {
		HashMap<String, ArrayList<String>> disProjection = db.getDiseaseProjectionBySrc(curParams);
		ArrayList<String> errors = new ArrayList<>();
		if (disProjection!=null&&!disProjection.containsKey("ERRORS")) {
			List<String> dis1List = disProjection.get("dis1List");
			List<String> dis2List = disProjection.get("dis2List");
			List<String> nrCommonGenes  = disProjection.get("nrCommonGenes"); //JP
			buildNet(dis1List, dis2List,nrCommonGenes, curParams.getSource(),curParams);
			CyNetwork mynet = CyActivator.getInstance().getApplicationManagerService().getCurrentNetwork();
			//JP
			// Create a new network view
			//CyNetwork mynet	= CyActivator.getInstance().getApplicationManagerService().getCurrentNetwork();
			if(mynet==null) {
				errors.add("Network creation cancelled by the user.");
				throw new DisGeNetException(errors.get(0));
			}
			else if (mynet.getNodeCount() > 0){
				CyNetworkView myView = CyActivator.getInstance().getNetworkViewFactoryService().createNetworkView(mynet);
				CyNetworkViewManager networkViewManager =  CyActivator.getInstance().getNetworkViewManagerService();
				applyNetworkStyle(myView);
				applyGridLayout(myView);
 				networkViewManager.addNetworkView(myView);
					
			}else {
				errors.add("The network is empty.");
				throw new DisGeNetException(errors.get(0));
			}
		}else {
			errors = disProjection.get("ERRORS");
			throw new DisGeNetException(errors.get(0));
		}
	
	}

	/**
	 * Generic function to create gene network
	 * @param curParams the current parameters of the network.
	 * @throws DisGeNetException low level exception with the message error for the user.
	 */
	public void buildGeneNet(GuiParameters curParams) throws DisGeNetException {
		HashMap<String, ArrayList<String>> geneProjection = db.getGeneProjectionBySrc(curParams);
		ArrayList<String> errors = new ArrayList<>();
		if(geneProjection!=null&&!geneProjection.containsKey("ERRORS")) {
			List<String> gene1List = geneProjection.get("gen1List");
			List<String> gene2List = geneProjection.get("gen2List");
			List<String> nrCommonDiseases = geneProjection.get("nrCommonDiseases");
			buildNet(gene1List, gene2List, nrCommonDiseases,curParams.getSource(), curParams);
			CyNetwork mynet	= CyActivator.getInstance().getApplicationManagerService().getCurrentNetwork();
			
			if(mynet==null) {
				errors.add("Network creation cancelled by the user.");
				throw new DisGeNetException(errors.get(0));
			}
			else if (mynet.getNodeCount() > 0){
				CyNetworkView myView = CyActivator.getInstance().getNetworkViewFactoryService().createNetworkView(mynet);
				CyNetworkViewManager networkViewManager =  CyActivator.getInstance().getNetworkViewManagerService();
				applyNetworkStyle(myView);
				applyGridLayout(myView);
 				networkViewManager.addNetworkView(myView);
					
			}else {
				errors.add("The network is empty.");
				throw new DisGeNetException(errors.get(0));
			}
		}else {
			errors = geneProjection.get("ERRORS");
			throw new DisGeNetException(errors.get(0));
		}
	}

	/**
	 * Function to retrieve all node names from currently selected network to be
	 * able to search for them.
	 * @return an arrayList containing the node names.
	 */
	public ArrayList<String> getNodeNamesFromNet() {
		ArrayList<String> nodeNames = new ArrayList<String>();

		CyNetwork current_network = CyActivator.getInstance().getApplicationManagerService().getCurrentNetwork();

		boolean diseaseNameExists = false;
		boolean geneNameExists = false;

		CyTable cyTable = current_network.getDefaultNodeTable();
		if(cyTable.getColumn("diseaseName") != null ){
			diseaseNameExists = true;
		}
		if(cyTable.getColumn("geneName") != null ){
			geneNameExists = true;
		}

		// retrieve all nodes from network
		//JAVI
		//ArrayList<CyNode> nodesList = (ArrayList) Cytoscape.getCyNodesList();
		//JAVI: Is it the same getting all the nodes from Cytoscape than gettting them from current_network?
		ArrayList<CyNode> nodesList = (ArrayList<CyNode>) current_network.getNodeList();


		Iterator<CyNode> nodeIt = nodesList.iterator();
		while (nodeIt.hasNext()) {
			CyNode node = nodeIt.next();

			// retrieve either diseaseName or geneName
			String name = "";

			String nodeType = current_network.getRow(node).get("nodeType", String.class);
			if (nodeType.equals("disease")) {
				//JAVI
				//name = cyNodeAttrs.getStringAttribute(node.getIdentifier(),"diseaseName");
				name = current_network.getRow(node).get("diseaseName", String.class);
			} else if (nodeType.equals("gene")) {
				//JAVI
				//name = cyNodeAttrs.getStringAttribute(node.getIdentifier(),
				//		"geneName");
				name = current_network.getRow(node).get("geneName", String.class);
			} else if (nodeType.equals("variant")) {
				//JAVI
				//name = cyNodeAttrs.getStringAttribute(node.getIdentifier(),
				//		"geneName");
				name = current_network.getRow(node).get("variantId", String.class);
			}
			try {
				if (!name.equals(null) && !name.equals(""))
					nodeNames.add(name);
			} catch (NullPointerException e) {
				
			}
		}
		return nodeNames;
	}

	/**
	 * Builds a new disease-disease or gene-gene network.
	 * @param list1 list of genes or diseases
	 * @param list2 duplication of the first list
	 * @param src selected source for the search
     * @param nrCommonEntities number of entities in common
	 * @param params current params for the network
	 * @throws DisGeNetException low level exception with the error message.
	 */
	public void buildNet(List<String> list1, List<String> list2, List<String> nrCommonEntities, String src, GuiParameters params) throws DisGeNetException  {
		this.params = params;
		Iterator<String> listIt1 = list1.iterator();
		Iterator<String> listIt2 = list2.iterator();
		Iterator<String> nrCommonEntitiesIt = nrCommonEntities.iterator();
		
		int size = list1.size();

		Double count = 0.0;
		CyNetwork cyNetwork = null;

		if (list1.size() >  0 && (list1.get(0).matches(DatabaseProps.DISEASE_CODE_REGEX))){

			cyNetwork = this.createCyNetwork(this.createNetworkName(params));

			Map <CyEdge,Map<String,Object>> edgeAttributes = new HashMap <>();
			while (listIt1.hasNext() && !params.isNetBuildInterrupted()) {
				String entity1 = listIt1.next();
				String entity2 = listIt2.next();
				int sharedEntities = Integer.parseInt(nrCommonEntitiesIt.next());
				CyEdge edge = checkInteraction(cyNetwork, entity1, entity2);
				Map <String,Object> attributes;
				attributes = edgeAttributes.getOrDefault(edge, new HashMap<String,Object>());
				if(attributes.isEmpty()) {
					attributes.put("interaction", "dd");
					attributes.put("source", src);
					attributes.put("nrCommonGenes", sharedEntities);
				}
				edgeAttributes.put(edge, attributes);
				_pct = (count++ * 90) / list1.size();
				_fireMoodEvent();
			}
			DisGeNetCyTableBase table = new DisGeNetCyTableBase(params);
			table.addNodeColumns(cyNetwork.getDefaultNodeTable());
			table.addEdgeColumns(cyNetwork.getDefaultEdgeTable());
			table.fillEdgeColumns(cyNetwork,edgeAttributes);
			table.fillNodeColumns(cyNetwork.getDefaultNodeTable(), nodeAttributes);
		} else {
			// in this case we want to build a geneNet

			cyNetwork = this.createCyNetwork(this.createNetworkName(params));

			Map <CyEdge,Map<String,Object>> edgeAttributes = new HashMap <>();
			while (listIt1.hasNext() && !params.isNetBuildInterrupted()) {
				String entity1 = listIt1.next();
				String entity2 = listIt2.next();
				CyEdge edge = checkInteraction(cyNetwork, entity1, entity2);
				Map <String,Object> attributes;
				attributes = edgeAttributes.getOrDefault(edge, new HashMap<String,Object>());
				if(attributes.isEmpty()) {
					attributes.put("interaction", "gg");
					attributes.put("source", src);
					attributes.put("nrAssociatedDiseases", 1);
				}else {
					attributes.put("nrAssociatedDiseases",1+(int)attributes.get("nrAssociatedDiseases"));
				}
				edgeAttributes.put(edge, attributes);
				_pct = (count++ * 90) / size;
				_fireMoodEvent();
			}
			DisGeNetCyTableBase table = new DisGeNetCyTableBase(params);
			table.addNodeColumns(cyNetwork.getDefaultNodeTable());
			table.addEdgeColumns(cyNetwork.getDefaultEdgeTable());
			table.fillEdgeColumns(cyNetwork,edgeAttributes);
			table.fillNodeColumns(cyNetwork.getDefaultNodeTable(), nodeAttributes);
		}
		//(CyApplicationManager.getCurrentNetwork().getNodeCount());
		if (!params.isNetBuildInterrupted()
				//JAVI
				//&& CyApplicationManager.getCurrentNetwork().getNodeCount() > 0) {
				&& CyActivator.getInstance().getApplicationManagerService().getCurrentNetwork().getNodeCount() > 0){
			
			_pct = 0.95;
			_fireMoodEvent();
			initAdditionalStyleAttributes(params);
			
			_pct = 1.0;
			_fireMoodEvent();
		} else if (params.isNetBuildInterrupted()) {
			CyActivator.getInstance().getNetworkManagerService().destroyNetwork(cyNetwork);
		} else if (CyActivator.getInstance().getApplicationManagerService().getCurrentNetwork()==null) {
			
		}else if (CyActivator.getInstance().getApplicationManagerService().getCurrentNetwork().getNodeCount() == 0 ){
			CyActivator.getInstance().getNetworkManagerService().destroyNetwork(cyNetwork);
			params.setEmptyNet(true);
		}

	}
	/*
	 * General Function to build a network with additional information for edges
	 * 
	 * parameters: two lists which contain pairwise entities in the same order
	 * -> list1[0] is pair with list2[0] and so on
	 * 
	 * several lists with attribute values (this was done because getting the
	 * attributes separately for each edge slowed down the program too much):
	 * ArrayList<String> assocTypeList ArrayList<String> labelList
	 * ArrayList<String> sentenceList ArrayList<String> pmidsList
	 * 
	 * src: important for the number of associated genes in attributes
	 */
	public void buildNetWithEdgeAttributes(
			ArrayList<String> intList,
			ArrayList<String> disList,
			ArrayList<String> genList, 
			ArrayList<String> assocList,
			ArrayList<String> assocTypeList, 
			ArrayList<String> srcList,
			ArrayList<String> sentenceList,
			ArrayList<String> pmidsList, 
			ArrayList<String> scoreList,
			ArrayList<String> eiList,
			ArrayList<String> elList,
            ArrayList<String> yearList,
            GuiParameters params) throws DisGeNetException {
		
		this.params = params;
		CyNetwork cyNetwork = this.createCyNetwork(this.createNetworkName(params));
		DisGeNetCyTableBase table = new DisGeNetCyTableBase(params);
		CyTable nodeTable = cyNetwork.getDefaultNodeTable();
		CyTable edgeTable = cyNetwork.getDefaultEdgeTable();
		table.addEdgeColumns(edgeTable);
		
		Iterator<String> listInteractionIt = intList.iterator();
		Iterator<String> disIt = disList.iterator();
		Iterator<String> genIt = genList.iterator();
		Iterator<String> assocIt = assocList.iterator();
		Iterator<String> assocTypeListIt = assocTypeList.iterator();
		Iterator<String> srcListIt = srcList.iterator();
		Iterator<String> sentenceListIt = sentenceList.iterator();
		Iterator<String> pmidsListIt = pmidsList.iterator();
		Iterator<String> scoreListIt = scoreList.iterator();
		Iterator<String> eiListIt = eiList.iterator();
		Iterator<String> elListIt = elList.iterator();
		Iterator<String> yearListIt = yearList.iterator();
		Double count = 0.0;
		Map <CyEdge,Map<String,Object>> edgeAttributes = new HashMap<>();
		while (disIt.hasNext() && !params.isNetBuildInterrupted() && disList.size() > 0) {
			Map <String,Object> attributes = new HashMap<>();
			String dis = disIt.next();
			String gen = genIt.next();

			CyEdge edge = addInteraction(cyNetwork, dis, gen);
			
			attributes.put("interaction",listInteractionIt.next());
			attributes.put("assoc",assocIt.next());
			attributes.put("assocType",assocTypeListIt.next());
			attributes.put("org_src",srcListIt.next());
			attributes.put("sentence",sentenceListIt.next());
			attributes.put("pmid",pmidsListIt.next());
			try {
				attributes.put("score",Double.parseDouble(scoreListIt.next()));
			}catch(NumberFormatException ex) {
				attributes.put("score", null);
			} catch (NullPointerException e) {
			attributes.put("score",null);
			}
			try {
				attributes.put("ei",Double.parseDouble(eiListIt.next()));
			} catch (NumberFormatException e) {
				attributes.put("ei",null);
			} catch (NullPointerException e) {
				attributes.put("ei",null);
			}
			
			if(elListIt.hasNext()) {
				attributes.put("el",elListIt.next());
			}
			if(yearListIt.hasNext()){
			    attributes.put("year", yearListIt.next());
            }
			edgeAttributes.put(edge, attributes);
			
			
			_pct = ((count++ * 90) / disList.size());
			_fireMoodEvent();
		}
		
		table.fillEdgeColumns(cyNetwork, edgeAttributes);
		table.fillNodeColumns(nodeTable, nodeAttributes);
		if (!params.isNetBuildInterrupted()
				&& CyActivator.getInstance().getApplicationManagerService().getCurrentNetwork().getNodeCount() > 0){
			CyActivator.getInstance().getNetworkViewFactoryService().createNetworkView(
					CyActivator.getInstance().getApplicationManagerService().getCurrentNetwork());
			_pct = 0.95;
			_fireMoodEvent();

			// here create style attributes
			initAdditionalStyleAttributes(params);
			_pct = 0.97;
			_fireMoodEvent();
			_pct = 1.0;
			_fireMoodEvent();
		} else if (params.isNetBuildInterrupted()) {
			CyActivator.getInstance().getNetworkManagerService().destroyNetwork(cyNetwork);
		} else if( CyActivator.getInstance().getApplicationManagerService().getCurrentNetwork().getNodeCount() == 0){
			CyActivator.getInstance().getNetworkManagerService().destroyNetwork(cyNetwork);
		}

	}

	/**
	 * Fills the attribute columns for the different type of networks.
	 * @param params the parameters given to the network.
	 */
	public void initAdditionalStyleAttributes(GuiParameters<?, ?> params) {
		/*
		 * loop through nodes -and add 'styleName' attribute -and add 'degree'
		 * attribute
		 */

		CyNetwork currentNet = CyActivator.getInstance().getApplicationManagerService().getCurrentNetwork();

		Iterator<CyNode> it = currentNet.getNodeList().iterator();
		
		while (it.hasNext()) {
			try {

				CyNode node = it.next();
				int nrConnectedNodes = 0 ;

				nrConnectedNodes = new HashSet <CyNode> (currentNet.getNeighborList(node, CyEdge.Type.ANY)).size();
				//Forgive me father for I have sinned.
				
				String nodeType = currentNet.getRow(node).get("nodeType", String.class);

				if (nodeType.equals("disease")) {
					//JAVI
					String name = currentNet.getRow(node).get("diseaseName", String.class);
					this.addCyNodeAttribute(currentNet, node, "styleName", name.trim());

					if (params.getActiveTab().equals("GeneDisTabPane")) {				
						this.addCyNodeAttribute(currentNet, node, "nrAssociatedGenes", nrConnectedNodes);
					} else if (params.getActiveTab().equals("VariantDisTabPane")) {
						this.addCyNodeAttribute(currentNet, node, "nrAssociatedVariants", nrConnectedNodes);
					}
					else {
						this.addCyNodeAttribute(currentNet, node, "nrAssociatedDiseases", nrConnectedNodes);
					}
				} else if (nodeType.equals("gene")) {
					String name = currentNet.getRow(node).get("geneName", String.class);

					this.addCyNodeAttribute(currentNet, node, "styleName", name.trim());

					if (params.getActiveTab().equals("GeneDisTabPane")) {
						this.addCyNodeAttribute(currentNet, node, "nrAssociatedDiseases", nrConnectedNodes);
					} else {
						this.addCyNodeAttribute(currentNet, node, "nrAssociatedGenes", nrConnectedNodes);
					}
				} else if (nodeType.equals("variant")) {
					String name = currentNet.getRow(node).get("variantId", String.class);
					this.addCyNodeAttribute(currentNet, node, "styleName", name.trim());

					if (params.getActiveTab().equals("VariantDisTabPane")) {
						this.addCyNodeAttribute(currentNet, node, "nrAssociatedDiseases", nrConnectedNodes);
					} else {
						this.addCyNodeAttribute(currentNet, node, "nrAssociatedVariants", nrConnectedNodes);
					}
				}

				this.addCyNodeAttribute(currentNet, node, "styleSize", nrConnectedNodes);

			} catch(Exception e) {
			}
		}
	}
	
	/**
	 * Applies the DisGeNET style to the given network and updates the view.
	 * @param netView the selected network view.
	 */
	private void applyNetworkStyle (CyNetworkView netView) {
		VisualMappingManager vmm = CyActivator.getInstance().getVisualMappingMgr();
		VisualStyle vs = DisGenetStyleManager.getVisualStyleByName(vmm, "DisGeNETstyleV2");
		vmm.setVisualStyle(vs, netView);
		netView.updateView();
	}
	
	/**
	 * Applies a grid layout to the given network view.
	 * @param netView the selected network view.
	 */
	private void applyGridLayout(CyNetworkView netView) {
		CyLayoutAlgorithm layoutAlgorithm = CyActivator.layoutAlgoManager.getLayout("grid");
		TaskIterator tasks = layoutAlgorithm.createTaskIterator(netView, layoutAlgorithm.createLayoutContext(), CyLayoutAlgorithm.ALL_NODE_VIEWS, null);
		DialogTaskManager dialogTaskManager =  CyActivator.getInstance().getDialogTaskManagerService();
		dialogTaskManager.execute(tasks);
	}
	
	/**
	 * Given the name of a node check's if exists or not in the network if not exists it will be created.
	 * @param cyNetwork network to check for.
	 * @param name name of the node to get or create.
	 * @return the instance of the found node or the created one.
	 */
	private CyNode createNodeIfNotExists(CyNetwork cyNetwork, String name) {
		CyNode node;
		if(nodeAttributes.get(name)==null){
			 node = cyNetwork.addNode();
			 nodeAttributes.put(name, node.getSUID());
		}else {
			node = cyNetwork.getNode(nodeAttributes.get(name));
		}
		return node;
	}
	
	/**
	 * Creates a new edge between the given nodes.
	 * @param cyNetwork the current network which contains the nodes.
	 * @param name1 the first node, if doesn't exist in the network it will be created.
	 * @param name2 the second node, if doesn't exist in the network it will be created.
	 * @return the created edge instance.
	 */
	private CyEdge addInteraction(CyNetwork cyNetwork, String name1, String name2) {
		CyNode firstNode;
		firstNode = createNodeIfNotExists(cyNetwork, name1);
		CyNode secondNode;
		secondNode = createNodeIfNotExists(cyNetwork, name2);
		CyEdge edge = cyNetwork.addEdge(firstNode, secondNode, false);
		return edge;
	}
	
	private CyEdge checkInteraction(CyNetwork cyNetwork, String node1, String node2) {
		CyEdge edge;
		if(cyNetwork.containsEdge(createNodeIfNotExists(cyNetwork, node1), createNodeIfNotExists(cyNetwork, node2))) {
			edge = cyNetwork.getConnectingEdgeList(getCyNode(cyNetwork, node1), getCyNode(cyNetwork, node2), CyEdge.Type.ANY).get(0);
		}else {
			edge = addInteraction(cyNetwork, node1, node2);
		}
		return edge;
	}

	public synchronized void addNetCreationListener(NetCreationListener l) {
		_listeners.add(l);
	}

	public synchronized void removeMoodListener(NetCreationListener l) {
		_listeners.remove(l);
	}

	private synchronized void _fireMoodEvent() {
		NetCreationEvent pct = new NetCreationEvent(this, _pct);
		Iterator<NetCreationListener> listeners = _listeners.iterator();
		while (listeners.hasNext()) {
			listeners.next()
			.NetCreationEventReceived(pct);
		}
	}
	
}
