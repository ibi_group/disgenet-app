package es.imim.DisGeNET.network;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import org.cytoscape.model.*;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.Task;
import org.cytoscape.work.TaskMonitor;
import org.cytoscape.work.Tunable;
import org.cytoscape.work.util.ListSingleSelection;

import es.imim.DisGeNET.exceptions.DisGeNetException;
import es.imim.DisGeNET.gui.GuiParameters;
import es.imim.DisGeNET.gui.GuiProps;
import es.imim.DisGeNET.internal.CyActivator;

public class ExpandCurrentNetworkTask extends AbstractTask implements Task {
	
	@Tunable (description="Select source:", gravity=2,groups={"Sources"},params="displayState=uncollapsed")
	public ListSingleSelection<String> sources;
	
	@Tunable (description="Select Disease class:", gravity=3,params="displayState=collapsed",groups={"Filters"},context=Tunable.GUI_CONTEXT)
	public ListSingleSelection<String> diseaseClasses;
	
	@Tunable (description="Select Association type:", gravity=4,params="displayState=collapsed",groups={"Filters"},context=Tunable.GUI_CONTEXT)
	public ListSingleSelection<String> assocTypes;
	
	@Tunable (description="Select Evidence Level:", gravity=5, params="displayState=collapsed",groups={"Filters"},context=Tunable.GUI_CONTEXT)
	public ListSingleSelection<String> evidenceLevels;

	@Tunable (description="Select score min range", gravity=6,params="displayState=collaspsed",groups={"Filters"},context=Tunable.GUI_CONTEXT,
			format="0.00")
	public Double minScoreRange;
	
	@Tunable (description="Select score max range", gravity=7,params="displayState=collaspsed",groups={"Filters"},context=Tunable.GUI_CONTEXT,
			format="0.00")
	public Double maxScoreRange;
	
	
	private GuiParameters nodeParams;
	private String netType;
	private String netName;
	private CyTable nodeTable;
	private CyTable netTable;
	private CyNode selectNode;
	private Map <String, Long> nodeMap;
	
	public ExpandCurrentNetworkTask(CyNetworkView cyNetworkView, View<CyNode> cyNodeView) {
		this.nodeParams = new GuiParameters<>();
		CyNetwork currNet = cyNetworkView.getModel();
		this.nodeTable = currNet.getDefaultNodeTable();
		this.netTable = currNet.getDefaultNetworkTable();
		this.netName = netTable.getRow(currNet.getSUID()).get("name", String.class);
		this.nodeMap = new HashMap <>();
		this.selectNode = cyNodeView.getModel();
		mapSuidsToNames(cyNetworkView.getModel()); //Control for the nodes that already exist in the network.
		checkNetType();
		this.diseaseClasses = new ListSingleSelection<String>(Arrays.asList(GuiProps.DISEASE_CLASS_OPTIONS));
		this.assocTypes = new ListSingleSelection<String>(Arrays.asList(GuiProps.ASSOCIATION_TYPE_OPTIONS));
		this.evidenceLevels = new ListSingleSelection<String>(Arrays.asList(GuiProps.EL_OPTIONS));
		this.minScoreRange = 0.;
		this.maxScoreRange = 1.;
	}
	
	@Override
	public void run(TaskMonitor taskMonitor) throws Exception {
		this.nodeParams.setSource(sources.getSelectedValue());
		this.nodeParams.setDiseaseClass(diseaseClasses.getSelectedValue());
		this.nodeParams.setAssociationType(assocTypes.getSelectedValue());
		this.nodeParams.setEl(evidenceLevels.getSelectedValue());
		this.nodeParams.setLowScore(minScoreRange.toString());
		this.nodeParams.setHighScore(maxScoreRange.toString());;
		Map <String,String> filteredNodes;
		taskMonitor.setTitle("Expanding network "+netName);
		taskMonitor.setProgress(0);
		Collection<CyRow> selectedRows = nodeTable.getMatchingRows("selected",true);
		if(!selectedRows.isEmpty()) {
			filteredNodes = filterNodes(selectedRows);
		}else {
			selectedRows.add(nodeTable.getRow(selectNode.getSUID()));
			filteredNodes = filterNodes(selectedRows);
		}
		NetworkBuilder networkBuilder = new NetworkBuilder();
		int iniNodeNum = CyActivator.getInstance().getApplicationManagerService().getCurrentNetwork().getNodeCount();
		switch(netType) {
		case "variantNet":
			nodeParams.setActiveTab("VariantDisTabPane");
			if(filteredNodes.containsKey("diseases")) {
				nodeParams.setDisSearchText(filteredNodes.get("diseases"));
				nodeMap = networkBuilder.expandVariantDiseaseNet(nodeParams,nodeMap);
				nodeParams.setDisSearchText(null);
				taskMonitor.setProgress(0.5);
			}
			if(filteredNodes.containsKey("variants")) {
				nodeParams.setVarSearchText(filteredNodes.get("variants"));
				nodeMap = networkBuilder.expandVariantDiseaseNet(nodeParams, nodeMap);
				nodeParams.setVarSearchText(null);
				taskMonitor.setProgress(1);
			}
			break;
		case "geneNet":
			nodeParams.setActiveTab("GeneDisTabPane");
			if(filteredNodes.containsKey("genes")) {
				nodeParams.setGenSearchText(filteredNodes.get("genes"));
				nodeMap = networkBuilder.expandGeneDiseaseNet(nodeParams,nodeMap);
				nodeParams.setGenSearchText(null);
				taskMonitor.setProgress(0.5);
			}
			if(filteredNodes.containsKey("diseases")) {
				nodeParams.setDisSearchText(filteredNodes.get("diseases"));
				nodeMap = networkBuilder.expandGeneDiseaseNet(nodeParams,nodeMap);
				nodeParams.setDisSearchText(null);
				taskMonitor.setProgress(1);
			}
			break;
		}
		int finNodeNum = CyActivator.getInstance().getApplicationManagerService().getCurrentNetwork().getNodeCount();
		if(finNodeNum==iniNodeNum) {
			throw new DisGeNetException("No results found for the given parameters.");
		}
	}
	
	private Map <String,String> filterNodes (Collection<CyRow> selectedRows) {
		Map<String,String> filteredNodes = new HashMap<>();
		List<String> geneNodes = new ArrayList<>();
		List<String> diseaseNodes = new ArrayList<>();
		List<String> variantNodes = new ArrayList<>();
		
		String nodeType;
		for (CyRow cyRow : selectedRows) {
			nodeType = cyRow.get("nodeType",String.class);
			switch(nodeType) {
			case "gene":
				geneNodes.add(cyRow.get("geneId", String.class));
				break;
			case "disease":
				diseaseNodes.add(cyRow.get("diseaseId", String.class));
				break;
			case "variant":
				variantNodes.add(cyRow.get("variantId", String.class));
				break;
			default:
				break;
			}
		}
		if(!geneNodes.isEmpty()) {
			filteredNodes.put("genes", searchFieldBuilderNum(geneNodes));
		}
		if(!diseaseNodes.isEmpty()) {
			filteredNodes.put("diseases", searchFieldBuilderString(diseaseNodes));
		}
		if(!variantNodes.isEmpty()) {
			filteredNodes.put("variants", searchFieldBuilderString(variantNodes));
		}
		
		return filteredNodes;
	}
	
	private String searchFieldBuilderNum (List<String> filteredNodes) {
		String searchField = "";
		Iterator <String> nodeIt = filteredNodes.iterator();
		while(nodeIt.hasNext()) {
			searchField+=nodeIt.next();
			if(nodeIt.hasNext()) {
				searchField+=",";
			}
		}
		return searchField;
	}
	
	private String searchFieldBuilderString (List<String> filteredNodes) {
		String searchField = "";
		Iterator <String> nodeIt = filteredNodes.iterator();
		while(nodeIt.hasNext()) {
			searchField+="'"+nodeIt.next()+"'";
			if(nodeIt.hasNext()) {
				searchField+=",";
			}
		}
		return searchField;
	}
	
	private void mapSuidsToNames (CyNetwork cyNetwork) {
		List <CyNode> nodeList = cyNetwork.getNodeList();
		for (CyNode cyNode : nodeList) {
			String nodeName = nodeTable.getRow(cyNode.getSUID()).get(CyNetwork.NAME, String.class);
			Long nodeSuid = nodeTable.getRow(cyNode.getSUID()).get("suid", Long.class);
			nodeMap.put(nodeName, nodeSuid);
		}
	}
	
	private void checkNetType () {
		this.netType="geneNet";
		if(nodeTable.getColumn("nodeType").getValues(String.class).contains("variant")) {
			this.netType = "variantNet";
			this.sources = new ListSingleSelection<String>(Arrays.asList(GuiProps.VARIANT_SOURCE_OPTIONS));
		}
		else {
			this.sources = new ListSingleSelection<String>(Arrays.asList(GuiProps.SOURCE_OPTIONS));
		}
	}
}
