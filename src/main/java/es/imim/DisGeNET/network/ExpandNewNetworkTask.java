package es.imim.DisGeNET.network;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyRow;
import org.cytoscape.model.CyTable;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.Task;
import org.cytoscape.work.TaskMonitor;
import org.cytoscape.work.Tunable;
import org.cytoscape.work.util.ListSingleSelection;

import es.imim.DisGeNET.gui.GuiParameters;
import es.imim.DisGeNET.gui.GuiProps;
import es.imim.DisGeNET.internal.CyActivator;
/**
 * Task class to create a new network with the selected nodes from a previous task.
 * @author jsauch
 *
 */
public class ExpandNewNetworkTask extends AbstractTask implements Task {
	
	@Tunable (description="Select source:", gravity=2,groups={"Sources"},params="displayState=uncollapsed")
	public ListSingleSelection<String> sources;
	
	@Tunable (description="Select Disease class:", gravity=3,params="displayState=collapsed",groups={"Filters"},context=Tunable.GUI_CONTEXT)
	public ListSingleSelection<String> diseaseClasses;
	
	@Tunable (description="Select Association type:", gravity=4,params="displayState=collapsed",groups={"Filters"},context=Tunable.GUI_CONTEXT)
	public ListSingleSelection<String> assocTypes;
	
	@Tunable (description="Select Evidence Level:", gravity=5, params="displayState=collapsed",groups={"Filters"},context=Tunable.GUI_CONTEXT)
	public ListSingleSelection<String> evidenceLevels;

	@Tunable (description="Select score min range", gravity=6,params="displayState=collaspsed",groups={"Filters"},context=Tunable.GUI_CONTEXT,
			format="0.00")
	public Double minScoreRange;
	
	@Tunable (description="Select score max range", gravity=7,params="displayState=collaspsed",groups={"Filters"},context=Tunable.GUI_CONTEXT,
			format="0.00")
	public Double maxScoreRange;
	
	private GuiParameters nodeParams;
	private String netType;
	private String netName;
	private CyTable nodeTable;
	private CyNode selectNode;
	private Map <String, Long> nodeMap;
	
	public ExpandNewNetworkTask(CyNetworkView cyNetworkView, View<CyNode> cyNodeView) {
		CyNetwork currNet = cyNetworkView.getModel();
		this.nodeTable = currNet.getDefaultNodeTable();
		this.netName = "Expanded network "+currNet.getDefaultNetworkTable().getRow(currNet.getSUID()).get(CyNetwork.NAME, String.class);
		this.nodeMap = new HashMap <>();
		this.selectNode = cyNodeView.getModel();
		this.nodeParams = new GuiParameters<>();
		checkNetType();
		this.diseaseClasses = new ListSingleSelection<String>(Arrays.asList(GuiProps.DISEASE_CLASS_OPTIONS));
		this.assocTypes = new ListSingleSelection<String>(Arrays.asList(GuiProps.ASSOCIATION_TYPE_OPTIONS));
		this.minScoreRange = 0.;
		this.maxScoreRange = 1.;
	}
	
	@Override
	public void run(TaskMonitor taskMonitor) throws Exception {
		this.nodeParams.setSource(sources.getSelectedValue());
		this.nodeParams.setDiseaseClass(diseaseClasses.getSelectedValue());
		this.nodeParams.setAssociationType(assocTypes.getSelectedValue());
		this.nodeParams.setEl(evidenceLevels.getSelectedValue());
		this.nodeParams.setLowScore(minScoreRange.toString());
		this.nodeParams.setHighScore(maxScoreRange.toString());
		Map <String,String> filteredNodes;
		taskMonitor.setTitle("Creating network "+netName);
		taskMonitor.setProgress(0);
		Collection<CyRow> selectedRows = nodeTable.getMatchingRows("selected",true);
		if(!selectedRows.isEmpty()) {
			filteredNodes = filterNodes(selectedRows);
		}else {
			selectedRows.add(nodeTable.getRow(selectNode.getSUID()));
			filteredNodes = filterNodes(selectedRows);
		}
		CyNetwork cyNetwork = CyActivator.getInstance().getNetworkFactoryService().createNetwork();
		CyNetworkView cyNetworkView = CyActivator.getInstance().getNetworkViewFactoryService().createNetworkView(cyNetwork);
		cyNetwork.getRow(cyNetwork).set(CyNetwork.NAME, netName);
		CyActivator.getInstance().getNetworkManagerService().addNetwork(cyNetwork);
		CyActivator.getInstance().getNetworkViewManagerService().addNetworkView(cyNetworkView);
		NetworkBuilder networkBuilder = new NetworkBuilder(nodeParams);
		networkBuilder.createNodeAttributeColumns(cyNetwork);
		networkBuilder.createCyEdgeAttributeColumns(cyNetwork);
		switch(netType) {
		case "variantNet":
			if(filteredNodes.containsKey("diseases")) {
				nodeParams.setDisSearchText(filteredNodes.get("diseases"));
				nodeMap = networkBuilder.expandVariantDiseaseNet(nodeParams,nodeMap);
				nodeParams.setDisSearchText(null);
				taskMonitor.setProgress(0.66);
			}
			if(filteredNodes.containsKey("variants")) {
				nodeParams.setVarSearchText(filteredNodes.get("variants"));
				nodeMap = networkBuilder.expandVariantDiseaseNet(nodeParams, nodeMap);
				nodeParams.setVarSearchText(null);
				taskMonitor.setProgress(1);
			}
			break;
		case "geneNet":
			if(filteredNodes.containsKey("genes")) {
				nodeParams.setGenSearchText(filteredNodes.get("genes"));
				nodeMap = networkBuilder.expandGeneDiseaseNet(nodeParams,nodeMap);
				nodeParams.setGenSearchText(null);
				taskMonitor.setProgress(0.5);
			}
			if(filteredNodes.containsKey("diseases")) {
				nodeParams.setDisSearchText(filteredNodes.get("diseases"));
				nodeMap = networkBuilder.expandGeneDiseaseNet(nodeParams,nodeMap);
				nodeParams.setDisSearchText(null);
				taskMonitor.setProgress(1);
			}
			break;
		}
	}
	/**
	 * Filters the selected nodes and sets its values into a map.
	 * @param selectedRows rows containing the data for the nodes.
	 * @return a map containing the search field for every node type.
	 */
	private Map <String,String> filterNodes (Collection<CyRow> selectedRows) {
		Map<String,String> filteredNodes = new HashMap<>();
		List<String> geneNodes = new ArrayList<>();
		List<String> diseaseNodes = new ArrayList<>();
		List<String> variantNodes = new ArrayList<>();
		
		String nodeType;
		for (CyRow cyRow : selectedRows) {
			nodeType = cyRow.get("nodeType",String.class);
			switch(nodeType) {
			case "gene":
				geneNodes.add(cyRow.get("geneId", String.class));
				break;
			case "disease":
				diseaseNodes.add(cyRow.get("diseaseId", String.class));
				break;
			case "variant":
				variantNodes.add(cyRow.get("variantId", String.class));
				break;
			default:
				break;
			}
		}
		if(!geneNodes.isEmpty()) {
			filteredNodes.put("genes", searchFieldBuilderNum(geneNodes));
		}
		if(!diseaseNodes.isEmpty()) {
			filteredNodes.put("diseases", searchFieldBuilderString(diseaseNodes));
		}
		if(!variantNodes.isEmpty()) {
			filteredNodes.put("variants", searchFieldBuilderString(variantNodes));
		}
		
		return filteredNodes;
	}
	
	private String searchFieldBuilderNum (List<String> filteredNodes) {
		String searchField = "";
		Iterator <String> nodeIt = filteredNodes.iterator();
		while(nodeIt.hasNext()) {
			searchField+=nodeIt.next();
			if(nodeIt.hasNext()) {
				searchField+=",";
			}
		}
		return searchField;
	}
	
	private String searchFieldBuilderString (List<String> filteredNodes) {
		String searchField = "";
		Iterator <String> nodeIt = filteredNodes.iterator();
		while(nodeIt.hasNext()) {
			searchField+="'"+nodeIt.next()+"'";
			if(nodeIt.hasNext()) {
				searchField+=",";
			}
		}
		return searchField;
	}
	
	private void checkNetType () {
		this.netType="geneNet";
		this.nodeParams.setActiveTab("GeneDisTabPane");
		if(!nodeTable.getRow(selectNode.getSUID()).get("nodeType", String.class).equals("gene")) {
			if(nodeTable.getColumn("nodeType").getValues(String.class).contains("variant")) {
				this.netType = "variantNet";
				this.nodeParams.setActiveTab("VariantDisTabPane");
				this.sources = new ListSingleSelection<String>(Arrays.asList(GuiProps.VARIANT_SOURCE_OPTIONS));
			}else {
				this.sources = new ListSingleSelection<String>(Arrays.asList(GuiProps.SOURCE_OPTIONS));
				this.evidenceLevels = new ListSingleSelection<String>(Arrays.asList(GuiProps.EL_OPTIONS));
			}
		}else {
			this.sources = new ListSingleSelection<String>(Arrays.asList(GuiProps.SOURCE_OPTIONS));
			this.evidenceLevels = new ListSingleSelection<String>(Arrays.asList(GuiProps.EL_OPTIONS));
		}
		
	}

	public String getNetType() {
		return netType;
	}

	public void setNetType(String netType) {
		this.netType = netType;
	}

	public String getNetName() {
		return netName;
	}

	public void setNetName(String netName) {
		this.netName = netName;
	}
	
	
	
	

}
