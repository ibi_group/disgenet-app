package es.imim.DisGeNET.network;

import org.cytoscape.work.FinishStatus;
import org.cytoscape.work.ObservableTask;
import org.cytoscape.work.TaskObserver;

import es.imim.DisGeNET.exceptions.DisGeNetException;

import java.util.HashMap;
import java.util.Map;

public class NetTaskObserver implements TaskObserver{

	private Map <String,String> result;  
	
	@Override
	public void taskFinished(ObservableTask task) {
		this.result = new HashMap<>();
		this.result = task.getResults(Map.class);
	}

	@Override
	public void allFinished(FinishStatus finishStatus) {
		Exception ex = finishStatus.getException();
		if(ex!=null) {
			this.result = new HashMap<>();
			if(ex.getClass().equals(DisGeNetException.class)) {
				this.result.put("DisGeNETException",ex.getMessage());
			}else {
				this.result.put("Error", "Check the cytoscape log for more information, CytoscapeConfiguration/3/framework-cytoscape.log");
			}
		}
	}
	
	public Map getResult () {
		return this.result;
	}
 
}
