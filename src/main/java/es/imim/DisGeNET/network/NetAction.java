package es.imim.DisGeNET.network;

/**
	DisGeNET: A Cytoscape plugin to visualize, integrate, search and analyze gene-disease networks
	Copyright (C) 2010  Michael Rautschka, Anna Bauer-Mehren
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyEdge;

import org.cytoscape.application.CyApplicationManager;

/*import org.cytoscape.task.Task;
import org.cytoscape.task.TaskMonitor;
import org.cytoscape.task.ui.JTaskConfig;
import org.cytoscape.task.util.TaskManager;*/

import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;
import org.cytoscape.work.TaskMonitor.Level;
import org.cytoscape.work.TaskManager;
import org.cytoscape.work.swing.DialogTaskManager;

//JAVI
import org.cytoscape.work.TaskIterator;
import org.cytoscape.view.model.CyNetworkView;

/*import org.cytoscape.util.CytoscapeAction;*/

import org.cytoscape.view.vizmap.VisualMappingManager;
import org.cytoscape.view.vizmap.VisualStyle;

/*import es.imim.DisGeNET.DisGeNET;*/

import es.imim.DisGeNET.internal.CyActivator;
import es.imim.DisGeNET.internal.tables.DisGeNetCyTableBase;
import es.imim.DisGeNET.Status;
import es.imim.DisGeNET.database.DatabaseImpl;
import es.imim.DisGeNET.exceptions.DisGeNetException;
import es.imim.DisGeNET.gui.GuiParameters;
import es.imim.DisGeNET.styles.DisGenetStyleManager;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.html.HTMLEditorKit;


/**
 * Class to handle the clicking of the create network Button
 */
public class NetAction implements ActionListener {

	JTabbedPane pane;
	
	private GuiParameters curParams;
	private ArrayList <String> errors;
		
	public NetAction() {
	}

	public NetAction(GuiParameters curParams) {
		this.curParams = curParams;
	}

	public NetAction(GuiParameters curParams, JTabbedPane pane) {
		this.curParams = curParams;
		this.pane = pane;
	}

	/**
	 * This method is called when the user clicks create network.
	 */
	public void actionPerformed(ActionEvent event) {
		
		boolean errorFlag = false;
		String errorMessage = "";
		
		//JAVI ADDED
		//Check if the score has a valid value
		try{
			Double lowScore = Double.parseDouble(this.curParams.getLowScore());
			Double highScore = Double.parseDouble(this.curParams.getHighScore());
			if(lowScore<0) {
				errorMessage+="Minimum range must be between 0 and 1";
				errorFlag = true;
			}
			if(highScore>1) {
				errorMessage+="Maximum range must be between 0 and 1";
				errorFlag = true;
			}
		}
		catch(NumberFormatException e){
			errorMessage+="The score range must be a number";
			errorFlag = true;
		}
		
		try{
			Double lowEi = Double.parseDouble(this.curParams.getLowEi());
			Double highEi = Double.parseDouble(this.curParams.getHighEi());
			if(lowEi<0) {
				errorMessage+="Minimum range must be between 0 and 1";
				errorFlag = true;
			}
			if(highEi>1) {
				errorMessage+="Maximum range must be between 0 and 1";
				errorFlag = true;
			}
		}
		catch(NumberFormatException e){
			errorMessage+="The score range must be a number";
			errorFlag = true;
		}
		
		if(!errorFlag) {
			AbstractTask create_disgenet_network_task = new NetTask(curParams);
			CyActivator.getInstance().getDialogTaskManagerService().execute(new TaskIterator(create_disgenet_network_task));
		}else {
			JOptionPane.showMessageDialog(null,	errorMessage);
		}
		
		
							
	}
	
	/**
	 * this is the dialog window which is shown if MNET or ALL is selected as
	 * source in gene panel because the resulting data would be too big.
	 */
	private class TutorialDialog extends JDialog {
		static final long serialVersionUID = -945045L;

		public TutorialDialog() {
			//JAVI
			//super(Cytoscape.getDesktop(), "Resulting Network too big", false);
			//javi: non-static metho getDesktopService() cannot be referenced from a static context
			//super(CyActivator.getDesktopService().getJFrame(), "Resulting network too big", false);
			setResizable(false);

			// main panel for dialog box
			JEditorPane editorPane = new JEditorPane();
			// editorPane.setMargin(new Insets(10,10,10,10));
			editorPane.setEditable(false);
			editorPane.setEditorKit(new HTMLEditorKit());
			editorPane.addHyperlinkListener(new HyperlinkAction(editorPane));

			/*JAVI TODO resources Fix*/
			String logoURL = "";
			String bgURL = "";
			//URL logoURL = DisGeNET.class
			//		.getResource("resources/DisGeNET_banner_logo_small.png");
			//URL bgURL = DisGeNET.class
			//		.getResource("resources/DisGeNET_banner_bg_small.jpg");

			String logoCode = "";
			if (logoURL != null) {
				logoCode = "<center><img src='" + logoURL + "'></center>";
			}

			editorPane
					.setText("<html><body style=\"padding:10px\" background=\""
							+ bgURL
							+ "\">"
							+ logoCode
							+ "<P align=center><b> Large network </b><BR>"
							//+ "<P align=center><b>Resulting Network too big </b><BR>"
						+
							// "</p><p align=justify>" +
							"The network resulting from the current settings is too big to be viewed <BR>"
							+ "within Cytoscape. If you want to analyze this network, please follow our Online Tutorial <BR>"							
							+ "<a href='http://disgenet.org/web/DisGeNET/v2.1/plugin#guide'>section 3.3</a><BR><BR>"
							+ "</P></body></html>");
							
			setContentPane(editorPane);
		}

		private class HyperlinkAction implements HyperlinkListener {
			JEditorPane pane;

			public HyperlinkAction(JEditorPane pane) {
				this.pane = pane;
			}

			public void hyperlinkUpdate(HyperlinkEvent event) {
				if (event.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
					Desktop dt = Desktop.getDesktop();
					try {
						dt.browse(new URI(event.getURL().toString()));
					} catch (IOException e) {
						e.printStackTrace();
					} catch (URISyntaxException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

}
	
//	public String getDiseaseClassId(String diseaseClassName) {
//
//		String meshClass = "";
//
//		String query2 = "select * from diseaseClass where diseaseClassName = \""
//				+ diseaseClassName + "\";";
//
//		ResultSet rsClass = null;
//
//		DatabaseImpl db1 = new DatabaseImpl();
//		rsClass = db1.runQuery(query2);
//
//		try {
//			while (rsClass.next()) {
//				// iterate over fields and store field id + value
//				meshClass = rsClass.getString("diseaseClass");
//				////(meshClass + "inside function");
//			}
//			rsClass.close();
//		} catch (SQLException e) {
//			e.printStackTrace();
//			
//		}
//
//		return meshClass;
//	}

