package es.imim.DisGeNET.network;

import org.cytoscape.model.CyNetwork;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import es.imim.DisGeNET.validation.NetworkValidation;

/**
 * Class that handles the display of gene nodes in networks containing variants.
 * @author jsauch
 *
 */
public class ShowGenesVariantTask extends AbstractTask {

	private boolean showGenes;
	private CyNetwork currentNet;
	
	public ShowGenesVariantTask(boolean showGenes, CyNetwork currentNet) {
		this.showGenes = showGenes;
		this.currentNet = currentNet;
	}
	
	@Override
	public void run(TaskMonitor taskMonitor) throws Exception {
		NetworkBuilder nb = new NetworkBuilder();
		taskMonitor.setProgress(-1);
		if(showGenes) {//Check if genes are displayed.
			if(!NetworkValidation.isExpandedWithGenes(currentNet)) {
				nb.showVariantNetWithGenes();
			}
		}else {
			if(NetworkValidation.isExpandedWithGenes(currentNet)) {
				nb.removeGenesFromVariantNet();
			}
		}
	}
	
}
