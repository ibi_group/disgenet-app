package es.imim.DisGeNET.network.colorMapping;

import java.util.ArrayList;
import java.util.List;

import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyTable;

import es.imim.DisGeNET.internal.CyActivator;

public class ColorTable {
	
	public static CyTable CreateColorTable (CyNetwork cyNet) {
		CyTable colorMapTable = cyNet.getTable(CyNode.class, CyNetwork.HIDDEN_ATTRS);
		List <String> columnNames = new ArrayList<>(DiseaseColourMapping.getDiseaseColourNameMapping().keySet());
		for (String columnName : columnNames) {
			if(colorMapTable.getColumn(columnName)==null) {
				colorMapTable.createColumn(columnName, Integer.class, false,0);
			}
		}
		return colorMapTable;
	}
	
}
