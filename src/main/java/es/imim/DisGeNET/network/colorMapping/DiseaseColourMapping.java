package es.imim.DisGeNET.network.colorMapping;

/**
	DisGeNET: A Cytoscape plugin to visualize, integrate, search and analyze gene-disease networks
	Copyright (C) 2010  Michael Rautschka, Anna Bauer-Mehren
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


import java.awt.Color;
import java.util.HashMap;

/**
 * simple function to map the color codes to the corresponding colours and names
 * @author michael
 *
 */

public class DiseaseColourMapping {
	public static HashMap<String, Color> getDiseaseColourMapping() {
		HashMap<String, Color> mapping = new HashMap<String, Color>();
		// ClassNr ClassName
		// C01 Bacterial Infections and Mycoses
		// C02 Virus Diseases
		// C03 Parasitic Diseases
		// C04 Neoplasms
		// C05 Musculoskeletal Diseases
		// C06 Digestive System Diseases
		// C07 Stomatognathic Diseases
		// C08 Respiratory Tract Diseases
		// C09 Otorhinolaryngologic Diseases
		// C10 Nervous system Diseases
		// C11 Eye Diseases
		// C12 Male Urogenital Diseases
		// C13 Female Urogenital Diseases and Pregnancy Complications
		// C14 Cardiovascular Diseases
		// C15 Hemic and Lymphatic Diseases
		// C16 Congenital, Hereditary, and Neonatal Diseases and Abnormalities
		// C17 Skin and Connective Tissue Diseases
		// C18 Nutritional and Metabolic Diseases
		// C19 Endocrine System Diseases
		// C20 Immune System Diseases
		// C21 Disorders of Environmental Origin
		// C22 Animal Diseases
		// C23 Pathological Conditions, Signs and Symptoms
		// F03 Mental Disorders
		mapping.put("C01", new Color(255, 255, 170)); // light pastel yellow
		mapping.put("C04", new Color(42, 212, 255)); // turquoise
		mapping.put("C05", new Color(127, 127, 255)); // blue-grey mix
		mapping.put("C06", new Color(255, 212, 255)); // rose
		mapping.put("C07", new Color(127, 255, 85)); // light green
		mapping.put("C08", new Color(237, 204, 105)); // very light brown
		mapping.put("C09", new Color(0, 255, 0)); // darker light green (lime)
		mapping.put("C10", new Color(255, 0, 170)); // pink
		mapping.put("C11", new Color(212, 0, 255)); // lila
		mapping.put("C12", new Color(36, 46, 136)); // dark blue
		mapping.put("C13", new Color(86, 4, 86)); // very dark purple
		mapping.put("C14", new Color(167, 167, 80)); // dark red - purple
		mapping.put("C15", new Color(4, 94, 4)); // dark green
		mapping.put("C16", new Color(0, 0, 255)); // blue
		mapping.put("C17", new Color(255, 170, 42)); // orange
		mapping.put("C18", new Color(173, 146, 25)); // dark khaki
		mapping.put("C19", new Color(212, 210, 253)); // very light lila-grey
		mapping.put("C20", new Color(211, 126, 254)); // normal lila
		mapping.put("C21", new Color(6, 200, 6)); // middle green
		mapping.put("C22", new Color(212, 110, 255)); // orange - red mix
		mapping.put("C23", new Color(165, 165, 165)); // grey
		mapping.put("C24", new Color(200, 18, 14)); //  
		mapping.put("C25", new Color(92, 61, 60)); //  
		mapping.put("C26", new Color(56, 86, 57)); //  		
		mapping.put("F01", new Color(255, 212, 127)); 
		mapping.put("F03", new Color(162, 10, 237)); // middle dark lila
		//mapping.put("NA", new Color(225, 225, 225)); // light grey
		mapping.put("null", new Color(225, 225, 225)); // light grey
		return mapping;
	}
	public static HashMap<String, String> getDiseaseColourNameMapping() {
		HashMap<String, String> mapping = new HashMap<String, String>();
		mapping.put("C01", "Infections");
        mapping.put("C04", "Neoplasms");
		mapping.put("C05", "Musculoskeletal Diseases"); 
		mapping.put("C06", "Digestive System Diseases"); 
		mapping.put("C07", "Stomatognathic Diseases"); 
		mapping.put("C08", "Respiratory Tract Diseases"); 
		mapping.put("C09", "Otorhinolaryngologic Diseases"); 
		mapping.put("C10", "Nervous System Diseases"); 
		mapping.put("C11", "Eye Diseases"); 
		mapping.put("C12", "Male Urogenital Diseases"); 
		mapping.put("C13", "Female Urogenital Diseases and Pregnancy Complications"); 
		mapping.put("C14", "Cardiovascular Diseases"); 
		mapping.put("C15", "Hemic and Lymphatic Diseases"); 
		mapping.put("C16", "Congenital, Hereditary, and Neonatal Diseases and Abnormalities"); 
		mapping.put("C17", "Skin and Connective Tissue Diseases"); 
		mapping.put("C18", "Nutritional and Metabolic Diseases"); 
		mapping.put("C19", "Endocrine System Diseases"); 
		mapping.put("C20", "Immune System Diseases"); 
		mapping.put("C21", "Disorders of Environmental Origin"); 
		mapping.put("C22", "Animal Diseases"); 
		mapping.put("C23", "Pathological Conditions, Signs and Symptoms"); 
		mapping.put("C24", "Occupational Diseases"); 
		mapping.put("C25", "Chemically-Induced Disorders"); 
		mapping.put("C26", "Wounds and Injuries"); 
		mapping.put("F01", "Behavior and Behavior Mechanisms"); 
		mapping.put("F03", "Mental Disorders"); 
		mapping.put("null", "Not Available"); 
		return mapping;
	}
	
	public static HashMap<String, Integer> getDiseaseColourNumericMapping() {
		HashMap<String, Integer> mapping = new HashMap<String, Integer>();
		mapping.put("C01", 1);
		mapping.put("C02", 2); 
		mapping.put("C03", 3); 
		mapping.put("C04", 4); 
		mapping.put("C05", 5); 
		mapping.put("C06", 6); 
		mapping.put("C07", 7); 
		mapping.put("C08", 8); 
		mapping.put("C09", 9); 
		mapping.put("C10", 10); 
		mapping.put("C11", 11); 
		mapping.put("C12", 12); 
		mapping.put("C13", 13); 
		mapping.put("C14", 14); 
		mapping.put("C15", 15); 
		mapping.put("C16", 16); 
		mapping.put("C17", 17); 
		mapping.put("C18", 18); 
		mapping.put("C19", 19); 
		mapping.put("C20", 20); 
		mapping.put("C21", 21); 
		mapping.put("C22", 22); 
		mapping.put("C23", 23); 
		mapping.put("C24", 24); 
		mapping.put("C25", 25); 
		mapping.put("C26", 26); 
		mapping.put("F01", 27); 
		mapping.put("F03", 28); 
		mapping.put("null", 0); 
		return mapping;
	}

}