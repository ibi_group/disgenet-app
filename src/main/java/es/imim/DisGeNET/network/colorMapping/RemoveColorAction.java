package es.imim.DisGeNET.network.colorMapping;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.JOptionPane;

import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.application.swing.CytoPanel;
import org.cytoscape.application.swing.CytoPanelName;
import org.cytoscape.application.swing.CytoPanelState;
import org.cytoscape.model.CyIdentifiable;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyTable;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.view.model.VisualLexicon;
import org.cytoscape.view.model.VisualProperty;
import org.cytoscape.view.presentation.RenderingEngine;
import org.cytoscape.view.presentation.customgraphics.CyCustomGraphics;

import es.imim.DisGeNET.internal.CyActivator;

public class RemoveColorAction implements ActionListener {
	
	public void removeColorClass(CyNetwork cyNet) {
		CyTable nodeTable = cyNet.getDefaultNodeTable();
		List <String> columnNames = new ArrayList<>(DiseaseColourMapping.getDiseaseColourNameMapping().keySet());
		for (String columnName : columnNames) {
			if(nodeTable.getColumn(columnName)!=null) {
				nodeTable.deleteColumn(columnName);
			}
		}
		CyNetworkView cyNetworkView = CyActivator.getInstance().getApplicationManagerService().getCurrentNetworkView();
		cyNetworkView.updateView();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		CyNetwork cyNet = CyActivator.getInstance().getCyApplicationManager().getCurrentNetwork();
		if(cyNet==null||cyNet.getNodeCount()==0) {
			JOptionPane.showMessageDialog(null, "No network view selected.");
		}else {
			removeColorClass(cyNet);
			CytoPanel cytoPanel = CyActivator.getInstance().getDesktopService().getCytoPanel(CytoPanelName.EAST);
			cytoPanel.setState(CytoPanelState.HIDE);
		}
	}

}
