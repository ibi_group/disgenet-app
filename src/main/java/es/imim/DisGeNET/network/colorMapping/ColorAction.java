package es.imim.DisGeNET.network.colorMapping;

import java.awt.BorderLayout;

/**
    DisGeNET: A Cytoscape plugin to visualize, integrate, search and analyze gene-disease networks
    Copyright (C) 2010  Michael Rautschka, Anna Bauer-Mehren

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.application.swing.CytoPanel;
import org.cytoscape.application.swing.CytoPanelComponent;
import org.cytoscape.application.swing.CytoPanelName;
import org.cytoscape.application.swing.CytoPanelState;
import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyRow;
import org.cytoscape.model.CyTable;
import org.cytoscape.service.util.CyServiceRegistrar;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.VisualLexicon;
import org.cytoscape.view.model.VisualLexiconNode;
import org.cytoscape.view.model.VisualProperty;
import org.cytoscape.view.presentation.RenderingEngine;
import org.cytoscape.view.presentation.customgraphics.CustomGraphicLayer;
import org.cytoscape.view.presentation.customgraphics.CyCustomGraphics;
import org.cytoscape.view.presentation.customgraphics.CyCustomGraphics2;
import org.cytoscape.view.presentation.customgraphics.CyCustomGraphics2Factory;
import org.cytoscape.view.presentation.property.values.CyColumnIdentifier;
import org.cytoscape.view.presentation.property.values.CyColumnIdentifierFactory;
import org.cytoscape.view.vizmap.VisualMappingFunction;
import org.cytoscape.view.vizmap.VisualMappingManager;
import org.cytoscape.view.vizmap.VisualStyle;

import es.imim.DisGeNET.tool.*;
import es.imim.DisGeNET.gui.DisColorLegendPanel;
import es.imim.DisGeNET.internal.CustomChartListener;
import es.imim.DisGeNET.internal.CyActivator;
import es.imim.DisGeNET.internal.DisGeNETCytoPanel;
import es.imim.DisGeNET.styles.DisGenetStyleManager;


/**
 * listener for the disease color button which calls the function
 * to color nodes according to their disease classes and puts
 * a legend into the results panel if it is not already there
 * @author michael
 *
 */
public class ColorAction implements ActionListener {

	private void addLegend() {
		CytoPanel cytoPanel = CyActivator.getInstance().getDesktopService().getCytoPanel(CytoPanelName.EAST);
		boolean tableExists = false;
		for (int i = 0; i < cytoPanel.getCytoPanelComponentCount(); i++) {
			if (cytoPanel.getComponentAt(i).getName()!=null&&cytoPanel.getComponentAt(i).getName().equals("DiseaseColourMappingPanel")) {
				tableExists = true;
			}
		}
		if (!tableExists) {
			DisColorLegendPanel disColorPanel = new DisColorLegendPanel(new BorderLayout());
			disColorPanel.setName("DiseaseColourMappingPanel");
			// get color list
			HashMap<String, Color> colourMappingMap = DiseaseColourMapping.getDiseaseColourMapping();
			
			HashMap<String, String> colorNameMapping = DiseaseColourMapping.getDiseaseColourNameMapping();
			
			JTable legendTable = new JTable(colourMappingMap.size(), 3);
			legendTable.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
			
			ArrayList<String> classNames = new ArrayList<String>();
			classNames.addAll(colorNameMapping.keySet());
			Collections.sort(classNames);
			
			Iterator<String> mapIt = classNames.iterator();
	
			int count = 0;
			legendTable.getColumnModel().getColumn(0).setMaxWidth(1);
			legendTable.getColumnModel().getColumn(1).setMaxWidth(35);
			legendTable.getColumnModel().getColumn(0).setCellRenderer(new BackgroundColorCellRenderer());
			legendTable.getColumnModel().getColumn(2).setCellRenderer(new TooltipTableCellRenderer());
			
			int width = 17;
			int height = 17;
			
			legendTable.setName("DiseaseColourMapping");
			
			while (mapIt.hasNext()) {
				
				String name = mapIt.next();
				Color color = colourMappingMap.get(name);
			
				// add circle to table
//				BufferedImage img = new BufferedImage( height, width, BufferedImage.TYPE_INT_RGB );
//				Graphics g = img.getGraphics();
//				 
//				// Create white background
//				g.setColor(Color.white);
//				g.fillRect(0, 0, width, height);
//
//				// add circle with colour from mapping table
//				g.setColor(color);
//				g.fillOval(0, 0, width-(width/10), height-(height/10));
				
				legendTable.setValueAt(color, count, 0);
				legendTable.setValueAt(name, count, 1);
				legendTable.setToolTipText(colorNameMapping.get(name));
				
				legendTable.setValueAt(colorNameMapping.get(name), count++, 2);
			}
			// add to cytoscape result panel (east)
			legendTable.setFillsViewportHeight(true);
			
			disColorPanel.add(legendTable, BorderLayout.CENTER);
			CyActivator.getInstance().getServiceRegistrar().registerService(disColorPanel, CytoPanelComponent.class, new Properties());
			for (int i = 0; i < cytoPanel.getCytoPanelComponentCount(); i++) {
				if (cytoPanel.getComponentAt(i).getName()!=null&&cytoPanel.getComponentAt(i).getName().equals("DiseaseColourMappingPanel")) {
					cytoPanel.setSelectedIndex(i);
				}
			}
		}
		cytoPanel.setState(CytoPanelState.DOCK);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		CyNetwork cyNet = CyActivator.getInstance().getCyApplicationManager().getCurrentNetwork();
		if(cyNet==null||cyNet.getNodeCount()==0) {
			JOptionPane.showMessageDialog(null, "No network view selected.");
		}else {
			CheckColorTable(cyNet);
			fillColorTable(cyNet);
			paintNodes(cyNet);
			addLegend();
		}
		
	}
	
	public Long CheckColorTable(CyNetwork cyNet) {
		CyTable table = ColorTable.CreateColorTable(cyNet);
		Long tableId = table.getSUID();
		return tableId;
	}

	public void fillColorTable(CyNetwork cyNet) {
		CyTable nodeTable = cyNet.getDefaultNodeTable();
		CyTable colorTable = cyNet.getTable(CyNode.class, CyNetwork.HIDDEN_ATTRS);
		List <CyRow> rows = nodeTable.getAllRows();
		Iterator <CyRow> rowIt = rows.iterator();
		
		while(rowIt.hasNext()) {
			CyRow row = rowIt.next();
			CyRow newRow = colorTable.getRow(row.get("SUID", Long.class));
			List <String> disClassList = new ArrayList<>();
			if(!row.get("nodeType", String.class).equals("disease")) {
				CyNode node = cyNet.getNode(row.get("SUID", Long.class));
				List <CyNode> neighborsNodes = cyNet.getNeighborList(node, CyEdge.Type.ANY);
				Set <String> disClassSet = new LinkedHashSet<>();
				for (CyNode cyNode : neighborsNodes) {
					List <String> disClasses = nodeTable.getRow(cyNode.getSUID()).getList("diseaseClass", String.class);
					if(disClasses!=null) {
						disClassSet.addAll(disClasses);	
					}
				}
				disClassList.addAll(disClassSet); 
			}else {
				if(row.getList("diseaseClass", String.class)!=null) {
					disClassList.addAll(row.getList("diseaseClass", String.class));
				}
				
			}
			if(!disClassList.isEmpty()){
				for (String columnName : disClassList) {
					if(columnName!=null) {
						newRow.set(columnName, 1);
					}
					
				}
			}else {
				newRow.set("null",1);
			}
			
		}
		Collection <CyColumn> columns = nodeTable.getColumns();
		Collection <String> columnNames = new ArrayList<>();
		for (CyColumn cyColumn : columns) {
			 columnNames.add(cyColumn.getName());
		}
		Set <String> colColorNames = DiseaseColourMapping.getDiseaseColourNameMapping().keySet();
		if(!columnNames.containsAll(colColorNames)) {
			for (String diseaseClassIdCol : colColorNames) {
				if(nodeTable.getColumn(diseaseClassIdCol)==null) {
					nodeTable.addVirtualColumn(diseaseClassIdCol, diseaseClassIdCol, colorTable, "SUID", false);
				}
			}
		}
	}
	
	public void paintNodes(CyNetwork cyNet) {
				
		List <Color> colors = new ArrayList<>(DiseaseColourMapping.getDiseaseColourMapping().values());
		CustomChartListener customChartListener = CyActivator.getInstance().getCustomGraphics();
		CyColumnIdentifierFactory columnIdFactoy = CyActivator.getInstance().getColumnIdentifierFactory();
		List <String> columnNames = new ArrayList<>(DiseaseColourMapping.getDiseaseColourMapping().keySet());
		List <CyColumnIdentifier> columnIds = new ArrayList<>();
		CyCustomGraphics2Factory<?> customGraphicsFactory = customChartListener.getFactory();
		
		for (String columnName : columnNames) {
			CyColumnIdentifier columnId = columnIdFactoy.createColumnIdentifier(columnName);
			columnIds.add(columnId);
		}
		Map<String,Object> chartProps = new HashMap<String,Object>();
		chartProps.put("cy_dataColumns", columnIds); 
		chartProps.put("cy_colors", colors);
		
		CyCustomGraphics2<?> customGraphics = customGraphicsFactory.getInstance(chartProps);

		CyNetworkView cyNetView = CyActivator.getInstance().getApplicationManagerService().getCurrentNetworkView();
		VisualMappingManager vmm = CyActivator.getInstance().getVisualMappingMgr();
		CyApplicationManager appManager = CyActivator.getInstance().getApplicationManagerService();
		RenderingEngine engine = appManager.getCurrentRenderingEngine();
		VisualLexicon lexicon = engine.getVisualLexicon();
		VisualProperty<CyCustomGraphics> visualProperty = (VisualProperty<CyCustomGraphics>) lexicon.lookup(CyNode.class, "NODE_CUSTOMGRAPHICS_1");
		if (visualProperty != null)
			cyNetView.setViewDefault(visualProperty, customGraphics);
		cyNetView.updateView();
	}
	
}



class BackgroundColorCellRenderer extends DefaultTableCellRenderer {  

	private static final long serialVersionUID = -8882225593125946081L;

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,  
			boolean hasFocus, int row, int column)  
	{  
		JLabel label = (JLabel)super.getTableCellRendererComponent(  
				table, value, isSelected, hasFocus, row, column  
				);
		
		if (value instanceof Color) {
			label.setText("");
			label.setBackground((Color) value);
		}
		
		return label;
	}  
} 

class TooltipTableCellRenderer extends DefaultTableCellRenderer {  

	private static final long serialVersionUID = -8882225593125946081L;

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,  
			boolean hasFocus, int row, int column)  
	{  
		JLabel label = (JLabel)super.getTableCellRendererComponent(  
				table, value, isSelected, hasFocus, row, column  
				);  
		if (value instanceof String) {  
			label.setText(value.toString());  
			label.setToolTipText(value.toString());
		}
		return label;
	} 


} 