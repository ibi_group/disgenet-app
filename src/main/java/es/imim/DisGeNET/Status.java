package es.imim.DisGeNET;

/**
	DisGeNET: A Cytoscape plugin to visualize, integrate, search and analyze gene-disease networks
	Copyright (C) 2010  Michael Rautschka, Anna Bauer-Mehren
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.io.Serializable;

/**
 * just some error messages; only used for DB connection so far
 * @author michael
 *
 */

public enum Status implements Serializable {
	
  /* Success */
  SUCCESS("SUCCESS"),
  
  /* Error Messages */
  FAIL("FAIL: An error occured"),
  FILE_ERROR("FILE_ERROR: There was a problem with the file."),
  SQL_ERROR("(SQL_ERROR) "),
  
  /* Connections */
  NETWORK_ERROR("NETWORK_ERROR: A problem with your network connection occured."),
  CONNECTION_ERROR("CONNECTION_ERROR: An error occurred while connecting."),
  CONNECTION_WRONG_ADDRESS_ERROR("CONNECTION_WRONG_ADDRESS_ERROR: Incorrect adress."),
  DISCONNECTION_ERROR("DISCONNECTION_ERROR: Error while disconnecting."),
  
  /* Database */
  DB_ERROR("DB_ERROR: A general problem with the database occured."),
  DB_WRITE_ERROR("DB_WRITE_ERROR: There were problems writing to the database."),
  DB_CONNECTION_ERROR("DB_CONNECTION_ERROR: Could not connect to the database."),
  DB_DRIVER_ERROR("DB_DRIVER_ERROR: An error with the database driver occured."),
  DB_ENTRY_ALREADY_IN_DB("DB_ENTRY_ALREADY_IN_DB: Entry already exists in database."),
  DB_CLOSE_CON_ERROR("DB_CLOSE_CON_ERROR: There was a problem closing the database connection."),
  DB_NO_SPECIFIED_DATABASE("DB_NO_SPECIFIED_DATABASE: There's no path for the database."),
  DB_FILE_NOT_SET("DB_FILE_NOT_SET: Cannot set a the file for the database.");
  private String statusMessage;
  
  /* constructor */
  Status(String statusMessage)
  {
    this.statusMessage = statusMessage; 
  }
  
  /* get method for statusMessage */
  public String getStatusMessage()
  {
    return statusMessage;
  }
}