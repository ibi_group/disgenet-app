package es.imim.DisGeNET.search;

/**
	DisGeNET: A Cytoscape plugin to visualize, integrate, search and analyze gene-disease networks
	Copyright (C) 2010  Michael Rautschka, Anna Bauer-Mehren
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * This class is part of the search field in the gui and was 
 * inspired by the tutorial that can be found here:
 * http://www.javafaq.nu/java-bookpage-20-4.html 
 */

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

//JAVI
//import org.cytoscape.Cytoscape;


public class MemComboAgent extends KeyAdapter {
	protected MemComboBox m_comboBox;
	protected JTextField m_editor;

	public MemComboAgent(MemComboBox comboBox) {
		m_comboBox = comboBox;
		m_editor = (JTextField) comboBox.getEditor().getEditorComponent();
		m_editor.addKeyListener(this);
	}

	public void keyReleased(KeyEvent e) {
		String str = m_editor.getText();

		// if esc or return is pressed 'leave' search field
		if (e.getKeyCode() == 27 || e.getKeyCode() == 10)
			return;

		if (str.length() == 0) {
			m_comboBox.setPopupVisible(false);
			m_comboBox.clear();
			return;
		}


		// if not backspace or remove key check key and get char
		if (!(e.getKeyCode() == 8) && !(e.getKeyCode() == 127)) {
			char ch = str.charAt(str.length() - 1);
			if (str.length() > 0
					&& str.substring(str.length() - 1, str.length())
							.equals("*")||str.substring(str.length() - 1, str.length())
							.equals(";")) {
				// do nothing, this char is definitely allowed, otherwise check
			} else {
				if ((ch == KeyEvent.CHAR_UNDEFINED
						|| Character.isISOControl(ch) || !Character
						.isLetterOrDigit(ch) && !Character.isWhitespace(ch)&&!Character.isValidCodePoint(ch))) {
					//JAVI
					//JOptionPane.showMessageDialog(Cytoscape.getDesktop(),
					//		"Character not allowed!");
					str = str.substring(0, str.length() - 1);
				}
			}
		}

		int pos = m_editor.getCaretPosition();

		m_comboBox.update(str);
		m_comboBox.setPos(pos);
		m_editor.setText(str);

		m_comboBox.setPopupVisible(true);

		for (int k = 0; k < m_comboBox.getItemCount(); k++) {
			String item = m_comboBox.getItemAt(k).toString();
			if (item.toLowerCase().startsWith(str.toLowerCase())) {
				m_editor.setText(item);
				m_editor.setCaretPosition(item.length());
				m_editor.moveCaretPosition(pos);
				break;
			}
		}

		// use min function here in case illegal char was entered and removed
		// again
		m_editor.setCaretPosition(Math.min(pos, str.length()));
	}
}
