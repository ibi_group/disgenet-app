package es.imim.DisGeNET.search;

/**
	DisGeNET: A Cytoscape plugin to visualize, integrate, search and analyze gene-disease networks
	Copyright (C) 2010  Michael Rautschka, Anna Bauer-Mehren
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * This class is part of the search field in the gui and was 
 * inspired by the tutorial that can be found here:
 * http://www.javafaq.nu/java-bookpage-20-4.html 
 */

import java.awt.event.*;
import java.net.*;
import javax.swing.*;
import javax.swing.event.*;

//JAVI
//import org.cytoscape.Cytoscape;

public class SearchField {

	protected JEditorPane m_browser;
	protected MemComboBox m_locator;
	protected AnimatedLabel m_runner;

	public SearchField() {
		m_runner = new AnimatedLabel("img/busy-icon", 14);
		m_locator = new MemComboBox();

		BrowserListener lst = new BrowserListener();
		m_locator.addActionListener(lst);
		MemComboAgent agent = new MemComboAgent(m_locator);
	}

	public MemComboBox getSearchBox() {
		return this.m_locator;
	}

	class BrowserListener implements ActionListener, HyperlinkListener {
		public void actionPerformed(ActionEvent evt) {

			String sUrl = (String) m_locator.getSelectedItem();
			if (sUrl == null || sUrl.length() == 0 || m_runner.getRunning())
				return;

			SearchExecuter loader = new SearchExecuter(sUrl);
			loader.start();
		}

		public void hyperlinkUpdate(HyperlinkEvent e) {
			URL url = e.getURL();
			if (url == null || m_runner.getRunning())
				return;

			SearchExecuter loader = new SearchExecuter(url.toString());
			loader.start();
		}
	}

	class SearchExecuter extends Thread {
		protected String m_sUrl;

		public SearchExecuter(String sUrl) {
			m_sUrl = sUrl;
		}

		public void run() {
			m_runner.setRunning(true);

			//JAVI
			//Cytoscape.getDesktop().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

			try {
				m_browser.setText(m_sUrl);
			} catch (Exception e) {
			}

			m_runner.setRunning(false);

			//javi
			//Cytoscape.getDesktop().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}
	}

	public static void main(String argv[]) {
		new SearchField();
	}
}





