package es.imim.DisGeNET.search;


/**
	DisGeNET: A Cytoscape plugin to visualize, integrate, search and analyze gene-disease networks
	Copyright (C) 2010  Michael Rautschka, Anna Bauer-Mehren
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * This class is part of the search field in the gui and was 
 * inspired by the tutorial that can be found here:
 * http://www.javafaq.nu/java-bookpage-20-4.html 
 */


import java.awt.AWTEvent;
import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.plaf.basic.BasicComboBoxUI;
import javax.swing.plaf.basic.BasicComboPopup;
import javax.swing.plaf.basic.ComboPopup;


public class MemComboBox extends JComboBox {

	private static final long serialVersionUID = 1L;

	public static final int MAX_MEM_LEN = 50;

	private int m_position;

	private ArrayList<String> completeItemList = new ArrayList<String>();

	public MemComboBox() {
		super();
		this.completeItemList = new ArrayList<String>();
		setEditable(true);
		setUI(new MyComboBoxUI());
	}

	public class myComboUI extends BasicComboBoxUI {
		protected ComboPopup createPopup() {
			BasicComboPopup popup = new BasicComboPopup(comboBox) {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				protected JScrollPane createScroller() {
					return new JScrollPane(list,
							ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
							ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
				}// end of method createScroller
			};
			return popup;
		}// end of method createPopup
	}// end of inner class myComboUI

	public class MyComboBoxUI extends BasicComboBoxUI {
		private int padding = 10;

		protected ComboPopup createPopup() {
			BasicComboPopup popup = new BasicComboPopup(comboBox) {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				public void show() {
					// Need to compute width of text
					int widest = getWidestItemWidth();

					// Get the box's size
					Dimension popupSize = comboBox.getSize();

					// Set the size of the popup
					popupSize.setSize(widest + (2 * padding),
							getPopupHeightForRowCount(comboBox
									.getMaximumRowCount()));

					// Compute the complete bounds
					Rectangle popupBounds = computePopupBounds(0, comboBox
							.getBounds().height, popupSize.width,
							popupSize.height);

					// Set the size of the scroll pane
					scroller.setMaximumSize(popupBounds.getSize());
					scroller.setPreferredSize(popupBounds.getSize());
					scroller.setMinimumSize(popupBounds.getSize());

					// Cause it to re-layout
					list.invalidate();

					// Handle selection of proper item
					int selectedIndex = comboBox.getSelectedIndex();
					if (selectedIndex == -1)
						list.clearSelection();
					else
						list.setSelectedIndex(selectedIndex);

					// Make sure the selected item is visible
					list.ensureIndexIsVisible(list.getSelectedIndex());

					// Use lightweight if asked for
					setLightWeightPopupEnabled(comboBox
							.isLightWeightPopupEnabled());

					// Show the popup
					show(comboBox, popupBounds.x, popupBounds.y);
				}
			};

			popup.getAccessibleContext().setAccessibleParent(comboBox);

			return popup;
		}

		public int getWidestItemWidth() {
			// Items, font
			int numItems = comboBox.getItemCount();
			Font font = comboBox.getFont();
			FontMetrics metrics = comboBox.getFontMetrics(font);

			// the widest width
			int widest = 0;

			for (int i = 0; i < numItems; i++) {
				// Get the item
				Object item = comboBox.getItemAt(i);

				// Calculate the width of this line
				int lineWidth = metrics.stringWidth(item.toString());

				// Use whatever's widest
				widest = Math.max(widest, lineWidth);
			}

			return widest;
		}
	}

	public class ReshowPopup implements Runnable {

		private JComponent _eventComponent;

		private Point _eventPoint, _mousePoint;

		public ReshowPopup() {
			final AWTEvent currentEvent = EventQueue.getCurrentEvent();
			if (currentEvent instanceof MouseEvent) {
				final MouseEvent mouseEvent = (MouseEvent) currentEvent;
				_eventComponent = (JComponent) mouseEvent.getSource();
				_eventPoint = _eventComponent.getLocationOnScreen();
				_mousePoint = mouseEvent.getPoint();
			} else {
				_eventComponent = null;
				_eventPoint = null;
				_mousePoint = null;
			}
		}

		public void run() {
			MemComboBox.this.hidePopup();
			MemComboBox.this.showPopup();
			checkMousePosition();
		}

		private final void checkMousePosition() {
			if (_eventComponent != null) {
				final Point newPoint = _eventComponent.getLocationOnScreen();
				final int difference = newPoint.x - _eventPoint.x;
				if (difference != 0) {
					try {
						new Robot().mouseMove(newPoint.x + _mousePoint.x,
								newPoint.y + _mousePoint.y);
					} catch (AWTException ex) {
					}
				}
			}
		}
	}

	void setPos(int pos) {
		this.m_position = pos;
	}

	private int getPos() {
		return this.m_position;
	}

	public void fillSearchBox(ArrayList<String> searchList) {
		// add Names to m_locator
		java.util.Iterator<String> searchListIt = searchList.iterator();
		while (searchListIt.hasNext()) {
			this.completeItemList.add(searchListIt.next());
		}
	}

	public void add(String item) {
		this.completeItemList.add(item);
	}

	public void clearList() {
		this.completeItemList.clear();
	}

	public void clear() {
		removeAllItems();
	}

	public void update(String enteredItem) {
		removeAllItems();
		java.util.Iterator<String> entityIt = this.completeItemList.iterator();
		int count = 0;

		String enteredItemRep = enteredItem.toLowerCase().replace("*", "(.*)");

		Pattern pattern = null;
		try {
			pattern = Pattern.compile(enteredItemRep);
		} catch (Exception e) {
		}

		while (entityIt.hasNext() && count < 50) {
			String entity = entityIt.next();

			Matcher matcher = null;
			try {
				matcher = pattern.matcher(entity.toLowerCase());

				if (matcher.matches()) {
					addItem(entity);
					count++;
				}
			} catch (Exception e) {
			}
		}
		SwingUtilities.invokeLater(new ReshowPopup());
	}

	private void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
		JComboBox box = (JComboBox) evt.getSource();

		Object comp = box.getUI().getAccessibleChild(box, 0);

		if (!(comp instanceof JPopupMenu))
			return;

		Object scrollObject = ((JComponent) comp).getComponent(0);

		if (!(scrollObject instanceof JScrollPane))
			return;

		JScrollPane scrollPane = (JScrollPane) scrollObject;

		scrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
	}
}
