package es.imim.DisGeNET.search;

/**
	DisGeNET: A Cytoscape plugin to visualize, integrate, search and analyze gene-disease networks
	Copyright (C) 2010  Michael Rautschka, Anna Bauer-Mehren
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * This class is part of the search field in the gui and was 
 * inspired by the tutorial that can be found here:
 * http://www.javafaq.nu/java-bookpage-20-4.html 
 */

import java.awt.Graphics;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class AnimatedLabel extends JLabel implements Runnable {

	/**
	 * the animated label does not really show up, still important because some implementation of runnable class is 
	 * needed for the search
	 */
	private static final long serialVersionUID = 4530264853949599644L;
	protected Icon[] m_icons;
	protected int m_index = 0;
	protected boolean m_isRunning;

	public AnimatedLabel(String pngName, int numPng) {
		m_icons = new Icon[numPng];

		pngName = "anilabel/ajax-loader-00";

		for (int k = 1; k < numPng; k++)
			m_icons[k - 1] = new ImageIcon(pngName + k + ".png");

		setIcon(m_icons[0]);
		Thread tr = new Thread(this);
		tr.setPriority(Thread.MAX_PRIORITY);
		tr.start();
	}

	public void setRunning(boolean isRunning) {
		m_isRunning = isRunning;
	}

	public boolean getRunning() {
		return m_isRunning;
	}

	public void run() {

		while (true) {
			if (m_isRunning) {
				m_index++;

				if (m_index >= m_icons.length)
					m_index = 0;

				setIcon(m_icons[m_index]);

				Graphics g = getGraphics();

				m_icons[m_index].paintIcon(this, g, 0, 0);
			} else {
				if (m_index > 0) {
					m_index = 0;

					setIcon(m_icons[0]);
				}
			}

			try {
				Thread.sleep(100);
			} catch (Exception ex) {
			}
		}
	}
}
