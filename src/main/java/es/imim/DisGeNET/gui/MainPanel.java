package es.imim.DisGeNET.gui;

/**
	DisGeNET: A Cytoscape plugin to visualize, integrate, search and analyze gene-disease networks
	Copyright (C) 2010  Michael Rautschka, Anna Bauer-Mehren

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.event.*;
import javax.swing.*;
import javax.swing.event.PopupMenuEvent;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.InsetsUIResource;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import javax.swing.plaf.basic.BasicTabbedPaneUI;

import org.cytoscape.model.CyNetwork;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskIterator;
import org.cytoscape.work.TaskMonitor;
import org.jdesktop.swingx.plaf.MultiThumbSliderUI;

import es.imim.DisGeNET.internal.CyActivator;
import es.imim.DisGeNET.internal.StartDisGeNETMenuAction;
import es.imim.DisGeNET.network.*;
import es.imim.DisGeNET.network.colorMapping.ColorAction;
import es.imim.DisGeNET.network.colorMapping.RemoveColorAction;
import es.imim.DisGeNET.search.MemComboBox;
import es.imim.DisGeNET.search.SearchField;
import es.imim.DisGeNET.validation.NetworkValidation;


/**
 * This class generates the main gui panel with the options and buttons
 * that call the functions to create networks, colour nodes,...
 * @author michael
 *
 */

public class MainPanel extends JPanel implements
PropertyChangeListener {

	private static final long serialVersionUID = 1L;
	private StartDisGeNETMenuAction menu;

	/**
	 * Creates new MainPanel
	 * @param menu the menu from it was called.
	 * @throws SQLException throws a high level sql exception
	 * @throws ClassNotFoundException throws a high level class not found exception.
	 */
	public MainPanel(StartDisGeNETMenuAction menu) throws ClassNotFoundException,
	SQLException {
		initComponents();
		this.menu = menu;
	}

	private void initComponents() throws ClassNotFoundException, SQLException {
		
		//JAVI
		/*
		logger.info("Cytoscape version:\t"
				+ CytoscapeInit.getProperties().getProperty(
						"cytoscape.version.number"));
		 */
		MainPane = new JTabbedPane();
				
		geneDisTabPane = new JPanel();
		geneDis_SourceLabel = new JLabel();
		geneDis_SourceComboBox = new JComboBox<String>();
		geneDisDis_SearchLabel = new JLabel();
		geneDisGen_SearchLabel = new JLabel();
		
		geneDis_scoreHighLabel = new JLabel();
		geneDis_scoreLowLabel = new JLabel();
		geneDis_eiHighLabel = new JLabel();
		geneDis_eiLowLabel = new JLabel();

		geneDis_scoreLowTextField = new JTextField(2);
		geneDis_scoreHighTextField = new JTextField(2);
		geneDis_eiLowTextField = new JTextField(2);
		geneDis_eiHighTextField = new JTextField(2);
		
		variantDisTabPane = new JPanel();
		variantDis_SourceLabel = new JLabel();
		variantDis_SourceComboBox = new JComboBox<>();
		variantDisDis_SearchLabel = new JLabel();
		variantDisVar_SearchLabel = new JLabel();
		variantDisGen_SearchLabel = new JLabel();
		varGenCheckboxLabel = new JLabel();
		
		variantDis_scoreHighLabel = new JLabel();
		variantDis_scoreLowLabel = new JLabel();
		variantDis_eiHighLabel = new JLabel();
		variantDis_eiLowLabel = new JLabel();
		
		variantDis_scoreLowTextField = new JTextField(2);
		variantDis_scoreHighTextField = new JTextField(2);
		variantDis_eiHighTextField = new JTextField(2);
		variantDis_eiLowTextField = new JTextField(2);
		
		geneDis_AssoComboBox = new JComboBox<>();
		geneDis_AssoLabel = new JLabel();
		geneDis_DisClassComboBox = new JComboBox<>();
		geneDis_ClassLabel = new JLabel();
		geneDis_ElComboBox = new JComboBox<>();
		geneDis_ElLabel = new JLabel();
		variantDis_AssoComboBox = new JComboBox<>();
		variantDis_AssoLabel = new JLabel();
		variantDis_DisClassComboBox = new JComboBox<>();
		variantDis_DisClassLabel = new JLabel();
		variantDis_ElComboBox = new JComboBox<>();
		variantDis_ElLabel = new JLabel();
		disProj_TabPane = new JPanel();
		disProj_SourceLabel = new JLabel();
		disProj_SourceComboBox = new JComboBox<>();
		disProj_SearchLabel = new JLabel();
		disProj_Label = new JLabel();
		disProj_DisClassComboBox = new JComboBox<>();
		geneProj_TabPane = new JPanel();
		geneProj_SourceLabel = new JLabel();
		geneProj_SourceComboBox = new JComboBox<>();
		geneProj_SearchLabel = new JLabel();
		geneProj_Label = new JLabel();
		geneProj_DisClassComboBox = new JComboBox<>();
		buttonColorPanel = new JPanel();
		buttonCreatePanel = new JPanel();
		createNetButton = new JButton();
		colorNodesButton = new JButton();
		removeColourButton = new JButton();
		exitButton = new JButton();
		genDisCheckbox = new JCheckBox();
		disCheckbox = new JCheckBox();
		genCheckbox = new JCheckBox();
		genDisSearchPanel = new JPanel();
		varDisSearchPanel = new JPanel();
		variantDisSearchPanel = new JPanel();
		genSearchPanel = new JPanel();
		disSearchPanel = new JPanel();
		varSearchPanel = new JPanel();
		
		params = new GuiParameters<>(); 
		
		// create search fields
		SearchField geneDisGen_SearchField_tmp = new SearchField();
		geneDisGen_SearchField = geneDisGen_SearchField_tmp.getSearchBox();
		geneDisGen_SearchField.setName("geneDisGen_SearchField");
		JTextField genDisGenEditor = (JTextField) geneDisGen_SearchField.getEditor().getEditorComponent();
		genDisGenEditor.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				params.setGenSearchText(geneDisGen_SearchField.getEditor().getItem().toString());
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				params.setGenSearchText(geneDisGen_SearchField.getEditor().getItem().toString());
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				params.setGenSearchText(geneDisGen_SearchField.getEditor().getItem().toString());
			}
		});
		
		SearchField geneDisDis_SearchField_tmp = new SearchField();
		geneDisDis_SearchField = geneDisDis_SearchField_tmp.getSearchBox();
		geneDisDis_SearchField.setName("geneDisDis_SearchField"); // NOI18N
		JTextField genDisDisEditor = (JTextField) geneDisDis_SearchField.getEditor().getEditorComponent();
		genDisDisEditor.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				params.setDisSearchText(geneDisDis_SearchField.getEditor().getItem().toString());
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				params.setDisSearchText(geneDisDis_SearchField.getEditor().getItem().toString());
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				params.setDisSearchText(geneDisDis_SearchField.getEditor().getItem().toString());
			}
		});
		
		SearchField variantDisVar_SearchField_tmp = new SearchField();
		variantDisVar_SearchField = variantDisVar_SearchField_tmp.getSearchBox();
		variantDisVar_SearchField.setName("variantDisVar_SearchField");
		JTextField varDisVarEditor = (JTextField) variantDisVar_SearchField.getEditor().getEditorComponent();
		varDisVarEditor.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				params.setVarSearchText(variantDisVar_SearchField.getEditor().getItem().toString());
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				params.setVarSearchText(variantDisVar_SearchField.getEditor().getItem().toString());
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				params.setVarSearchText(variantDisVar_SearchField.getEditor().getItem().toString());
			}
		});
		
		SearchField variantDisDis_SearchField_tmp = new SearchField();
		variantDisDis_SearchField = variantDisDis_SearchField_tmp.getSearchBox();
		variantDisDis_SearchField.setName("variantDisDis_SearchField");
		JTextField varDisDisEditor = (JTextField) variantDisDis_SearchField.getEditor().getEditorComponent();
		varDisDisEditor.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				params.setDisSearchText(variantDisDis_SearchField.getEditor().getItem().toString());
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				params.setDisSearchText(variantDisDis_SearchField.getEditor().getItem().toString());
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				params.setDisSearchText(variantDisDis_SearchField.getEditor().getItem().toString());
			}
		});
		
		SearchField variantDisGen_SearchField_tmp = new SearchField();
		variantDisGen_SearchField = variantDisGen_SearchField_tmp.getSearchBox();
		variantDisGen_SearchField.setName("variantDisGen_SearchField");
		JTextField varDisGenEditor = (JTextField) variantDisGen_SearchField.getEditor().getEditorComponent();
		varDisGenEditor.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				params.setGenSearchText(variantDisGen_SearchField.getEditor().getItem().toString());
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				params.setGenSearchText(variantDisGen_SearchField.getEditor().getItem().toString());
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				params.setGenSearchText(variantDisGen_SearchField.getEditor().getItem().toString());
			}
		});
		
		SearchField disProj_SearchField_tmp = new SearchField();
		disProj_SearchField = disProj_SearchField_tmp.getSearchBox();
		disProj_SearchField.setName("disProj_SearchField");
		JTextField disProjEditor = (JTextField) disProj_SearchField.getEditor().getEditorComponent();
		disProjEditor.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				params.setDisSearchText(disProj_SearchField.getEditor().getItem().toString());
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				params.setDisSearchText(disProj_SearchField.getEditor().getItem().toString());
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				params.setDisSearchText(disProj_SearchField.getEditor().getItem().toString());
			}
		});
		SearchField geneProj_SearchField_tmp = new SearchField();
		geneProj_SearchField = geneProj_SearchField_tmp.getSearchBox();
		geneProj_SearchField.setName("geneProj_SearchField"); 
		JTextField geneProjEditor = (JTextField) geneProj_SearchField.getEditor().getEditorComponent();
		geneProjEditor.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				params.setGenSearchText(geneProj_SearchField.getEditor().getItem().toString());
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				params.setGenSearchText(geneProj_SearchField.getEditor().getItem().toString());
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				params.setGenSearchText(geneProj_SearchField.getEditor().getItem().toString());
			}
		});
		
		MainPane.setName("MainPane");
		MainPane.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
			    updateParams();
			}
		});

		geneDisTabPane.setName("GeneDisTabPane"); 
		geneDis_SourceLabel.setText(GuiProps.GENE_DIS_SOURCE_LABEL); 
		geneDis_SourceLabel.setName("geneDis_SourceLabel"); 
		geneDis_SourceComboBox.setModel(new DefaultComboBoxModel<String>(GuiProps.SOURCE_OPTIONS));
		geneDis_SourceComboBox.setName("geneDis_SourceComboBox"); 
		geneDis_SourceComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				geneDis_SourceComboBoxActionPerformed(evt);
			}
		});

		geneDis_scoreHighLabel.setText(GuiProps.GENE_DIS_UP_SCORE_LABEL);
		geneDis_scoreLowLabel.setName("geneDis_scoreLowLabel");
		geneDis_scoreHighLabel.setName("geneDis_scoreUpLabel");
		geneDis_scoreLowLabel.setToolTipText("Filter to a given score");
		geneDis_scoreHighLabel.setToolTipText("Filter to a given score");

		geneDis_scoreHighTextField.setName("geneDis_scoreUpTextField");
		geneDis_scoreHighTextField.setText("1.0");
		params.setHighScore("1.0");
		geneDis_scoreHighTextField.getDocument().addDocumentListener(
				new DocumentListener(){
					public void changedUpdate(DocumentEvent documentEvent){
						params.setActiveTab(getActiveTab().getName());
						params.setHighScore(geneDis_scoreHighTextField.getText());	
					}
					public void insertUpdate(DocumentEvent documentEvent){
						params.setActiveTab(getActiveTab().getName());
						params.setHighScore(geneDis_scoreHighTextField.getText());
					}
					public void removeUpdate(DocumentEvent documentEvent){
						params.setActiveTab(getActiveTab().getName());
						params.setHighScore(geneDis_scoreHighTextField.getText());
					}
				}
		);

		geneDis_scoreLowLabel.setText(GuiProps.GENE_DIS_LOW_SCORE_LABEL);
		geneDis_scoreLowTextField.setName("geneDis_scoreLowTextField");
		geneDis_scoreLowTextField.setText("0.0");
		params.setLowScore("0.0");
		geneDis_scoreLowTextField.getDocument().addDocumentListener(
			new DocumentListener(){
				public void changedUpdate(DocumentEvent documentEvent){
					params.setActiveTab(getActiveTab().getName());
					params.setLowScore(geneDis_scoreLowTextField.getText());	
				}
				public void insertUpdate(DocumentEvent documentEvent){
					params.setActiveTab(getActiveTab().getName());
					params.setLowScore(geneDis_scoreLowTextField.getText());
				}
				public void removeUpdate(DocumentEvent documentEvent){
					params.setActiveTab(getActiveTab().getName());
					params.setLowScore(geneDis_scoreLowTextField.getText());
				}
			}
		);

		geneDis_eiLowLabel.setText(GuiProps.EI_LOW_LABEL);
		geneDis_eiHighLabel.setText(GuiProps.EI_HIGH_LABEL);
		geneDis_eiLowLabel.setName("geneDis_eiLowLabel");
		geneDis_eiHighLabel.setName("geneDis_eiHighLabel");
		geneDis_eiLowLabel.setToolTipText("Filter to a given evidence index");
		geneDis_eiHighLabel.setToolTipText("Filter to a given evidence index");

		geneDis_eiHighTextField.setName("geneDis_eiUpTextField");
		geneDis_eiHighTextField.setText("1.0");
		params.setHighEi("1.0");
		geneDis_eiHighTextField.getDocument().addDocumentListener(
				new DocumentListener(){
					public void changedUpdate(DocumentEvent documentEvent){
						params.setActiveTab(getActiveTab().getName());
						params.setHighEi(geneDis_eiHighTextField.getText());	
					}
					public void insertUpdate(DocumentEvent documentEvent){
						params.setActiveTab(getActiveTab().getName());
						params.setHighEi(geneDis_eiHighTextField.getText());
					}
					public void removeUpdate(DocumentEvent documentEvent){
						params.setActiveTab(getActiveTab().getName());
						params.setHighEi(geneDis_eiHighTextField.getText());
					}
				}
		);
		
		geneDis_eiLowTextField.setName("geneDis_eiLowTextField");
		geneDis_eiLowTextField.setText("0.0");
		params.setLowEi("0.0");
		geneDis_eiLowTextField.getDocument().addDocumentListener(
			new DocumentListener(){
				public void changedUpdate(DocumentEvent documentEvent){
					params.setActiveTab(getActiveTab().getName());
					params.setLowEi(geneDis_eiLowTextField.getText());	
				}
				public void insertUpdate(DocumentEvent documentEvent){
					params.setActiveTab(getActiveTab().getName());
					params.setLowEi(geneDis_eiLowTextField.getText());
				}
				public void removeUpdate(DocumentEvent documentEvent){
					params.setActiveTab(getActiveTab().getName());
					params.setLowEi(geneDis_eiLowTextField.getText());
				}
			}
		);
		
		geneDisDis_SearchLabel.setText(GuiProps.GENE_DIS_SEARCH_DIS_LABEL); 
		geneDisDis_SearchLabel.setName("geneDis_SearchLabel");
		geneDisDis_SearchLabel.setToolTipText("Search diseases by identifier or name");

		geneDisGen_SearchLabel.setText(GuiProps.GENE_DIS_SEARCH_GENE_LABEL); 
		geneDisGen_SearchLabel.setName("geneDis_SearchLabel");
		geneDisGen_SearchLabel.setToolTipText("Search genes by identifier or name");
		
		GroupLayout genDisSearchPanelLayout = new GroupLayout(genDisSearchPanel);
		genDisSearchPanel.setLayout(genDisSearchPanelLayout);
		genDisSearchPanelLayout
		.setHorizontalGroup(
				genDisSearchPanelLayout
				.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(
						genDisSearchPanelLayout
						.createSequentialGroup()
						.addGroup(
								genDisSearchPanelLayout
								.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(
										genDisSearchPanelLayout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(geneDis_scoreLowLabel,GroupLayout.PREFERRED_SIZE,70,GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
										.addComponent(geneDis_scoreLowTextField)
										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
										.addComponent(geneDis_scoreHighLabel,GroupLayout.PREFERRED_SIZE,25,GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
										.addComponent(geneDis_scoreHighTextField)
								)
								.addGroup(
										genDisSearchPanelLayout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(geneDis_eiLowLabel,GroupLayout.PREFERRED_SIZE,70,GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
										.addComponent(geneDis_eiLowTextField)
										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
										.addComponent(geneDis_eiHighLabel,GroupLayout.PREFERRED_SIZE,25,GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
										.addComponent(geneDis_eiHighTextField)
								)
								.addGroup(
										genDisSearchPanelLayout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(geneDisDis_SearchLabel,GroupLayout.PREFERRED_SIZE,120,GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
										)
								.addGroup(
										genDisSearchPanelLayout
										.createSequentialGroup()
										.addGap(12,12,12)
										.addComponent(geneDisDis_SearchField,GroupLayout.PREFERRED_SIZE,188,GroupLayout.PREFERRED_SIZE)
										)
								.addGroup(
										genDisSearchPanelLayout
										.createSequentialGroup()
										.addGap(12,12,12)
										.addComponent(geneDisGen_SearchField,GroupLayout.PREFERRED_SIZE,188,GroupLayout.PREFERRED_SIZE)
										)
								.addGroup(
										genDisSearchPanelLayout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(geneDisGen_SearchLabel,GroupLayout.PREFERRED_SIZE,120,GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
										)
								)																																																				.addContainerGap()));
		genDisSearchPanelLayout
		.setVerticalGroup(
				genDisSearchPanelLayout
				.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(
						genDisSearchPanelLayout
						.createSequentialGroup()
						.addGap(3,3,3)
						.addGroup(
								genDisSearchPanelLayout
								.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(geneDis_scoreLowLabel)
								.addComponent(geneDis_scoreLowTextField)
								.addComponent(geneDis_scoreHighLabel)
								.addComponent(geneDis_scoreHighTextField)
								
								)
						.addGroup(
								genDisSearchPanelLayout
								.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(geneDis_eiLowLabel)
								.addComponent(geneDis_eiLowTextField)
								.addComponent(geneDis_eiHighLabel)
								.addComponent(geneDis_eiHighTextField)
								)
						.addGroup(
								genDisSearchPanelLayout
								.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(geneDisDis_SearchLabel)
						)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(geneDisDis_SearchField,GroupLayout.PREFERRED_SIZE,GroupLayout.DEFAULT_SIZE,GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
						.addGroup(
								genDisSearchPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(geneDisGen_SearchLabel)
								)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(geneDisGen_SearchField,GroupLayout.PREFERRED_SIZE,GroupLayout.DEFAULT_SIZE,GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
						)
				);

		GroupLayout genSearchPanelLayout = new GroupLayout(genSearchPanel);
		genSearchPanel.setLayout(genSearchPanelLayout);
		genSearchPanelLayout
		.setHorizontalGroup(
				genSearchPanelLayout
				.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(
						genSearchPanelLayout
						.createSequentialGroup()
						.addGroup(
								genSearchPanelLayout
								.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(
										genSearchPanelLayout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(geneProj_SearchLabel,GroupLayout.PREFERRED_SIZE,100,GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
										)
								.addGroup(
										genSearchPanelLayout
										.createSequentialGroup()
										.addGap(12,12,12)
										.addComponent(geneProj_SearchField,GroupLayout.PREFERRED_SIZE,188,GroupLayout.PREFERRED_SIZE)
										)
								)
						.addContainerGap()
						)
				);
		
		genSearchPanelLayout
		.setVerticalGroup(
				genSearchPanelLayout
				.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(
						genSearchPanelLayout
						.createSequentialGroup()
						.addGap(27, 27, 27)
						.addGroup(
								genSearchPanelLayout
								.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(geneProj_SearchLabel)
								)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(geneProj_SearchField,GroupLayout.PREFERRED_SIZE,GroupLayout.DEFAULT_SIZE,GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
						)
				);

		GroupLayout disSearchPanelLayout = new GroupLayout(disSearchPanel);
		disSearchPanel.setLayout(disSearchPanelLayout);
		disSearchPanelLayout
		.setHorizontalGroup(
				disSearchPanelLayout
				.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(
						disSearchPanelLayout
						.createSequentialGroup()
						.addGroup(
								disSearchPanelLayout
								.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(
										disSearchPanelLayout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(disProj_SearchLabel,GroupLayout.PREFERRED_SIZE,100,GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
										)
								.addGroup(
										disSearchPanelLayout
										.createSequentialGroup()
										.addGap(12,12,12)
										.addComponent(disProj_SearchField,GroupLayout.PREFERRED_SIZE,188,GroupLayout.PREFERRED_SIZE)
										)
								)
						.addContainerGap()
						)
				);
		disSearchPanelLayout
		.setVerticalGroup(
				disSearchPanelLayout
				.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(
						disSearchPanelLayout
						.createSequentialGroup()
						.addGap(27, 27, 27)
						.addGroup(
								disSearchPanelLayout
								.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(disProj_SearchLabel)
								)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(disProj_SearchField,GroupLayout.PREFERRED_SIZE,GroupLayout.DEFAULT_SIZE,GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
						)
				);
		
		geneDis_AssoLabel.setText(GuiProps.GENE_DIS_ASSO_LABEL); 
		geneDis_AssoLabel.setName("geneDis_AssoLabel"); 
		
		geneDis_AssoComboBox.setModel(new DefaultComboBoxModel<Object>(
				GuiProps.ASSOCIATION_TYPE_OPTIONS));
		geneDis_AssoComboBox.setName("geneDis_AssoComboBox"); 
		geneDis_AssoComboBox.addPopupMenuListener(new PopupMenuListener() {
			public void popupMenuCanceled(PopupMenuEvent evt) {
			}

			public void popupMenuWillBecomeInvisible(PopupMenuEvent evt) {
			}

			public void popupMenuWillBecomeVisible(PopupMenuEvent evt) {
				geneDis_AssoComboBoxPopupMenuWillBecomeVisible(evt);
			}
		});
		geneDis_AssoComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				geneDis_AssoComboBoxActionPerformed(evt);
			}
		});
		
		geneDis_ClassLabel.setText(GuiProps.GENE_DIS_CLASS_LABEL); 
		geneDis_ClassLabel.setName("geneDis_ClassLabel"); 
		
		geneDis_DisClassComboBox.setModel(new DefaultComboBoxModel<String>(GuiProps.DISEASE_CLASS_OPTIONS));
		geneDis_DisClassComboBox.setName("geneDis_DisClassComboBox"); 
		geneDis_DisClassComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				geneDis_disClassComboBoxActionPerformed(evt);
			}
		});
		
		geneDis_ElLabel.setText(GuiProps.EL_LABEL);
		geneDis_ElLabel.setName("geneDis_ElLabel");
		
		geneDis_ElComboBox.setModel(new DefaultComboBoxModel<String>(GuiProps.EL_OPTIONS));
		geneDis_ElComboBox.setName("geneDis_ElComboBox"); 
		geneDis_ElComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				geneDis_ElComboBoxActionPerformed(evt);
			}
		});

		GroupLayout GeneDisTabPaneLayout = new GroupLayout(geneDisTabPane);
		geneDisTabPane.setLayout(GeneDisTabPaneLayout);
		GeneDisTabPaneLayout.setHorizontalGroup(
				GeneDisTabPaneLayout
				.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(
						GeneDisTabPaneLayout
						.createSequentialGroup()
						.addContainerGap()
						.addGroup(
								GeneDisTabPaneLayout
								.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(geneDis_ElComboBox,GroupLayout.PREFERRED_SIZE,216,GroupLayout.PREFERRED_SIZE)
								.addComponent(geneDis_AssoComboBox,GroupLayout.PREFERRED_SIZE,216,GroupLayout.PREFERRED_SIZE)
								.addComponent(geneDis_DisClassComboBox,GroupLayout.PREFERRED_SIZE,216,GroupLayout.PREFERRED_SIZE)
								.addComponent(geneDis_SourceComboBox,GroupLayout.PREFERRED_SIZE,216,GroupLayout.PREFERRED_SIZE)
								.addGroup(
										GeneDisTabPaneLayout
										.createSequentialGroup()
										.addComponent(geneDis_SourceLabel)
										.addContainerGap(131,Short.MAX_VALUE)
										)
								.addGroup(
										GeneDisTabPaneLayout
										.createSequentialGroup()
										.addComponent(geneDis_AssoLabel,GroupLayout.DEFAULT_SIZE,GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
										.addGap(274, 274,274))
								.addGroup(
										GeneDisTabPaneLayout
										.createSequentialGroup()
										.addComponent(geneDis_ClassLabel)
										.addContainerGap(67,Short.MAX_VALUE)
										)
								.addGroup(
										GeneDisTabPaneLayout
										.createSequentialGroup()
										.addComponent(geneDis_ElLabel)
										.addContainerGap(67,Short.MAX_VALUE)
										)
								.addGroup(
										GeneDisTabPaneLayout
										.createSequentialGroup()
										.addComponent(genDisSearchPanel,GroupLayout.PREFERRED_SIZE,216,GroupLayout.PREFERRED_SIZE)
										.addContainerGap(113,Short.MAX_VALUE)
										)
								)
						)
				);
		GeneDisTabPaneLayout
		.setVerticalGroup(
				GeneDisTabPaneLayout
				.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(
						GeneDisTabPaneLayout
						.createSequentialGroup()
						.addContainerGap()
						.addComponent(geneDis_SourceLabel)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(geneDis_SourceComboBox,GroupLayout.PREFERRED_SIZE,GroupLayout.DEFAULT_SIZE,GroupLayout.PREFERRED_SIZE)
						.addGap(18, 18, 18)
						.addComponent(geneDis_AssoLabel)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(geneDis_AssoComboBox,GroupLayout.PREFERRED_SIZE,GroupLayout.DEFAULT_SIZE,GroupLayout.PREFERRED_SIZE)
						.addGap(18, 18, 18)
						.addComponent(geneDis_ClassLabel)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(geneDis_DisClassComboBox,GroupLayout.PREFERRED_SIZE,GroupLayout.DEFAULT_SIZE,GroupLayout.PREFERRED_SIZE)
						.addGap(18, 18, 18)
						.addComponent(geneDis_ElLabel)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(geneDis_ElComboBox,GroupLayout.PREFERRED_SIZE,GroupLayout.DEFAULT_SIZE,GroupLayout.PREFERRED_SIZE)
						.addGap(18, 18, 18)
						.addComponent(genDisSearchPanel)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addContainerGap()
						)
				);
		
		MainPane.addTab("<html><div style='padding:4px 0px;'><strong>"+GuiProps.GENE_DIS_TAB_TITLE+"</strong></div></html>", geneDisTabPane);

		variantDisTabPane.setName("VariantDisTabPane"); 
		variantDis_SourceLabel.setText(GuiProps.VARIANT_DIS_SOURCE_LABEL); 
		variantDis_SourceLabel.setName("variantDis_SourceLabel"); 
		variantDis_SourceComboBox.setModel(new DefaultComboBoxModel<String>(GuiProps.VARIANT_SOURCE_OPTIONS));
		variantDis_SourceComboBox.setName("variantDis_SourceComboBox"); 
		variantDis_SourceComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				variantDis_SourceComboBoxActionPerformed(evt);
			}
		});
		
		variantDis_scoreLowLabel.setText(GuiProps.VARIANT_DIS_LOW_SCORE_LABEL);
		variantDis_scoreHighLabel.setText(GuiProps.VARIANT_DIS_UP_SCORE_LABEL);
		variantDis_scoreLowLabel.setName("variantDis_scoreLowLabel");
		variantDis_scoreHighLabel.setName("variantDis_scoreUpLabel");
		variantDis_scoreLowLabel.setToolTipText("Filter to a given score");
		variantDis_scoreHighLabel.setToolTipText("Filter to a given score");

		variantDis_scoreHighTextField.setName("variantDis_scoreUpTextField");
		variantDis_scoreHighTextField.setText("1.0");
		params.setHighScore("1.0");
		variantDis_scoreHighTextField.getDocument().addDocumentListener(
				new DocumentListener()
				{
					public void changedUpdate(DocumentEvent documentEvent){
						params.setActiveTab(getActiveTab().getName());
						params.setHighScore(variantDis_scoreHighTextField.getText());	
					}
					public void insertUpdate(DocumentEvent documentEvent){
						params.setActiveTab(getActiveTab().getName());
						params.setHighScore(variantDis_scoreHighTextField.getText());
					}
					public void removeUpdate(DocumentEvent documentEvent){
						params.setActiveTab(getActiveTab().getName());
						params.setHighScore(variantDis_scoreHighTextField.getText());
					}
				});

		variantDis_scoreLowTextField.setName("variantDis_scoreLowTextField");
		variantDis_scoreLowTextField.setText("0.0");
		variantDis_scoreLowTextField.getDocument().addDocumentListener(
				new DocumentListener(){
					public void changedUpdate(DocumentEvent documentEvent){
						params.setActiveTab(getActiveTab().getName());
						params.setLowScore(variantDis_scoreLowTextField.getText());
					}
					public void insertUpdate(DocumentEvent documentEvent){
						params.setActiveTab(getActiveTab().getName());
						params.setLowScore(variantDis_scoreLowTextField.getText());
					}
					public void removeUpdate(DocumentEvent documentEvent){
						params.setActiveTab(getActiveTab().getName());
						params.setLowScore(variantDis_scoreLowTextField.getText());
					}
			});
		
		variantDis_eiLowLabel.setText(GuiProps.EI_LOW_LABEL);
		variantDis_eiHighLabel.setText(GuiProps.EI_HIGH_LABEL);
		variantDis_eiLowLabel.setName("variantDis_eiLowLabel");
		variantDis_eiHighLabel.setName("variantDis_eiHighLabel");
		variantDis_eiLowLabel.setToolTipText("Filter to a given evidence index");
		variantDis_eiHighLabel.setToolTipText("Filter to a given evidence index");

		variantDis_eiHighTextField.setName("variantDis_eiHighTextField");
		variantDis_eiHighTextField.setText("1.0");
		params.setHighEi("1.0");
		variantDis_eiHighTextField.getDocument().addDocumentListener(
				new DocumentListener()
				{
					public void changedUpdate(DocumentEvent documentEvent){
						params.setActiveTab(getActiveTab().getName());
						params.setHighEi(variantDis_eiHighTextField.getText());	
					}
					public void insertUpdate(DocumentEvent documentEvent){
						params.setActiveTab(getActiveTab().getName());
						params.setHighEi(variantDis_eiHighTextField.getText());
					}
					public void removeUpdate(DocumentEvent documentEvent){
						params.setActiveTab(getActiveTab().getName());
						params.setHighEi(variantDis_eiHighTextField.getText());
					}
				});

		variantDis_eiLowTextField.setName("variantDis_eiLowTextField");
		variantDis_eiLowTextField.setText("0.0");
		params.setLowEi("0.0");
		variantDis_eiLowTextField.getDocument().addDocumentListener(
				new DocumentListener(){
					public void changedUpdate(DocumentEvent documentEvent){
						params.setActiveTab(getActiveTab().getName());
						params.setLowEi(variantDis_eiLowTextField.getText());
					}
					public void insertUpdate(DocumentEvent documentEvent){
						params.setActiveTab(getActiveTab().getName());
						params.setLowEi(variantDis_eiLowTextField.getText());
					}
					public void removeUpdate(DocumentEvent documentEvent){
						params.setActiveTab(getActiveTab().getName());
						params.setLowEi(variantDis_eiLowTextField.getText());
					}
			});
		
		variantDisDis_SearchLabel.setText(GuiProps.VARIANT_DIS_SEARCH_DIS_LABEL); 
		variantDisDis_SearchLabel.setName("variantDisDis_SearchLabel");
		variantDisDis_SearchLabel.setToolTipText("Search diseases by identifier or name");

		variantDisVar_SearchLabel.setText(GuiProps.VARIANT_DIS_SEARCH_VAR_LABEL); 
		variantDisVar_SearchLabel.setName("variantDisVar_SearchLabel");
		variantDisVar_SearchLabel.setToolTipText("Search variants by identifier or name");
		
		variantDisGen_SearchLabel.setText(GuiProps.VARIANT_DIS_SEARCH_GEN_LABEL);
		variantDisGen_SearchLabel.setName("variantDisGen_SearchLabel");
		variantDisGen_SearchLabel.setToolTipText("Search genes associated to variants by identifier or name");
		
		varGenCheckbox = new JCheckBox();
		varGenCheckbox.setName("varGenCheckbox");
		varGenCheckbox.setText(GuiProps.VARIANT_VAR_GEN_CHECKBOX_LABEL);
		varGenCheckbox.setToolTipText(GuiProps.VARIANT_VAR_GEN_CHECKBOX_TOOLTIP);
		varGenCheckbox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				varGenCheckbox_ActionPerformed(evt);
			}
		});
			
		GroupLayout variantDisSearchPanelLayout = new GroupLayout(variantDisSearchPanel);
		variantDisSearchPanel.setLayout(variantDisSearchPanelLayout);
		variantDisSearchPanelLayout
		.setHorizontalGroup(
				variantDisSearchPanelLayout
				.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(
						variantDisSearchPanelLayout
						.createSequentialGroup()
						.addGroup(
								variantDisSearchPanelLayout
								.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(
										variantDisSearchPanelLayout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(variantDis_scoreLowLabel,GroupLayout.PREFERRED_SIZE,70,GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
										.addComponent(variantDis_scoreLowTextField)
										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
										.addComponent(variantDis_scoreHighLabel,GroupLayout.PREFERRED_SIZE,25,GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
										.addComponent(variantDis_scoreHighTextField)
										)
								.addGroup(
										variantDisSearchPanelLayout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(variantDis_eiLowLabel,GroupLayout.PREFERRED_SIZE,70,GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
										.addComponent(variantDis_eiLowTextField)
										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
										.addComponent(variantDis_eiHighLabel,GroupLayout.PREFERRED_SIZE,25,GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
										.addComponent(variantDis_eiHighTextField)
								)
								.addGroup(
										variantDisSearchPanelLayout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(variantDisDis_SearchLabel,GroupLayout.PREFERRED_SIZE,120,GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
										)
								.addGroup(
										variantDisSearchPanelLayout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(variantDisDis_SearchField,GroupLayout.PREFERRED_SIZE,188,GroupLayout.PREFERRED_SIZE)
										)
								.addGroup(
										variantDisSearchPanelLayout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(variantDisVar_SearchLabel,GroupLayout.PREFERRED_SIZE,120,GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
										)
								.addGroup(
										variantDisSearchPanelLayout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(variantDisVar_SearchField,GroupLayout.PREFERRED_SIZE,188,GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
										)
								.addGroup(
										variantDisSearchPanelLayout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(variantDisGen_SearchLabel,GroupLayout.PREFERRED_SIZE,120,GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
										)
								.addGroup(
										variantDisSearchPanelLayout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(variantDisGen_SearchField,GroupLayout.PREFERRED_SIZE,188,GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
										)
								.addGroup(
										variantDisSearchPanelLayout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(varGenCheckbox,GroupLayout.PREFERRED_SIZE,188,GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
										)
								)
						.addContainerGap()
						)
				);
		
		variantDisSearchPanelLayout
		.setVerticalGroup(
				variantDisSearchPanelLayout
				.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(
						variantDisSearchPanelLayout
						.createSequentialGroup()
						.addGap(3,3,3)
						.addGroup(
								variantDisSearchPanelLayout
								.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(variantDis_scoreLowLabel)
								.addComponent(variantDis_scoreLowTextField)
								.addComponent(variantDis_scoreHighLabel)
								.addComponent(variantDis_scoreHighTextField)
								)
						.addGroup(
								variantDisSearchPanelLayout
								.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(variantDis_eiLowLabel)
								.addComponent(variantDis_eiLowTextField)
								.addComponent(variantDis_eiHighLabel)
								.addComponent(variantDis_eiHighTextField)
								)
						.addGroup(
								variantDisSearchPanelLayout
								.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(variantDisDis_SearchLabel)
								)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(variantDisDis_SearchField,GroupLayout.PREFERRED_SIZE,GroupLayout.DEFAULT_SIZE,GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
						.addGroup(
								variantDisSearchPanelLayout
								.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(variantDisVar_SearchLabel)
								)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(variantDisVar_SearchField,GroupLayout.PREFERRED_SIZE,GroupLayout.DEFAULT_SIZE,GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
						.addGroup(
								variantDisSearchPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(variantDisGen_SearchLabel)
								)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(variantDisGen_SearchField,GroupLayout.PREFERRED_SIZE,GroupLayout.DEFAULT_SIZE,GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
						.addGroup(
								variantDisSearchPanelLayout
								.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(varGenCheckbox)
								)
						.addContainerGap(GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
						)
				);
																																																				
		variantDis_AssoComboBox.setModel(new DefaultComboBoxModel<Object>(GuiProps.VARIANT_ASSOCIATION_TYPE_OPTIONS));
		variantDis_AssoComboBox.setName("variantDis_AssoComboBox"); 
		variantDis_AssoComboBox.addPopupMenuListener(
				new PopupMenuListener() {
					public void popupMenuCanceled(PopupMenuEvent evt) {
					}
					public void popupMenuWillBecomeInvisible(PopupMenuEvent evt) {
					}
					public void popupMenuWillBecomeVisible(PopupMenuEvent evt) {
						geneDis_AssoComboBoxPopupMenuWillBecomeVisible(evt);
					}
				});
		variantDis_AssoComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				variantDis_AssoComboBoxActionPerformed(evt);
			}
		});

		GroupLayout variantSearchPanelLayout = new GroupLayout(varSearchPanel);
		varSearchPanel.setLayout(variantSearchPanelLayout);
		variantSearchPanelLayout.setHorizontalGroup(
				variantSearchPanelLayout
				.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(
						variantSearchPanelLayout
						.createSequentialGroup()
						.addGroup(
								variantSearchPanelLayout
								.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(
										variantSearchPanelLayout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(variantDisVar_SearchLabel,GroupLayout.PREFERRED_SIZE,100,GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
										)
								.addGroup(
										variantSearchPanelLayout
										.createSequentialGroup()
										.addGap(12,12,12)
										.addComponent(variantDisVar_SearchLabel,GroupLayout.PREFERRED_SIZE,188,GroupLayout.PREFERRED_SIZE)
										)
								)
						.addContainerGap()
						)
				);
		
		variantSearchPanelLayout.setVerticalGroup(
				variantSearchPanelLayout
				.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(
						variantSearchPanelLayout
						.createSequentialGroup()
						.addGap(27, 27, 27)
						.addGroup(
								variantSearchPanelLayout
								.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(variantDisVar_SearchLabel)
								)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(variantDisVar_SearchField,GroupLayout.PREFERRED_SIZE,GroupLayout.DEFAULT_SIZE,GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
						)
				);
		
		GroupLayout varDisSearchPanelLayout = new GroupLayout(varDisSearchPanel);
		varDisSearchPanel.setLayout(varDisSearchPanelLayout);
		varDisSearchPanelLayout.setHorizontalGroup(
				varDisSearchPanelLayout
				.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(
						varDisSearchPanelLayout
						.createSequentialGroup()
						.addGroup(
								varDisSearchPanelLayout
								.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(varDisSearchPanelLayout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(variantDisDis_SearchLabel,GroupLayout.PREFERRED_SIZE,100,GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
										)
								.addGroup(
										varDisSearchPanelLayout
										.createSequentialGroup()
										.addGap(12,12,12)
										.addComponent(variantDisDis_SearchField,GroupLayout.PREFERRED_SIZE,188,GroupLayout.PREFERRED_SIZE)
										)
								)
						.addContainerGap()
						)
				);
		varDisSearchPanelLayout.setVerticalGroup(
				varDisSearchPanelLayout
				.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(
						varDisSearchPanelLayout
						.createSequentialGroup()
						.addGap(27, 27, 27)
						.addGroup(
								varDisSearchPanelLayout
								.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(variantDisDis_SearchLabel)
								)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(variantDisDis_SearchField,GroupLayout.PREFERRED_SIZE,GroupLayout.DEFAULT_SIZE,GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
						)
				);
		
		variantDis_AssoLabel.setText(GuiProps.VARIANT_DIS_ASSO_LABEL); 
		variantDis_AssoLabel.setName("variantDis_AssoLabel"); 

		variantDis_DisClassLabel.setText(GuiProps.VARIANT_DIS_CLASS_LABEL); 
		variantDis_DisClassLabel.setName("variantDis_ClassLabel");
		
//		variantDis_ElLabel.setText(GuiProps.EL_LABEL);
//		variantDis_ElLabel.setName("variantDis_ElLabel");

		variantDis_DisClassComboBox.setModel(new DefaultComboBoxModel<String>(GuiProps.DISEASE_CLASS_OPTIONS));
		variantDis_DisClassComboBox.setName("variantDis_DisClassComboBox"); 
		variantDis_DisClassComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				variantDis_disClassComboBoxActionPerformed(evt);
			}
		});
		
//		variantDis_ElComboBox.setModel(new DefaultComboBoxModel<String>(GuiProps.EL_OPTIONS));
//		variantDis_ElComboBox.setName("variantDis_ElComboBox"); 
//		variantDis_ElComboBox.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent evt) {
//				variantDis_ElComboBoxActionPerformed(evt);
//			}
//		});
		
		GroupLayout variantDisTabPaneLayout = new GroupLayout(variantDisTabPane);
		variantDisTabPane.setLayout(variantDisTabPaneLayout);
		variantDisTabPaneLayout.setHorizontalGroup(
				variantDisTabPaneLayout
				.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(
						variantDisTabPaneLayout
						.createSequentialGroup()
						.addContainerGap()
						.addGroup(
								variantDisTabPaneLayout
								.createParallelGroup(GroupLayout.Alignment.LEADING)
//								.addComponent(variantDis_ElComboBox,GroupLayout.PREFERRED_SIZE,216,GroupLayout.PREFERRED_SIZE)
								.addComponent(variantDis_AssoComboBox,GroupLayout.PREFERRED_SIZE,216,GroupLayout.PREFERRED_SIZE)
								.addComponent(variantDis_DisClassComboBox,GroupLayout.PREFERRED_SIZE,216,GroupLayout.PREFERRED_SIZE)
								.addComponent(variantDis_SourceComboBox,GroupLayout.PREFERRED_SIZE,216,GroupLayout.PREFERRED_SIZE)
								.addGroup(
										variantDisTabPaneLayout
										.createSequentialGroup()
										.addComponent(variantDis_SourceLabel)
										.addContainerGap(131,Short.MAX_VALUE))
								.addGroup(
										variantDisTabPaneLayout
										.createSequentialGroup()
										.addComponent(variantDis_AssoLabel,GroupLayout.DEFAULT_SIZE,GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
										.addGap(274, 274,274)
										)
								.addGroup(
										variantDisTabPaneLayout
										.createSequentialGroup()
										.addComponent(variantDis_DisClassLabel)
										.addContainerGap(67,Short.MAX_VALUE)
										)
//								.addGroup(
//										variantDisTabPaneLayout
//										.createSequentialGroup()
//										.addComponent(variantDis_ElLabel)
//										.addContainerGap(67,Short.MAX_VALUE)
//										)
								.addGroup(
										variantDisTabPaneLayout
										.createSequentialGroup()
										.addComponent(variantDisSearchPanel,GroupLayout.PREFERRED_SIZE,216,GroupLayout.PREFERRED_SIZE)
										.addContainerGap(113,Short.MAX_VALUE)
										)
								)
						)
				);
		
		variantDisTabPaneLayout.setVerticalGroup(
				variantDisTabPaneLayout
				.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(
						variantDisTabPaneLayout
						.createSequentialGroup()
						.addContainerGap()
						.addComponent(variantDis_SourceLabel)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(variantDis_SourceComboBox,GroupLayout.PREFERRED_SIZE,GroupLayout.DEFAULT_SIZE,GroupLayout.PREFERRED_SIZE)
						.addGap(18, 18, 18)
						.addComponent(variantDis_AssoLabel)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(variantDis_AssoComboBox,GroupLayout.PREFERRED_SIZE,GroupLayout.DEFAULT_SIZE,GroupLayout.PREFERRED_SIZE)
						.addGap(18, 18, 18)
						.addComponent(variantDis_DisClassLabel)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(variantDis_DisClassComboBox,GroupLayout.PREFERRED_SIZE,GroupLayout.DEFAULT_SIZE,GroupLayout.PREFERRED_SIZE)
						.addGap(18, 18, 18)
//						.addComponent(variantDis_ElLabel)
//						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
//						.addComponent(variantDis_ElComboBox,GroupLayout.PREFERRED_SIZE,GroupLayout.DEFAULT_SIZE,GroupLayout.PREFERRED_SIZE)
//						.addGap(18, 18, 18)
						.addComponent(variantDisSearchPanel)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addContainerGap()
						)
				);
		
		MainPane.addTab("<html><div style='padding:4px 0px;'>"+GuiProps.VARIANT_DIS_TAB_TITLE+"</div></html>", variantDisTabPane);
				
		disProj_TabPane.setName("disProj_TabPane"); 

		disProj_SourceLabel.setText(GuiProps.DIS_PROJ_SOURCE_LABEL); 
		disProj_SourceLabel.setName("disProj_SourceLabel"); 

		disProj_SourceComboBox.setModel(new DefaultComboBoxModel<Object>(GuiProps.SOURCE_OPTIONS));
		disProj_SourceComboBox.setName("disProj_SourceComboBox"); 
		disProj_SourceComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				disProj_SourceComboBoxActionPerformed(evt);
			}
		});

		disProj_SearchLabel.setText(GuiProps.DIS_PROJ_SEARCH_LABEL); 
		disProj_SearchLabel.setToolTipText("Search disease by identifier or name");
		disProj_SearchLabel.setName("disProj_SearchLabel"); 
		disProj_SearchField.setName("disProj_SearchField"); 
		disProj_Label.setText(GuiProps.DIS_PROJ_LABEL); 
		disProj_Label.setName("disProj_Label"); 

		disProj_DisClassComboBox.setModel(new DefaultComboBoxModel<Object>(GuiProps.DISEASE_CLASS_OPTIONS));
		disProj_DisClassComboBox.setName("disProj_DisClassComboBox"); 
		disProj_DisClassComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				disProj_DisClassComboBoxActionPerformed(evt);
			}
		});
		
		GroupLayout disProj_TabPaneLayout = new GroupLayout(disProj_TabPane);
		disProj_TabPane.setLayout(disProj_TabPaneLayout);
		disProj_TabPaneLayout.setHorizontalGroup(
				disProj_TabPaneLayout
				.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(
						disProj_TabPaneLayout
						.createSequentialGroup()
						.addContainerGap()
						.addGroup(
								disProj_TabPaneLayout
								.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(disProj_Label)
								.addComponent(disProj_DisClassComboBox,GroupLayout.Alignment.LEADING,GroupLayout.PREFERRED_SIZE,216,GroupLayout.PREFERRED_SIZE)
								.addComponent(disProj_SourceLabel,GroupLayout.Alignment.LEADING)
								.addComponent(disProj_SourceComboBox,GroupLayout.Alignment.LEADING,GroupLayout.PREFERRED_SIZE,216,GroupLayout.PREFERRED_SIZE)
								.addComponent(disSearchPanel,GroupLayout.Alignment.TRAILING,GroupLayout.PREFERRED_SIZE,216,GroupLayout.PREFERRED_SIZE)
								)
						.addGap(11, 11, 11)
						)
				);
		
		disProj_TabPaneLayout.setVerticalGroup(
				disProj_TabPaneLayout
				.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(
						disProj_TabPaneLayout
						.createSequentialGroup()
						.addContainerGap()
						.addComponent(disProj_SourceLabel)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(disProj_SourceComboBox,GroupLayout.PREFERRED_SIZE,GroupLayout.DEFAULT_SIZE,GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(disProj_Label)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(disProj_DisClassComboBox,GroupLayout.PREFERRED_SIZE,GroupLayout.DEFAULT_SIZE,GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,76, Short.MAX_VALUE)
						.addComponent(disSearchPanel,GroupLayout.PREFERRED_SIZE,GroupLayout.DEFAULT_SIZE,GroupLayout.PREFERRED_SIZE)
						.addGap(28, 28, 28)
						)
				);

		//MainPane.addTab("<html><div style='padding-bottom:4px;'>"+GuiProps.DIS_PROJ_TAB_TITLE+"</div></html>", disProj_TabPane);

		geneProj_TabPane.setName("geneProj_TabPane"); 

		geneProj_SourceLabel.setText(GuiProps.GENE_PROJ_SOURCE_LABEL); 
		geneProj_SourceLabel.setName("geneProj_SourceLabel"); 

		geneProj_SourceComboBox.setModel(new DefaultComboBoxModel<String>(GuiProps.SOURCE_OPTIONS));
		geneProj_SourceComboBox.setName("geneProj_SourceComboBox"); 
		geneProj_SourceComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				geneProj_SourceComboBoxActionPerformed(evt);
			}
		});

		geneProj_SearchLabel.setText(GuiProps.GENE_PROJ_SEARCH_LABEL); 
		geneProj_SearchLabel.setName("geneProj_SearchLabel"); 
		geneProj_SearchLabel.setToolTipText("Search genes by identifier or gene symbol");

		geneProj_Label.setText(GuiProps.GENE_PROJ_LABEL); 
		geneProj_Label.setName("geneProj_Label"); 
		geneProj_Label.setAlignmentY(SwingConstants.CENTER);

		geneProj_DisClassComboBox.setModel(new DefaultComboBoxModel<String>(GuiProps.DISEASE_CLASS_OPTIONS));
		geneProj_DisClassComboBox.setName("geneProj_DisClassComboBox"); 

		GroupLayout geneProj_TabPaneLayout = new GroupLayout(geneProj_TabPane);
		geneProj_TabPane.setLayout(geneProj_TabPaneLayout);

		geneProj_TabPaneLayout.setHorizontalGroup(
				geneProj_TabPaneLayout
				.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(
						geneProj_TabPaneLayout
						.createSequentialGroup()
						.addContainerGap()
						.addGroup(
								geneProj_TabPaneLayout
								.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(geneProj_Label)
								.addComponent(geneProj_DisClassComboBox,GroupLayout.Alignment.LEADING,GroupLayout.PREFERRED_SIZE,216,GroupLayout.PREFERRED_SIZE)
								.addComponent(geneProj_SourceLabel,GroupLayout.Alignment.LEADING)
								.addComponent(geneProj_SourceComboBox,GroupLayout.Alignment.LEADING,GroupLayout.PREFERRED_SIZE,216,GroupLayout.PREFERRED_SIZE)
								.addComponent(genSearchPanel,GroupLayout.Alignment.TRAILING,GroupLayout.PREFERRED_SIZE,216,GroupLayout.PREFERRED_SIZE)
								)
						.addGap(11, 11, 11)
						)
				);
		geneProj_TabPaneLayout.setVerticalGroup(
				geneProj_TabPaneLayout
				.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(
						geneProj_TabPaneLayout
						.createSequentialGroup()
						.addContainerGap()
						.addComponent(geneProj_SourceLabel)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(geneProj_SourceComboBox,GroupLayout.PREFERRED_SIZE,GroupLayout.DEFAULT_SIZE,GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(geneProj_Label)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(geneProj_DisClassComboBox,GroupLayout.PREFERRED_SIZE,GroupLayout.DEFAULT_SIZE,GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,76, Short.MAX_VALUE)
						.addComponent(genSearchPanel,GroupLayout.PREFERRED_SIZE,GroupLayout.DEFAULT_SIZE,GroupLayout.PREFERRED_SIZE)
						.addGap(28, 28, 28)
						)
				);

		//MainPane.addTab("<html><div style='padding-bottom:4px;'>"+GuiProps.GENE_PROJ_TAB_TITLE+"</div></html>", geneProj_TabPane);

		buttonColorPanel.setBorder(BorderFactory.createEtchedBorder());
		buttonColorPanel.setName("buttonPanel"); 


		createNetButton.setText(GuiProps.CREATE_NET_BUTTON_TEXT); 
		createNetButton.setToolTipText(GuiProps.CREATE_NET_BUTTON_TOOLTIP);
		createNetButton.setName("createNetButton"); 
		createNetButton.addActionListener(new NetAction(params, MainPane));

		// button to add disease class multi-colouring
		colorNodesButton.setText(GuiProps.COLOR_NODES_BUTTON_TEXT); 
		colorNodesButton.setToolTipText(GuiProps.COLOR_NODES_BUTTON_TOOLTIP);
		colorNodesButton.setName("colorNodesButton"); 
		colorNodesButton.addActionListener(new ColorAction());

		// button to remove disease class multi-colouring
		removeColourButton = new JButton(GuiProps.REMOVE_COLOR_BUTTON_TEXT);
		removeColourButton.setToolTipText(GuiProps.REMOVE_COLOR_BUTTON_TOOLTIP);
		removeColourButton.setName("removeColorNodesButton");
		removeColourButton.addActionListener(new RemoveColorAction());

		buttonColorPanel.add(colorNodesButton);
		buttonColorPanel.add(removeColourButton);
		
		buttonCreatePanel.add(createNetButton);
		
		GridLayout colorButtonPanelLayout = new java.awt.GridLayout(2, 1); 
		
		buttonColorPanel.setLayout(colorButtonPanelLayout);
		
		GridLayout createButtonPanelLayout = new java.awt.GridLayout(1,1);
		buttonCreatePanel.setLayout(createButtonPanelLayout);
		
		exitButton.setText(GuiProps.EXIT_BUTTON_TEXT); 
		exitButton.setName("exitButton");
		exitButton.setToolTipText(GuiProps.EXIT_BUTTON_TEXT);
		exitButton.addActionListener(new MainPanel.CloseAction(this));
	
		GroupLayout layout = new GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(
				layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
						.addGroup(
								layout.createParallelGroup(GroupLayout.Alignment.CENTER)
								.addGroup(
										layout.createParallelGroup()
										.addGap(100,100,100)
										.addComponent(exitButton,GroupLayout.PREFERRED_SIZE,100,GroupLayout.PREFERRED_SIZE)
										)
								.addGroup(
										layout.createParallelGroup()
										.addComponent(buttonCreatePanel,GroupLayout.PREFERRED_SIZE,247,Short.MAX_VALUE)
										)
								.addGroup(
										layout.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												layout.createParallelGroup(GroupLayout.Alignment.LEADING,false)
												.addComponent(buttonColorPanel,0,247,Short.MAX_VALUE)
												.addComponent(MainPane,GroupLayout.PREFERRED_SIZE,247,GroupLayout.PREFERRED_SIZE)
												)
										)
								).addContainerGap(GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)));
		layout.setVerticalGroup(
				layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
						.addComponent(MainPane,GroupLayout.PREFERRED_SIZE,570,GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(buttonColorPanel,GroupLayout.PREFERRED_SIZE,GroupLayout.DEFAULT_SIZE,GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(buttonCreatePanel,GroupLayout.PREFERRED_SIZE,35,GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(exitButton).addContainerGap(25, Short.MAX_VALUE)
						)
				);

		// do some mac specific stuff for gui
		if (System.getProperty("os.name").toLowerCase().startsWith("mac os x")) {
			// -> run on a mac
			// set smaller sizes for buttons
			exitButton.putClientProperty("JComponent.sizeVariant", "mini");

			UIDefaults defaults = UIManager.getDefaults();
			defaults.put("TabbedPane.useSmallLayout", Boolean.TRUE);
			defaults.put("TabbedPane.darkShadow", Color.black);
		}

		MainPane.setForegroundAt(0, Color.black);
		MainPane.setForegroundAt(1, Color.black);
		//MainPane.setForegroundAt(2, Color.black);

		MainPane.setBorder(BorderFactory.createEtchedBorder());
		MainPane.setUI(new GradientUI());

		// set renderer for comboboxes
		geneDis_SourceComboBox.setRenderer(new ToolTipComboBoxRenderer());
		geneDis_AssoComboBox.setRenderer(new ToolTipComboBoxRenderer());
		geneDis_DisClassComboBox.setRenderer(new ToolTipComboBoxRenderer());

		//geneProj_SourceComboBox.setRenderer(new ToolTipComboBoxRenderer());
		//geneProj_DisClassComboBox.setRenderer(new ToolTipComboBoxRenderer());

		//disProj_SourceComboBox.setRenderer(new ToolTipComboBoxRenderer());
		//disProj_DisClassComboBox.setRenderer(new ToolTipComboBoxRenderer());

		MainPane.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                JTabbedPane source = (JTabbedPane) changeEvent.getSource();
                String selectedTabName = source.getSelectedComponent().getName();
                for(int i = 0; i < source.getTabCount(); i++) {
                    Component c = source.getComponentAt(i);
                    if(c.getName().equals(selectedTabName)){
                        source.setTitleAt(i,"<html><div style='padding:4px 0px'><strong>"+getTitleForTabName(c.getName())+"</strong></div></html>");
                    }else{
                        source.setTitleAt(i,"<html><div style='padding:4px 0px'>"+getTitleForTabName(c.getName())+"</div></html>");
                    }
                }
            }

            private String getTitleForTabName(String tabName){
                switch (tabName){
                    case "GeneDisTabPane":
                        return GuiProps.GENE_DIS_TAB_TITLE;
                    case "VariantDisTabPane":
                        return GuiProps.VARIANT_DIS_TAB_TITLE;
                }
                return "DEFAULT TITLE";
            }
        });

		FillSearchBoxesTask searchBoxTask = new FillSearchBoxesTask();
		TaskIterator taskIterator = CyActivator.getInstance().getGlobalTaskIterator();
		taskIterator.append(searchBoxTask);
		CyActivator.getInstance().getDialogTaskManagerService().execute(taskIterator);


	}

	protected void geneDis_AssoComboBoxPopupMenuWillBecomeVisible(
			PopupMenuEvent evt) {
	}
	
	protected void variantDis_AssoComboBoxPopupMenuWillBecomeVisible(
			PopupMenuEvent evt) {
		
	}

	public GuiParameters<Object, Object> getParams() {
		return params;
	}

	public void setParams(GuiParameters<Object, Object> params) {
		this.params = params;
	}

	/*
	 * Combobox Renderer 
	 */
	class ToolTipComboBoxRenderer extends BasicComboBoxRenderer {

		private static final long serialVersionUID = 6796327624807352734L;

		Color blueGV = new Color(0,102,255); // genetic variation colour
		Color grayM = new Color(153,153,153);// marker color
		Color dark_redCM = new Color(153, 51, 51);// Causal Mutation colour
		//Color dark_redCM = new Color(0, 0, 0);// Causal Mutation colour
		Color purpleT = new Color(204,0,204);// therapeutic colour
		Color light_green = new Color(155, 255, 100);
		Color dark_green = new Color(0, 180, 0);
		Color medium_green = new Color(45, 180, 160);

		public Component getListCellRendererComponent(JList list, Object value,
				int index, boolean isSelected, boolean cellHasFocus) {

			HashMap<String, Color> colMap = new HashMap<String, Color>();
			// marker colour
			colMap.put(GuiProps.ASSOCIATION_TYPE_OPTIONS[1], grayM);
			// therapeutic colour
			colMap.put(GuiProps.ASSOCIATION_TYPE_OPTIONS[2], purpleT);
			// genetic variation colour
			colMap.put(GuiProps.ASSOCIATION_TYPE_OPTIONS[3], blueGV);
			// Causal Mutation colour
			colMap.put(GuiProps.ASSOCIATION_TYPE_OPTIONS[3], dark_redCM);			
			// regulatory modification colour
			colMap.put(GuiProps.ASSOCIATION_TYPE_OPTIONS[4], dark_green);
			// altered expression colour
			colMap.put(GuiProps.ASSOCIATION_TYPE_OPTIONS[5], medium_green);
			// methylation phosphorylation colour
			//colMap.put(GuiProps.ASSOCIATION_TYPE_OPTIONS[6], light_green);
						
			if (isSelected) {
				setBackground(list.getSelectionBackground());
				setForeground(list.getSelectionForeground());
			} else {
				setBackground(list.getBackground());
				setForeground(list.getForeground());
			}
			setFont(list.getFont());
			setText((value == null) ? "" : value.toString());
			return this;
		}
	}

	/*
	 * actionlisteners for source combo boxes
	 */
	private void geneDis_SourceComboBoxActionPerformed(
			java.awt.event.ActionEvent evt) {
		params.setActiveTab(getActiveTab().getName());
		params.setSource(geneDis_SourceComboBox.getSelectedItem()
				.toString());
		// update search box with new parameters
		try {
			fillSearchBox(geneDisDis_SearchField, "genDisDis");
			fillSearchBox(geneDisGen_SearchField, "genDisGen");
		} catch (SQLException e) {
			
		}
		
	}
	
	private void variantDis_SourceComboBoxActionPerformed(
			java.awt.event.ActionEvent evt) {
		params.setActiveTab(getActiveTab().getName());
		params.setSource(variantDis_SourceComboBox.getSelectedItem()
				.toString());
		// update search box with new parameters
		try {
			fillSearchBox(variantDisDis_SearchField, "varDisDis");
			fillSearchBox(variantDisVar_SearchField, "varDisVar");
			fillSearchBox(variantDisGen_SearchField, "varDisGen");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void varGenCheckbox_ActionPerformed (
			java.awt.event.ActionEvent evt) {
			params.setVarGeneDisNetwork(varGenCheckbox.isSelected());
			CyNetwork cyNetwork = CyActivator.getInstance().getApplicationManagerService().getCurrentNetwork();
			if(NetworkValidation.isVariantNet(cyNetwork)) {
				AbstractTask showGenesTask = new ShowGenesVariantTask(varGenCheckbox.isSelected(),cyNetwork);
				CyActivator.getInstance().getDialogTaskManagerService().execute(new TaskIterator(showGenesTask));
			}
	}

	//JAVI
	/*
	private void geneDis_scoreTextFieldActionPerformed(	java.awt.event.ActionEvent evt) {
		params.setActiveTab(getActiveTab().getName());
		params.setGenDisScore(this.geneDis_scoreTextField.getText());
		System.out.println("Score: "+this.geneDis_scoreTextField.getText());
	}*/
	private void geneProj_SourceComboBoxActionPerformed(
			java.awt.event.ActionEvent evt) {
		params.setActiveTab(getActiveTab().getName());
		params.setSource(geneProj_SourceComboBox.getSelectedItem()
				.toString());

		// update search box with new parameters
		
		try {
			fillSearchBox(geneProj_SearchField, "gen");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void disProj_SourceComboBoxActionPerformed(
			java.awt.event.ActionEvent evt) {
		params.setActiveTab(getActiveTab().getName());
		params.setSource(disProj_SourceComboBox.getSelectedItem()
				.toString());
		// update search box with new parameters
		try {
			fillSearchBox(disProj_SearchField, "dis");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * actionlisteners for disease class comboboxes
	 */
	protected void geneDis_disClassComboBoxActionPerformed(ActionEvent evt) {
		params.setActiveTab(getActiveTab().getName());
		params.setDiseaseClass(geneDis_DisClassComboBox.getSelectedItem()
				.toString());
		// update search box with new parameters
		try {
			fillSearchBox(geneDisGen_SearchField, "genDisGen");
			fillSearchBox(geneDisDis_SearchField, "genDisDis");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void variantDis_disClassComboBoxActionPerformed(ActionEvent evt) {
		params.setActiveTab(getActiveTab().getName());
		params.setDiseaseClass(variantDis_DisClassComboBox.getSelectedItem()
				.toString());
		// update search box with new parameters
		try {
			fillSearchBox(variantDisVar_SearchField, "varDisVar");
			fillSearchBox(variantDisDis_SearchField, "varDisDis");
			fillSearchBox(variantDisGen_SearchField, "varDisGen");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void disProj_DisClassComboBoxActionPerformed(ActionEvent evt) {
		params.setActiveTab(getActiveTab().getName());
		params.setDiseaseClass(disProj_DisClassComboBox.getSelectedItem()
				.toString());
		// update search box with new parameters
		try {
			fillSearchBox(disProj_SearchField, "dis");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void geneProj_DisClassComboBoxActionPerformed(ActionEvent evt) {
		params.setActiveTab(getActiveTab().getName());
		params.setDiseaseClass(geneProj_DisClassComboBox.getSelectedItem()
				.toString());
		// update search box with new parameters
		try {
			fillSearchBox(geneProj_SearchField, "gen");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * actionlistener for association box
	 */
	private void geneDis_AssoComboBoxActionPerformed(
			java.awt.event.ActionEvent evt) {
		params.setActiveTab(getActiveTab().getName());
		params.setAssociationType(geneDis_AssoComboBox.getSelectedItem()
				.toString());
		// update search box with new parameters
		try {
			fillSearchBox(geneDisGen_SearchField, "genDisGen");
			fillSearchBox(geneDisDis_SearchField, "genDisDis");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*
	 * actionlistener for association box
	 */
	private void variantDis_AssoComboBoxActionPerformed(
			java.awt.event.ActionEvent evt) {
		params.setActiveTab(getActiveTab().getName());
		params.setAssociationType(variantDis_AssoComboBox.getSelectedItem()
				.toString());
		// update search box with new parameters
		try {
			fillSearchBox(variantDisVar_SearchField, "varDisVar");
			fillSearchBox(variantDisDis_SearchField, "varDisDis");
			fillSearchBox(variantDisGen_SearchField, "varDisGen");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void geneDis_ElComboBoxActionPerformed(ActionEvent evt) {
		params.setActiveTab(getActiveTab().getName());
		params.setEl(geneDis_ElComboBox.getSelectedItem()
				.toString());
		// update search box with new parameters
		try {
			fillSearchBox(geneDisGen_SearchField, "genDisGen");
			fillSearchBox(geneDisDis_SearchField, "genDisDis");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void variantDis_ElComboBoxActionPerformed(ActionEvent evt) {
		params.setActiveTab(getActiveTab().getName());
		params.setEl(variantDis_ElComboBox.getSelectedItem()
				.toString());
		// update search box with new parameters
		try {
			fillSearchBox(variantDisVar_SearchField, "varDisVar");
			fillSearchBox(variantDisDis_SearchField, "varDisDis");
			fillSearchBox(variantDisGen_SearchField, "varDisGen");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// Variables declaration - do not modify
	private JPanel geneDisTabPane;
	private JPanel variantDisTabPane;
	private JPanel disProj_TabPane;
	private JPanel geneProj_TabPane;
	private JTabbedPane MainPane;
	private JPanel buttonColorPanel;
	private JPanel buttonCreatePanel;
	private JButton colorNodesButton;
	private JButton createNetButton;
	private JComboBox<Object> disProj_DisClassComboBox;
	private JLabel disProj_Label;
	private JLabel disProj_SearchLabel;
	private JComboBox<Object> disProj_SourceComboBox;
	private JLabel disProj_SourceLabel;
	private JLabel geneDis_SourceLabel;
	private JLabel variantDis_SourceLabel;
	private JButton exitButton;
	private JComboBox<Object> geneDis_AssoComboBox;
	private JComboBox<Object> variantDis_AssoComboBox;
	private JLabel geneDis_AssoLabel;
	private JLabel geneDis_ClassLabel;
	private JLabel geneDis_ElLabel;
	private JLabel variantDis_AssoLabel;
	private JLabel variantDis_DisClassLabel;
	private JLabel variantDis_ElLabel;
	private JLabel geneDisGen_SearchLabel;
	private JLabel geneDisDis_SearchLabel;
	private JLabel variantDisVar_SearchLabel;
	private JLabel variantDisDis_SearchLabel;
	private JLabel variantDisGen_SearchLabel;
	private JLabel geneDis_scoreHighLabel;
	private JLabel geneDis_scoreLowLabel;
	private JLabel variantDis_scoreHighLabel;
	private JLabel variantDis_scoreLowLabel;
	private JTextField geneDis_scoreHighTextField;
	private JTextField geneDis_scoreLowTextField;
	private JTextField variantDis_scoreHighTextField;
	private JTextField variantDis_scoreLowTextField;
	
	//EI
	private JLabel geneDis_eiHighLabel;
	private JLabel geneDis_eiLowLabel;
	private JTextField geneDis_eiHighTextField;
	private JTextField geneDis_eiLowTextField;
	
	private JLabel variantDis_eiHighLabel;
	private JLabel variantDis_eiLowLabel;
	private JTextField variantDis_eiHighTextField;
	private JTextField variantDis_eiLowTextField;
	
	private JComboBox <String> geneDis_SourceComboBox;
	private JComboBox <String> geneDis_DisClassComboBox;
	private JComboBox <String> variantDis_SourceComboBox;
	private JComboBox <String> variantDis_DisClassComboBox;
	private JComboBox <String> geneProj_DisClassComboBox;
	private JComboBox <String> geneDis_ElComboBox;
	private JComboBox <String> variantDis_ElComboBox;
	private JLabel geneProj_Label;
	private JLabel geneProj_SearchLabel;
	private JComboBox <String> geneProj_SourceComboBox;
	private JLabel geneProj_SourceLabel;
	private JCheckBox genDisCheckbox;
	private JCheckBox genCheckbox;
	private JCheckBox disCheckbox;
	private JCheckBox varGenCheckbox;
	private JLabel varGenCheckboxLabel;
	private JPanel genDisSearchPanel;
	private JPanel variantDisSearchPanel;
	private JPanel genSearchPanel;
	private JPanel disSearchPanel;
	private JPanel varSearchPanel;
	private JPanel varDisSearchPanel;
	private JButton removeColourButton;
	//private MultiThumbSliderUI eiRangeSlider;
	// search fields
	private es.imim.DisGeNET.search.MemComboBox geneDisGen_SearchField;
	private es.imim.DisGeNET.search.MemComboBox geneDisDis_SearchField;
	private es.imim.DisGeNET.search.MemComboBox variantDisVar_SearchField;
	private es.imim.DisGeNET.search.MemComboBox variantDisDis_SearchField;
	private es.imim.DisGeNET.search.MemComboBox variantDisGen_SearchField;
	private es.imim.DisGeNET.search.MemComboBox geneProj_SearchField;
	private es.imim.DisGeNET.search.MemComboBox disProj_SearchField;

	private GuiParameters<Object, Object> params;


	/**
	 * Handles the press of the Close button
	 */
	private class CloseAction extends AbstractAction {

		private static final long serialVersionUID = 5370062466656537630L;
		MainPanel mainPanel;

		CloseAction(MainPanel mainPanel) {
			this.mainPanel = mainPanel;
		}

		public void actionPerformed(ActionEvent e) {
			MainPanel.this.menu.unregister();
		}
	}

	/**
	 * function to fill list of search field which will be used for
	 * auto-complete etc.
	 * 
	 * parameters: m_locator: search box to be filled 
	 * src : source to fill from;
	 * 		can be - 'net' current network (if diseaseName and/or geneName present) -
	 * 		'gen' gene list from DB - 'dis' disease list from DB - 'genDis' both gene
	 * 		and disease list from DB
	 * 
	 */
	private void fillSearchBox(MemComboBox m_locator, String src) throws SQLException  {

		//JAVI. Uncommented to test
		
		NetworkBuilder nb = new NetworkBuilder();
		ArrayList<String> searchList = new ArrayList<String>();
				
		switch (src) {
			case "gen":
				searchList = nb.getDB().getGeneList(params);
				break;
			case "dis":
				searchList = nb.getDB().getDiseaseList(params);
				break;
			case "genDisDis":
				searchList = nb.getDB().getDiseaseList(params);
				break;
			case "genDisGen":
				searchList = nb.getDB().getGeneList(params);
				break;
			case "varDisDis":
				searchList = nb.getDB().getDiseaseListVariant(params);
				break;
			case "varDisVar":
				searchList = nb.getDB().getVariantList(params);
				break;
			case "varDisGen":
				searchList = nb.getDB().getGeneListVariant(params);
				break;
			default:
				break;
		}	
		// add Names to m_locator
		m_locator.clearList();
		java.util.Iterator<String> searchListIt = searchList.iterator();
		while (searchListIt.hasNext()) {
			m_locator.add(searchListIt.next());
		}
	}
		
	public Component getActiveTab() {
		return MainPane.getSelectedComponent();
	}
	
	public void updateParams() {
		Component tab = this.getActiveTab();
		String tabName = tab.getName();
		switch (tabName) {
		case "GeneDisTabPane":
			params.setSource(geneDis_SourceComboBox.getSelectedItem().toString());
			params.setAssociationType(geneDis_AssoComboBox.getSelectedItem().toString());
			params.setDiseaseClass(geneDis_DisClassComboBox.getSelectedItem().toString());
			params.setLowScore(geneDis_scoreLowTextField.getText());
			params.setHighScore(geneDis_scoreHighTextField.getText());
			params.setGenSearchText(geneDisGen_SearchField.getEditor().getItem().toString());
			params.setDisSearchText(geneDisDis_SearchField.getEditor().getItem().toString());
			break;
		case "VariantDisTabPane":
			params.setSource(variantDis_SourceComboBox.getSelectedItem().toString());
			params.setAssociationType(variantDis_AssoComboBox.getSelectedItem().toString());
			params.setDiseaseClass(variantDis_DisClassComboBox.getSelectedItem().toString());
			params.setLowScore(variantDis_scoreLowTextField.getText());
			params.setHighScore(variantDis_scoreHighTextField.getText());
			params.setVarSearchText(variantDisVar_SearchField.getEditor().getItem().toString());
			params.setDisSearchText(variantDisDis_SearchField.getEditor().getItem().toString());
			params.setGenSearchText(variantDisGen_SearchField.getEditor().getItem().toString());
			params.setVarGeneDisNetwork(varGenCheckbox.isSelected());
			break;
		case "disProj_TabPane":
			params.setSource(disProj_SourceComboBox.getSelectedItem().toString());
			params.setDiseaseClass(disProj_DisClassComboBox.getSelectedItem().toString());
			params.setDisSearchText(disProj_SearchField.getEditor().getItem().toString());
			break;
		case "geneProj_TabPane":
			params.setSource(geneProj_SourceComboBox.getSelectedItem().toString());
			params.setDiseaseClass(geneProj_DisClassComboBox.getSelectedItem().toString());
			params.setGenSearchText(geneProj_SearchField.getEditor().getItem().toString());
			break;
		default:
			break;
		}
		params.setActiveTab(tabName);
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
	}
	
	private class FillSearchBoxesTask extends AbstractTask {
		protected volatile boolean cancelled = false;
		
		@Override
		public void run(TaskMonitor taskMonitor) throws Exception {
			taskMonitor.setProgress(0);
			taskMonitor.setTitle("Loading DisGeNET data...");
			//taskMonitor.setStatusMessage("Loading diseaseProjection search box... ");
            //fillSearchBox(disProj_SearchField, "dis");
			taskMonitor.setProgress(0.25);
			//taskMonitor.setStatusMessage("Loading geneProjection search box... ");
			//fillSearchBox(geneProj_SearchField, "gen");
			//taskMonitor.setProgress(0.5);
			taskMonitor.setStatusMessage("Loading geneDiseaseNetwork search boxes... ");
			fillSearchBox(geneDisDis_SearchField, "genDisDis");
            taskMonitor.setProgress(0.5);
			fillSearchBox(geneDisGen_SearchField, "genDisGen");
			taskMonitor.setProgress(0.75);
			taskMonitor.setStatusMessage("Loading variantDiseaseNetwork search boxes... ");
			fillSearchBox(variantDisDis_SearchField,"varDisDis");
			fillSearchBox(variantDisVar_SearchField,"varDisVar");
			fillSearchBox(variantDisGen_SearchField,"varDisGen");
			taskMonitor.setProgress(1);
		}
		
		@Override
		public void cancel() {
			cancelled = true;
		}
		
	}
}

/***
 * Custom Tab-Pane-UI to have coloured tabs
 * 
 * code adapted from plaf.basic.BasicTabbedPaneUI* Each of these
 * classes:* plaf.metal.MetalTabbedPaneUI*
 * com.sun.java.swing.plaf.windows.WindowsTabbedPaneUI*
 * com.sun.java.swing.plaf.motif.MotifTabbedPaneUI* extend BasicTabbedPaneUI
 * which contains a methodpaintTabBackground.* So we'll just extend the class,
 * override the method and insert our* gradient paint code...
 */
class GradientUI extends BasicTabbedPaneUI {
	public void paintTabBackground(Graphics g, int tabPlacement, int tabIndex,
			int x, int y, int w, int h, boolean isSelected) {
		Graphics2D g2 = (Graphics2D) g;

		GradientPaint gradient;
		GradientPaint selected;

		Color pink = new Color(255, 0, 170);
		Color pink_selected = new Color(225, 0, 150);

		Color blue = new Color(0, 0, 255);
		Color blue_selected = new Color(0, 0, 200);
		
		Color purple = new Color(100,0,255);
		Color purple_selected = new Color(60,0,235);

		switch (tabIndex) {
		case 0:
			gradient = new GradientPaint(x, y + h / 2, pink, x + w, y + h / 2,
					blue, true);
			selected = new GradientPaint(x, y + h / 2, pink_selected, x + w, y
					+ h / 2, blue_selected, true);
			g2.setPaint(!isSelected ? gradient : selected);
			break;
		case 1:
			gradient = new GradientPaint(x, y + h / 2, pink, x + w, y + h / 2,
					purple, true);
			selected = new GradientPaint(x, y + h / 2, pink_selected, x + w, y
					+ h / 2, purple_selected, true);
			g2.setPaint(!isSelected ? gradient : selected);
			break;
		case 2:
			gradient = new GradientPaint(x, y + h / 2, pink, x + w, y + h / 2,
					pink, true);
			selected = new GradientPaint(x, y + h / 2, pink_selected, x + w, y
					+ h / 2, pink_selected, true);
			g2.setPaint(!isSelected ? gradient : selected);
			break;
		case 3:
			gradient = new GradientPaint(x, y + h / 2, blue, x + w, y + h / 2,
					blue, true);
			selected = new GradientPaint(x, y + h / 2, blue_selected, x + w, y
					+ h / 2, blue_selected, true);
			g2.setPaint(!isSelected ? gradient : selected);
			break;
		default:
			gradient = new GradientPaint(x, y + h / 2, Color.gray, x + w, y + h
					/ 2, Color.gray, true);
			selected = new GradientPaint(x, y + h / 2, Color.darkGray, x + w, y
					+ h / 2, Color.DARK_GRAY, true);
			g2.setPaint(!isSelected ? gradient : selected);
		}

		switch (tabPlacement) {
		case LEFT:
			g.fillRect(x + 1, y + 1, w - 1, h - 3);
			break;
		case RIGHT:
			g.fillRect(x, y + 1, w - 2, h - 3);
			break;
		case BOTTOM:
			g.fillRect(x + 1, y, w - 3, h - 1);
			break;
		case TOP:
		default:
			g.fillRect(x + 1, y + 1, w - 3, h - 1);
		}
	}
	
	
}