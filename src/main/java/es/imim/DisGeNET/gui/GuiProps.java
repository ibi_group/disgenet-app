	package es.imim.DisGeNET.gui;

import java.util.Arrays;

/**
	DisGeNET: A Cytoscape plugin to visualize, integrate, search and analyze gene-disease networks
	Copyright (C) 2010  Michael Rautschka, Anna Bauer-Mehren
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * this class simply holds all strings used in gui panel
 */
public class GuiProps {
	
	public static final String GENE_DIS_SOURCE_LABEL = "Select Source";
	public static final String GENE_DIS_SEARCH_GENE_LABEL = "Search Gene";
	public static final String GENE_DIS_SEARCH_DIS_LABEL = "Search Disease";
	public static final String GENE_DIS_LOW_SCORE_LABEL = "Score from";
	public static final String GENE_DIS_UP_SCORE_LABEL = "to";
	public static final String EI_HIGH_LABEL = "to";
	public static final String EI_LOW_LABEL = "EI  from";
	public static final String GENE_DIS_SEARCH_TEXT = "";
	public static final String GENE_DIS_ASSO_LABEL = "Select Association Type";
	public static final String GENE_DIS_CLASS_LABEL = "Select Disease Class";
	public static final String GENE_DIS_TAB_TITLE = "Gene Disease Network";
	
	
	public static final String VARIANT_DIS_SOURCE_LABEL = "Select Source";
	public static final String VARIANT_DIS_SEARCH_VAR_LABEL = "Search Variant";
	public static final String VARIANT_DIS_SEARCH_DIS_LABEL = "Search Disease";
	public static final String VARIANT_DIS_SEARCH_GEN_LABEL = "Search Gene";
	public static final String VARIANT_VAR_GEN_CHECKBOX_LABEL = "Show associated genes";
	public static final String VARIANT_VAR_GEN_CHECKBOX_TOOLTIP = "Shows the gene nodes associated to the variants";
	public static final String VARIANT_DIS_LOW_SCORE_LABEL = "Score from";
	public static final String VARIANT_DIS_UP_SCORE_LABEL = "to";
	public static final String VARIANT_DIS_SEARCH_TEXT = "";
	public static final String VARIANT_DIS_ASSO_LABEL = "Select Association Type";
	public static final String VARIANT_DIS_CLASS_LABEL = "Select Disease Class";
	public static final String VARIANT_DIS_TAB_TITLE = "Variant Disease Network";
	
	public static final String DIS_PROJ_SOURCE_LABEL = "Select Source";
	public static final String DIS_PROJ_SEARCH_LABEL = "Search Disease";
	//public static final String DIS_PROJ_SEARCH_LABEL = "Search";
	public static final String DIS_PROJ_SEARCH_FIELD = "";
	public static final String DIS_PROJ_LABEL = "Select Disease Class";
	public static final String DIS_PROJ_TAB_TITLE = "Disease Projections";
	
	public static final String GENE_PROJ_SOURCE_LABEL = "Select Source";
	public static final String GENE_PROJ_SEARCH_LABEL = "Search Gene";
	//public static final String GENE_PROJ_SEARCH_LABEL = "Search";
	public static final String GENE_PROJ_SEARCH_FIELD = "";
	public static final String GENE_PROJ_LABEL = "Select Disease Class";
	public static final String GENE_PROJ_TAB_TITLE = "Gene Projections";
	
	public static final String COLOR_NODES_BUTTON_TEXT = "Colour nodes with disease class";
	public static final String COLOR_NODES_BUTTON_TOOLTIP = "Colours the network according to disease class";
	
	public static final String REMOVE_COLOR_BUTTON_TEXT = "Remove disease class colours";
	public static final String REMOVE_COLOR_BUTTON_TOOLTIP = "Removes the disease class colours and restores the default DisGeNET style";
	
	public static final String CREATE_NET_BUTTON_TEXT = "Create Network";
	public static final String CREATE_NET_BUTTON_TOOLTIP = "Creates network with selected parameters";
	
	public static final String EXIT_BUTTON_TEXT = "Exit Plugin";
	
	//public static final String[] ASSOCIATION_TYPE_OPTIONS = new String[] { "Any", "Marker", "Therapeutic", "Genetic variation", "Regulatory modification","     └ Altered expression", "     └ Methylation/phosphorylation" };
	//public static final String[] ASSOCIATION_TYPE_OPTIONS = new String[] { "Any", "Therapeutic", "Biomarker", "└ Chromosomal Rearrengement",  "Fusion Gene",   "└PostTranslational Modification", "     └Altered Expression", "     └Genetic Variation", "Causal Mutation", "Modifying Mutation", "Somatic Causal Mutation", "Germline Causal Mutation", "Germline Modifying Mutation", "Somatic Modifying Mutation", "Susceptibility Mutation"};

	public static final String[] ASSOCIATION_TYPE_OPTIONS = new String[] { "Any", "Therapeutic", "Biomarker", "Chromosomal Rearrengement",  "Fusion Gene",   "PostTranslational Modification", "Altered Expression", "Genetic Variation", "Causal Mutation", "Modifying Mutation", "Somatic Causal Mutation", "Germline Causal Mutation", "Germline Modifying Mutation", "Somatic Modifying Mutation", "Susceptibility Mutation"};
	public static final String[] DISEASE_CLASS_OPTIONS = new String[] { "Any", "Infections", "Neoplasms", "Musculoskeletal Diseases", "Digestive System Diseases", "Stomatognathic Diseases", "Respiratory Tract Diseases", "Otorhinolaryngologic Diseases", "Nervous System Diseases", "Eye Diseases", "Male Urogenital Diseases", "Female Urogenital Diseases and Pregnancy Complications", "Cardiovascular Diseases", "Hemic and Lymphatic Diseases", "Congenital, Hereditary, and Neonatal Diseases and Abnormalities", "Skin and Connective Tissue Diseases", "Nutritional and Metabolic Diseases", "Endocrine System Diseases", "Immune System Diseases", "Animal Diseases ","Disorders of Environmental Origin", "Pathological Conditions, Signs and Symptoms", "Wounds and Injuries","Behavior and Behavior Mechanisms", "Mental Disorders", "Unclassified" };
	//public static final String[] DISEASE_CLASS_OPTIONS = new String[] { "Any", "Bacterial Infections and Mycoses", "Virus Diseases", "Parasitic Diseases", "Neoplasms", "Musculoskeletal Diseases", "Digestive System Diseases", "Stomatognathic Diseases", "Respiratory Tract Diseases", "Otorhinolaryngologic Diseases", "Nervous System Diseases", "Eye Diseases", "Male Urogenital Diseases", "Female Urogenital Diseases and Pregnancy Complications", "Cardiovascular Diseases", "Hemic and Lymphatic Diseases", "Congenital, Hereditary, and Neonatal Diseases and Abnormalities", "Skin and Connective Tissue Diseases", "Nutritional and Metabolic Diseases", "Endocrine System Diseases", "Immune System Diseases", "Disorders of Environmental Origin", "Animal Diseases", "Pathological Conditions, Signs and Symptoms", "Behavior and Behavior Mechanisms", "Mental Disorders", "Unclassified" };
	public static final String[] SOURCE_OPTIONS = new String[] { "CURATED", "INFERRED","ANIMAL_MODELS","ALL", "BEFREE", "CGI","CLINGEN","CLINVAR","CTD_human","CTD_mouse","CTD_rat","GENOMICS_ENGLAND","GWASCAT","GWASDB","HPO","LHGDN","MGD","ORPHANET","PSYGENET","RGD","UNIPROT"};
	public static final String[] VARIANT_SOURCE_OPTIONS = new String[] {"CURATED","ALL","BEFREE", "CLINVAR","GWASCAT","GWASDB","UNIPROT"};
	public static final String[] VARIANT_ASSOCIATION_TYPE_OPTIONS = new String[] {"Any","Genetic Variation", "Causal Mutation", "Susceptibility Mutation"};
	
	public static final String EL_LABEL = "Evidence Level";
	public static final String[] EL_OPTIONS = new String [] {"Any","moderate","strong","disputed","limited","definitive","no reported evidence"};

}


