package es.imim.DisGeNET.gui;

/**
	DisGeNET: A Cytoscape plugin to visualize, integrate, search and analyze gene-disease networks
	Copyright (C) 2010  Michael Rautschka, Anna Bauer-Mehren
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
 
/**
 * this class handles all the settings from the gui panel and is
 * used to hand them over to the functions that create the networks
 * @author michael
 */

public class GuiParameters<PostTranslational_Modification, Altered_Expression> {

	private String source;
	private String associationType;
	private String diseaseClass;
	private String lowScore;
	private String highScore;
	private String lowEi;
	private String highEi;
	private String el;
	private String genSearchText;
	private String disSearchText;
	private String varSearchText;
	private String networkName;
	private String activeTab;
	private boolean netBuildInterrupted;
	private boolean emptyNet;
	private boolean varGeneDisNetwork;

	public GuiParameters() {
	}

	public String getActiveTab() {
		return activeTab;
	}
	public void setActiveTab(String activeTab) {
		this.activeTab = activeTab;
	}
	public String getNetworkName() {
		return networkName;
	}
	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}
	public String getGenSearchText() {
		return genSearchText;
	}
	public void setGenSearchText(String genSearchText) {
		this.genSearchText = genSearchText;
	}	
	
	public String getDisSearchText() {
		return disSearchText;
	}
	public void setDisSearchText(String disSearchText) {
		this.disSearchText = disSearchText;
	}
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getAssociationType() {
		return associationType;
	}

	public void setAssociationType(String associationType) {
		this.associationType = associationType;
	}

	public String getDiseaseClass() {
		return diseaseClass;
	}

	public void setDiseaseClass(String diseaseClass) {
		this.diseaseClass = diseaseClass;
	}

	public String getLowScore() {
		return lowScore;
	}

	public void setLowScore(String lowScore) {
		this.lowScore = lowScore;
	}

	public String getHighScore() {
		return highScore;
	}
	
	public void setHighScore(String highScore) {
		this.highScore = highScore;
	}
	
	public String getLowEi() {
		return lowEi;
	}

	public void setLowEi(String lowEi) {
		this.lowEi = lowEi;
	}

	public String getHighEi() {
		return highEi;
	}

	public void setHighEi(String hightEi) {
		this.highEi = hightEi;
	}

	public String getEl() {
		return el;
	}

	public void setEl(String el) {
		this.el = el;
	}
	
	public String getVarSearchText() {
		return varSearchText;
	}

	public void setVarSearchText(String varSearchText) {
		this.varSearchText = varSearchText;
	}
	
	public void setNetBuildInterrupted(boolean netBuildInterrupted) {
		this.netBuildInterrupted = netBuildInterrupted;
	}

	public boolean isNetBuildInterrupted() {
		return netBuildInterrupted;
	}

	public void setEmptyNet(boolean emptyNet) {
		this.emptyNet = emptyNet;
	}

	public boolean isEmptyNet() {
		return emptyNet;
	}

	public boolean isVarGeneDisNetwork() {
		return varGeneDisNetwork;
	}

	public void setVarGeneDisNetwork(boolean varGeneDisNetwork) {
		this.varGeneDisNetwork = varGeneDisNetwork;
	}

}
