package es.imim.DisGeNET.gui;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JPanel;

import org.cytoscape.application.swing.CytoPanelComponent;
import org.cytoscape.application.swing.CytoPanelName;

public class DisColorLegendPanel extends JPanel implements CytoPanelComponent {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 543904314490529366L;

	public DisColorLegendPanel(BorderLayout borderLayout) {
		super(borderLayout);
	}
	
	@Override
	public Component getComponent() {
		return this;
	}

	@Override
	public CytoPanelName getCytoPanelName() {
		return CytoPanelName.EAST;
	}

	@Override
	public String getTitle() {
		return "Disease Class Legend";
	}

	@Override
	public Icon getIcon() {
		return null;
	}

}
