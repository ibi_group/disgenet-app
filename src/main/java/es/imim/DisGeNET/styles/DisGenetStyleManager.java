/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.imim.DisGeNET.styles;

import java.io.InputStream;
import java.util.Set;
import org.cytoscape.session.events.SessionLoadedEvent;
import org.cytoscape.session.events.SessionLoadedListener;
import org.cytoscape.task.read.LoadVizmapFileTaskFactory;
import org.cytoscape.view.vizmap.VisualMappingManager;
import org.cytoscape.view.vizmap.VisualStyle;
import org.cytoscape.view.vizmap.VisualStyleFactory;

/**
 * Class that loads the DisGeNET style if the current session of the user doesn't contains it.
 * @author jsauch
 */
public class DisGenetStyleManager implements SessionLoadedListener {

    private static DisGenetStyleManager uniqueInstance;

    private LoadVizmapFileTaskFactory loadVizmapFileTaskFactory;
    private VisualMappingManager vmm;
    private VisualStyleFactory vsf;
    private static final String DisGeNETStyle = "DisGeNETstyle";
    private String[] styles = {DisGeNETStyle}; 

    public static synchronized DisGenetStyleManager getInstance(LoadVizmapFileTaskFactory loadVizmapFileTaskFactory, VisualMappingManager vmm,
                                                                VisualStyleFactory vsf){
            if (uniqueInstance == null){
                    uniqueInstance = new DisGenetStyleManager(loadVizmapFileTaskFactory, vmm, vsf);
            }
            return uniqueInstance;
    }

    /** Constructor. */
    private DisGenetStyleManager(LoadVizmapFileTaskFactory loadVizmapFileTaskFactory, VisualMappingManager vmm,VisualStyleFactory vsf){
            this.loadVizmapFileTaskFactory = loadVizmapFileTaskFactory;
            this.vmm = vmm;
            this.vsf = vsf;
    }

    
    
    /** 
     * Load the visual styles of the app. 
     */
    public void loadStyles(){
                
            for (String styleName: styles){
                System.out.println("Load visual style: " + styleName);
                String resource = String.format("/styles/%s.xml", styleName);
                InputStream styleStream = getClass().getResourceAsStream(resource);
                //(styleStream.toString());
                // Check if already existing
                VisualStyle style = getVisualStyleByName(vmm, styleName);
                if (styleName.equals(style.getTitle())){
                	continue;
                } else {
                    System.out.println("Loaded disgenet style");
                    loadVizmapFileTaskFactory.loadStyles(styleStream);
                }
            }
    }
    
    
    /** 
     * Get the visual style by name.
     * @param vmm the visualmappingmanager of the Cytoscape session.
     * @param styleName the name of the style to load.
     * @return the visual style with the given name or the default style if the given style is not found.
     */
    public static VisualStyle getVisualStyleByName(VisualMappingManager vmm, String styleName){
            Set<VisualStyle> styles = vmm.getAllVisualStyles();
            for (VisualStyle style: styles){
                    if (style.getTitle().equals(styleName)){
                        return style;
                    }
            }
            System.out.println("style [" + styleName +"] not in VisualStyles, default style used.");
            return vmm.getDefaultVisualStyle();
    }
        
    @Override
    public void handleEvent(SessionLoadedEvent e) {
    	loadStyles();
    }
}
