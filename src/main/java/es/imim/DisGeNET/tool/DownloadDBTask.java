package es.imim.DisGeNET.tool;

/**
	DisGeNET: A Cytoscape plugin to visualize, integrate, search and analyze gene-disease networks
	Copyright (C) 2010  Michael Rautschka, Anna Bauer-Mehren
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.HttpURLConnection;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.swing.JOptionPane;
import javax.net.ssl.HttpsURLConnection;
/*JAVI
import org.apache.tools.tar.TarEntry;
import org.apache.tools.tar.TarInputStream;
*/

/*JAVI
import org.cytoscape.task.Task;
*/
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.ObservableTask;

import es.imim.DisGeNET.internal.CyActivator;
import org.cytoscape.work.TaskMonitor;

/**
 * helper class to download extract the database file 
 * @author michael
 *
 */
//JAVI
//public class DownloadDBTask implements Task {
public class DownloadDBTask extends AbstractTask {	

	private TaskMonitor taskMonitor = null;
	private static boolean interrupted = false;
	private boolean completedSuccessfully = false;
	private String resultTitle;
	private String url = "";
	private String destination = "";
	private String destinationPath = "";

	public static boolean isInterrupted() {
		return interrupted;
	}

	public static boolean getFile(String source, String destination, TaskMonitor tmon) {

		boolean result = false;
		double remoteSize = getRemoteFileSize(source);

		URL url = null;
		try {
			url = new URL(source);
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}

		InputStream in = null;
		try {
			in = url.openStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(destination);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Error: Unable to write output file.");
		}
		byte[] buf = new byte[4 * 1024]; // 4K buffer
		int bytesRead;
		double written = 0.0;
		int beforePerc = 0;
		if (remoteSize > 0)
			System.out.print("0%");
		try {
			while ((bytesRead = in.read(buf)) != -1 && !isInterrupted()) {
				out.write(buf, 0, bytesRead);
				written += bytesRead;
				int percFinished = (int) ((written / remoteSize) * 100);
				tmon.setProgress(percFinished/100);
				tmon.setStatusMessage("Downloading database file... "+percFinished+"% downloaded");
				tmon.setProgress(percFinished/100);
				if ((int) ((written / remoteSize) * 100) % 2 == 0
						&& remoteSize > 0
						&& (int) ((written / remoteSize) * 100) != beforePerc)
					if ((int) ((written / remoteSize) * 100) % 10 == 0) {
						System.out.print("*"
								+ (int) ((written / remoteSize) * 100) + "%");
						beforePerc = (int) ((written / remoteSize) * 100);
					} else {
						System.out.print("*");
						beforePerc = (int) ((written / remoteSize) * 100);
					}
			}
			result = true;
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Error: Unable to transfer file.");
		}
		
		return result;
	}

	public static boolean untarFile(String tarFile, String untarDir) throws IOException {
		/*JAVI
		TarInputStream tis = new TarInputStream(new FileInputStream(tarFile));

		System.out.println("Reading TarInputStream... ");
		TarEntry tarEntry = tis.getNextEntry();
		if(new File(untarDir).exists()){
			while (tarEntry != null && !isInterrupted()){
				File destPath = new File(untarDir + File.separatorChar + tarEntry.getName());
				System.out.println("Processing " + destPath.getAbsoluteFile());
				if(!tarEntry.isDirectory()){
					FileOutputStream fout = new FileOutputStream(destPath);
					tis.copyEntryContents(fout);
					fout.close();
				}else{
					destPath.mkdir();
				}
				tarEntry = tis.getNextEntry();
			}
			tis.close();
		}else{
			System.out.println("That destination directory doesn't exist! " + untarDir);
		}
		*/
		return true;
	}

	public static boolean unpackFile(String source, String destination) {
		InputStream is = null;
		FileOutputStream os = null;
		boolean result = false;

		if (source.toLowerCase().endsWith("gz")) {
			try {
				is = new GZIPInputStream(new FileInputStream(source));
				os = new FileOutputStream(destination);

				byte[] buffer = new byte[8192];

				for (int length; (length = is.read(buffer)) != -1;) {
					if (!isInterrupted())
						os.write(buffer, 0, length);
				}
				result = true;
			} catch (IOException e) {
				System.err.println("Error: Unable to unpack " + source);
			} finally {
				if (os != null)
					try {
						os.close();
					} catch (IOException e) {
					}
					if (is != null)
						try {
							is.close();
						} catch (IOException e) {
						}
			}
		} else if (source.toLowerCase().endsWith("zip")) {
			// use different method for zip
			try {
				FileInputStream fin = new FileInputStream(source);
				ZipInputStream zin = new ZipInputStream(fin);
				ZipEntry ze = null;
				while ((ze = zin.getNextEntry()) != null) {
					System.out.println("Unzipping " + ze.getName());
					//FileOutputStream fout = new FileOutputStream(destination);
					BufferedOutputStream fout = new BufferedOutputStream(new FileOutputStream(destination));
					for (int c = zin.read(); c != -1; c = zin.read()) {
						if (!isInterrupted())
							fout.write(c);
					}
					zin.closeEntry();
					fout.close();
				}
				zin.close();
				result = true;
			} catch (IOException e) {
				System.err.println("Error: Unable to unpack " + source);
			} finally {
				if (os != null)
					try {
						os.close();
					} catch (IOException e) {
					}
					if (is != null)
						try {
							is.close();
						} catch (IOException e) {
						}
			}
		}

		return result;
	}

	public static void deleteFile(String fileName) {
		// A File object to represent the filename
		File f = new File(fileName);

		// Make sure the file or directory exists and isn't write protected
		if (!f.exists())
			throw new IllegalArgumentException(
					"Delete: no such file or directory: " + fileName);

		if (!f.canWrite())
			throw new IllegalArgumentException("Delete: write protected: "
					+ fileName);

		// If it is a directory, make sure it is empty
		if (f.isDirectory()) {
			String[] files = f.list();
			if (files.length > 0)
				throw new IllegalArgumentException(
						"Delete: directory not empty: " + fileName);
		}

		// Attempt to delete it
		boolean success = f.delete();

		if (!success)
			throw new IllegalArgumentException("Delete: deletion failed");
	}

	public static long getRemoteFileSize(String urlParam) {
		URL url;
		HttpsURLConnection conn;
		long size = 0;

		try {
			url = new URL(urlParam);
			//System.out.println(url +" url" );
			conn = (HttpsURLConnection)url.openConnection();
			size = conn.getContentLength();
			if (size < 0)
				System.out.println("Could not determine file size.");
			conn.getInputStream().close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return size;
	}

	//JAVI. Does not override?
	//@Override
	public String getTitle() {
		StringBuffer state = new StringBuffer("Downloading Database file...");
		return state.toString();
	}

	//JAVI. Does not override?
	//@Override
	public void halt() {
		this.interrupted = true;

		//JAVI. null is ok?
		//JOptionPane.showMessageDialog(Cytoscape.getDesktop(),
		JOptionPane.showMessageDialog(null,
				"Downloading of database file was canceled by user.");
		//deleteDirectory(new File(destinationPath));
	}

	public void cancel(){
		this.halt();
	}


	public DownloadDBTask(String url, String dest, String path) {
		this.destination = dest;
		this.destinationPath = path;
		this.url = url;
	}

	@Override
	//JAVI
	//public void run() {
	public void run(TaskMonitor taskMonitor) throws Exception{
		if (taskMonitor == null) {
			throw new IllegalStateException("The Task Monitor has not been set.");
		}

		try {
			
			taskMonitor.setTitle("Downloading DisGeNET database");

			taskMonitor.setProgress(0);
			
			taskMonitor.setStatusMessage("Downloading database file...");

//			if (getFile(url, destination + ".tar.gz", taskMonitor)) {
//
//				taskMonitor.setStatus("Extracting database file...");
//				if (unpackFile(destination + ".tar.gz", destination + ".tar"))
//					deleteFile(destination + ".tar.gz");
//				taskMonitor.setStatus("Untar database file...");
//				if (FileDownloadTools.untarFile(destination + ".tar", destinationPath)) {
//					deleteFile(destination + ".tar");
//				}
			
			if (getFile(url, destination + ".gz", taskMonitor)) {
					System.out.println(destination);
					taskMonitor.setStatusMessage("Extracting database file...");
					unpackFile(destination + ".gz", destination );
//					if (unpackFile(destination + ".gz", destination ))
//						deleteFile(destination + ".gz");
					taskMonitor.setStatusMessage("unzip database file...");
					//System.out.println("Unzip database file...");
					

				
			}
// old version when db was packed as zip -> really!!! slow performance of java unzipping				
//			if (getFile(url,
//				destination + ".zip", taskMonitor)) {
//
//				taskMonitor.setStatus("Extracting database file...");
//				if (unpackFile(destination + ".zip", destination))
//					deleteFile(destination + ".zip");

			
		} catch (Exception e) {
			//JAVI. Is there a way to set an exception?
			//taskMonitor.setException(e, "Downloading of Database file was cancelled!");
			taskMonitor.setStatusMessage("Downloading of Database file was cancelled!" + e.toString());
			System.out.println("Download cancelled");
			
			System.out.println("trying to delete file");
			deleteFile(destination);
			completedSuccessfully = false;
		}
		completedSuccessfully = true;
	}

	/*JAVI
	@Override
	public void setTaskMonitor(TaskMonitor taskMonitor)
	throws IllegalThreadStateException {
		if (this.taskMonitor != null) {
			throw new IllegalStateException("Task Monitor has already been set.");
		}
		this.taskMonitor = taskMonitor;
	}*/

	static public boolean deleteDirectory(File path) {
		if( path.exists() ) {
			File[] files = path.listFiles();
			for(int i=0; i<files.length; i++) {
				if(files[i].isDirectory()) {
					deleteDirectory(files[i]);
				}
				else {
					files[i].delete();
				}
			}
		}
		return( path.delete() );
	}
	
}
