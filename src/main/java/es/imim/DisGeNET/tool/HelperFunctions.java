package es.imim.DisGeNET.tool;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/*
	DisGeNET: A Cytoscape plugin to visualize, integrate, search and analyze gene-disease networks
	Copyright (C) 2010  Michael Rautschka, Anna Bauer-Mehren
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

public class HelperFunctions {

	/**
	 * functions to sort string lists by line length
	 * @author michael
	 *
	 */
	public class byLineLengthDesc implements java.util.Comparator {
		public int compare(Object o1, Object o2) {
			int sdif = ((String)o2).length() - ((String)o1).length();
			return sdif;
		}
	}
	
	public class byLineLengthAsc implements java.util.Comparator {
		public int compare(Object o1, Object o2) {
			int sdif = ((String)o1).length() - ((String)o2).length();
			return sdif;
		}
	}
	
	public static boolean checkStringIsNotEmpty(String string) {
		boolean flag=true;
		if(string==null||string.isEmpty()) {
			flag=false;
		}
		return flag;
	}
	
	/**
	 * Iterates from a list of combinations of diseases classes and returns de disease classes found in non-repeated values array.
	 * @param disClasses the list of diseases classes 
	 * @return all the disease classes found non-repeated.
	 */
	public static List <String> diseaseClassSplitter (List <String> disClasses) {
		Iterator <String> disClassIt =  disClasses.iterator();
		List <String> disClassFound = new ArrayList<>();
		while(disClassIt.hasNext()) {
			String disCodes =  disClassIt.next(); //Extracts the found disease codes in a single line string.
			String[] splitedCodes = disCodes.split(",");
			for(String disCode : splitedCodes) {
				if(!disClassFound.contains(disCode)) {
					disClassFound.add(disCode);
				}
			}
		}
		return disClassFound;
	}
	
	public static String implodeString (String glue, Collection<String> list) {
	    // list is empty, return empty string
	    if (list == null || list.isEmpty()) {
	        return "";
	    }
	 
	    Iterator<String> iter = list.iterator();
	 
	    // init the builder with the first element
	    StringBuilder sb = new StringBuilder();
	    String item = null;
	    do {
	    	item = iter.next();
	    	if(item != null && !item.equals("")) {
		    	sb.append(item);
		    }
	    	
	    }while(item==null || item.equals(""));
	    
	 
	    // concat each element
	    while (iter.hasNext()) {
	    	item = iter.next();
	    	if(item != null && !item.equals("")) {
	    		sb.append(glue).append(item);
		    }	    
    	}
	 
	    // return result
	    return sb.toString();
	}
	
	public static String implodeInteger (String glue, Collection<Integer> list) {
	    // list is empty, return empty string
	    if (list == null || list.isEmpty()) {
	        return "";
	    }
	 
	    Iterator<Integer> iter = list.iterator();
	 
	    // init the builder with the first element
	    StringBuilder sb = new StringBuilder();
	    Integer item = iter.next();
	    if(item != null ) {
	    	sb.append(item);
	    }
	    
	 
	    // concat each element
	    while (iter.hasNext()) {
	    	item = iter.next();
	    	if(item != null) {
	    		sb.append(glue).append(item);
		    }
	        
	    }
	 
	    // return result
	    return sb.toString();
	}
	
}
