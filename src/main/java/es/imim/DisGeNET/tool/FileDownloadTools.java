package es.imim.DisGeNET.tool;

/**
	DisGeNET: A Cytoscape plugin to visualize, integrate, search and analyze gene-disease networks
	Copyright (C) 2010  Michael Rautschka, Anna Bauer-Mehren
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/*ĴAVI
*/
import org.apache.tools.tar.TarEntry;
import org.apache.tools.tar.TarInputStream;


/** 
 * general helper class to download files and extract them;
 * used to download database file
 * @author michael
 *
 */
public class FileDownloadTools {

	
	public static boolean getFile(String source, String destination) {
		
		boolean result = false;
		double remoteSize = getRemoteFileSize(source);

		URL url = null;
		try {
			url = new URL(source);
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}

		InputStream in = null;
		try {
			in = url.openStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(destination);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		byte[] buf = new byte[4 * 1024]; // 4K buffer
		int bytesRead;
		double written = 0.0;
		int beforePerc = 0;
		if (remoteSize > 0)
			System.out.print("0%");
		try {
			while ((bytesRead = in.read(buf)) != -1) {
				out.write(buf, 0, bytesRead);
				written += bytesRead;
				if ((int) ((written / remoteSize) * 100) % 2 == 0
						&& remoteSize > 0
						&& (int) ((written / remoteSize) * 100) != beforePerc)
					if ((int) ((written / remoteSize) * 100) % 10 == 0) {
						System.out.print("*"
								+ (int) ((written / remoteSize) * 100) + "%");
						beforePerc = (int) ((written / remoteSize) * 100);
					} else {
						System.out.print("*");
						beforePerc = (int) ((written / remoteSize) * 100);
					}
			}
			result = true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public static boolean untarFile(String tarFile, String untarDir) throws IOException {
		/* JAVI
		TarInputStream tis = new TarInputStream(new FileInputStream(tarFile));

		logger.info("Reading TarInputStream... ");
		TarEntry tarEntry = tis.getNextEntry();
		if(new File(untarDir).exists()){
			while (tarEntry != null){
				File destPath = new File(untarDir + File.separatorChar + tarEntry.getName());
				logger.info("Processing " + destPath.getAbsoluteFile());
				if(!tarEntry.isDirectory()){
					FileOutputStream fout = new FileOutputStream(destPath);
					tis.copyEntryContents(fout);
					fout.close();
				}else{
					destPath.mkdir();
				}
				tarEntry = tis.getNextEntry();
			}
			tis.close();
		}else{
			logger.error("That destination directory doesn't exist! " + untarDir);
		}
		*/
		return true;
	}
	

	public static boolean unpackFile(String source, String destination) {
		InputStream is = null;
		FileOutputStream os = null;
		boolean result = false;

		if (source.toLowerCase().endsWith("gz")) {
			try {
				is = new GZIPInputStream(new FileInputStream(source));
				os = new FileOutputStream(destination);

				byte[] buffer = new byte[8192];

				for (int length; (length = is.read(buffer)) != -1;)
					os.write(buffer, 0, length);
				result = true;
			} catch (IOException e) {
			} finally {
				if (os != null)
					try {
						os.close();
					} catch (IOException e) {
					}
				if (is != null)
					try {
						is.close();
					} catch (IOException e) {
					}
			}
		} else if (source.toLowerCase().endsWith("zip")) {
			// use different method for zip
			try {

				FileInputStream fin = new FileInputStream(source);
				ZipInputStream zin = new ZipInputStream(fin);
				ZipEntry ze = null;
				while ((ze = zin.getNextEntry()) != null) {
					FileOutputStream fout = new FileOutputStream(destination);
					for (int c = zin.read(); c != -1; c = zin.read()) {
						fout.write(c);
					}
					zin.closeEntry();
					fout.close();
				}
				zin.close();
				result = true;
			} catch (IOException e) {
			} finally {
				if (os != null)
					try {
						os.close();
					} catch (IOException e) {
					}
				if (is != null)
					try {
						is.close();
					} catch (IOException e) {
					}
			}
		}

		return result;
	}

	public static void deleteFile(String fileName) {
		// A File object to represent the filename
		File f = new File(fileName);

		// Make sure the file or directory exists and isn't write protected
		if (!f.exists())
			throw new IllegalArgumentException(
					"Delete: no such file or directory: " + fileName);

		if (!f.canWrite())
			throw new IllegalArgumentException("Delete: write protected: "
					+ fileName);

		// If it is a directory, make sure it is empty
		if (f.isDirectory()) {
			String[] files = f.list();
			if (files.length > 0)
				throw new IllegalArgumentException(
						"Delete: directory not empty: " + fileName);
		}

		// Attempt to delete it
		boolean success = f.delete();

		if (!success)
			throw new IllegalArgumentException("Delete: deletion failed");
	}

	public static long getRemoteFileSize(String urlParam) {
		URL url;
		URLConnection conn;
		long size = 0;

		try {
			url = new URL(urlParam);
			conn = url.openConnection();
			size = conn.getContentLength();
			if (size < 0)
			conn.getInputStream().close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return size;
	}
}
