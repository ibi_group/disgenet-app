package es.imim.DisGeNET.tool;

import java.util.Map;

public class OperationResult {
	
	private String message;
	private Map result;
	private Map errors;
	
	public OperationResult () {
		
	}
	
	public OperationResult (String message, Map result, Map errors) {
		this.message = message;
		this.result = result;
		this.errors = errors;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Map getCallResult() {
		return result;
	}

	public void setCallResult(Map callResult) {
		this.result = callResult;
	}
	
}
